import axios from 'axios';
import qs from 'qs';
import { getOAuthToken } from './persistence/store.config';
import { navigateAndSimpleReset } from '@/navigators/Root';
import { SCREEN, SERVICE_URL } from '../constant';
import { StatusCodes } from 'http-status-codes';
import DeviceInfo from 'react-native-device-info';

export const Host = SERVICE_URL.DEHR_HOST;
export const Version = 'v1';
export const BaseURL = `${Host}/${Version}/`; // Develop

export const Google_URL = SERVICE_URL.GOOGLE_MAP_URL;

axios.defaults.baseURL = `${BaseURL}`;
axios.defaults.timeout = 20000;
const uniqueId = DeviceInfo.getUniqueId();

axios.interceptors.request.use(
    async (config) => {
        const authorization = await getOAuthToken();
        if (authorization) {
            config.headers.Authorization = `Bearer ${authorization}`;
        }
        config.headers = {
            ...config.headers,
            'x-device-id': uniqueId,
        };
        console.log('%c%s', 'color: blue; background: black; font-size: 12px;', 'HTTP Request', config.url, '\n', config);

        return config;
    },
    (error) => Promise.reject(error)
);

axios.interceptors.response.use(
    (response) => {
        console.log('%c%s', 'color: green; background: black; font-size: 12px;', 'HTTP Response', response.config.url, '\n', response);
        return response;
    },
    (error) => {
        console.log('%c%s', 'color: red; background: black; font-size: 12px;', 'HTTP Response', error);

        return Promise.reject(error);
    }
);

const handleError = (error) => {
    const { response } = error || {};
    if (response) {
        const { status, data } = response;

        if (status === StatusCodes.UNAUTHORIZED) {
            navigateAndSimpleReset(SCREEN.LOGIN);
        }

        return data;
    }

    return error;
};

const preprocessResponse = (result) => {
    const { message, data } = result || {};
    if (message === 'OK') {
        return data;
    }
    return result;
};

export default class RequestHelper {
    static async getHeader() {
        const defaultHeader = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        };
        return defaultHeader;
    }

    static async getHeaderUploadFile() {
        return {
            'Content-Type': 'multipart/form-data',
        };
    }

    static async getUserInfoFromFacebook(token) {
        return axios
            .get(`https://graph.facebook.com/v2.5/me?fields=email,name,friends&access_token=${token}`)
            .then((data) => {
                return data.data;
            })
            .catch((e) => {
                throw e;
            });
    }

    static async getChatBot(data) {
        return axios({
            method: 'post',
            url: 'http://13.213.42.49:5005/webhooks/rest/webhook',
            header: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            data: data,
        })
            .then((data) => {
                return data.data;
            })
            .then((data) => {
                return preprocessResponse(data);
            })
            .catch((e) => {
                throw e;
            });
    }

    static async get(url, params) {
        try {
            const res = await axios.get(url, {
                headers: await this.getHeader(),
                params,
                paramsSerializer: (params) => {
                    return qs.stringify(params, { arrayFormat: 'repeat' });
                },
            });
            return preprocessResponse(res.data);
        } catch (e) {
            throw handleError(e);
        }
    }

    static async post(url, data) {
        try {
            const res = await axios({
                method: 'post',
                url,
                headers: await this.getHeader(),
                data,
            });
            return preprocessResponse(res.data);
        } catch (e) {
            throw handleError(e);
        }
    }

    static async postUpload(url, data) {
        try {
            const res = await axios({
                method: 'post',
                url,
                headers: {
                    Accept: '*/*',
                    'Content-Type': 'multipart/form-data',
                },
                data,
            });
            return preprocessResponse(res.data);
        } catch (e) {
            throw handleError(e);
        }
    }

    static async put(apiUrl, data) {
        let defaultHeader = await this.getHeader();
        if (data instanceof FormData) {
            defaultHeader = {
                ...defaultHeader,
                Accept: '*/*',
                'Content-Type': 'multipart/form-data',
            };
        }
        try {
            const res = await axios({
                method: 'put',
                url: apiUrl,
                headers: defaultHeader,
                data,
            });
            return preprocessResponse(res.data);
        } catch (e) {
            throw handleError(e);
        }
    }

    static uploadFile = (file, signedRequest) => {
        return new Promise((resolve) => {
            const xhr = new XMLHttpRequest();
            xhr.open('PUT', signedRequest);
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        const { responseURL } = xhr;
                        const index = responseURL.indexOf('?');
                        const url = responseURL.substring(0, index);
                        resolve({
                            status: xhr.status,
                            url,
                        });
                    } else {
                        resolve(null);
                    }
                }
            };

            xhr.send(file);
        }).catch((err) => {
            return err;
        });
    };

    static async delete(apiUrl, data) {
        return axios({
            method: 'delete',
            url: apiUrl,
            headers: await this.getHeader(),
            data,
        })
            .then((data) => {
                return data.data;
            })
            .then((data) => {
                return preprocessResponse(data);
            })
            .catch((e) => {
                throw handleError(e);
            });
    }
}
