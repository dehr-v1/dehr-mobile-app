import RequestHelper from '../api.service';

export const searchConfig = () => RequestHelper.get('jobs/configurations');

export const searchAdvance = (data) => RequestHelper.get(`jobs/search?page=1&offset=10&${data}`);

export const getJobSearch = (data) => RequestHelper.get(`jobs/hot?page=${data.page}&offset=${data.limit}&sort=createdAt:-1`);

export const searchResume = () => RequestHelper.get('resumes');

export const getPeopleSearch = data => RequestHelper.get(`members/popular?page=${data.page}&offset=${data.limit}&sort=createdAt:-1`)

export const searchJobs = data => RequestHelper.get(`jobs/search?page=${data.page}&offset=${data.limit}&keywords=${encodeURI(data.keyword)}`);

export const searchPeoples = data => RequestHelper.get(`members/search?page=${data.page}&offset=${data.limit}&sort=createdAt:-1&keywords=${encodeURI(data.keyword)}`)

