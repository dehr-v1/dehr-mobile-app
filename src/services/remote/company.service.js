import RequestHelper from '../api.service';

export const getCompanies = (data) => RequestHelper.get('companies');
