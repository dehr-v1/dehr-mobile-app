import RequestHelper from '../api.service';

export const getAccountInfo = () => RequestHelper.get('members/me');

export const updateAccountInfo = (data) => RequestHelper.put('members/me', data);

export const updateResume = (data) => RequestHelper.put('resumes/me', data);

export const getLocation = () => RequestHelper.get(`index/countries`);

export const uploadImage = (data) => RequestHelper.postUpload('uploads', data);

export const createResume = (data) => RequestHelper.post('resumes/me', data);

export const setOption = (data) => RequestHelper.put('members/contacts/exchange-options', data);