import RequestHelper from '../api.service';

export const getLisUser = (token, timeseed) => RequestHelper.get(`chatv2/chat_user?authorization=${token}&timeseed=${timeseed}`);

export const getListConversation = (token, timeseed) => RequestHelper.get(`chatv2/chat?authorization=${token}&timeseed=${timeseed}`);

export const createConversation = (id, token, timeseed) =>
    RequestHelper.get(`chatv2/chat_create?userid=${id}&authorization=${token}&timeseed=${timeseed}`);

export const getListMessageChat = (id, token, timeseed) =>
    RequestHelper.get(`chatv2/chat_message?_id=${id}&authorization=${token}&timeseed=${timeseed}`);

export const createtMessageChat = (token, channelID, message, timeseed) =>
    RequestHelper.get(`chatv2/chat_message_create?authorization=${token}&timeseed=${timeseed}&channelID=${channelID}&message=${message}`);

export const createMessageChatBot = (data) => RequestHelper.getChatBot(data);
