import RequestHelper from '../api.service';

export const getNotifications = (page) => RequestHelper.get(`notifications?page=${page}`);
