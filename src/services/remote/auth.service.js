import RequestHelper from '../api.service';

export const loginUser = (data) => RequestHelper.post('members/login', data);

export const registerUser = (data) => RequestHelper.post('members/registration', data);

export const forgotUser = (data) => RequestHelper.get(`members/password?email=${data.email}`);

export const verifyEmail = (data) => RequestHelper.get(`members/email/activation/${data.code}`);

export const changePassUser = (data) => RequestHelper.post('members/password', data);

export const updateNotificationToken = (data) => RequestHelper.put('members/me/notification/token', data);

export const logOut = () => RequestHelper.post(`members/logout`);
