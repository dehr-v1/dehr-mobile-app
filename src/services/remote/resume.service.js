import RequestHelper from '../api.service';

export const getResumeList = (data) => RequestHelper.get(`resumes`);

export const getInfo = (data) => RequestHelper.get(`member/${data.value}`);

export const getProfile = () => RequestHelper.get('member/me');

export const createResume = (data) => RequestHelper.get(`job/createResumedo?resume=${data.value}`);
