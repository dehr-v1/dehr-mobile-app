import RequestHelper from '../api.service';
import * as EthCrypto from 'eth-crypto';

export const acceptUnlock = (data) => RequestHelper.postUpload('wallet/svAcceptUnlock', data);

export const rejectUnlock = (data) => RequestHelper.postUpload('wallet/svRejectUnlock', data);

export const upgradePro = (data) => RequestHelper.postUpload('member/upgradePro', data);

export const getPopup = (data) => RequestHelper.get(`popup/index?st_col=created_at&st_type=-1&pg_page=1&pg_size=10&timeseed=${new Date().getTime()}`);

export const decryptWithPrivateKey = async (privateKey, encryptedData) => await EthCrypto.decryptWithPrivateKey(privateKey, encryptedData);