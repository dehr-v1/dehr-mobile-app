import RequestHelper from '../api.service';

export const getMissions = () => RequestHelper.get('missions');

export const acceptMission = (data) =>
    RequestHelper.get(
        `mission/missionAccept?timeseed=${new Date().getTime()}&authorization=${data.token}&missiondo_id=${data.missiondo_id}`
    );

export const doneMission = (data) =>
    RequestHelper.get(
        `mission/missionDone?timeseed=${new Date().getTime()}&authorization=${data.token}&missiondo_id=${data.missiondo_id}&answer1=${
            data.answer1
        }&answer2=${data.answer2}`
    );

export const getReward = (data) =>
    RequestHelper.get(
        `mission/missionReward?timeseed=${new Date().getTime()}&authorization=${data.token}&missiondo_id=${data.missiondo_id}&wallet=${
            data.wallet
        }`
    );
