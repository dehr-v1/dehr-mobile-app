import RequestHelper from '../api.service';
import { DEHR_CONTRACT_ADDRESS, RPC_ENDPOINT } from '../../constant/value.constant';
import * as KardiaClient from 'kardia-js-sdk';
import Web3 from 'web3';

const kardiaClient = new KardiaClient.default({ endpoint: RPC_ENDPOINT });
const krc20Instance = kardiaClient.krc20;
const deHrInstance = new KardiaClient.KRC20({ provider: RPC_ENDPOINT, address: DEHR_CONTRACT_ADDRESS });

export const createWalletBlockChain = async () => await KardiaClient.KardiaAccount.generateWallet();

export const importWalletBlockChain = async (mnemonic) => await KardiaClient.KardiaAccount.getWalletFromMnemonic(mnemonic);

export const getWalletFromPK = async (privateKey) => await KardiaClient.KardiaAccount.getWalletFromPK(privateKey);

export const transferDeHrToken = async (destination, amount, seedPhrase) => {
    return await deHrInstance.transfer(seedPhrase.privateKey, destination, amount, {}, true);
};

export const registerWalletBlockChain = async (email, phone) =>
    '0x' + Buffer.from(new Web3().utils.sha3(`${email}-${phone}`).substring(2), 'hex').toString('hex');

export const createWallet = (data) => RequestHelper.post(`members/me/wallet`, data);

export const loginWallet = (data, token) => RequestHelper.post(`wallet/svLoginWallet?authorization=${token}`, data);

export const getListTransaction = (page, filter) => RequestHelper.get(`wallets/transactions?page=${page}${filter}`);

export const balanceWallet = async (address) => {
    await krc20Instance.getFromAddress(DEHR_CONTRACT_ADDRESS);
    const balanceDhr = await krc20Instance.balanceOf(address);
    return balanceDhr;
};

export const kycSetup = (data) => RequestHelper.put('members/me', data);

export const uploadImage = (data) => RequestHelper.postUpload('upload/index', data);
