import RequestHelper from '../api.service';

export const getJob = (data) => RequestHelper.get('jobs');

export const getHotJob = (data) => RequestHelper.get(`jobs/hot?page=${data.page}&offset=${data.limit}&sort=createdAt:-1`);

export const getLatestJob = (data) => RequestHelper.get(`jobs/lasted?page=${data.page}&offset=${data.limit}&sort=createdAt:-1`);

export const getAppliedListJob = (data) => RequestHelper.get(`jobs/applied?page=${data.page}&offset=${data.limit}&sort=createdAt:-1`);

export const applyJob = (data) => RequestHelper.post(`job-applications`, data);

export const getAppliedJob = (data) => RequestHelper.get(`job-applications/me/applied`);

export const searchJobs = (data) => RequestHelper.get(`jobs/search?page=${data.page}&offset=${data.limit}&sort=createdAt:-1&keywords=${data.keywords}`);

export const searchPeople = (data) => RequestHelper.get(`members/search?page=${data.page}&offset=${data.limit}&sort=createdAt:-1&keywords=${data.keywords}`);

export const getRecommendJobs = (data) => RequestHelper.get(`job-applications?page=${data.page}&offset=${data.limit}&sort=createdAt:-1`);

export const getUserAppliedJobs = (data) => RequestHelper.get(`job-applications/me/applied?page=${data.page}&offset=${data.limit}&sort=createdAt:-1`);

export const getUserPostedJobs = (data) => RequestHelper.get(`job-applications/me/published?page=${data.page}&offset=${data.limit}&sort=createdAt:-1`);

export const postJob = (data) => RequestHelper.post('jobs', data);
