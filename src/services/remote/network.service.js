import RequestHelper from '../api.service';

export const getLisNetworkRecommened = () => RequestHelper.get(`networks/recommended`);

export const getLisNetwork = () => RequestHelper.get(`networks/networks`);

export const getLisNetworkRequest = () => RequestHelper.get(`/networks/requested`);

export const networkRequest = (data) => RequestHelper.post(`networks`, data);

export const networkAccept = (id, data) => RequestHelper.put(`networks/${id}`, data);

export const cancelNetworkRequest = (id, data) => RequestHelper.put(`networks/${id}`, data);

