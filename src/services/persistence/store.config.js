import AsyncStorage from '@react-native-async-storage/async-storage';

const StorageKey = {
    OAUTH_TOKEN: '@OAUTH_TOKEN',
    CURRENT_USER: '@CURRENT_USER',
    FCM_TOKEN: '@FCM_TOKEN',
    AREA_DATA: '@AREA_DATA',
    LANGUAGE: '@LANGUAGE',
    SEED_PHASE: '@SEED_PHASE',
    PASS_WALLET: '@PASS_WALLET',
    FIRST_TIME: '@FIRST_TIME',
    POP_UP: '@POP_UP',
    POP_UP_FIRST: '@POP_UP_FIRST',
};

let token;
export const getOAuthToken = async () => {
    try {
        token = await AsyncStorage.getItem(StorageKey.OAUTH_TOKEN);
        // eslint-disable-next-line no-empty
    } catch (e) { }
    return token;
};

export const setOAuthToken = async (token) => {
    try {
        await AsyncStorage.setItem(StorageKey.OAUTH_TOKEN, token);
        // eslint-disable-next-line no-empty
    } catch (e) { }
};

export const removeOAuthToken = async () => {
    try {
        await AsyncStorage.removeItem(StorageKey.OAUTH_TOKEN);
        // eslint-disable-next-line no-empty
    } catch (e) { }
    token = null;
};

export const getLanguage = async () => {
    try {
        return await AsyncStorage.getItem(StorageKey.LANGUAGE);
        // eslint-disable-next-line no-empty
    } catch (e) { }
    return null;
};

export const setLanguage = async (key) => {
    try {
        await AsyncStorage.setItem(StorageKey.LANGUAGE, key);
        // eslint-disable-next-line no-empty
    } catch (e) { }
};

export const getCurrentUser = async () => {
    try {
        return await AsyncStorage.getItem(StorageKey.CURRENT_USER);
    } catch (e) { }
    return null;
};

export const setCurrentUser = async (user) => {
    try {
        await AsyncStorage.setItem(StorageKey.CURRENT_USER, user);
    } catch (e) { }
};

export const removeCurrentUser = async () => {
    try {
        await AsyncStorage.removeItem(StorageKey.CURRENT_USER);
        // eslint-disable-next-line no-empty
    } catch (e) { }
};

export const getSeedPhase = async () => {
    try {
        return await AsyncStorage.getItem(StorageKey.SEED_PHASE);
    } catch (e) { }
    return null;
};

export const setSeedPhase = async (seedPhase) => {
    try {
        await AsyncStorage.setItem(StorageKey.SEED_PHASE, seedPhase);
    } catch (e) { }
};

export const removeSeedPhase = async () => {
    try {
        await AsyncStorage.removeItem(StorageKey.SEED_PHASE);
    } catch (e) { }
};

export const getPassWallet = async () => {
    try {
        return await AsyncStorage.getItem(StorageKey.PASS_WALLET);
    } catch (e) { }
    return null;
};

export const setPassWallet = async (passWallet) => {
    try {
        await AsyncStorage.setItem(StorageKey.PASS_WALLET, passWallet);
    } catch (e) { }
};

export const removePassWallet = async () => {
    try {
        await AsyncStorage.removeItem(StorageKey.PASS_WALLET);
    } catch (e) { }
};

export const getFirstTime = async () => {
    try {
        return await AsyncStorage.getItem(StorageKey.FIRST_TIME);
    } catch (e) { }
    return null;
};

export const setFirstTime = async (firstTime) => {
    try {
        await AsyncStorage.setItem(StorageKey.FIRST_TIME, firstTime);
    } catch (e) { }
};

export const removeFirstTime = async () => {
    try {
        await AsyncStorage.removeItem(StorageKey.FIRST_TIME);
        // eslint-disable-next-line no-empty
    } catch (e) { }
};

export const getPopUp = async () => {
    try {
        return await AsyncStorage.getItem(StorageKey.POP_UP);
    } catch (e) { }
    return null;
};

export const setPopUp = async (popup) => {
    try {
        await AsyncStorage.setItem(StorageKey.POP_UP, popup);
    } catch (e) { }
};

export const getFirstPopUp = async () => {
    try {
        return await AsyncStorage.getItem(StorageKey.POP_UP_FIRST);
    } catch (e) { }
    return null;
};

export const setFirstPopUp = async (popup) => {
    try {
        await AsyncStorage.setItem(StorageKey.POP_UP_FIRST, popup);
    } catch (e) { }
};

export const removeDataLogOut = async () => {
    try {
        await AsyncStorage.removeItem(StorageKey.OAUTH_TOKEN);
        await AsyncStorage.removeItem(StorageKey.POP_UP_FIRST);
        await AsyncStorage.removeItem(StorageKey.FIRST_TIME);
        await AsyncStorage.removeItem(StorageKey.SEED_PHASE);
    } catch (e) { }
};
