import messaging from '@react-native-firebase/messaging';
import { NOTIFICATION_TYPE, SCREEN } from '../constant/index';

export default class FirebaseMessagingHelper {
    static async requestUserPermission() {
        const authStatus = await messaging().requestPermission();
        const enabled = authStatus === messaging.AuthorizationStatus.AUTHORIZED || authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    }

    static async handleMessage(listener) {
        const unsubscribe = messaging().onMessage(async (remoteMessage) => {
            listener(remoteMessage);
        });
        return unsubscribe;
    }

    static async handleTokenRefresh(listener) {
        messaging().onTokenRefresh(async (token) => listener(token));
    }

    static async handleNotificationFromQuit(navigation) {
        messaging()
            .getInitialNotification()
            .then((remoteMessage) => {
                if (remoteMessage) {
                    this.handleNotification(navigation, remoteMessage?.data);
                }
            });
    }

    static async handleNotificationFromBackground(navigation) {
        messaging().onNotificationOpenedApp((remoteMessage) => {
            this.handleNotification(navigation, remoteMessage?.data);
        });
    }

    static async handleNotification(navigation, payload) {
        switch (payload.type) {
            case NOTIFICATION_TYPE.PROFILE:
                navigation.navigate('Home');
                return navigation.navigate(SCREEN.PROFILE_VIEW_PAGE);
            case NOTIFICATION_TYPE.NETWORK:
                navigation.navigate('Message');
                return navigation.navigate(SCREEN.CONNECT_NETWORK, { isFromNotification: true });
            case NOTIFICATION_TYPE.WALLET:
                return navigation.navigate('Wallet');
            case NOTIFICATION_TYPE.KYC:
                return navigation.navigate('Wallet');
            case NOTIFICATION_TYPE.MISSION:
                return navigation.navigate('Mission', { isMission: true });
            case NOTIFICATION_TYPE.REFERRAL:
                return navigation.navigate('Mission', { isMission: false });
            case NOTIFICATION_TYPE.JOB_APPLICATION:
                return navigation.navigate(SCREEN.JOB_POSTED, { jobId: payload._id });
            //TODO
            case NOTIFICATION_TYPE.DAILY_QUEST:
            default:
                return;
        }
    }
}
