import { NotificationAction } from './notification.create.action';

export const actionGetNotifications = (payload, callback) => ({
    type: NotificationAction.NOTIFICATION_GET_NOTIFICATIONS.PENDING,
    payload,
    callback,
});
export const actionAddPendingNotification = (payload, callback) => ({
    type: NotificationAction.NOTIFICATION_HANDLE_NOTIFICATION.PENDING,
    payload,
    callback,
});
export const actionRemovePendingNotification = (payload, callback) => ({
    type: NotificationAction.NOTIFICATION_HANDLE_NOTIFICATION.SUCCESS,
    callback,
});
export const actionAddNewNotification = (payload, callback) => ({
    type: NotificationAction.NOTIFICATION_NEW_NOTIFICATION.PENDING,
    payload,
    callback,
});
export const actionRemoveNewNotification = (payload, callback) => ({
    type: NotificationAction.NOTIFICATION_NEW_NOTIFICATION.SUCCESS,
    callback,
});
