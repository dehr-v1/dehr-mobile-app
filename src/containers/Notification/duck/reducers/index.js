import { NotificationAction } from '../actions';

const notification = {
    pendingNotification: null,
    isHasNew: false,
    isLoading: false,
};
const notificationReducer = (state = notification, action) => {
    const { type, payload } = action;
    switch (type) {
        case NotificationAction.NOTIFICATION_GET_NOTIFICATIONS.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case NotificationAction.NOTIFICATION_GET_NOTIFICATIONS.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case NotificationAction.NOTIFICATION_GET_NOTIFICATIONS.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case NotificationAction.NOTIFICATION_HANDLE_NOTIFICATION.PENDING:
            return {
                ...state,
                pendingNotification: payload.notificationPayload,
                isLoading: true,
            };
        case NotificationAction.NOTIFICATION_HANDLE_NOTIFICATION.SUCCESS:
            return {
                ...state,
                pendingNotification: null,
                isLoading: false,
            };
        case NotificationAction.NOTIFICATION_HANDLE_NOTIFICATION.ERROR:
            return {
                ...state,
                pendingNotification: null,
                isLoading: false,
            };
        case NotificationAction.NOTIFICATION_NEW_NOTIFICATION.PENDING:
            return {
                ...state,
                isHasNew: true,
                isLoading: true,
            };
        case NotificationAction.NOTIFICATION_NEW_NOTIFICATION.SUCCESS:
            return {
                ...state,
                isHasNew: false,
                isLoading: false,
            };
        case NotificationAction.NOTIFICATION_NEW_NOTIFICATION.ERROR:
            return {
                ...state,
                isHasNew: false,
                isLoading: false,
            };
        default:
            return state;
    }
};

export { notificationReducer };
