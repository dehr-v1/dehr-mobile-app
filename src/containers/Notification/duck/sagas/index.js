import { all } from 'redux-saga/effects';
import { watchGetNotifications } from './notification.saga';

export function* notificationSaga() {
    yield all([watchGetNotifications()]);
}
