import { call, put, takeLatest } from 'redux-saga/effects';
import { getNotifications } from '../../../../services/remote/notification.service';
import { NotificationAction } from '../actions';

function* onGetNotifications(action) {
    try {
        const { payload, callback } = action;
        const { page } = payload;
        const response = yield call(() => getNotifications(page));
        const notifcations = response.data.map((data) => {
            return {
                _id: data._id,
                title: data.title,
                body: data.body,
                fullName: data.data?.fullName,
                address: data.data?.address,
                from: data.data?.from,
                amount: data.data?.amount,
                jobName: data.data?.jobName,
                requesterId: data.data?.requesterId,
                image: data.data?.image,
                type: data.data?.type,
                createdAt: data.createdAt,
            };
        });
        const pageList = { page: response.page, totalPages: response.totalPages, items: notifcations };
        yield put({
            type: NotificationAction.NOTIFICATION_GET_NOTIFICATIONS.SUCCESS,
            payload: pageList,
        });
        callback && callback(pageList);
    } catch (error) {
        yield put({
            type: NotificationAction.NOTIFICATION_GET_NOTIFICATIONS.ERROR,
            payload: error,
        });
    }
}

export function* watchGetNotifications() {
    yield takeLatest(NotificationAction.NOTIFICATION_GET_NOTIFICATIONS.PENDING, onGetNotifications);
}
