import React, { useState } from 'react';
import { View, Text, StyleSheet, StatusBar, FlatList, TouchableOpacity, Dimensions } from 'react-native';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import { UIHeader, UIInfiniteFlatList } from '../../components/common';
import { fontFamily } from '../../theme/Fonts';
import { Colors } from '../../theme/Variables';
import { useNavigation } from '@react-navigation/native';
import { SCREEN } from '../../constant';
import { BASE_IMAGE_URL } from '../../constant/value.constant';
import { verticalScale, CommonUtils } from '../../utils';
import { UIText, UIAvatar } from '../../components/elements';
import { actionGetNotifications, actionAddPendingNotification, actionRemoveNewNotification } from './duck/actions';
import { connect } from 'react-redux';
import ParsedText from 'react-native-parsed-text';
import moment from 'moment';
const { width } = Dimensions.get('window');

const IndexNotificationContainer = (props) => {
    const { Layout, Gutters, Images, Fonts } = useTheme();
    const { notificationReducer, actionGetNotifications, actionAddPendingNotification, actionRemoveNewNotification } = props || {};
    const { isLoading } = notificationReducer || {};
    const { t } = useTranslation();
    const navigation = useNavigation();

    const onFetchData = async (page, useCallback) => {
        actionGetNotifications({ page }, (pageList) => {
            useCallback && useCallback(pageList);
            actionRemoveNewNotification();
        });
    };

    const onclick = (item) => {
        actionAddPendingNotification({ notificationPayload: item });
        navigation.navigate(SCREEN.MAIN);
    };

    const renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => onclick(item)} activeOpacity={0.6}>
                <View style={[Gutters.smallHPadding, styles.contentContainer]}>
                    <UIAvatar url={item.image ? BASE_IMAGE_URL + item.image : null} defaultImage={Images.ic_logo_round} />
                    <View style={{ width: verticalScale(4) }} />
                    <View style={[Layout.fill, Layout.colum, Gutters.smallHPadding]}>
                        <ParsedText
                            parse={[
                                { pattern: RegExp(` |^${item.fullName}`), style: styles.txtHighlight },
                                { pattern: RegExp(` |^${item.address}`), style: styles.txtHighlight },
                                { pattern: RegExp(` |^${item.from}`), style: styles.txtHighlight },
                                { pattern: RegExp(` |^${item.amount}`), style: styles.txtHighlight },
                                { pattern: RegExp(` |^${item.jobName}`), style: styles.txtHighlight },
                                { pattern: RegExp(` |^Someone`), style: styles.txtHighlight },
                            ]}
                        >
                            {item.body}
                        </ParsedText>
                        <UIText style={styles.txtDate} font="regular" text={CommonUtils.formatShortTime(item.createdAt)} />
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    return (
        <View style={[Layout.fill, Layout.column, styles.container]}>
            <UIHeader LeftIcon="back" title={t('home.notication')} onPress={() => navigation.goBack()} />
            <UIInfiniteFlatList onFetchData={onFetchData.bind(this)} renderItem={renderItem} />
        </View>
    );
};

const mapStateToProps = (state) => ({
    notificationReducer: state.notificationReducer,
});

export default connect(mapStateToProps, {
    actionGetNotifications,
    actionAddPendingNotification,
    actionRemoveNewNotification,
})(IndexNotificationContainer);

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.gray,
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentContainer: {
        paddingHorizontal: verticalScale(14),
        width: width - verticalScale(36),
        height: verticalScale(100),
        marginTop: verticalScale(20),
        backgroundColor: Colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 8,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 4,
        paddingVertical: verticalScale(16),
    },
    txtTitle: {
        color: Colors.title,
        fontSize: verticalScale(14),
        flexShrink: 1,
        fontFamily: fontFamily.bold,
    },
    txtHighlight: {
        fontFamily: fontFamily.bold,
    },
    txtContent: {
        color: Colors.title,
        fontSize: verticalScale(14),
        fontFamily: fontFamily.regular,
    },
    txtDate: {
        color: Colors.lightGray,
        fontSize: verticalScale(12),
    },
});
