import {
    CreateProfile,
    UpdateHeadline,
    UpdateAbout,
    SpecialistSkill,
    UpdateSpecialistSkill,
    OtherSkill,
    UpdateOtherSkill,
    Language,
    UpdateLanguage,
    Hobbies,
    UpdateHobbies,
    SiteCompany,
    UpdateSiteCompany,
    SocialLinks,
    UpdateSocialLink,
    Education,
    UpdateEducation,
    Experience,
    UpdateExperience,
    CurrentPosition,
    UpdateCurrentPosition,
    IndustrialKnowledge,
    AccountInfo,
    UpdateAccountInfo,
    UpdateContactPrice,
} from './Profile/pages';
import { UISearchScreen } from './Profile/components';
// LOGIN
import IndexLoginContainer from './Login';
import {
    Register as IndexRegisterContainer,
    ForgotPass as IndexForgotContainer,
    CreatePass as IndexCreatePassContainer,
    VerifyEmail as IndexVerifyContainer,
} from './Login/pages';
// CHAT
import { IndexMessageContainer, IndexNewMessageContainer, IndexConnectNetWorkContainer } from './Chat/pages';

// JOBS

// WALLET
import {
    CreatePassword,
    SecureWallet,
    ConfirmSecure,
    ImportSeedPhrase,
    SetupKYC,
    EnterPhoneNumber,
    VerifyPhoneKyc,
    FillInfoKyc,
    UploadFileKyc,
    DoneKyc,
    AllTransactionPage,
} from './Wallet/pages';

// HOME
import IndexHomeContainer from './Home';
import IndexChatContainer from './Chat';
import IndexJobsContainer from './Jobs';
import IndexWalletContainer from './Wallet';
import IndexMissionContainer from './Mission';
import IndexProfileContainer from './Profile';
import IndexNotificationContainer from './Notification';

// JOB
import {
    SearchJobPage,
    SearchAdvancePage,
    JobPostedPage,
    HiringProcessPage,
    PeopleMatchedPage,
    BestOfYouPage,
    EssDetailPage,
    ClassPage,
    OrganisationPage,
    PeopleSponserPage,
    ActiveProAccount,
    Summary,
    PeopleDetailPage,
    ProfileViewPage,
    ListPopUpPage,
    PopUpDetailsPage,
    AllJobPage
} from './Home/pages';

import SplashPage from './Splash';

export { default as IndexWelcomeContainer } from './Welcome';

export {
    SplashPage,
    // LOGIN
    IndexLoginContainer,
    IndexRegisterContainer,
    IndexForgotContainer,
    IndexCreatePassContainer,
    IndexVerifyContainer,
    // HOME
    IndexHomeContainer,
    IndexChatContainer,
    IndexWalletContainer,
    IndexMissionContainer,
    IndexNotificationContainer,
    // PROFILE
    IndexProfileContainer,
    CreateProfile,
    UpdateHeadline,
    UpdateAbout,
    SpecialistSkill,
    UpdateSpecialistSkill,
    OtherSkill,
    UpdateOtherSkill,
    Language,
    UpdateLanguage,
    UISearchScreen,
    Hobbies,
    UpdateHobbies,
    SiteCompany,
    UpdateSiteCompany,
    SocialLinks,
    UpdateSocialLink,
    Education,
    UpdateEducation,
    Experience,
    UpdateExperience,
    CurrentPosition,
    UpdateCurrentPosition,
    IndustrialKnowledge,
    AccountInfo,
    UpdateAccountInfo,
    UpdateContactPrice,
    // CHAT
    IndexMessageContainer,
    IndexNewMessageContainer,
    IndexConnectNetWorkContainer,
    // JOBS
    IndexJobsContainer,
    // WALLET
    CreatePassword,
    SecureWallet,
    ConfirmSecure,
    ImportSeedPhrase,
    SetupKYC,
    EnterPhoneNumber,
    VerifyPhoneKyc,
    FillInfoKyc,
    UploadFileKyc,
    DoneKyc,
    AllTransactionPage,
    // JOB
    SearchJobPage,
    SearchAdvancePage,
    JobPostedPage,
    PeopleMatchedPage,
    HiringProcessPage,
    BestOfYouPage,
    EssDetailPage,
    ClassPage,
    OrganisationPage,
    PeopleSponserPage,
    ActiveProAccount,
    Summary,
    PeopleDetailPage,
    ProfileViewPage,
    ListPopUpPage,
    AllJobPage
};
