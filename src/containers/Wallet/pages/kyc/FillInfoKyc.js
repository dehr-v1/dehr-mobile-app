import React, { useState } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { UIHeader, UITextInputForm } from '../../../../components/common';
import { UIButton, UIText, UIRadio } from '../../../../components/elements';
import { Colors } from '../../../../theme/Variables';
import { useTheme } from '@/theme';
import { moderateScale, verticalScale } from '../../../../utils/ScalingUtils';
import { UIStep } from '../../components';
import { useTranslation } from 'react-i18next';
import { useNavigation, useRoute } from '@react-navigation/native';
import { SCREEN } from '../../../../constant';

const FillInfoKyc = () => {
    const { t } = useTranslation();
    const { Layout } = useTheme();
    const navigation = useNavigation();
    const route = useRoute();
    const [gender, setGender] = useState(0);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [passportId, setPassportId] = useState('');

    renderStep = () => {
        return <UIStep step={2} />;
    };

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={styles.container}>
                <UIHeader onPress={() => navigation.goBack()} LeftIcon="back" centerComponent={renderStep} />
                <View style={styles.contentContainer}>
                    <UIText style={styles.txtTitle} font="black" text={t('wallet.fill_information')} />
                    <UITextInputForm title={t('wallet.first_name')} onChangeText={(text) => setFirstName(text)} required />
                    <UITextInputForm title={t('wallet.last_name')} onChangeText={(text) => setLastName(text)} required />
                    <UIText font="bold" style={styles.txtGender} text={t('wallet.gender')} />
                    <View style={[Layout.row]}>
                        <UIRadio text={t('wallet.male')} isCheck={gender === 0} onPressRadio={() => setGender(0)} />
                        <UIRadio text={t('wallet.female')} isCheck={gender === 1} onPressRadio={() => setGender(1)} />
                    </View>
                    <UITextInputForm title={t('wallet.passport_id')} onChangeText={(text) => setPassportId(text)} required />
                    <UIButton
                        style={{ marginTop: verticalScale(50) }}
                        onPress={() =>
                            navigation.navigate(SCREEN.UPLOAD_FILE_KYC, {
                                phoneNumber: route.params.phoneNumber,
                                firstName: firstName,
                                gender: gender,
                                lastName: lastName,
                                passportId: passportId,
                            })
                        }
                        text={t('wallet.continue')}
                    />
                </View>
            </View>
        </ScrollView>
    );
};

export default FillInfoKyc;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        paddingTop: verticalScale(30),
        flex: 1,
    },
    txtTitle: {
        fontSize: moderateScale(36),
        color: Colors.blueDark,
        lineHeight: moderateScale(46),
        marginBottom: verticalScale(80),
    },
    txtGender: {
        fontSize: verticalScale(14),
        color: Colors.blueDark,
    }
});
