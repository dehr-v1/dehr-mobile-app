import SetupKYC from './SetupKYC';
import EnterPhoneNumber from './EnterPhoneNumber';
import VerifyPhoneKyc from './VerifyPhoneKyc';
import FillInfoKyc from './FillInfoKyc';
import UploadFileKyc from './UploadFileKyc';
import DoneKyc from './DoneKyc';

export { SetupKYC, EnterPhoneNumber, VerifyPhoneKyc, FillInfoKyc, UploadFileKyc, DoneKyc };
