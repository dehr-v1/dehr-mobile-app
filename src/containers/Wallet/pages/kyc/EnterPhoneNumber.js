import React, { useState, useRef } from 'react';
import { View, StyleSheet, TouchableOpacity, TextInput, Platform, ScrollView } from 'react-native';
import { UIHeader, UITextInputForm } from '../../../../components/common';
import { UIButton, UIText } from '../../../../components/elements';
import { Colors } from '../../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../../utils/ScalingUtils';
import { UIStep } from '../../components';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import { SCREEN } from '../../../../constant';
import { fontFamily } from '../../../../theme/Fonts';
import PhoneInput from 'react-native-phone-number-input';
import Toast from '../../../../components/elements/Toast';

const EnterPhoneNumber = () => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const [phoneNumber, setPhoneNumber] = useState('');
    const phoneInput = useRef(null);

    const renderStep = () => {
        return <UIStep step={1} />;
    };

    const goNext = () => {
        if (phoneInput.current?.isValidNumber(phoneNumber)) {
            navigation.navigate(SCREEN.VERIFY_PHONE_KYC, { phoneNumber: phoneNumber });
        } else {
            Toast('Invalid phone number', 'error');
        }
    };

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={styles.container}>
                <UIHeader onPress={() => navigation.goBack()} LeftIcon="back" centerComponent={renderStep} />
                <View style={styles.contentContainer}>
                    <UIText style={styles.txtTitle} font="black" text={t('wallet.enter_phone')} />
                    <UIText style={styles.txtDes} text={t('wallet.enter_phone_des')} />
                    {/* <TouchableOpacity onPress={() => navigation.navigate(SCREEN.SEARCH_SCREEN,{
                        title: t('wallet.choose_country'),
                        data: [{ name: 'Viet Nam' }, { name: 'China' }, { name: "France" }],
                        placeholder: t('profile.industry_ex'),
                        onChoose: item => {
                            setCountry(item.name)
                        },
                    })} >
                        <UITextInputForm
                            placeholder={t('wallet.choose_country')}
                            title={t('wallet.phone')}
                            value={country}
                            editable={false}/>
                    </TouchableOpacity>
                    <View style={styles.textInputPhone} >
                        <TextInput style={[styles.input, {width: verticalScale(45)}]} placeholderTextColor={Colors.lightGray} placeholder="+84"/>
                        <View style={styles.line} />
                        <TextInput  style={[styles.input, {width: verticalScale(200)}]} placeholderTextColor={Colors.lightGray}  placeholder="33453443"/>
                    </View> */}
                    <PhoneInput
                        ref={phoneInput}
                        defaultValue={phoneNumber}
                        defaultCode="VN"
                        layout="first"
                        withShadow
                        autoFocus
                        containerStyle={styles.phoneContainer}
                        textContainerStyle={styles.textInput}
                        onChangeFormattedText={(text) => {
                            setPhoneNumber(text);
                        }}
                    />
                    <UIButton onPress={goNext} style={{ marginTop: verticalScale(150) }} text={t('wallet.confirm')} />
                </View>
            </View>
        </ScrollView>
    );
};

export default EnterPhoneNumber;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        paddingTop: verticalScale(30),
        flex: 1,
    },
    txtTitle: {
        fontSize: moderateScale(36),
        color: Colors.blueDark,
        lineHeight: moderateScale(46),
    },
    txtDes: {
        marginTop: verticalScale(80),
        marginBottom: verticalScale(25),
        color: Colors.blueDark,
    },
    textInputPhone: {
        height: verticalScale(50),
        backgroundColor: Colors.gray,
        borderRadius: verticalScale(6),
        borderColor: Colors.border,
        borderWidth: 1,
        paddingTop: verticalScale(15),
        paddingRight: verticalScale(30),
        paddingBottom: Platform.OS == 'ios' ? verticalScale(15) : verticalScale(10),
        paddingLeft: verticalScale(15),
        color: Colors.blueDark,
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
        lineHeight: Platform.OS == 'ios' ? verticalScale(20) : verticalScale(25),
        flexDirection: 'row',
        alignItems: 'center',
    },
    line: {
        height: '80%',
        borderLeftWidth: 1,
        borderColor: Colors.black,
        marginHorizontal: verticalScale(5),
    },
    input: {
        fontSize: moderateScale(16),
        fontFamily: fontFamily.regular,
        color: Colors.blueDark,
        height: verticalScale(50),
    },
});
