import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { UIHeader } from '../../../../components/common';
import { UIButton, UIText } from '../../../../components/elements';
import { Colors } from '../../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../../utils/ScalingUtils';
import { UIStep } from '../../components';
import useCountdown from '../../../../theme/hooks/useCountdown';
import { useTranslation } from 'react-i18next';
import { useNavigation, useRoute } from '@react-navigation/native';
import { SCREEN } from '../../../../constant';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import { fontFamily } from '../../../../theme/Fonts';
import { getCurrentUser, setCurrentUser } from '../../../../services/persistence/store.config';
import auth from '@react-native-firebase/auth';
import Toast from '../../../../components/elements/Toast';
import CountDown from 'react-native-countdown-component';

const VerifyPhoneKyc = () => {
    const { t } = useTranslation();
    const COUNTDOWN_TIME = 180;
    const navigation = useNavigation();
    const route = useRoute();
    const [disabled, setDisabled] = useState(true);
    // const { secondsLeft, restartCountdown } = useCountdown(10);
    // const [timerSecond, setTimerSecond] = useState(0);
    // const [timerMinute, setTimerMinute] = useState(0);
    const [confirm, setConfirm] = useState(null);
    const [countDownId, setCountDownId] = useState('1');

    const { phoneNumber } = route.params;

    const [code, setCode] = useState('');

    useEffect(async () => {
        await doSendCode();
    }, []);

    const doSendCode = async () => {
        const confirmation = await auth().verifyPhoneNumber(phoneNumber);
        setConfirm(confirmation);
    };

    const resetOTP = async () => {
        if (disabled) {
            return;
        }
        setDisabled(true);
        setCountDownId(Math.random().toString(36).substr(2, 5));
        await doSendCode();
    };

    const isConfirmEnable = () => {
        return code.length == 6;
    };

    const doVerify = async () => {
        try {
            const credential = auth.PhoneAuthProvider.credential(confirm.verificationId, code);
            const firebaseUser = await auth().signInWithCredential(credential);
            const user = (await getCurrentUser()) || null;
            const userUpdate = JSON.parse(user);
            userUpdate.phoneNumber = phoneNumber;
            setCurrentUser(JSON.stringify(userUpdate));
            navigation.navigate(SCREEN.FILL_INFO_KYC, route.params);
        } catch (error) {
            Toast('Invalid code', 'error');
        }
    };

    return (
        <View style={styles.container}>
            <UIHeader onPress={() => navigation.goBack()} LeftIcon="back" />
            <View style={styles.contentContainer}>
                <UIText style={styles.txtTitle} font="black" text={t('wallet.verify_phone')} />
                <UIText style={styles.txtDes} text={t('wallet.verify_phone_des')} />
                <UIText style={styles.txtPhone} text={phoneNumber} />
                <OTPInputView
                    style={styles.otpInput}
                    onCodeChanged={async (code) => {
                        setCode(code);
                    }}
                    pinCount={6}
                    autoFocusOnLoad
                    codeInputFieldStyle={styles.underlineStyleBase}
                    codeInputHighlightStyle={styles.underlineStyleHighLighted}
                />
                <View style={styles.resendContainer}>
                    <TouchableOpacity onPress={resetOTP} disabled={disabled}>
                        <UIText
                            font="bold"
                            style={disabled ? styles.btnResendDisable : styles.btnResendEnable}
                            text={t('wallet.resend_code')}
                        />
                    </TouchableOpacity>
                    <CountDown
                        id={countDownId}
                        size={10}
                        until={COUNTDOWN_TIME}
                        onFinish={() => {
                            setDisabled(false);
                        }}
                        digitStyle={{
                            backgroundColor: 'transparent',
                            borderColor: 'transparent',
                            borderRadius: 0,
                            marginHorizontal: 0,
                        }}
                        digitTxtStyle={styles.txtTime}
                        timeLabelStyle={styles.txtTime}
                        separatorStyle={styles.txtTime}
                        timeToShow={['M', 'S']}
                        timeLabels={{ h: null, m: null, s: null }}
                        onChange={() => {}}
                        // running={this.state.runTimer}
                        showSeparator
                    />
                </View>
                <UIButton
                    onPress={doVerify}
                    style={{ marginTop: 'auto', marginBottom: verticalScale(10) }}
                    text={t('wallet.confirm')}
                    disable={!isConfirmEnable()}
                />
            </View>
        </View>
    );
};

export default VerifyPhoneKyc;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        paddingTop: verticalScale(30),
        flex: 1,
    },
    txtTitle: {
        fontSize: moderateScale(36),
        color: Colors.blueDark,
        lineHeight: moderateScale(46),
    },
    txtDes: {
        marginTop: verticalScale(80),
        color: Colors.blueDark,
    },
    txtPhone: {
        fontSize: moderateScale(16),
        color: Colors.blue,
    },
    otpInput: {
        height: verticalScale(100),
        width: '100%',
        marginTop: verticalScale(40),
    },
    borderStyleHighLighted: {
        borderColor: Colors.blueDark,
    },
    underlineStyleBase: {
        width: verticalScale(50),
        height: verticalScale(70),
        borderWidth: 1,
        fontSize: moderateScale(40),
        backgroundColor: Colors.gray,
        color: Colors.blueDark,
    },
    underlineStyleHighLighted: {
        borderColor: Colors.blueDark,
    },
    btnResendEnable: {
        color: Colors.blueDark,
        fontSize: moderateScale(14),
        fontFamily: fontFamily.bold,
    },
    btnResendDisable: {
        color: Colors.disable,
        fontSize: moderateScale(14),
        fontFamily: fontFamily.regular,
    },
    txtTime: {
        fontSize: moderateScale(14),
        color: Colors.blue,
    },
    resendContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: verticalScale(30),
    },
});
