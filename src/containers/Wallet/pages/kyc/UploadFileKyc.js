import React, { useState, useRef } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, ScrollView } from 'react-native';
import { UIHeader, UIModal, UITextInputForm, UIUploadPhotoModal } from '../../../../components/common';
import { UIButton, UIText } from '../../../../components/elements';
import { Colors } from '../../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../../utils/ScalingUtils';
import { UIStep } from '../../components';
import { useTranslation } from 'react-i18next';
import { useNavigation, useRoute } from '@react-navigation/native';
import { SCREEN } from '../../../../constant';
import { useTheme } from '@/theme';
import ImagePicker from 'react-native-image-crop-picker';
import { actionKycSetup, actionUploadImage } from '../../duck/actions';
import { connect } from 'react-redux';

const UploadFileKyc = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { params } = useRoute() || {};
    const { Images } = useTheme();
    let frontRef = useRef();
    let passportRef = useRef();
    let selfieRef = useRef();
    const { walletReducer, actionUploadImage, actionKycSetup } = props || {};

    const [frontPassportUrl, setFrontPassportUrl] = useState('');
    const [passportUrl, setPassportUrl] = useState('');
    const [selfieUrl, setSelfieUrl] = useState('');
    let frontImage = useRef(null);
    let passportImage = useRef(null);
    let selfieImage = useRef(null);

    const [frontPassport, setFrontPassport] = useState('');
    const [passport, setPassport] = useState('');
    const [selfie, setSelfie] = useState('');

    const PHOTO_CONFIG = { width: 300, height: 400, cropping: true, includeBase64: true };

    renderStep = () => {
        return <UIStep step={3} />;
    };

    const onTakeFrontPassport = () => {
        ImagePicker.openCamera(PHOTO_CONFIG).then((image) => {
            frontRef.close();
            const { path } = image;
            setFrontPassport(path);
            frontImage.current = image;
        });
    };

    const onChooseFrontPassport = () => {
        ImagePicker.openPicker(PHOTO_CONFIG).then((image) => {
            frontRef.close();
            const { path } = image;
            setFrontPassport(path);
            frontImage.current = image;
        });
    };

    const onTakePassport = () => {
        ImagePicker.openCamera(PHOTO_CONFIG).then((image) => {
            passportRef.close();
            const { path } = image;
            setPassport(path);
            passportImage.current = image;
        });
    };

    const onChoosePassport = () => {
        ImagePicker.openPicker(PHOTO_CONFIG).then((image) => {
            passportRef.close();
            const { path } = image;
            setPassport(path);
            passportImage.current = image;
        });
    };

    const onTakeSelfire = () => {
        ImagePicker.openCamera(PHOTO_CONFIG).then((image) => {
            selfieRef.close();
            const { path } = image;
            setSelfie(path);
            selfieImage.current = image;
        });
    };

    const onChooseSelfire = () => {
        ImagePicker.openPicker(PHOTO_CONFIG).then((image) => {
            selfieRef.close();
            const { path } = image;
            setSelfie(path);
            selfieImage.current = image;
        });
    };

    const onKycDone = () => {
        const data = {
            phoneNumber: params.phoneNumber,
            firstName: params.firstName,
            gender: params.gender,
            lastName: params.lastName,
            passportId: params.passportId,
            passportFront: frontImage.current,
            passportBack: passportImage.current,
            passportSelfie: selfieImage.current,
        };
        actionKycSetup({ data }, (data) => {
            if (data != null) {
                navigation.navigate(SCREEN.DONE_KYC);
            }
        });
    };

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={styles.container}>
                <UIHeader onPress={() => navigation.goBack()} LeftIcon="back" centerComponent={renderStep} />
                <View style={styles.contentContainer}>
                    <UIText style={styles.txtTitle} font="black" text={t('wallet.fill_information')} />
                    <View style={{ marginBottom: verticalScale(25) }}>
                        <UIText font="bold" style={styles.txtUploadTitle} text={t('wallet.front_passport')} />
                        <View style={styles.uploadContainer}>
                            <Image
                                style={styles.uploadFile}
                                source={frontPassport ? { uri: frontPassport } : Images.wallet.ic_front_passport}
                            />
                            <UIButton onPress={() => frontRef.open()} style={styles.btnUploadPhoto} text={t('wallet.upload_photo')} />
                        </View>
                    </View>
                    <View style={{ marginBottom: verticalScale(25) }}>
                        <UIText font="bold" style={styles.txtUploadTitle} text={t('wallet.passport_information')} />
                        <View style={styles.uploadContainer}>
                            <Image style={styles.uploadFile} source={passport ? { uri: passport } : Images.wallet.ic_passport} />
                            <UIButton onPress={() => passportRef.open()} style={styles.btnUploadPhoto} text={t('wallet.upload_photo')} />
                        </View>
                    </View>
                    <View style={{ marginBottom: verticalScale(25) }}>
                        <UIText font="bold" style={styles.txtUploadTitle} text={t('wallet.your_selfie')} />
                        <View style={styles.uploadContainer}>
                            <Image style={styles.uploadFile} source={selfie ? { uri: selfie } : Images.wallet.ic_selfie} />
                            <UIButton onPress={() => selfieRef.open()} style={styles.btnUploadPhoto} text={t('wallet.upload_photo')} />
                        </View>
                    </View>
                    <UIButton onPress={onKycDone} style={{ marginTop: 'auto' }} text={t('wallet.continue')} />
                </View>
                <UIModal ref={(ref) => (frontRef = ref)}>
                    <>
                        <View style={{ flex: 1 }} />
                        <TouchableOpacity activeOpacity={1} style={{ width: '88%' }}>
                            <View style={styles.viewModalTop}>
                                <TouchableOpacity onPress={onTakeFrontPassport}>
                                    <UIText text={t('profile.take_photo')} style={styles.textSelectImg} font="bold" />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={onChooseFrontPassport}>
                                    <UIText
                                        text={t('profile.choose_from')}
                                        style={[styles.textSelectImg, { marginBottom: 0 }]}
                                        font="bold"
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{ height: verticalScale(10) }} />
                            <TouchableOpacity style={styles.viewModalBot} onPress={() => frontRef.close()}>
                                <UIText text={t('profile.cancel')} style={styles.textCancel} font="bold" />
                            </TouchableOpacity>
                        </TouchableOpacity>
                    </>
                </UIModal>
                <UIModal ref={(ref) => (passportRef = ref)}>
                    <>
                        <View style={{ flex: 1 }} />
                        <TouchableOpacity activeOpacity={1} style={{ width: '88%' }}>
                            <View style={styles.viewModalTop}>
                                <TouchableOpacity onPress={onTakePassport}>
                                    <UIText text={t('profile.take_photo')} style={styles.textSelectImg} font="bold" />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={onChoosePassport}>
                                    <UIText
                                        text={t('profile.choose_from')}
                                        style={[styles.textSelectImg, { marginBottom: 0 }]}
                                        font="bold"
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{ height: verticalScale(10) }} />
                            <TouchableOpacity style={styles.viewModalBot} onPress={() => passportRef.close()}>
                                <UIText text={t('profile.cancel')} style={styles.textCancel} font="bold" />
                            </TouchableOpacity>
                        </TouchableOpacity>
                    </>
                </UIModal>
                <UIModal ref={(ref) => (selfieRef = ref)}>
                    <>
                        <View style={{ flex: 1 }} />
                        <TouchableOpacity activeOpacity={1} style={{ width: '88%' }}>
                            <View style={styles.viewModalTop}>
                                <TouchableOpacity onPress={onTakeSelfire}>
                                    <UIText text={t('profile.take_photo')} style={styles.textSelectImg} font="bold" />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={onChooseSelfire}>
                                    <UIText
                                        text={t('profile.choose_from')}
                                        style={[styles.textSelectImg, { marginBottom: 0 }]}
                                        font="bold"
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{ height: verticalScale(10) }} />
                            <TouchableOpacity style={styles.viewModalBot} onPress={() => selfieRef.close()}>
                                <UIText text={t('profile.cancel')} style={styles.textCancel} font="bold" />
                            </TouchableOpacity>
                        </TouchableOpacity>
                    </>
                </UIModal>
            </View>
        </ScrollView>
    );
};

const mapStateToProps = (state) => ({
    walletReducer: state.walletReducer,
});
export default connect(mapStateToProps, {
    actionKycSetup,
    actionUploadImage,
})(UploadFileKyc);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        paddingTop: verticalScale(30),
        flex: 1,
    },
    txtTitle: {
        fontSize: moderateScale(36),
        color: Colors.blueDark,
        lineHeight: moderateScale(46),
        marginBottom: verticalScale(40),
    },
    txtUploadTitle: {
        color: Colors.blueDark,
        fontSize: moderateScale(14),
    },
    uploadContainer: {
        flexDirection: 'row',
        marginTop: verticalScale(10),
        justifyContent: 'space-between',
    },
    uploadFile: {
        width: verticalScale(100),
        height: verticalScale(100),
        borderRadius: verticalScale(8),
        borderColor: Colors.border,
        borderWidth: 1,
    },
    btnUploadPhoto: {
        height: verticalScale(42),
        width: verticalScale(142),
    },
    viewModalTop: {
        padding: verticalScale(12),
        backgroundColor: Colors.white,
        borderRadius: 10,
    },
    viewModalBot: {
        marginBottom: verticalScale(20),
        paddingVertical: verticalScale(12),
        backgroundColor: Colors.white,
        borderRadius: 10,
    },
    textSelectImg: {
        alignSelf: 'center',
        marginBottom: verticalScale(10),
        borderRadius: 10,
    },
    textCancel: {
        textTransform: 'capitalize',
        lineHeight: verticalScale(20),
        alignSelf: 'center',
        borderRadius: 10,
    },
});
