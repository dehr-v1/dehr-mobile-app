import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Colors } from '../../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../../utils/ScalingUtils';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import { UIButton, UIText } from '../../../../components/elements';
import { useNavigation } from '@react-navigation/native';
import { SCREEN } from '../../../../constant';
import { UIHeader } from '../../../../components/common';
import { connect } from 'react-redux';

const SetupKYC = () => {
    const { Images } = useTheme();
    const { t } = useTranslation();
    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <UIHeader />
            <View style={styles.contentContainer}>
                <Image resizeMode="contain" style={styles.bgKyc} source={Images.wallet.bg_kyc} />
                <UIText style={styles.txtTitle} font="black" text={t('wallet.dehr_kyc')} />
                <UIText style={styles.txtDes} text={t('wallet.dehr_kyc_des')} />
                <UIButton onPress={() => navigation.navigate(SCREEN.ENTER_PHONE_KYC)} text={t('wallet.kyc_now')} />
            </View>
        </View>
    );
};

const mapStateToProps = (state) => ({
    walletReducer: state.walletReducer,
});
export default connect(mapStateToProps, {})(SetupKYC);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        flex: 1,
        alignItems: 'center',
    },
    bgKyc: {
        width: '100%',
        marginTop: verticalScale(30),
    },
    txtTitle: {
        fontSize: moderateScale(24),
        color: Colors.blueDark,
        marginTop: verticalScale(30),
        marginBottom: verticalScale(20),
    },
    txtDes: {
        fontSize: moderateScale(16),
        textAlign: 'center',
        marginBottom: verticalScale(50),
    },
});
