import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import { Colors } from '../../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../../utils/ScalingUtils';
import { UIButton, UIText } from '../../../../components/elements';
import { SCREEN } from '../../../../constant';

const DoneKyc = () => {
    const { Images } = useTheme();
    const { t } = useTranslation();
    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <View style={styles.contentContainer}>
                <Image resizeMode="contain" style={styles.bg} source={Images.wallet.bg_done_kyc} />
                <UIText font="black" style={styles.txtTitle} text={t('wallet.verifying')} />
                <UIText style={styles.txtDes} text={t('wallet.take_48h')} />
                <UIButton onPress={() => navigation.navigate(SCREEN.MAIN, { screen: 'Wallet' })} text={t('wallet.back_to_wallet')} />
            </View>
        </View>
    );
};

export default DoneKyc;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        paddingTop: verticalScale(30),
        alignItems: 'center',
    },
    bg: {
        width: '100%',
        marginTop: verticalScale(30),
    },
    txtTitle: {
        fontSize: moderateScale(24),
        color: Colors.blueDark,
        marginBottom: verticalScale(20),
    },
    txtDes: {
        fontSize: moderateScale(16),
        color: Colors.blueDark,
        marginBottom: verticalScale(50),
    },
});
