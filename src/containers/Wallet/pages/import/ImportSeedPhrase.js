import React, { useState, useRef } from 'react';
import { View, StyleSheet, ScrollView, Dimensions, TouchableWithoutFeedback } from 'react-native';
import { UIHeader, UITextInputForm, UIModal } from '../../../../components/common';
import { UIButton, UIText } from '../../../../components/elements';
import { Colors } from '../../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../../utils/ScalingUtils';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import { actionImportWallet } from '../../duck/actions';
import { connect } from 'react-redux';
const { width } = Dimensions.get('window');

const ImportSeedPhrase = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { authReducer, actionImportWallet } = props || {};
    const { user } = authReducer || {};
    const [seedPhrase, setSeedPhrase] = useState('');
    const [loading, isLoading] = useState(false);
    let confirmModalRef = useRef();

    const onPressDone = (confirmModal) => {
        isLoading(true);
        const wallet = user.wallet;
        const data = {
            wallet,
            seedPhrase,
        };
        setTimeout(() => {
            actionImportWallet({ data }, (isCheck) => {
                isLoading(false);
                if (isCheck) {
                    navigation.goBack();
                } else {
                    confirmModal.open();
                }
            });
        }, 10);
    };

    const onPressBack = (confirmModal) => {
        navigation.goBack();
        confirmModal.close();
    };

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={styles.container}>
                <UIHeader onPress={() => navigation.goBack()} LeftIcon="back" />
                <View style={styles.contentContainer}>
                    <UIText style={styles.txtTitle} font="black" text={t('wallet.import_from_seed')} />
                    <UITextInputForm
                        containerStyle={styles.inputSeedPhrase}
                        title={t('wallet.seed_phrase')}
                        onChangeText={(text) => setSeedPhrase(text)}
                    />
                    <UITextInputForm
                        containerStyle={{ marginBottom: verticalScale(25) }}
                        secureTextEntry
                        title={t('wallet.new_password')}
                    />
                    <UITextInputForm secureTextEntry title={t('wallet.confirm_password')} />
                    <View style={styles.bottomContainer}>
                        <UIText style={styles.txtTerm} text={t('wallet.term_condition')} />
                        <UIButton onPress={() => onPressDone(confirmModalRef)} text={t('wallet.import')} isLoading={loading} />
                    </View>
                </View>
                <UIModal ref={(ref) => (confirmModalRef = ref)}>
                    <TouchableWithoutFeedback>
                        <View style={styles.topUpContainer}>
                            <UIText font="bold" style={styles.txtTitleModal} text={t('common.error')} />
                            <UIText style={styles.txtDesc} text={t('wallet.address_wallet_error')} />
                            <UIButton onPress={() => onPressBack(confirmModalRef)} text={t('wallet.got_it')} />
                        </View>
                    </TouchableWithoutFeedback>
                </UIModal>
            </View>
        </ScrollView>
    );
};

const mapStateToProps = (state) => ({
    walletReducer: state.walletReducer,
    authReducer: state.authReducer,
});
export default connect(mapStateToProps, {
    actionImportWallet,
})(ImportSeedPhrase);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        flex: 1,
    },
    txtTitle: {
        fontSize: verticalScale(36),
        lineHeight: verticalScale(46),
        marginTop: verticalScale(25),
    },
    inputSeedPhrase: {
        marginTop: verticalScale(40),
        marginBottom: verticalScale(25),
    },
    bottomContainer: {
        marginTop: 'auto',
        marginBottom: verticalScale(40),
    },
    txtTerm: {
        marginBottom: verticalScale(20),
        textAlign: 'center',
        fontSize: moderateScale(12),
        color: Colors.blueDark,
    },
    topUpContainer: {
        width: width - verticalScale(60),
        borderRadius: verticalScale(10),
        padding: verticalScale(20),
        backgroundColor: Colors.white,
        alignItems: 'center',
    },
    txtTitleModal: {
        fontSize: moderateScale(20),
        lineHeight: moderateScale(26),
        width: '100%',
        textAlign: 'center',
        marginBottom: verticalScale(15),
    },
    txtDesc: {
        textAlign: 'center',
        marginBottom: verticalScale(30),
    },
});
