import React, { useEffect, useState } from 'react';
import { View, StyleSheet, ScrollView, Image } from 'react-native';
import { UIHeader } from '../../../../components/common';
import { UIButton, UIText } from '../../../../components/elements';
import { UISeedPhrase, UIStep } from '../../components';
import { useTranslation } from 'react-i18next';
import { moderateScale, verticalScale } from '../../../../utils/ScalingUtils';
import { useTheme } from '@/theme';
import { Colors } from '../../../../theme/Variables';
import { useNavigation, useRoute } from '@react-navigation/native';
import { SCREEN } from '../../../../constant';
import { actionRegisterWallet } from '../../duck/actions';
import { connect } from 'react-redux';

const ConfirmSecure = (props) => {
    const route = useRoute();
    const { t } = useTranslation();
    const { Images } = useTheme();
    const navigation = useNavigation();
    const { actionRegisterWallet } = props || {};
    const [done, setDone] = useState(false);
    const [data, setData] = useState([
        { value: 5, isChoose: false },
        { value: 9, isChoose: false },
        { value: 12, isChoose: false },
    ]);
    const [correctIndex, setCorrectIndex] = useState([]);
    const [index, setIndex] = useState(0);

    // useEffect(() => {
    //     const value1 = newRandomNumber
    //     const value2 = newRandomNumber
    //     const value3 = newRandomNumber
    //     setData([{ value: value1, isChoose: false }, { value: value2, isChoose: false }, { value: value3, isChoose: false }])
    // }, [])

    const stepComponent = () => {
        return <UIStep step={3} />;
    };

    const newRandomNumber = () => {
        const number = Math.Math.floor(Math.random() * 12) + 1;
        return number;
    };

    const onPressSeedPhrase = (item, i) => {
        if (data[index].value == i + 1) {
            setData(
                data.map((e, ind) => {
                    return index == ind ? { value: data[index].value, isChoose: true } : e;
                })
            );
            setCorrectIndex([...correctIndex, i]);
            setIndex(index + 1);
        }
    };

    const onPressDone = () => {
        actionRegisterWallet({}, (data) => {
            if (data) {
                navigation.navigate(SCREEN.MAIN, { screen: 'Wallet' });
            }
        });
    };

    const renderConfirmSecure = () => {
        return (
            <>
                <UIText style={styles.txtTitle} font="black" text={t('wallet.confirm_secure')} />
                <View style={styles.selectWordContainer}>
                    <UIText style={styles.txtSelectWord} text={t('wallet.select_word')} />
                    <View style={styles.confirmContainer}>
                        {data.map((e, i) => {
                            return (
                                <View
                                    style={[
                                        styles.confirmText,
                                        data[i].isChoose && { backgroundColor: Colors.green, borderStyle: 'solid' },
                                    ]}
                                >
                                    <UIText style={[data[i].isChoose && { color: Colors.white }]} text={e.value} />
                                </View>
                            );
                        })}
                    </View>
                </View>
                <UISeedPhrase
                    indexCorrect={correctIndex}
                    listSeedPhase={route.params}
                    onPressItem={onPressSeedPhrase}
                    style={{ marginTop: verticalScale(50) }}
                    isShowNumber={false}
                />
                <UIButton onPress={() => setDone(true)} style={{ marginTop: verticalScale(80) }} text={t('wallet.continue')} />
            </>
        );
    };

    const renderDone = () => {
        return (
            <>
                <Image resizeMode="contain" style={styles.bgCongrat} source={Images.wallet.bg_congratulation} />
                <View style={styles.txtCongratContainer}>
                    <UIText style={styles.txtCongrat} font="black" text={t('wallet.congratulations')} />
                    <UIText style={styles.txtProtect} text={t('wallet.protect_success')} />
                </View>
                <UIText font="bold" style={styles.txtLeaveHint} text={t('wallet.leave_a_hint')} />
                <UIText style={styles.txtLeaveHintDes} text={t('wallet.leave_a_hint_des')} />
                <UIButton
                    onPress={onPressDone}
                    style={{ marginTop: 'auto', marginBottom: verticalScale(20) }}
                    text={t('wallet.continue')}
                />
            </>
        );
    };

    return (
        <View style={styles.container}>
            <UIHeader onPress={() => navigation.goBack()} LeftIcon="back" centerComponent={stepComponent} />
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.contentContainer}>{done ? renderDone() : renderConfirmSecure()}</View>
            </ScrollView>
        </View>
    );
};

const mapStateToProps = (state) => ({
    walletReducer: state.walletReducer,
});
export default connect(mapStateToProps, {
    actionRegisterWallet,
})(ConfirmSecure);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        flex: 1,
    },
    txtTitle: {
        fontSize: moderateScale(36),
        lineHeight: moderateScale(46),
        marginTop: verticalScale(25),
        marginBottom: verticalScale(23),
    },
    selectWordContainer: {
        borderWidth: 1,
        borderRadius: verticalScale(6),
        borderColor: Colors.border,
        backgroundColor: Colors.gray,
        paddingHorizontal: verticalScale(20),
        paddingVertical: verticalScale(16),
    },
    txtSelectWord: {
        fontSize: moderateScale(16),
        color: Colors.blueDark,
    },
    confirmText: {
        width: verticalScale(86),
        height: verticalScale(40),
        borderColor: Colors.border,
        borderWidth: 1,
        borderStyle: 'dashed',
        borderRadius: verticalScale(6),
        justifyContent: 'center',
        alignItems: 'center',
    },
    confirmContainer: {
        marginTop: verticalScale(20),
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: verticalScale(5),
    },
    bgCongrat: {
        width: '100%',
        marginTop: verticalScale(20),
    },
    txtCongratContainer: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: verticalScale(20),
    },
    txtCongrat: {
        fontSize: moderateScale(24),
        color: Colors.blueDark,
    },
    txtProtect: {
        textAlign: 'center',
        fontSize: moderateScale(16),
        lineHeight: moderateScale(22),
        marginTop: verticalScale(20),
    },
    txtLeaveHint: {
        marginTop: verticalScale(25),
        fontSize: moderateScale(16),
        color: '#162b8a',
        marginBottom: verticalScale(20),
    },
    txtLeaveHintDes: {
        fontSize: moderateScale(14),
        lineHeight: moderateScale(22),
        color: Colors.blueDark,
    },
});
