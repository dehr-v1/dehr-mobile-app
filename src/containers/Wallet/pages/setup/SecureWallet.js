import React, { useState, useRef, useEffect } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, Dimensions, ScrollView, TouchableWithoutFeedback } from 'react-native';
import { UIHeader, UIModal, UITextInputForm } from '../../../../components/common';
import { UIButton, UIText } from '../../../../components/elements';
import { UISeedPhrase, UIStep } from '../../components';
import { useTranslation } from 'react-i18next';
import { moderateScale, verticalScale } from '../../../../utils/ScalingUtils';
import { useTheme } from '@/theme';
import { Colors } from '../../../../theme/Variables';
import { useNavigation } from '@react-navigation/native';
import { fontFamily } from '../../../../theme/Fonts';
import { SCREEN } from '../../../../constant';
import { actionCreateWallet } from '../../duck/actions';
import { connect } from 'react-redux';

const SecureWallet = (props) => {
    const { t } = useTranslation();
    const { Images } = useTheme();
    const navigation = useNavigation();
    const [step, setStep] = useState(0);
    const { actionCreateWallet } = props || {};
    const [seedPhase, setRenderSeedPhase] = useState([]);
    const [loading, isLoading] = useState(false);
    const [loadingSecure, isLoadingSecure] = useState(false);

    let modalRef = useRef();
    let skilRef = useRef();

    useEffect(() => {}, []);

    const stepComponent = () => {
        return <UIStep step={2} />;
    };

    const stepStart = () => {
        isLoading(true);
        setTimeout(() => {
            actionCreateWallet({}, (seedphaseString) => {
                const listSeedPhase = seedphaseString.split(' ');
                setRenderSeedPhase(listSeedPhase);
                isLoading(false);
                setStep(1);
            });
        }, 10);
    };

    const secureStart = (skilref) => {
        isLoadingSecure(true);
        setTimeout(() => {
            actionCreateWallet({}, (seedphaseString) => {
                const listSeedPhase = seedphaseString.split(' ');
                setRenderSeedPhase(listSeedPhase);
                isLoadingSecure(false);
                skilref.close();
                setStep(1);
            });
        }, 10);
    };

    const renderIntroSureWallet = () => {
        return (
            <>
                <UIText style={styles.txtTitle} font="black" text={t('wallet.secure_wallet')} />
                <Image resizeMode="contain" style={styles.imgBg} source={Images.wallet.bg_secure_wallet} />
                <Text style={styles.txtSeedPhrase}>
                    {t('wallet.secure_wallet_des_1')}
                    <TouchableOpacity onPress={() => modalRef.open()} style={{ marginTop: -verticalScale(3) }}>
                        <Text style={styles.linkSeedPhrase}>{t('wallet.seed_phrase_des')}</Text>
                    </TouchableOpacity>
                    {t('wallet.secure_wallet_des_2')}
                </Text>
                <UIButton isLoading={loading} onPress={stepStart} style={styles.btn} text={t('wallet.start')} />
                <UIButton onPress={() => skilRef.open()} type="BgGrey" text={t('wallet.remind_later')} />
            </>
        );
    };

    const renderTutorial = () => {
        return (
            <>
                <UIText style={styles.txtTitle} font="black" text={t('wallet.secure_wallet')} />
                <UIText style={styles.txtUsePhrase} text={t('wallet.use_seed_phrase')} />
                <View style={styles.whyImportantContainer}>
                    <Image style={styles.icQuestion} source={Images.wallet.ic_question} />
                    <UIText style={styles.txtImportant} text={t('wallet.why_important')} />
                </View>
                <View style={styles.tutoContainer}>
                    <UIText style={styles.txt14} text={t('wallet.tuto_1')} />
                    <Text style={styles.txtSecurity_lever}>
                        {t('wallet.security_lever')} <Text style={styles.txtVeryStrong}>{t('wallet.very_strong')}</Text>
                    </Text>
                    <View style={{ flexDirection: 'row', marginTop: verticalScale(15) }}>
                        <View style={styles.greenLevel} />
                        <View style={styles.greenLevel} />
                        <View style={styles.greenLevel} />
                        <View style={styles.greenLevel} />
                    </View>
                    <UIText font="bold" style={styles.txtTitleList} text={t('wallet.risks_are')} />
                    <UIText style={styles.txt14} text={t('wallet.you_lose')} />
                    <UIText style={styles.txt14} text={t('wallet.you_forgot')} />
                    <UIText style={styles.txt14} text={t('wallet.someone_finds')} />
                    <UIText style={styles.txtOtherOption} text={t('wallet.other_option')} />
                    <UIText font="bold" style={styles.txtTitleList} text={t('wallet.tips')} />
                    <UIText style={styles.txt14} text={t('wallet.store_bank')} />
                    <UIText style={styles.txt14} text={t('wallet.store_safe')} />
                    <UIText style={styles.txt14} text={t('wallet.store_multiple')} />
                </View>
                <UIButton
                    onPress={() => setStep(2)}
                    style={{ marginTop: 'auto', marginBottom: verticalScale(20) }}
                    text={t('wallet.next')}
                />
            </>
        );
    };

    const renderWriteSeedPhrase = () => {
        return (
            <>
                <UIText style={styles.txtTitle} font="black" text={t('wallet.write_seed_phrase')} />
                <UIText style={[styles.txtUsePhrase, { marginTop: verticalScale(17) }]} text={t('wallet.write_seed_phrase_des')} />
                <UISeedPhrase isSecure listSeedPhase={seedPhase} style={styles.seedPhraseContaier} />
                <UIButton
                    onPress={() => navigation.navigate(SCREEN.CONFIRM_SECURE, seedPhase)}
                    style={styles.btnContinue}
                    text={t('wallet.continue')}
                />
            </>
        );
    };

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={styles.container}>
                <UIHeader onPress={() => navigation.goBack()} LeftIcon="back" centerComponent={stepComponent} />
                <View style={styles.contentContainer}>
                    {step == 0 && renderIntroSureWallet()}
                    {step == 1 && renderTutorial()}
                    {step == 2 && renderWriteSeedPhrase()}
                </View>
                <UIModal ref={(ref) => (modalRef = ref)}>
                    <>
                        <View style={{ flex: 1 }} />
                        <TouchableWithoutFeedback onPress={() => {}}>
                            <View style={styles.modalContainer}>
                                <UIText style={styles.txtModalTitle} font="bold" text={t('wallet.what_is_seed_phrase')} />
                                <UIText style={styles.txtModalDes} text={t('wallet.what_is_seed_phrase_des')} />
                                <UIButton
                                    onPress={() => modalRef.close()}
                                    style={{ marginTop: verticalScale(30) }}
                                    text={t('wallet.got_it')}
                                />
                            </View>
                        </TouchableWithoutFeedback>
                    </>
                </UIModal>
                <UIModal ref={(ref) => (skilRef = ref)}>
                    <TouchableWithoutFeedback onPress={() => {}}>
                        <View style={styles.modalSkipContainer}>
                            <View style={styles.skipContainer}>
                                <UIText style={styles.txtModalTitle} font="bold" text={t('wallet.skip_security')} />
                                <UIText style={styles.txtModalDes} text={t('wallet.skip_security_des')} />
                                <View style={styles.btnModalContainer}>
                                    <UIButton
                                        style={styles.btnModal}
                                        isLoading={loadingSecure}
                                        onPress={() => secureStart(skilRef)}
                                        type="BgWhite"
                                        text={t('wallet.secure_now')}
                                    />
                                    <UIButton
                                        style={styles.btnModal}
                                        onPress={() => {
                                            skilRef.close();
                                            navigation.navigate(SCREEN.MAIN, { screen: 'Wallet' });
                                        }}
                                        text={t('wallet.skip')}
                                    />
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </UIModal>
            </View>
        </ScrollView>
    );
};

const mapStateToProps = (state) => ({
    walletReducer: state.walletReducer,
});
export default connect(mapStateToProps, {
    actionCreateWallet,
})(SecureWallet);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        flex: 1,
    },
    txtTitle: {
        fontSize: moderateScale(36),
        lineHeight: moderateScale(46),
        marginTop: verticalScale(25),
        marginBottom: verticalScale(23),
    },
    txtDes: {
        fontSize: verticalScale(16),
        lineHeight: verticalScale(22),
        marginTop: verticalScale(30),
        color: Colors.blueDark,
    },
    imgBg: {
        width: '100%',
    },
    btn: {
        marginTop: verticalScale(40),
        marginBottom: verticalScale(20),
    },
    txtUsePhrase: {
        fontSize: moderateScale(16),
        color: Colors.title,
    },
    whyImportantContainer: {
        flexDirection: 'row',
        marginTop: verticalScale(5),
    },
    icQuestion: {
        width: verticalScale(16),
        height: verticalScale(16),
    },
    txtImportant: {
        fontSize: moderateScale(14),
        lineHeight: moderateScale(22),
        color: '#162b8a',
        marginLeft: verticalScale(3),
    },
    tutoContainer: {
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(6),
        backgroundColor: Colors.gray,
        padding: verticalScale(20),
        marginTop: verticalScale(20),
    },
    txt14: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
    },
    txtSecurity_lever: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
        fontFamily: fontFamily.regular,
    },
    txtVeryStrong: {
        fontFamily: fontFamily.bold,
        fontSize: moderateScale(14),
        color: Colors.green,
    },
    greenLevel: {
        width: verticalScale(36),
        height: verticalScale(6),
        backgroundColor: Colors.green,
        borderRadius: verticalScale(20),
        marginRight: verticalScale(8),
    },
    txtTitleList: {
        marginTop: verticalScale(15),
        color: Colors.blueDark,
        fontSize: moderateScale(14),
    },
    txtOtherOption: {
        marginTop: verticalScale(15),
        fontSize: moderateScale(14),
        color: Colors.blueDark,
    },
    seedPhraseContaier: {
        marginTop: verticalScale(70),
    },
    btnContinue: {
        marginTop: 'auto',
        marginBottom: verticalScale(20),
    },
    txtSeedPhrase: {
        fontSize: moderateScale(16),
        fontFamily: fontFamily.regular,
        color: Colors.hint,
        textAlign: 'left',
        lineHeight: moderateScale(22),
    },
    linkSeedPhrase: {
        fontSize: moderateScale(16),
        fontFamily: fontFamily.regular,
        color: Colors.blue,
        lineHeight: moderateScale(22),
    },
    modalContainer: {
        backgroundColor: Colors.white,
        width: '100%',
        paddingHorizontal: verticalScale(30),
        paddingVertical: verticalScale(30),
        borderTopLeftRadius: verticalScale(20),
        borderTopRightRadius: verticalScale(20),
        alignItems: 'flex-start',
    },
    txtModalTitle: {
        fontSize: moderateScale(20),
        color: Colors.blueDark,
    },
    txtModalDes: {
        fontSize: moderateScale(16),
        color: '#3e3f41',
        marginTop: verticalScale(20),
    },
    modalSkipContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    skipContainer: {
        backgroundColor: Colors.white,
        width: Dimensions.get('screen').width - verticalScale(60),
        paddingHorizontal: verticalScale(20),
        paddingVertical: verticalScale(25),
        borderRadius: verticalScale(20),
        alignItems: 'center',
    },
    btnModal: {
        flex: 0.4,
        height: verticalScale(42),
    },
    btnModalContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: verticalScale(30),
        width: '100%',
    },
});
