import React, { useState } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { UIHeader, UITextInputForm } from '../../../../components/common';
import { UIButton, UIText } from '../../../../components/elements';
import { UIStep } from '../../components';
import { setPassWallet } from '../../../../services/persistence/store.config';
import { useTranslation } from 'react-i18next';
import { verticalScale } from '../../../../utils/ScalingUtils';
import { useNavigation } from '@react-navigation/native';
import { SCREEN } from '../../../../constant';
import { Colors } from '../../../../theme/Variables';

const CreatePassword = () => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const [password, setPassword] = useState('');

    const stepComponent = () => {
        return <UIStep step={1} />;
    };

    const onPressCreatePass = () => {
        setPassWallet(password);
        navigation.navigate(SCREEN.SECURE_WALLET);
    };

    return (
        <ScrollView>
            <View style={styles.container}>
                <UIHeader onPress={() => navigation.goBack()} LeftIcon="back" centerComponent={stepComponent} />
                <View style={styles.contentContainer}>
                    <UIText style={styles.txtTitle} font="black" text={t('wallet.create_password')} />
                    <UIText style={styles.txtDes} text={t('wallet.password_unlock')} />
                    <UITextInputForm secureTextEntry containerStyle={styles.inputPassword} title={t('wallet.new_password')} />
                    <UITextInputForm secureTextEntry title={t('wallet.confirm_password')} onChangeText={(text) => setPassword(text)} />
                    <View style={styles.bottomContent}>
                        <UIText style={styles.txtBottom} text={t('wallet.password_can_not_recover')} />
                        <UIButton onPress={onPressCreatePass} style={styles.btnStyle} text={t('wallet.create_password_btn')} />
                    </View>
                </View>
            </View>
        </ScrollView>
    );
};

export default CreatePassword;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        flex: 1,
    },
    txtTitle: {
        fontSize: verticalScale(36),
        lineHeight: verticalScale(46),
        marginTop: verticalScale(25),
    },
    txtDes: {
        fontSize: verticalScale(14),
        lineHeight: verticalScale(20),
        marginTop: verticalScale(80),
    },
    inputPassword: {
        marginTop: verticalScale(25),
        marginBottom: verticalScale(25),
    },
    bottomContent: {
        marginTop: verticalScale(80),
    },
    txtBottom: {
        fontSize: verticalScale(12),
        lineHeight: verticalScale(16),
        textAlign: 'center',
    },
    btnStyle: {
        marginBottom: verticalScale(44),
        marginTop: verticalScale(12),
    },
});
