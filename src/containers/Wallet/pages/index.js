import { CreatePassword, SecureWallet, ConfirmSecure } from './setup';
import { ImportSeedPhrase } from './import';
import { AllTransactionPage } from './transaction';
import { SetupKYC, EnterPhoneNumber, VerifyPhoneKyc, FillInfoKyc, UploadFileKyc, DoneKyc } from './kyc';

export {
    CreatePassword,
    SecureWallet,
    ConfirmSecure,
    ImportSeedPhrase,
    SetupKYC,
    EnterPhoneNumber,
    VerifyPhoneKyc,
    FillInfoKyc,
    UploadFileKyc,
    DoneKyc,
    AllTransactionPage,
};
