import React, { useState, useRef, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, FlatList, Image } from 'react-native';
import { useTheme } from '@/theme';
import { moderateScale, verticalScale, CommonUtils } from '../../../../utils';
import { UIHeader, UIInfiniteFlatList, UIModal } from '../../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { UIButton, UIText, UIButtonChoose } from '../../../../components/elements';
import { useNavigation } from '@react-navigation/native';
import { connect } from 'react-redux';
import moment from 'moment';
import { TRANSACTION_TYPE, TRANSACTION_FILTER } from '../../../../constant/value.constant';
import { TransactionModal } from '../../modals/index';
import { actionListTransaction } from '../../duck/actions';

const AllTransactionPage = (props) => {
    const { authReducer, actionListTransaction } = props || {};
    const { Images } = useTheme();
    const { t } = useTranslation();
    const { user } = authReducer || {};
    const navigation = useNavigation();
    const [hiringFilter, setHiringFilter] = useState(TRANSACTION_FILTER.ALL);
    const [selectedItem, setSelectecItem] = useState(null);

    let modalRef = useRef();

    useEffect(() => {}, []);

    const onFetchData = async (page, useCallback) => {
        actionListTransaction({ wallet: user.wallet, page, filterType: hiringFilter }, (pageList) => {
            useCallback && useCallback(pageList);
        });
    };

    const getIcon = (type) => {
        switch (type) {
            case TRANSACTION_TYPE.WITHDRAW:
                return Images.wallet.ic_withdraw;
            case TRANSACTION_TYPE.DEPOSITED:
                return Images.wallet.ic_deposit;
            default:
                return Images.wallet.ic_exchange;
        }
    };

    const getText = (type) => {
        switch (type) {
            case TRANSACTION_TYPE.WITHDRAW:
                return t('wallet.withdrawn');
            case TRANSACTION_TYPE.DEPOSITED:
                return t('wallet.received');
            default:
                return t('wallet.exchanged');
        }
    };

    const getAmount = (isReceive, amount) => {
        if (isReceive) return `+ ${CommonUtils.convertFromWei(amount)}`;
        else return `- ${CommonUtils.convertFromWei(amount)}`;
    };

    const renderItem = ({ item, index }) => {
        const icon = getIcon(item.type);
        return (
            <TouchableOpacity onPress={() => onPressItem(item)} style={styles.itemContainer}>
                <Image style={styles.icType} source={icon} />
                <View style={styles.itemColumnContainer}>
                    <View style={styles.itemRowContainer}>
                        <UIText style={styles.txtType} font="medium" text={getText(item.type)} />
                        <UIText font="bold" style={styles.txtValue} text={getAmount(item.isReceive, item.amount)} />
                    </View>
                    <View style={styles.itemRowContainer}>
                        <UIText
                            style={{ ...styles.txtType }}
                            font="bold"
                            ellipsizeMode="tail"
                            numberOfLines={1}
                            text={t(item.isReceive ? 'wallet.from_sender' : 'wallet.to_receiver', {
                                name: item.isReceive ? item.from : item.to,
                            })}
                        />
                        <UIText style={styles.txtDate} text={moment(item.create_at).format('hh:mm - MMM DD, YYYY')} />
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    const renderRowItem = (title, text) => {
        return (
            <View style={styles.row}>
                <UIText style={styles.txtTextRow} text={title} />
                <UIText font="medium" style={styles.txtRow} text={text} />
            </View>
        );
    };

    const onPressItem = (item) => {
        setSelectecItem(item);
        modalRef.current.open();
    };

    return (
        <View style={styles.container}>
            <UIHeader onPress={() => navigation.goBack()} LeftIcon="back" title={t('wallet.transactions')} titleStyle={styles.txtTitle} />
            <View style={styles.contentContainer}>
                <View style={{ flexDirection: 'row', marginBottom: verticalScale(20) }}>
                    <UIButtonChoose
                        isChoose={hiringFilter == TRANSACTION_FILTER.ALL}
                        onPress={() => setHiringFilter(TRANSACTION_FILTER.ALL)}
                        text={t('home.all')}
                    />
                    <UIButtonChoose
                        isChoose={hiringFilter == TRANSACTION_FILTER.RECEIVE}
                        onPress={() => setHiringFilter(TRANSACTION_FILTER.RECEIVE)}
                        text={t('wallet.deposit')}
                    />
                    <UIButtonChoose
                        isChoose={hiringFilter == TRANSACTION_FILTER.WITHDRAW}
                        onPress={() => setHiringFilter(TRANSACTION_FILTER.WITHDRAW)}
                        text={t('wallet.withdraw')}
                    />
                </View>
                {hiringFilter == TRANSACTION_FILTER.ALL && (
                    <UIInfiniteFlatList key={TRANSACTION_FILTER.ALL} onFetchData={onFetchData.bind(this)} renderItem={renderItem} />
                )}
                {hiringFilter == TRANSACTION_FILTER.RECEIVE && (
                    <UIInfiniteFlatList key={TRANSACTION_FILTER.RECEIVE} onFetchData={onFetchData.bind(this)} renderItem={renderItem} />
                )}
                {hiringFilter == TRANSACTION_FILTER.WITHDRAW && (
                    <UIInfiniteFlatList key={TRANSACTION_FILTER.WITHDRAW} onFetchData={onFetchData.bind(this)} renderItem={renderItem} />
                )}
            </View>

            <UIModal ref={modalRef} animationType={'slide'}>
                <TransactionModal onPressCancelModal={() => modalRef.current.close()} transaction={selectedItem} />
            </UIModal>
        </View>
    );
};

const mapStateToProps = (state) => ({
    authReducer: state.authReducer,
    walletReducer: state.walletReducer,
});

export default connect(mapStateToProps, {
    actionListTransaction,
})(AllTransactionPage);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(18),
    },
    containerTextChart: {
        position: 'absolute',
        left: verticalScale(70),
        top: verticalScale(20),
    },
    containerTextChartTotal: {
        height: verticalScale(45),
    },
    logoDeHR: {
        width: verticalScale(100),
        height: verticalScale(30),
    },
    logoDeHRSmall: {
        marginStart: verticalScale(6),
        width: verticalScale(45),
        height: verticalScale(15),
    },
    txtDeHR: {
        fontSize: moderateScale(28),
        color: Colors.white,
    },
    txtSeeAll: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
    },
    txtDeHRPercent: {
        marginStart: verticalScale(6),
        fontSize: moderateScale(14),
        color: '#00e626',
    },
    txtDeHRCoin: {
        fontSize: moderateScale(14),
        color: Colors.white,
    },
    bgWalletSetup: {
        width: '100%',
        marginTop: verticalScale(30),
    },
    txtWalletSetup: {
        fontSize: verticalScale(24),
        color: Colors.blueDark,
        marginTop: verticalScale(30),
    },
    txtImport: {
        fontSize: verticalScale(16),
        color: Colors.blueDark,
        textAlign: 'center',
        marginTop: verticalScale(20),
    },
    btnCreate: {
        marginTop: verticalScale(45),
        marginBottom: verticalScale(20),
    },
    avatar: {
        width: verticalScale(40),
        height: verticalScale(40),
        borderRadius: verticalScale(20),
    },
    txtWallet: {
        fontSize: moderateScale(12),
        color: '#707070',
    },
    txtUsername: {
        fontSize: moderateScale(24),
        color: Colors.title,
    },
    center: {
        alignItems: 'flex-start',
        marginLeft: verticalScale(12),
    },
    icNoti: {
        width: verticalScale(36),
        height: verticalScale(36),
    },
    walletContainer: {
        flex: 1,
        paddingHorizontal: verticalScale(15.5),
    },
    chart: {
        width: '100%',
        height: verticalScale(275),
        backgroundColor: Colors.blueDark,
        borderRadius: verticalScale(8),
    },
    txtAction: {
        fontSize: moderateScale(12),
        color: Colors.hint,
        textTransform: 'uppercase',
        marginTop: verticalScale(10),
        marginStart: verticalScale(10),
    },
    icAction: {
        width: verticalScale(60),
        height: verticalScale(60),
        borderRadius: verticalScale(30),
    },
    btnAction: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    actionContainer: {
        marginTop: verticalScale(25),
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    txtLatest: {
        fontSize: moderateScale(16),
        color: Colors.blueDark,
    },
    latestContainer: {
        marginTop: verticalScale(35),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginBottom: verticalScale(15),
    },
    icLogo: {
        width: verticalScale(47),
        height: verticalScale(15.5),
        tintColor: Colors.blueDark,
    },
    btnTab: {
        width: verticalScale(85),
        height: verticalScale(36),
        borderRadius: verticalScale(6),
        backgroundColor: '#140f1d5e',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: verticalScale(2),
    },
    txtTab: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
    },
    icPoint: {
        width: verticalScale(24),
        height: verticalScale(24),
    },
    icType: {
        width: verticalScale(40),
        height: verticalScale(40),
        borderRadius: verticalScale(20),
        marginRight: verticalScale(10),
    },
    txtType: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
        width: verticalScale(200),
    },
    txtDate: {
        fontSize: moderateScale(12),
        color: Colors.lightGray,
    },
    txtBalance: {
        fontSize: moderateScale(12),
        color: '#132474',
    },
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: verticalScale(10),
        marginBottom: verticalScale(7),
        paddingHorizontal: verticalScale(12),
    },
    itemColumnContainer: {
        flexDirection: 'column',
        alignSelf: 'stretch',
        flex: 1,
    },
    itemRowContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'stretch',
    },
    txtValue: {
        color: Colors.black,
    },
    icPointItem: {
        width: verticalScale(14),
        height: verticalScale(14),
        marginLeft: verticalScale(5),
    },
    icLogoItem: {
        width: verticalScale(25.5),
        height: verticalScale(8.5),
        tintColor: Colors.blueDark,
        marginLeft: verticalScale(5),
    },
    txtTextRow: {
        color: Colors.hint,
    },
    txtRow: {
        color: Colors.blueDark,
    },
    row: {
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: verticalScale(10),
        flexDirection: 'row',
    },
    modalContainer: {
        backgroundColor: Colors.white,
        width: '100%',
        paddingHorizontal: verticalScale(30),
        paddingVertical: verticalScale(30),
        borderTopLeftRadius: verticalScale(20),
        borderTopRightRadius: verticalScale(20),
        alignItems: 'center',
    },
    txtTitleModal: {
        fontSize: moderateScale(22),
        color: Colors.blueDark,
        marginBottom: verticalScale(20),
    },
    txtValueModal: {
        fontSize: moderateScale(30),
        marginBottom: verticalScale(10),
        lineHeight: moderateScale(36),
    },
    txtTitle: {
        fontSize: moderateScale(24),
        color: Colors.blueDark,
    },
    txtTextRow: {
        color: Colors.hint,
    },
    txtRow: {
        color: Colors.blueDark,
    },
    row: {
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: verticalScale(10),
        flexDirection: 'row',
    },
});
