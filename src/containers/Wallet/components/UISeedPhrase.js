import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, TouchableWithoutFeedback } from 'react-native';
import { UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { useTheme } from '@/theme';
import { BlurView } from '@react-native-community/blur';
import { useTranslation } from 'react-i18next';

const UISeedPhrase = (props) => {
    const { style, isSecure = false, listSeedPhase = [], isShowNumber = true, onPressItem, indexCorrect } = props;
    const { Images } = useTheme();
    const { t } = useTranslation();

    const [secure, setSecure] = useState(isSecure);

    const onPress = (item, index) => {
        onPressItem && onPressItem(item, index);
    };

    const renderItem = (item, index) => {
        const isChoose = indexCorrect && indexCorrect.includes(index);
        return (
            <TouchableOpacity
                onPress={() => onPress(item, index)}
                style={[styles.itemContainer, isChoose && { backgroundColor: '#00a3ff' }]}
            >
                {isShowNumber && <UIText style={styles.txt} font="medium" text={`${index + 1}. `} />}
                <UIText style={[styles.txt, isChoose && { color: Colors.white }]} font="medium" text={item} />
            </TouchableOpacity>
        );
    };

    return (
        <TouchableWithoutFeedback onPress={() => setSecure(false)}>
            <View style={[styles.container, style]}>
                {listSeedPhase.map((e, i) => renderItem(e, i))}
                {secure && (
                    <BlurView style={styles.hideBlurContainer} blurType="light" blurAmount={4} reducedTransparencyFallbackColor="grey" />
                )}
                {secure && (
                    <View style={styles.hideContainer}>
                        <Image source={Images.wallet.ic_hide} style={styles.icHide} />
                        <UIText font="bold" style={styles.txtTapToReveal} text={t('wallet.tap_to_reveal')} />
                        <UIText style={styles.txtNoOne} text={t('wallet.make_sure_no_one')} />
                    </View>
                )}
            </View>
        </TouchableWithoutFeedback>
    );
};

export default UISeedPhrase;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingHorizontal: verticalScale(12),
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    itemContainer: {
        width: '31%',
        height: verticalScale(40),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(6),
        marginBottom: verticalScale(16),
        paddingTop: verticalScale(6),
    },
    txt: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
    },
    hideBlurContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    hideContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: '#00000057',
        borderRadius: verticalScale(8),
        alignItems: 'center',
        justifyContent: 'center',
    },
    icHide: {
        width: verticalScale(44),
        height: verticalScale(44),
    },
    txtTapToReveal: {
        fontSize: moderateScale(18),
        color: Colors.white,
        marginVertical: verticalScale(10),
    },
    txtNoOne: {
        color: Colors.white,
        fontSize: moderateScale(14),
    },
});
