import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Colors } from '../../../theme/Variables';
import { verticalScale } from '../../../utils/ScalingUtils';

const UIStep = (props) => {
    const { step } = props;
    return (
        <View style={{ flexDirection: 'row' }}>
            <View style={[styles.step, step >= 1 && styles.stepActive]} />
            <View style={[styles.step, step >= 2 && styles.stepActive]} />
            <View style={[styles.step, step >= 3 && styles.stepActive]} />
        </View>
    );
};

export default UIStep;

const styles = StyleSheet.create({
    step: {
        width: verticalScale(20),
        height: verticalScale(6),
        backgroundColor: Colors.disable,
        marginHorizontal: verticalScale(4),
        borderRadius: verticalScale(10),
    },
    stepActive: {
        backgroundColor: Colors.blueDark,
    },
});
