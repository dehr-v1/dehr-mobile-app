import { WalletAction } from '../actions';

const wallet = {
    wallet: {},
    isLoading: false,
    coinDeHRCache: 0,
    pageListTransaction: {},
};
const walletReducer = (state = wallet, action) => {
    const { type, payload } = action;
    switch (type) {
        case WalletAction.CREATE_WALLET.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case WalletAction.CREATE_WALLET.SUCCESS:
            return {
                ...state,
                isLoading: false,
                wallet: payload,
            };
        case WalletAction.CREATE_WALLET.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case WalletAction.IMPORT_WALLET.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case WalletAction.IMPORT_WALLET.SUCCESS:
            return {
                ...state,
                isLoading: false,
                wallet: payload,
            };
        case WalletAction.IMPORT_WALLET.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case WalletAction.REGISTER_WALLET.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case WalletAction.REGISTER_WALLET.SUCCESS:
            return {
                ...state,
                isLoading: false,
                wallet: payload,
            };
        case WalletAction.REGISTER_WALLET.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case WalletAction.BALANCE_WALLET.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case WalletAction.BALANCE_WALLET.SUCCESS:
            return {
                ...state,
                isLoading: false,
                coinDeHRCache: payload,
            };
        case WalletAction.BALANCE_WALLET.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case WalletAction.KYC_SETUP.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case WalletAction.KYC_SETUP.SUCCESS:
            return {
                ...state,
                isLoading: false,
                wallet: payload,
            };
        case WalletAction.KYC_SETUP.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case WalletAction.UPLOAD_IMAGE.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case WalletAction.UPLOAD_IMAGE.SUCCESS:
            return {
                ...state,
                isLoading: false,
                wallet: payload,
            };
        case WalletAction.UPLOAD_IMAGE.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case WalletAction.LIST_TRANSACTION.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case WalletAction.LIST_TRANSACTION.SUCCESS:
            return {
                ...state,
                isLoading: false,
                pageListTransaction: payload,
            };
        case WalletAction.LIST_TRANSACTION.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case WalletAction.CREATE_TRANSACTION.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case WalletAction.CREATE_TRANSACTION.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case WalletAction.CREATE_TRANSACTION.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
};

export { walletReducer };
