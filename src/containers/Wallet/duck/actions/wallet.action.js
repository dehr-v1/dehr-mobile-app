import { WalletAction } from './wallet.create.action';

export const actionCreateWallet = (payload, callback) => ({
    type: WalletAction.CREATE_WALLET.PENDING,
    payload,
    callback,
});

export const actionRegisterWallet = (payload, callback) => ({
    type: WalletAction.REGISTER_WALLET.PENDING,
    payload,
    callback,
});

export const actionImportWallet = (payload, callback) => ({
    type: WalletAction.IMPORT_WALLET.PENDING,
    payload,
    callback,
});

export const actionBalanceWallet = (payload, callback) => ({
    type: WalletAction.BALANCE_WALLET.PENDING,
    payload,
    callback,
});

export const actionKycSetup = (payload, callback) => ({
    type: WalletAction.KYC_SETUP.PENDING,
    payload,
    callback,
});

export const actionUploadImage = (payload, callback) => ({
    type: WalletAction.UPLOAD_IMAGE.PENDING,
    payload,
    callback,
});

export const actionListTransaction = (payload, callback) => ({
    type: WalletAction.LIST_TRANSACTION.PENDING,
    payload,
    callback,
});

export const actionCreateTransaction = (payload, callback) => ({
    type: WalletAction.CREATE_TRANSACTION.PENDING,
    payload,
    callback,
});
