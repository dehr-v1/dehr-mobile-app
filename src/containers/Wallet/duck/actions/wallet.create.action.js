import { createActionSet } from '../../../../utils';

export const WalletAction = {
    CREATE_WALLET: createActionSet('CREATE_WALLET'),
    REGISTER_WALLET: createActionSet('REGISTER_WALLET'),
    IMPORT_WALLET: createActionSet('IMPORT_WALLET'),
    LOGIN_WALLET: createActionSet('LOGIN_WALLET'),
    BALANCE_WALLET: createActionSet('BALANCE_WALLET'),
    KYC_SETUP: createActionSet('KYC_SETUP'),
    UPLOAD_IMAGE: createActionSet('UPLOAD_IMAGE'),
    LIST_TRANSACTION: createActionSet('LIST_TRANSACTION'),
    CREATE_TRANSACTION: createActionSet('CREATE_TRANSACTION'),
};
