import { all } from 'redux-saga/effects';
import {
    watchonCreateWallet,
    watchonImportWallet,
    watchonRegisterWallet,
    watchonBalanceWallet,
    watchonKycSetup,
    watchonUpload,
    watchOnListTransaction,
} from './wallet.saga';

export function* walletSaga() {
    yield all([
        watchonCreateWallet(),
        watchonImportWallet(),
        watchonRegisterWallet(),
        watchonBalanceWallet(),
        watchonKycSetup(),
        watchonUpload(),
        watchOnListTransaction(),
    ]);
}
