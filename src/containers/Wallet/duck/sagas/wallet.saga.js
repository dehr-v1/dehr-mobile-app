import { put, call, takeLatest } from 'redux-saga/effects';
import Toast from '../../../../components/elements/Toast';
import { getCurrentUser, getOAuthToken, setSeedPhase, getSeedPhase, getHashCheckSum } from '../../../../services/persistence/store.config';
import { LoginAction } from '../../../Login/duck/actions';
import { WalletAction } from '../actions';
import { ProfileAction } from '../../../Profile/duck/actions';
import {
    createWalletBlockChain,
    registerWalletBlockChain,
    createWallet,
    importWalletBlockChain,
    loginWallet,
    balanceWallet,
    kycSetup,
    uploadImage,
    getListTransaction,
    transferDeHrToken,
} from '../../../../services/remote/wallet.service';
import * as EthUtils from 'ethereumjs-util';
import { toChecksumAddress } from 'ethereum-checksum-address';
import { TRANSACTION_TYPE, TRANSACTION_FILTER } from '../../../../constant/value.constant';

function* onCreateWallet(action) {
    const { payload, callback } = action;
    let mnemonic = '';
    let privateKey = '';
    let publicKey = '';
    let address = '';
    try {
        const wallet = yield call(() => createWalletBlockChain());
        address = wallet.address;
        mnemonic = wallet.mnemonic.phrase;
        privateKey = wallet.privateKey;
        const bufferPrivateKey = Buffer.from(privateKey.substring(2), 'hex');
        const bufferPublicKey = EthUtils.privateToPublic(Buffer.from(bufferPrivateKey, 'hex'));
        publicKey = '0x' + bufferPublicKey.toString('hex');
        const data = { address: address, privateKey: privateKey, publicKey: publicKey };
        setSeedPhase(JSON.stringify(data));
        yield put({
            type: WalletAction.CREATE_WALLET.SUCCESS,
            payload: mnemonic,
        });
        callback && callback(mnemonic);
    } catch (error) {
        yield put({
            type: WalletAction.CREATE_WALLET.ERROR,
            payload: error,
        });
    }
}

export function* watchonCreateWallet() {
    yield takeLatest(WalletAction.CREATE_WALLET.PENDING, onCreateWallet);
}

function* onImportWallet(action) {
    const { payload, callback } = action;
    let privateKey = '';
    let publicKey = '';
    let address = '';
    try {
        const seedPhrase = payload.data.seedPhrase;
        const addressOld = payload.data.wallet;
        const wallet = yield call(() => importWalletBlockChain(seedPhrase));
        address = toChecksumAddress(wallet.address);
        privateKey = wallet.privateKey;
        const bufferPrivateKey = Buffer.from(privateKey.substring(2), 'hex');
        const bufferPublicKey = EthUtils.privateToPublic(Buffer.from(bufferPrivateKey, 'hex'));
        publicKey = '0x' + bufferPublicKey.toString('hex');
        const data = { address: address, privateKey: privateKey, publicKey: publicKey };

        if (addressOld) {
            if (addressOld.toLowerCase() === address.toLowerCase()) {
                setSeedPhase(JSON.stringify(data));
                yield put({
                    type: WalletAction.IMPORT_WALLET.SUCCESS,
                    payload: '',
                });
                callback && callback(true);
            } else {
                yield put({
                    type: WalletAction.IMPORT_WALLET.ERROR,
                    payload: '',
                });
                callback && callback(false);
            }
        } else {
            const getUser = yield call(() => getCurrentUser());
            const user = JSON.parse(getUser);
            const checksum = yield call(() => registerWalletBlockChain(user.email, user.phoneNumber));
            const walletData = {
                address: address,
                publicKey,
                checksum,
            };
            const response = yield call(() => createWallet(walletData));
            setSeedPhase(JSON.stringify(data));
            yield put({
                type: WalletAction.IMPORT_WALLET.SUCCESS,
                payload: '',
            });
            yield put({
                type: WalletAction.REGISTER_WALLET.SUCCESS,
                payload: response.data,
            });
            yield put({
                type: ProfileAction.GET_ACCOUNT_INFO.PENDING,
            });
            callback && callback(true);
        }
    } catch (error) {
        yield put({
            type: WalletAction.IMPORT_WALLET.ERROR,
            payload: error,
        });
        callback && callback(false);
    }
}

export function* watchonImportWallet() {
    yield takeLatest(WalletAction.IMPORT_WALLET.PENDING, onImportWallet);
}

function* onCreateTransaction(action) {
    const { payload, callback } = action;
    try {
        const { amount } = payload.data;
        const getWallet = yield call(() => getSeedPhase());
        const seedPhrase = JSON.parse(getWallet);
        const destination = toChecksumAddress(payload.data.destination);
        const response = yield call(() => transferDeHrToken(destination, amount, seedPhrase));
        yield put({
            type: WalletAction.CREATE_TRANSACTION.SUCCESS,
            payload: response,
        });
        callback && callback(true);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: WalletAction.CREATE_TRANSACTION.ERROR,
            payload: error,
        });
        callback && callback(false);
    }
}

export function* watchonCreateTransaction() {
    yield takeLatest(WalletAction.CREATE_TRANSACTION.PENDING, onCreateTransaction);
}

function* onRegisterWallet(action) {
    const { payload, callback } = action;
    try {
        //Remove get email and phone from server.
        // const getUser = yield call(() => getCurrentUser());
        // const user = JSON.parse(getUser);
        // const checksum = yield call(() => registerWalletBlockChain(user.email, user.phoneNumber));
        const hashCheckSum = yield call(() => getHashCheckSum());
        const getWallet = yield call(() => getSeedPhase());
        const seedPhase = JSON.parse(getWallet);
        const data = {
            address: seedPhase.address,
            publicKey: seedPhase.publicKey,
            hashCheckSum,
        };
        const response = yield call(() => createWallet(data));
        yield put({
            type: WalletAction.REGISTER_WALLET.SUCCESS,
            payload: response.data,
        });
        yield put({
            type: ProfileAction.GET_ACCOUNT_INFO.PENDING,
        });
        callback && callback(true);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: WalletAction.REGISTER_WALLET.ERROR,
            payload: error,
        });
    }
}

export function* watchonRegisterWallet() {
    yield takeLatest(WalletAction.REGISTER_WALLET.PENDING, onRegisterWallet);
}

function* onLoginWallet(action) {
    const { payload, callback } = action;
    try {
        const seedPhase = yield call(() => getSeedPhase());
        const data = { mnemonic: seedPhase };
        const token = yield call(() => getOAuthToken());
        const response = yield call(() => loginWallet(data, token));
        yield put({
            type: WalletAction.LOGIN_WALLET.SUCCESS,
            payload: response.data.address,
        });
        callback && callback(response.data.address);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: WalletAction.LOGIN_WALLET.ERROR,
            payload: error,
        });
    }
}

export function* watchonLoginWallett() {
    yield takeLatest(WalletAction.LOGIN_WALLET.PENDING, onLoginWallet);
}

function* onBalanceWallet(action) {
    try {
        const { payload, callback } = action;
        const getWallet = yield call(() => getSeedPhase());
        const wallet = JSON.parse(getWallet);
        const balanceDHR = yield call(() => balanceWallet(toChecksumAddress(wallet?.address)));
        if (balanceDHR) {
            yield put({
                type: WalletAction.BALANCE_WALLET.SUCCESS,
                payload: balanceDHR,
            });
            callback && callback(balanceDHR);
        } else {
            Toast(`${response.msg}`, 'error');
            yield put({
                type: WalletAction.BALANCE_WALLET.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        yield put({
            type: WalletAction.BALANCE_WALLET.ERROR,
            payload: error,
        });
    }
}

export function* watchonBalanceWallet() {
    yield takeLatest(WalletAction.BALANCE_WALLET.PENDING, onBalanceWallet);
}

function* onKycSetup(action) {
    try {
        const { payload, callback } = action;
        let { data } = payload;

        const formData = new FormData();
        if (data.firstName) {
            formData.append('firstName', data.firstName);
        }
        if (data.lastName) {
            formData.append('lastName', data.lastName);
        }
        if (data.phoneNumber) {
            formData.append('phoneNumber', data.phoneNumber);
        }
        if (data.gender) {
            formData.append('gender', data.gender);
        }
        if (data.passportId) {
            formData.append('passportId', data.passportId);
        }

        if (data.passportFront) {
            const imageFront = {
                uri: data.passportFront.path,
                type: data.passportFront.mime,
                name: data.passportFront.mime.replace('/', '.'),
            };
            formData.append('passportFront', imageFront);
        }
        if (data.passportBack) {
            const imageBack = {
                uri: data.passportBack.path,
                type: data.passportBack.mime,
                name: data.passportBack.mime.replace('/', '.'),
            };
            formData.append('passportBack', imageBack);
        }

        if (data.passportSelfie) {
            const imageSelfie = {
                uri: data.passportSelfie.path,
                type: data.passportSelfie.mime,
                name: data.passportSelfie.mime.replace('/', '.'),
            };
            formData.append('passportSelfie', imageSelfie);
        }
        const res = yield call(() => kycSetup(formData));

        yield put({
            type: WalletAction.KYC_SETUP.SUCCESS,
        });
        yield put({
            type: ProfileAction.GET_ACCOUNT_INFO.PENDING,
        });
        callback && callback(res);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: WalletAction.KYC_SETUP.ERROR,
            payload: error,
        });
    }
}

export function* watchonKycSetup() {
    yield takeLatest(WalletAction.KYC_SETUP.PENDING, onKycSetup);
}

function* onUploadImage(action) {
    try {
        const { payload, callback } = action;
        const data = payload;
        const formData = new FormData();
        const image = {
            uri: data.image.path,
            type: data.image.mime,
            name: data.image.mime.replace('/', '.'),
        };
        formData.append('uploadFile', image);
        const response = yield call(() => uploadImage(formData));
        if (response.success == 1) {
            yield put({
                type: WalletAction.UPLOAD_IMAGE.SUCCESS,
            });
            yield put({
                type: WalletAction.UPLOAD_IMAGE.ERROR,
            });
            callback && callback(response.data);
        } else {
            Toast(`${response.msg}`, 'error');
            yield put({
                type: WalletAction.UPLOAD_IMAGE.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        yield put({
            type: WalletAction.UPLOAD_IMAGE.ERROR,
            payload: error,
        });
    }
}

export function* watchonUpload() {
    yield takeLatest(WalletAction.UPLOAD_IMAGE.PENDING, onUploadImage);
}

function getTransactionType(wallet, data) {
    if (data.args.from.toLowerCase() === wallet.toLowerCase()) return TRANSACTION_TYPE.WITHDRAW;
    if (data.args.to.toLowerCase() === wallet.toLowerCase()) return TRANSACTION_TYPE.DEPOSITED;
}

function isReceive(wallet, data) {
    if (data.args.from.toLowerCase() === wallet.toLowerCase()) return false;
    else return true;
}

function getTransactionFilter(filterType) {
    switch (filterType) {
        case TRANSACTION_FILTER.ALL:
            return '';
        case TRANSACTION_FILTER.RECEIVE:
            return '&receive=true';
        case TRANSACTION_FILTER.WITHDRAW:
            return '&send=true';
        default:
            return '';
    }
}

function getContractName(data) {
    return `${data.memberFrom?.firstName || data.memberTo?.firstName} ${data.memberFrom?.lastName || data.memberTo?.lastName}`;
}

function getName(data) {
    if (data) {
        return `${data.firstName} ${data.lastName}`;
    }
}

function* onListTransaction(action) {
    try {
        const { payload, callback } = action;
        const { page, filterType, wallet } = payload;
        const filter = getTransactionFilter(filterType);
        const response = yield call(() => getListTransaction(page || 1, filter));
        const listTransation = response.data.map((data) => {
            return {
                amount: data.amount,
                contractName: getContractName(data),
                from: getName(data.memberFrom) ? getName(data.memberFrom) : data.from,
                to: getName(data.memberTo) ? getName(data.memberTo) : data.to,
                code: data.blockNumber,
                type: getTransactionType(wallet, data),
                createdAt: data.createdAt,
                isReceive: isReceive(wallet, data),
            };
        });
        const pageList = { page: response.page, totalPages: response.totalPages, items: listTransation };
        yield put({
            type: WalletAction.LIST_TRANSACTION.SUCCESS,
            payload: pageList,
        });
        callback && callback(pageList);
    } catch (error) {
        yield put({
            type: WalletAction.LIST_TRANSACTION.ERROR,
            payload: error,
        });
    }
}

export function* watchOnListTransaction() {
    yield takeLatest(WalletAction.LIST_TRANSACTION.PENDING, onListTransaction);
}
