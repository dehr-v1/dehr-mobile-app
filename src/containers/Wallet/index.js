import React, { useState, useRef, useEffect, useCallback } from 'react';
import {
    View,
    TouchableOpacity,
    Image,
    StyleSheet,
    ScrollView,
    StatusBar,
    FlatList,
    Dimensions,
    ImageBackground,
    TouchableWithoutFeedback,
    Share,
} from 'react-native';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../theme/Variables';
import { moderateScale, verticalScale, CommonUtils } from '../../utils';
import { UIButton, UIText } from '../../components/elements';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { SCREEN } from '../../constant';
import { UIHeader, UIModal, UITextInputForm } from '../../components/common';
import { connect } from 'react-redux';
import { actionBalanceWallet, actionListTransaction } from './duck/actions';
import { LineChart } from 'react-native-chart-kit';
import moment from 'moment';
import { BASE_IMAGE_URL, KYC_STATUS } from '../../constant';
import { getSeedPhase } from '../../services/persistence/store.config';
import Web3 from 'web3';
import { TransactionModal, DepositModal, WithdrawModal } from './modals/index';
import { TRANSACTION_TYPE } from '../../constant/value.constant';
import { balanceWallet } from '../../services/remote/wallet.service';
const { width } = Dimensions.get('window');
const web3 = new Web3();

const fakeDataChart = {
    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    datasets: [
        {
            data: [30, 90, 67, 54, 10, 2, 23, 40, 60, 70, 100, 200],
        },
    ],
};

const IndexWalletContainer = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { Layout, Images } = useTheme();
    const { authReducer, profileReducer, walletReducer, actionBalanceWallet, actionListTransaction } = props || {};
    const { user } = authReducer || {};
    const { accountInfo } = profileReducer || {};
    const { coinDeHRCache } = walletReducer || 0;
    const [isPoint, setIsPoint] = useState(false);
    const [selectedItem, setSelectecItem] = useState({});
    const [isSetup, setIsSetup] = useState(false);
    const [chart, setListChart] = useState(fakeDataChart);
    const [coinDeHR, setCoinDeHR] = useState(0);
    const [showMvp, setShowMvp] = useState(false);
    const [address, setAddress] = useState('');
    const [listTransation, setListTransation] = useState([]);

    let modalRef = useRef();
    let modalWithDrawRef = useRef();
    let modalReceiveRef = useRef();

    useEffect(() => {
        if (coinDeHRCache != 0) {
            setCoinDeHR(CommonUtils.convertFromWei(coinDeHRCache));
        }
        if (user.kycStatus === KYC_STATUS.UNVERIFIED) {
            navigation.navigate(SCREEN.SETUP_KYC);
        } else if (user.kycStatus === KYC_STATUS.VERIFYING) {
            navigation.navigate(SCREEN.DONE_KYC);
        } else {
            actionListTransaction({ wallet: user.wallet }, (pageList) => {
                setListTransation(pageList.items.slice(0, 5));
                if (pageList.items.length > 0) {
                    setSelectecItem(pageList.items[0]);
                }
            });
        }
    }, []);

    useFocusEffect(
        useCallback(() => {
            const iniPage = async () => {
                const walletData = await getSeedPhase();

                const address = JSON.parse(walletData)?.address;
                if (address) {
                    setAddress(address);
                    actionBalanceWallet({ address }, (balanceDHR) => {
                        setCoinDeHR(CommonUtils.convertFromWei(balanceDHR));
                        setIsSetup(false);
                    });
                } else {
                    setIsSetup(true);
                }
            };
            iniPage();
        }, [accountInfo])
    );

    const createWallet = () => {
        if (user.kycStatus === KYC_STATUS.UNVERIFIED) {
            navigation.navigate(SCREEN.SETUP_KYC);
        } else if (user.kycStatus === KYC_STATUS.VERIFYING) {
            navigation.navigate(SCREEN.DONE_KYC);
        } else {
            navigation.navigate(SCREEN.CREATE_PASSWORD);
        }
    };

    const importWallet = () => {
        if (user.kycStatus === KYC_STATUS.UNVERIFIED) {
            navigation.navigate(SCREEN.SETUP_KYC);
        } else if (user.kycStatus === KYC_STATUS.VERIFYING) {
            navigation.navigate(SCREEN.DONE_KYC);
        } else {
            navigation.navigate(SCREEN.IMPORT_SEED_PHRASE);
        }
    };

    const renderChart = () => {
        return (
            <ImageBackground source={Images.home.bg_chart} style={styles.containerChart} imageStyle={{ borderRadius: 16 }}>
                <LineChart
                    bezier
                    data={chart}
                    width={width - verticalScale(30)}
                    height={verticalScale(260)}
                    yAxisLabel=""
                    yAxisSuffix=" DeHR"
                    chartConfig={{
                        backgroundColor: 'transparent',
                        backgroundGradientTo: 'transparent',
                        backgroundGradientFromOpacity: 0,
                        backgroundGradientFrom: 'transparent',
                        backgroundGradientToOpacity: 0,
                        decimalPlaces: 0,
                        color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        style: {
                            borderRadius: 16,
                        },
                        propsForDots: {
                            r: '4',
                            strokeWidth: '2',
                            stroke: '#93ff00',
                        },
                        propsForLabels: {
                            fontSize: moderateScale(9),
                        },
                    }}
                    style={{
                        borderRadius: 16,
                    }}
                    verticalLabelRotation={20}
                    formatXLabel={(label) => label.toUpperCase()}
                />
                <View style={[Layout.column, styles.containerTextChart]}>
                    <Image style={styles.logoDeHR} source={Images.logo} resizeMode="cover" />
                    <View style={[Layout.row, styles.containerTextChartTotal]}>
                        <UIText font="bold" style={styles.txtDeHR} text={coinDeHR} />
                    </View>
                </View>
            </ImageBackground>
        );
    };

    const renderNoWallet = () => {
        return (
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.contentContainer}>
                    <Image resizeMode="contain" source={Images.wallet.bg_wallet_setup} style={styles.bgWalletSetup} />
                    <UIText font="bold" style={styles.txtWalletSetup} text={t('wallet.wallet_setup')} />
                    <UIText style={styles.txtImport} text={t('wallet.import_or_create')} />
                    {user.wallet == null && <UIButton onPress={createWallet} style={styles.btnCreate} text={t('wallet.wallet_create')} />}
                    <UIButton onPress={importWallet} style={styles.btnImport} text={t('wallet.import_seed')} type={''} />
                    {/* <UIButton style={styles.btnCreate} onPress={() => navigation.navigate(SCREEN.SETUP_KYC)} text={t('Setup KYC')} type="BgGrey" /> */}
                </View>
            </ScrollView>
        );
    };

    const renderAvatar = () => {
        return (
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
                <Image
                    style={styles.avatar}
                    source={accountInfo.avatar_url ? { uri: BASE_IMAGE_URL + accountInfo.avatar_url } : Images.profile.ic_avatar}
                />
            </TouchableOpacity>
        );
    };

    const renderTitle = () => {
        return (
            <View>
                <UIText style={styles.txtWallet} text={`${t('wallet.wallet')},`} />
                <UIText numberOfLines={1} ellipsizeMode="tail" font="bold" style={styles.txtUsername} text={user.username} />
            </View>
        );
    };

    const renderNotiIcon = () => {
        return <Image style={styles.icNoti} source={Images.wallet.ic_noti} />;
    };

    const renderIconAction = (icon, text) => {
        return (
            <TouchableOpacity
                onPress={() => {
                    switch (text) {
                        case t('wallet.deposit'):
                            return modalReceiveRef.current.open();
                        case t('wallet.withdraw'):
                            return modalWithDrawRef.current.open();
                    }
                }}
                style={styles.btnAction}
            >
                <Image style={styles.icAction} source={icon} />
                <UIText style={styles.txtAction} text={text} />
            </TouchableOpacity>
        );
    };

    const getIcon = (type) => {
        switch (type) {
            case TRANSACTION_TYPE.WITHDRAW:
                return Images.wallet.ic_withdraw;
            case TRANSACTION_TYPE.DEPOSITED:
                return Images.wallet.ic_deposit;
            default:
                return Images.wallet.ic_exchange;
        }
    };

    const getText = (type) => {
        switch (type) {
            case TRANSACTION_TYPE.WITHDRAW:
                return t('wallet.withdrawn');
            case TRANSACTION_TYPE.DEPOSITED:
                return t('wallet.received');
            default:
                return t('wallet.exchanged');
        }
    };

    const getAmount = (isReceive, amount) => {
        if (isReceive) return `+ ${CommonUtils.convertFromWei(amount)}`;
        else return `- ${CommonUtils.convertFromWei(amount)}`;
    };

    const renderItem = ({ item, index }) => {
        const icon = getIcon(item.type);
        return (
            <TouchableOpacity onPress={() => onPressItem(item)} style={styles.itemContainer}>
                <Image style={styles.icType} source={icon} />
                <View style={styles.itemColumnContainer}>
                    <View style={styles.itemRowContainer}>
                        <UIText style={styles.txtType} font="medium" text={getText(item.type)} />
                        <UIText font="bold" style={styles.txtValue} text={getAmount(item.isReceive, item.amount)} />
                    </View>
                    <View style={styles.itemRowContainer}>
                        <UIText
                            style={{ ...styles.txtType }}
                            font="bold"
                            ellipsizeMode="tail"
                            numberOfLines={1}
                            text={t(item.isReceive ? 'wallet.from_sender' : 'wallet.to_receiver', {
                                name: item.isReceive ? item.from : item.to,
                            })}
                        />
                        <UIText style={{ ...styles.txtDate }} text={moment(item.createAt).format('hh:mm - MMM DD, YYYY')} />
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    const onPressItem = (item) => {
        setSelectecItem(item);
        modalRef.current.open();
    };

    return (
        <View style={styles.container}>
            {isSetup ? null : (
                <UIHeader
                    onPressRight={() => navigation.navigate(SCREEN.NOTIFICATION)}
                    centerStyle={styles.center}
                    leftComponent={renderAvatar}
                    centerComponent={renderTitle}
                    rightComponent={renderNotiIcon}
                />
            )}
            {isSetup ? (
                renderNoWallet()
            ) : (
                <ScrollView>
                    <View style={styles.walletContainer}>
                        {renderChart()}
                        <View style={styles.actionContainer}>
                            {renderIconAction(Images.wallet.ic_deposit, t('wallet.deposit'))}
                            {renderIconAction(Images.wallet.ic_withdraw, t('wallet.withdraw'))}
                            {showMvp && renderIconAction(Images.wallet.ic_send, t('wallet.send'))}
                            {showMvp && renderIconAction(Images.wallet.ic_exchange, t('wallet.exchange'))}
                        </View>
                        <View style={styles.latestContainer}>
                            <UIText style={styles.txtLatest} font="bold" text={t('wallet.latest_transactions')} />
                            <View style={{ flexDirection: 'row' }}>
                                {listTransation.length > 0 && (
                                    <TouchableOpacity onPress={() => navigation.navigate(SCREEN.ALL_TRANSACTION)}>
                                        <UIText font="medium" style={styles.txtSeeAll} text={t('home.see_all')} />
                                    </TouchableOpacity>
                                )}
                            </View>
                        </View>
                        <FlatList
                            renderItem={renderItem}
                            extraData={listTransation}
                            data={listTransation}
                            keyExtractor={(_, index) => index.toString()}
                        />
                    </View>
                </ScrollView>
            )}

            <UIModal ref={modalRef} animationType={'slide'}>
                <TransactionModal onPressCancelModal={() => modalRef.current.close()} transaction={selectedItem} />
            </UIModal>
            <UIModal ref={modalReceiveRef} animationType={'slide'}>
                <DepositModal onPressCancelModal={() => modalReceiveRef.current.close()} address={address} />
            </UIModal>
            <UIModal ref={modalWithDrawRef} animationType={'slide'}>
                <WithdrawModal onDone={() => modalWithDrawRef.current.close()} balance={coinDeHR} />
            </UIModal>
        </View>
    );
};

const mapStateToProps = (state) => ({
    walletReducer: state.walletReducer,
    authReducer: state.authReducer,
    profileReducer: state.profileReducer,
});
export default connect(mapStateToProps, {
    actionBalanceWallet,
    actionListTransaction,
})(IndexWalletContainer);

const styles = StyleSheet.create({
    containerChart: {
        flex: 1,
        width: null,
        height: null,
    },
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        alignItems: 'center',
    },
    containerTextChart: {
        position: 'absolute',
        left: verticalScale(70),
        top: verticalScale(20),
    },
    containerTextChartTotal: {
        height: verticalScale(45),
        marginTop: verticalScale(15),
    },
    logoDeHR: {
        width: verticalScale(100),
        height: verticalScale(30),
    },
    logoDeHRSmall: {
        marginStart: verticalScale(6),
        width: verticalScale(45),
        height: verticalScale(15),
    },
    txtDeHR: {
        fontSize: moderateScale(24),
        color: Colors.white,
    },
    txtSeeAll: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
    },
    txtDeHRPercent: {
        marginStart: verticalScale(6),
        fontSize: moderateScale(14),
        color: '#00e626',
    },
    txtDeHRCoin: {
        fontSize: moderateScale(14),
        color: Colors.white,
    },
    bgWalletSetup: {
        width: '100%',
        marginTop: verticalScale(30),
    },
    txtWalletSetup: {
        fontSize: verticalScale(24),
        color: Colors.blueDark,
        marginTop: verticalScale(30),
    },
    txtImport: {
        fontSize: verticalScale(16),
        color: Colors.blueDark,
        textAlign: 'center',
        marginTop: verticalScale(20),
    },
    btnCreate: {
        marginTop: verticalScale(30),
    },
    btnImport: {
        marginTop: verticalScale(30),
    },
    avatar: {
        width: verticalScale(40),
        height: verticalScale(40),
        borderRadius: verticalScale(20),
    },
    txtWallet: {
        fontSize: moderateScale(12),
        color: '#707070',
    },
    txtUsername: {
        fontSize: moderateScale(24),
        color: Colors.title,
    },
    center: {
        alignItems: 'flex-start',
        marginLeft: verticalScale(12),
    },
    icNoti: {
        width: verticalScale(36),
        height: verticalScale(36),
    },
    walletContainer: {
        flex: 1,
        paddingHorizontal: verticalScale(15.5),
    },
    chart: {
        width: '100%',
        height: verticalScale(275),
        backgroundColor: Colors.blueDark,
        borderRadius: verticalScale(8),
    },
    txtAction: {
        fontSize: moderateScale(12),
        color: Colors.hint,
        textTransform: 'uppercase',
        marginTop: verticalScale(10),
        marginStart: verticalScale(10),
    },
    icAction: {
        width: verticalScale(60),
        height: verticalScale(60),
        borderRadius: verticalScale(30),
    },
    btnAction: {
        alignItems: 'center',
        flexDirection: 'column',
    },
    actionContainer: {
        marginTop: verticalScale(25),
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignSelf: 'stretch',
    },
    txtLatest: {
        fontSize: moderateScale(16),
        color: Colors.blueDark,
    },
    latestContainer: {
        marginTop: verticalScale(35),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: verticalScale(15),
    },
    icLogo: {
        width: verticalScale(47),
        height: verticalScale(15.5),
        tintColor: Colors.blueDark,
    },
    btnTab: {
        width: verticalScale(85),
        height: verticalScale(36),
        borderRadius: verticalScale(6),
        backgroundColor: '#140f1d5e',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: verticalScale(2),
    },
    txtTab: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
    },
    icPoint: {
        width: verticalScale(24),
        height: verticalScale(24),
    },
    icType: {
        width: verticalScale(40),
        height: verticalScale(40),
        borderRadius: verticalScale(20),
        marginRight: verticalScale(10),
    },
    txtType: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
        width: verticalScale(200),
    },
    txtDate: {
        fontSize: moderateScale(12),
        color: Colors.lightGray,
    },
    txtBalance: {
        fontSize: moderateScale(12),
        color: '#132474',
    },
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: verticalScale(10),
        marginBottom: verticalScale(7),
        paddingHorizontal: verticalScale(12),
    },
    itemColumnContainer: {
        flexDirection: 'column',
        alignSelf: 'stretch',
        flex: 1,
    },
    itemRowContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'stretch',
    },
    txtValue: {
        color: Colors.black,
    },
    icPointItem: {
        width: verticalScale(14),
        height: verticalScale(14),
        marginLeft: verticalScale(5),
    },
    icLogoItem: {
        width: verticalScale(25.5),
        height: verticalScale(8.5),
        tintColor: Colors.blueDark,
        marginLeft: verticalScale(5),
    },
    txtTextRow: {
        color: Colors.hint,
    },
    txtRow: {
        color: Colors.blueDark,
    },
    row: {
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: verticalScale(10),
        flexDirection: 'row',
    },
    modalContainer: {
        backgroundColor: Colors.white,
        width: '100%',
        paddingHorizontal: verticalScale(30),
        paddingVertical: verticalScale(30),
        borderTopLeftRadius: verticalScale(20),
        borderTopRightRadius: verticalScale(20),
        alignItems: 'center',
    },
    txtTitleModal: {
        fontSize: moderateScale(22),
        color: Colors.blueDark,
        marginBottom: verticalScale(20),
    },
    txtValueModal: {
        fontSize: moderateScale(30),
        marginBottom: verticalScale(10),
        lineHeight: moderateScale(36),
    },
});
