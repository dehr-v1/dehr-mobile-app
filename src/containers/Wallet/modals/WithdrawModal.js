import { toChecksumAddress } from 'ethereum-checksum-address';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { StyleSheet, TouchableWithoutFeedback, View, Text } from 'react-native';
import { connect } from 'react-redux';
import { UITextInputForm } from '../../../components/common';
import { UIButton, UIText } from '../../../components/elements';
import Toast from '../../../components/elements/Toast';
import { Colors } from '../../../theme/Variables';
import { moderateScale } from '../../../utils';
import { verticalScale } from '../../../utils/ScalingUtils';
import { actionCreateTransaction } from '../duck/actions';
import { fontFamily } from '../../../theme/Fonts';

const WithdrawModal = (props) => {
    const { pRef, actionCreateTransaction, balance, onDone } = props || {};
    const { t } = useTranslation();
    const [destination, setDestination] = useState('');
    const [amount, setAmount] = useState(null);
    const [loading, isLoading] = useState(false);

    const validate = () => {
        try {
            if (amount === null || amount === '' || parseInt(amount) > parseInt(balance) || parseInt(amount) <= 0) {
                Toast('Invalid amount', 'error');
                return false;
            }
        } catch {
            Toast('Invalid address', 'error');
            return false;
        }
        try {
            toChecksumAddress(destination);
        } catch {
            Toast('Invalid address', 'error');
            return false;
        }
        return true;
    };

    const onPressWithdraw = async () => {
        if (!validate) {
            return;
        }

        isLoading(true);
        const data = {
            amount,
            destination,
        };
        actionCreateTransaction({ data }, (isSuccess) => {
            isLoading(false);
            if (isSuccess) {
                Toast('SUCCESS!');
                onDone();
            } else {
                Toast('ERROR!', 'error');
            }
        });
    };

    return (
        <>
            <View style={{ flex: 1 }} />
            <TouchableWithoutFeedback onPress={() => {}}>
                <View style={styles.modalContainer}>
                    <View style={styles.tipHeader} />
                    <UIText font="bold" style={styles.txtTitleModal} text={t('wallet.withdraw')} />
                    <View style={styles.balanceContainer}>
                        <Text style={{ flex: 1 }}>{t('wallet.my_dehr')}</Text>
                        <Text style={styles.textAmount}>{balance}</Text>
                    </View>
                    <UITextInputForm
                        containerStyle={styles.rowItem}
                        title={t('wallet.address_for_account')}
                        onChangeText={(text) => setDestination(text)}
                    />
                    <UITextInputForm containerStyle={styles.rowItem} title={t('wallet.amount')} onChangeText={(text) => setAmount(text)} />
                    <UIButton
                        onPress={onPressWithdraw}
                        style={{ marginTop: verticalScale(140) }}
                        text={t('wallet.withdraw_my_dehr')}
                        isLoading={loading}
                    />
                </View>
            </TouchableWithoutFeedback>
        </>
    );
};

const mapStateToProps = (state) => ({
    walletReducer: state.walletReducer,
    authReducer: state.authReducer,
    profileReducer: state.profileReducer,
});
export default connect(mapStateToProps, {
    actionCreateTransaction,
})(WithdrawModal);

const styles = StyleSheet.create({
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        flex: 1,
    },
    modalContainer: {
        backgroundColor: Colors.white,
        width: '100%',
        paddingHorizontal: verticalScale(30),
        paddingTop: verticalScale(10),
        paddingBottom: verticalScale(30),
        borderTopLeftRadius: verticalScale(20),
        borderTopRightRadius: verticalScale(20),
        alignItems: 'center',
    },
    tipHeader: {
        width: verticalScale(70),
        height: verticalScale(5),
        marginBottom: verticalScale(20),
        backgroundColor: Colors.grayHiring,
        alignItems: 'center',
        borderRadius: verticalScale(20),
        justifyContent: 'center',
    },
    txtTitleModal: {
        fontSize: moderateScale(22),
        color: Colors.blueDark,
        marginBottom: verticalScale(20),
    },
    txtValueModal: {
        fontSize: moderateScale(30),
        marginBottom: verticalScale(10),
        lineHeight: moderateScale(36),
    },
    txtTextRow: {
        color: Colors.hint,
    },
    txtRow: {
        color: Colors.blueDark,
    },
    row: {
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: verticalScale(10),
        flexDirection: 'row',
    },
    txtAction: {
        fontSize: moderateScale(12),
        color: Colors.hint,
        textTransform: 'uppercase',
        marginTop: verticalScale(10),
        marginStart: verticalScale(10),
    },
    btnAction: {
        alignItems: 'center',
        flexDirection: 'column',
    },
    actionContainer: {
        marginTop: verticalScale(25),
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignSelf: 'stretch',
    },
    rowItem: {
        marginBottom: verticalScale(25),
    },
    balanceContainer: {
        height: verticalScale(80),
        paddingHorizontal: verticalScale(15),
        flexDirection: 'row',
        backgroundColor: Colors.gray,
        alignContent: 'center',
        alignItems: 'center',
        borderRadius: verticalScale(6),
        fontFamily: fontFamily.bold,
    },
    textAmount: {
        flex: 1,
        textAlign: 'right',
        fontSize: 32,
        color: Colors.green,
        fontFamily: fontFamily.bold,
    },
});
