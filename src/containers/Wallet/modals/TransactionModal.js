import moment from 'moment';
import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { Dimensions, StyleSheet, TouchableWithoutFeedback, View } from 'react-native';
import Web3 from 'web3';
import { UIButton, UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale, CommonUtils } from '../../../utils';
import { TRANSACTION_TYPE } from '../../../constant/value.constant';

const TransactionModal = (props) => {
    const { pRef, transaction, onPressCancelModal } = props;
    const { t } = useTranslation();
    const web3 = new Web3();

    const renderRowItem = (title, text) => {
        return (
            <View style={styles.row}>
                <UIText style={{ ...styles.txtTextRow, flex: 2 }} text={title} />
                <UIText
                    font="medium"
                    style={{ ...styles.txtRow, flex: 3, textAlign: 'right' }}
                    ellipsizeMode="tail"
                    numberOfLines={1}
                    text={text}
                />
            </View>
        );
    };

    const getText = (type) => {
        switch (type) {
            case TRANSACTION_TYPE.WITHDRAW:
                return t('wallet.withdrawn');
            case TRANSACTION_TYPE.DEPOSITED:
                return t('wallet.received');
            default:
                return t('wallet.exchanged');
        }
    };

    const getAmount = (isReceive, amount) => {
        if (isReceive) return `+ ${CommonUtils.convertFromWei(amount)}`;
        else return `- ${CommonUtils.convertFromWei(amount)}`;
    };

    return (
        <>
            <View style={{ flex: 1 }} />
            <TouchableWithoutFeedback onPress={() => {}}>
                <View style={styles.modalContainer}>
                    <View style={styles.tipHeader} />
                    <UIText font="bold" style={styles.txtTitleModal} text={t('wallet.transaction_receipt')} />
                    <UIText
                        font="bold"
                        style={[styles.txtValueModal, { color: !transaction.isReceive ? Colors.error : Colors.green }]}
                        text={getAmount(transaction.isReceive, transaction.amount)}
                    />
                    {renderRowItem(t('wallet.type'), getText(transaction.type))}
                    {renderRowItem(t('wallet.transaction_code'), transaction.code)}
                    {renderRowItem(t('wallet.time'), moment(transaction.date).format('hh:mm - MMM DD, YYYY'))}
                    {renderRowItem(
                        t(transaction.isReceive ? 'wallet.from' : 'wallet.to'),
                        transaction.isReceive ? transaction.from : transaction.to
                    )}
                    <UIButton onPress={onPressCancelModal} style={{ marginTop: verticalScale(90) }} text={t('wallet.got_it')} />
                </View>
            </TouchableWithoutFeedback>
        </>
    );
};

export default TransactionModal;

const styles = StyleSheet.create({
    modalContainer: {
        backgroundColor: Colors.white,
        width: '100%',
        paddingHorizontal: verticalScale(30),
        paddingTop: verticalScale(10),
        paddingBottom: verticalScale(30),
        borderTopLeftRadius: verticalScale(20),
        borderTopRightRadius: verticalScale(20),
        alignItems: 'center',
    },
    tipHeader: {
        width: verticalScale(70),
        height: verticalScale(5),
        marginBottom: verticalScale(20),
        backgroundColor: Colors.grayHiring,
        alignItems: 'center',
        borderRadius: verticalScale(20),
        justifyContent: 'center',
    },
    txtTitleModal: {
        fontSize: moderateScale(22),
        color: Colors.blueDark,
        marginBottom: verticalScale(20),
    },
    txtValueModal: {
        fontSize: moderateScale(30),
        marginBottom: verticalScale(10),
        lineHeight: moderateScale(36),
    },
    txtTextRow: {
        color: Colors.hint,
    },
    txtRow: {
        color: Colors.blueDark,
    },
    row: {
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: verticalScale(10),
        flexDirection: 'row',
    },
});
