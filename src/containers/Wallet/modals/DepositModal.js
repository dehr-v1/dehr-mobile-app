import { useTheme } from '@/theme';
import Clipboard from '@react-native-community/clipboard';
import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { Image, Share, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import { UITextInputForm } from '../../../components/common';
import { UIText } from '../../../components/elements';
import Toast from '../../../components/elements/Toast';
import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../utils';

const DepositModal = (props) => {
    const { pRef, address, onPressCancelModal } = props;
    const { t } = useTranslation();
    const { Layout, Images } = useTheme();

    const renderIconAction = (icon, text) => {
        return (
            <TouchableOpacity
                onPress={() => {
                    switch (text) {
                        case t('common.copy'):
                            Toast(t('wallet.copied'), 'success');
                            return Clipboard.setString(address);
                        case t('common.share'):
                            return Share.share({
                                message: address,
                            });
                    }
                }}
                style={styles.btnAction}
            >
                <Image style={styles.icAction} source={icon} />
                <UIText style={styles.txtAction} text={text} />
            </TouchableOpacity>
        );
    };

    return (
        <>
            <View style={{ flex: 1 }} />
            <TouchableWithoutFeedback onPress={() => {}}>
                <View style={styles.modalContainer}>
                    <View style={styles.tipHeader} />
                    <UIText font="bold" style={styles.txtTitleModal} text={t('wallet.deposit')} />
                    <QRCode size={verticalScale(200)} value={address} />

                    <UITextInputForm
                        containerStyle={{ marginTop: verticalScale(25) }}
                        title={t('wallet.address_for_account')}
                        value={address}
                        editable={false}
                    />

                    <View style={styles.actionContainer}>
                        {renderIconAction(Images.wallet.ic_copy, t('common.copy'))}
                        {renderIconAction(Images.wallet.ic_share, t('common.share'))}
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </>
    );
};

export default DepositModal;

const styles = StyleSheet.create({
    modalContainer: {
        backgroundColor: Colors.white,
        width: '100%',
        paddingHorizontal: verticalScale(30),
        paddingTop: verticalScale(10),
        paddingBottom: verticalScale(30),
        borderTopLeftRadius: verticalScale(20),
        borderTopRightRadius: verticalScale(20),
        alignItems: 'center',
    },
    tipHeader: {
        width: verticalScale(70),
        height: verticalScale(5),
        marginBottom: verticalScale(20),
        backgroundColor: Colors.grayHiring,
        alignItems: 'center',
        borderRadius: verticalScale(20),
        justifyContent: 'center',
    },
    txtTitleModal: {
        fontSize: moderateScale(22),
        color: Colors.blueDark,
        marginBottom: verticalScale(20),
    },
    txtValueModal: {
        fontSize: moderateScale(30),
        marginBottom: verticalScale(10),
        lineHeight: moderateScale(36),
    },
    txtTextRow: {
        color: Colors.hint,
    },
    txtRow: {
        color: Colors.blueDark,
    },
    row: {
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: verticalScale(10),
        flexDirection: 'row',
    },
    txtAction: {
        fontSize: moderateScale(12),
        color: Colors.hint,
        textTransform: 'uppercase',
        marginTop: verticalScale(10),
        marginStart: verticalScale(10),
    },
    icAction: {
        width: verticalScale(60),
        height: verticalScale(60),
        borderRadius: verticalScale(30),
    },
    btnAction: {
        alignItems: 'center',
        flexDirection: 'column',
    },
    actionContainer: {
        marginTop: verticalScale(25),
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignSelf: 'stretch',
        marginBottom: verticalScale(100),
    },
});
