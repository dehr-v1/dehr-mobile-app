import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Image, ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import RNPickerSelect from 'react-native-picker-select';

import { UIHeader } from '../../../components/common';
import { Colors } from '../../../theme/Variables';
import { UIButton, UIText } from '../../../components/elements';
import { moderateScale, verticalScale } from '../../../utils';
import { fontFamily } from '../../../theme/Fonts';
import InputEdit from '../../../components/InputEdit';
import { useTheme } from '@/theme';
import { actionSearchConfig } from '../../Home/duck/actions';
import { connect } from 'react-redux';
import { SCREEN } from '../../../constant';
import { actionGetCompanies, actionPostJob } from '../duck/actions';
import moment from 'moment';

const PostJobPage = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const route = useRoute();
    const { params } = route || {};
    const { onUpdateListPostedJob } = params || {};
    const { Images } = useTheme();
    const { actionSearchConfig, actionGetCompanies, actionPostJob } = props || {};
    const [listConfig, setListConfig] = useState({});

    const [disabled, setDisabled] = useState(true);
    const [titleJob, setTitleJob] = useState('');
    const [isValidTitleJob, checkTitleJob] = useState(false);

    const [companyName, setCompanyName] = useState('');
    const [isValidCompanyName, checkCompanyName] = useState(false);
    const [listCompanies, setListCompanies] = useState([]);

    const [jobDescription, setJobDescription] = useState('');
    const [isValidJobDescription, checkJobDescription] = useState(false);

    const [jobRequire, setJobRequire] = useState('');
    const [isValidJobRequire, checkJobRequire] = useState(false);

    const [otherBenefits, setOtherBenefits] = useState('');
    const [isValidOtherBenefits, checkOtherBenefits] = useState(false);

    const [jobType, setJobType] = useState('');
    const [listJobType, setListJobType] = useState([]);

    const [experience, setExperience] = useState('');
    const [listExperience, setListExperience] = useState([]);

    const [location, setLocation] = useState('');
    const [listLocation, setListLocation] = useState([]);

    const [salary, setSalary] = useState('');
    const [listSalary, setListSalary] = useState([]);

    const validatorInputTitleJob = (text, isValid) => {
        setTitleJob(text);
        checkTitleJob(isValid);
        setDisabled(!(isValidTitleJob && isValid));
    };

    const validatorInputJobDescription = (text, isValid) => {
        setJobDescription(text);
        checkJobDescription(isValid);
        setDisabled(!(isValidJobDescription && isValid));
    };

    const validatorInputJobRequie = (text, isValid) => {
        setJobRequire(text);
        checkJobRequire(isValid);
        setDisabled(!(isValidJobRequire && isValid));
    };

    const validatorInputOtherBenefits = (text, isValid) => {
        setOtherBenefits(text);
        checkOtherBenefits(isValid);
        setDisabled(!(isValidOtherBenefits && isValid));
    };

    const onSubmit = () => {
        const data = {
            name: titleJob,
            company: companyName,
            desc: jobDescription,
            // jobRequire,
            location: location,
            levelRequired: experience,
            salaryRange: salary,
            type: jobType,
            expiredAt: moment().add(7, 'days').format(),
            // otherBenefits,
        };

        actionPostJob({ data }, (result) => {
            onUpdateListPostedJob(result);
            navigation.goBack();
        });
    };

    const initConfig = (config) => {
        //Level
        setExperience(config.levelRequired.default);
        const experienceNew = [];
        config.levelRequired.values.map((item) => {
            experienceNew.push({ label: item, value: item });
        });
        setListExperience(experienceNew);
        //Type
        setJobType(config.type.default);
        const jobTypeNew = [];
        config.type.values.map((item) => {
            jobTypeNew.push({ label: item, value: item });
        });
        setListJobType(jobTypeNew);
        //Salary
        setSalary(config.salaryRange.default);
        const salaryRangeNew = [];
        config.salaryRange.values.map((item) => {
            let label = '';
            let fromSalary = item.slice(0, item.indexOf(','));
            let toSalary = item.slice(item.indexOf(',') + 1);
            if (parseFloat(fromSalary) < 0) {
                label = `0$ - ${parseFloat(toSalary).toLocaleString()}$`;
            } else if (parseFloat(toSalary) < 0) {
                label = `More ${parseFloat(fromSalary).toLocaleString()}$`;
            } else {
                label = `${parseFloat(fromSalary).toLocaleString()}$ - ${parseFloat(toSalary).toLocaleString()}$`;
            }
            salaryRangeNew.push({ label, value: item });
        });
        setListSalary(salaryRangeNew);
        //Location
        setLocation(config.location.default.code);
        setListLocation(config.location.values);
    };

    const onPressLocation = () => {
        navigation.navigate(SCREEN.SEARCH_SCREEN, {
            title: t('search.advance_location'),
            data: listLocation,
            placeholder: t('search.advance_location_search'),
            onChoose: (item) => {
                setLocation(item.name);
            },
        });
    };

    useEffect(() => {
        actionSearchConfig({}, (listConfig) => {
            setListConfig(listConfig);
            initConfig(listConfig);
        });
        actionGetCompanies({}, (cpn) => {
            const companyArr = [];
            cpn.data.map((item) => {
                companyArr.push({ label: item.name, value: item._id });
            });
            setListCompanies(companyArr);
        });
    }, []);

    return (
        <View style={styles.container}>
            <UIHeader LeftIcon="close" onPress={() => navigation.goBack()} title={t('jobs.post_your_jobs')} />
            <ScrollView contentContainerStyle={{ paddingHorizontal: 30, paddingVertical: 20 }}>
                <View style={styles.inputView}>
                    <UIText font="bold" style={styles.textEdit} text={t('jobs.title_job')} />
                    <InputEdit
                        style={styles.inputText}
                        placeholder={t('jobs.title_job')}
                        placeholderTextColor={Colors.placeholder}
                        pattern={[/.{1,}/]}
                        onChangeText={(text, isValids) => {
                            const isValid = isValids?.every((valid) => valid);
                            validatorInputTitleJob(text, isValid);
                        }}
                    />
                    {!isValidTitleJob && titleJob.length > 0 && <UIText style={styles.textError} text={t('jobs.title_job')} />}
                </View>
                <View style={styles.inputView}>
                    <UIText font="bold" style={styles.textEdit} text={t('jobs.company_name')} />
                    <View style={styles.pickerContainer}>
                        <RNPickerSelect
                            textInputProps={styles.picker}
                            onValueChange={(value) => setCompanyName(value)}
                            style={{ ...pickerSelectStyles }}
                            useNativeAndroidPickerStyle={false}
                            value={companyName}
                            items={listCompanies}
                        />
                        <View style={styles.icDownContainer}>
                            <Image style={styles.icDown} source={Images.profile.ic_down} />
                        </View>
                    </View>
                </View>
                <View style={styles.inputView}>
                    <UIText font="bold" style={styles.textEdit} text={t('jobs.location')} />
                    <TouchableOpacity onPress={onPressLocation}>
                        <View style={styles.pickerContainer}>
                            <UIText text={location} />
                            <View style={styles.icDownContainer}>
                                <Image style={styles.icDown} source={Images.profile.ic_down} />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.inputView}>
                    <UIText font="bold" style={styles.textEdit} text={t('jobs.job_type')} />
                    <View style={styles.pickerContainer}>
                        <RNPickerSelect
                            textInputProps={styles.picker}
                            onValueChange={(value) => setJobType(value)}
                            style={{ ...pickerSelectStyles }}
                            useNativeAndroidPickerStyle={false}
                            value={jobType}
                            items={listJobType}
                        />
                        <View style={styles.icDownContainer}>
                            <Image style={styles.icDown} source={Images.profile.ic_down} />
                        </View>
                    </View>
                </View>
                <View style={styles.inputView}>
                    <UIText font="bold" style={styles.textEdit} text={t('jobs.experience_level')} />
                    <View style={styles.pickerContainer}>
                        <RNPickerSelect
                            textInputProps={styles.picker}
                            onValueChange={(value) => setExperience(value)}
                            style={{ ...pickerSelectStyles }}
                            useNativeAndroidPickerStyle={false}
                            value={experience}
                            items={listExperience}
                        />
                        <View style={styles.icDownContainer}>
                            <Image style={styles.icDown} source={Images.profile.ic_down} />
                        </View>
                    </View>
                </View>
                <View style={styles.inputView}>
                    <UIText font="bold" style={styles.textEdit} text={t('jobs.salary')} />
                    <View style={styles.pickerContainer}>
                        <RNPickerSelect
                            textInputProps={styles.picker}
                            onValueChange={(value) => {
                                setSalary(value);
                            }}
                            style={{ ...pickerSelectStyles }}
                            useNativeAndroidPickerStyle={false}
                            value={salary}
                            items={listSalary}
                        />
                        <View style={styles.icDownContainer}>
                            <Image style={styles.icDown} source={Images.profile.ic_down} />
                        </View>
                    </View>
                </View>
                <View style={styles.inputView}>
                    <UIText font="bold" style={styles.textEdit} text={t('jobs.job_description')} />
                    <InputEdit
                        style={[styles.inputText, { height: verticalScale(182), paddingVertical: verticalScale(14) }]}
                        placeholder={t('jobs.job_description')}
                        placeholderTextColor={Colors.placeholder}
                        pattern={[/.{1,}/]}
                        numberOfLines={4}
                        multiline
                        onChangeText={(text, isValids) => {
                            const isValid = isValids?.every((valid) => valid);
                            validatorInputJobDescription(text, isValid);
                        }}
                    />
                    {!isValidJobDescription && jobDescription.length > 0 && (
                        <UIText style={styles.textError} text={t('jobs.job_description')} />
                    )}
                </View>
                <View style={styles.inputView}>
                    <UIText font="bold" style={styles.textEdit} text={t('jobs.job_requirement')} />
                    <InputEdit
                        style={[styles.inputText, { height: verticalScale(182), paddingVertical: verticalScale(14) }]}
                        placeholder={t('jobs.job_requirement')}
                        placeholderTextColor={Colors.placeholder}
                        pattern={[/.{1,}/]}
                        numberOfLines={4}
                        multiline
                        onChangeText={(text, isValids) => {
                            const isValid = isValids?.every((valid) => valid);
                            validatorInputJobRequie(text, isValid);
                        }}
                    />
                    {!isValidJobRequire && jobRequire.length > 0 && <UIText style={styles.textError} text={t('jobs.job_requirement')} />}
                </View>
                <View style={styles.inputView}>
                    <UIText font="bold" style={styles.textEdit} text={t('jobs.other_benefits')} />
                    <InputEdit
                        style={[styles.inputText, { height: verticalScale(182), paddingVertical: verticalScale(14) }]}
                        placeholder={t('jobs.other_benefits')}
                        placeholderTextColor={Colors.placeholder}
                        pattern={[/.{1,}/]}
                        numberOfLines={4}
                        multiline
                        onChangeText={(text, isValids) => {
                            const isValid = isValids?.every((valid) => valid);
                            validatorInputOtherBenefits(text, isValid);
                        }}
                    />
                    {!isValidOtherBenefits && otherBenefits.length > 0 && (
                        <UIText style={styles.textError} text={t('jobs.other_benefits')} />
                    )}
                </View>
                <View style={styles.btnRow}>
                    <View style={styles.btnColumn}>
                        <UIButton type="BgWhite" onPress={() => navigation.goBack()} text={t('jobs.cancel')} />
                    </View>
                    <View style={styles.btnColumn}>
                        <UIButton onPress={onSubmit} disable={disabled} text={t('jobs.post_now')} />
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

const mapStateToProps = () => ({});

export default connect(mapStateToProps, {
    actionSearchConfig,
    actionGetCompanies,
    actionPostJob,
})(PostJobPage);

const styles = StyleSheet.create({
    btnRow: { flexDirection: 'row', marginHorizontal: verticalScale(-7) },
    btnColumn: { flexGrow: 1, flexBasis: 0, paddingHorizontal: verticalScale(7) },
    container: {
        backgroundColor: Colors.white,
        flex: 1,
    },
    inputView: {
        width: '100%',
        marginBottom: verticalScale(20),
        justifyContent: 'center',
        flexDirection: 'column',
    },
    textEdit: {
        fontSize: moderateScale(14),
        color: Colors.title,
        marginBottom: verticalScale(5),
    },
    inputText: {
        backgroundColor: Colors.gray,
        width: '100%',
        borderRadius: 8,
        height: verticalScale(50),
        fontSize: moderateScale(16),
        paddingStart: verticalScale(20),
        paddingEnd: verticalScale(20),
        fontFamily: fontFamily.regular,
        color: Colors.blueDark,
        borderColor: Colors.border,
        borderWidth: 1,
    },
    textError: {
        fontSize: moderateScale(9),
        color: 'red',
    },
    pickerContainer: {
        height: verticalScale(50),
        width: '100%',
        backgroundColor: Colors.gray,
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(6),
        justifyContent: 'center',
        paddingHorizontal: verticalScale(15),
    },
    icDownContainer: {
        position: 'absolute',
        right: verticalScale(15),
    },
    picker: {
        // backgroundColor:'red',
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
    },
    icDown: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        color: Colors.blueDark,
        width: '100%',
        height: verticalScale(50),
        // paddingTop: verticalScale(10),
        lineHeight: verticalScale(20),
    },
    inputAndroid: {
        color: Colors.blueDark,
        width: '100%',
        height: verticalScale(50),
        paddingTop: verticalScale(10),
        lineHeight: verticalScale(20),
    },
});
