import React, { useEffect, useState } from 'react';
import { Dimensions, Image, ImageBackground, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useTranslation } from 'react-i18next';
import _get from 'lodash/get';
import moment from 'moment';

import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../utils';
import { useTheme } from '@/theme';
import { UIText } from '../../../components/elements';
import { connect } from 'react-redux';
import { actionGetAccountInfo } from '../../Profile/duck/actions';
import { color } from 'react-native-reanimated';

const BulletListItem = (props) => {
    const { children } = props;
    return (
        <View style={{ flexDirection: 'row', marginBottom: verticalScale(5) }}>
            <View style={{ flexBasis: verticalScale(14), paddingTop: verticalScale(6) }}>
                <View
                    style={{
                        width: verticalScale(6),
                        height: verticalScale(6),
                        backgroundColor: Colors.lightGray2,
                        borderRadius: verticalScale(6),
                    }}
                />
            </View>
            <UIText style={{ flexGrow: 1, lineHeight: verticalScale(18), fontSize: moderateScale(14) }} text={children} />
        </View>
    );
};

const { height } = Dimensions.get('window');

const JobDetails = (props) => {
    const { Images } = useTheme();
    const { t } = useTranslation();
    const { job, onApply, actionGetAccountInfo, authReducer, type } = props || {};
    const { user } = authReducer || {};
    const date = _get(job, 'job.createdAt');
    const companyName = _get(job, 'job.name');
    const location = _get(job, 'job.location');
    const description = _get(job, 'job.desc');
    const levelRequired = _get(job, 'job.levelRequired');
    const salaryRange = _get(job, 'job.salaryRange');
    const requirement = _get(job, 'job.requirement');
    const otherBenefits = _get(job, 'job.notes');
    const isApplied = _get(job, 'isApplied');
    const numberOfApplied = _get(job, 'job.numberOfApplied') || 0;
    const numberOfViewer = _get(job, 'job.numberOfViewer') || 0;
    const [role, setRole] = useState(user.role ? user.role : 'member');

    return (
        <>
            <View style={{ flex: 1 }} />
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.wrapper}>
                        <View style={styles.companyContainer}>
                            <ImageBackground
                                style={styles.imageContainer}
                                imageStyle={styles.imageContainer}
                                source={Images.profile.ic_avatar}
                            />
                            <UIText
                                numberOfLines={1}
                                ellipsizeMode="tail"
                                style={{ color: Colors.blueDark }}
                                font="bold"
                                text={companyName}
                            />
                        </View>
                        <View style={styles.positionContainer}>
                            <View style={{ flexGrow: 1 }}>
                                <View style={styles.rowText}>
                                    <Image source={Images.profile.ic_position_2} style={styles.icPosition} />
                                    <UIText style={styles.txtLocation} text={companyName} />
                                </View>
                                <View style={styles.rowText}>
                                    <Image source={Images.home.ic_building} style={styles.icPosition} />
                                    <UIText style={styles.txtLocation} text={location} />
                                </View>
                                <UIText
                                    numberOfLines={3}
                                    ellipsizeMode="tail"
                                    style={{ fontSize: moderateScale(14) }}
                                    text={`${levelRequired || ''}  •  ${_get(salaryRange, 'from', '')}$ - ${_get(
                                        salaryRange,
                                        'to',
                                        ''
                                    )}$/month`}
                                />
                            </View>
                            <View style={styles.typeContainer}>
                                <Image style={styles.icClock} source={Images.home.ic_clock} />
                                <UIText style={styles.txtType} text={t('home.full_time')} />
                            </View>
                        </View>
                        <View style={{ flexGrow: 1, flexDirection: 'row' }}>
                            {date && (
                                <>
                                    <UIText
                                        style={{ color: Colors.lightGray, fontSize: moderateScale(12) }}
                                        text={moment(date).fromNow()}
                                    />
                                    <UIText
                                        style={{ marginHorizontal: verticalScale(5), fontSize: moderateScale(12), color: Colors.lightGray }}
                                        text="•"
                                    />
                                </>
                            )}
                            <UIText style={{ color: Colors.blueLight2, fontSize: moderateScale(12) }} text={`0 ${t('jobs.applicants')}`} />
                            {role === 'pro' && type == 1 && (
                                <>
                                    <UIText
                                        style={{ marginHorizontal: verticalScale(5), fontSize: moderateScale(12), color: Colors.lightGray }}
                                        text="•"
                                    />
                                    <UIText
                                        font="medium"
                                        style={{ color: Colors.blueLight2, fontSize: moderateScale(12) }}
                                        text={`${numberOfViewer} ${t('jobs.viewers')}`}
                                    />
                                </>
                            )}
                            {role === 'pro' && type == 1 && (
                                <>
                                    <UIText
                                        style={{ marginHorizontal: verticalScale(5), fontSize: moderateScale(12), color: Colors.lightGray }}
                                        text="•"
                                    />
                                    <UIText
                                        font="medium"
                                        style={{ color: Colors.blueLight2, fontSize: moderateScale(12) }}
                                        text={`${numberOfApplied} ${t('jobs.applied')}`}
                                    />
                                </>
                            )}
                        </View>
                        {role === 'pro' && type == 1 && (
                            <View style={{ flexDirection: 'row', marginHorizontal: verticalScale(-8), marginTop: verticalScale(20) }}>
                                <View style={{ flexBasis: 0, flexGrow: 1, paddingHorizontal: verticalScale(8) }}>
                                    <TouchableOpacity style={styles.applyButton}>
                                        <UIText style={styles.buttonText} font="bold" text={t('jobs.edit_this_job')} />
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexBasis: 0, flexGrow: 1, paddingHorizontal: verticalScale(8) }}>
                                    <TouchableOpacity style={[styles.applyButton, { backgroundColor: Colors.lightYellow }]}>
                                        <UIText style={styles.buttonText} font="bold" text={t('jobs.show')} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        )}
                        {description && (
                            <View style={{ marginTop: verticalScale(10) }}>
                                <UIText font="bold" text={t('jobs.job_description')} />
                                <BulletListItem>{description}</BulletListItem>
                            </View>
                        )}
                        {requirement && (
                            <View style={{ marginTop: verticalScale(10) }}>
                                <UIText font="bold" text={t('jobs.requirement')} />
                                <BulletListItem>{requirement}</BulletListItem>
                            </View>
                        )}
                        {otherBenefits && (
                            <View style={{ marginTop: verticalScale(10) }}>
                                <UIText font="bold" text={t('jobs.other_benefits')} />
                                <BulletListItem>{otherBenefits}</BulletListItem>
                            </View>
                        )}
                    </View>
                    {role === 'pro' && type == 1 ? (
                        <View>
                            <View style={styles.applied}>
                                <UIText
                                    font="bold"
                                    style={{ fontSize: moderateScale(14), textTransform: 'capitalize' }}
                                    text={`${t('jobs.applied')} (23)`}
                                ></UIText>
                                <TouchableOpacity>
                                    <UIText style={styles.appliedSeeAll} text={t('jobs.see_all')} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.appliedMember}>
                                <ImageBackground
                                    style={styles.imageContainer}
                                    imageStyle={styles.imageContainer}
                                    source={Images.profile.ic_avatar}
                                />
                                <View style={{ paddingLeft: verticalScale(5) }}>
                                    <UIText style={{ fontSize: moderateScale(14) }} font="bold">
                                        Theresa Webb
                                    </UIText>
                                    <UIText style={{ fontSize: moderateScale(14) }}>UI/UX designer - Junior</UIText>
                                </View>
                            </View>
                            <View style={styles.appliedMember}>
                                <ImageBackground
                                    style={styles.imageContainer}
                                    imageStyle={styles.imageContainer}
                                    source={Images.profile.ic_avatar}
                                />
                                <View style={{ paddingLeft: verticalScale(5) }}>
                                    <UIText style={{ fontSize: moderateScale(14) }} font="bold">
                                        Theresa Webb
                                    </UIText>
                                    <UIText style={{ fontSize: moderateScale(14) }}>UI/UX designer - Junior</UIText>
                                </View>
                            </View>
                            <View style={styles.appliedMember}>
                                <ImageBackground
                                    style={styles.imageContainer}
                                    imageStyle={styles.imageContainer}
                                    source={Images.profile.ic_avatar}
                                />
                                <View style={{ paddingLeft: verticalScale(5) }}>
                                    <UIText style={{ fontSize: moderateScale(14) }} font="bold">
                                        Theresa Webb
                                    </UIText>
                                    <UIText style={{ fontSize: moderateScale(14) }}>UI/UX designer - Junior</UIText>
                                </View>
                            </View>
                        </View>
                    ) : (
                        <View style={styles.wrapper}>
                            <View style={styles.divideLine} />
                            {isApplied || type == 2 ? (
                                <View style={styles.appliedButton}>
                                    <UIText
                                        style={[
                                            styles.buttonText,
                                            {
                                                color: Colors.lightGray4,
                                            },
                                        ]}
                                        font="bold"
                                        text={t('jobs.applied')}
                                    />
                                </View>
                            ) : (
                                <TouchableOpacity onPress={onApply} style={styles.applyButton}>
                                    <UIText style={styles.buttonText} font="bold" text={t('jobs.apply_now')} />
                                </TouchableOpacity>
                            )}
                            <UIText style={styles.hint} text="Your mached score is low, improve skill to apply again" />
                        </View>
                    )}
                </ScrollView>
                <SafeAreaView />
            </View>
        </>
    );
};

const mapStateToProps = (state) => ({
    walletReducer: state.walletReducer,
    authReducer: state.authReducer,
    profileReducer: state.profileReducer,
    homeReducer: state.homeReducer,
});
export default connect(mapStateToProps, {
    actionGetAccountInfo,
})(JobDetails);

const styles = StyleSheet.create({
    rowText: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    wrapper: {
        paddingHorizontal: verticalScale(30),
    },
    container: {
        width: '100%',
        backgroundColor: Colors.white,
        borderTopLeftRadius: verticalScale(10),
        borderTopRightRadius: verticalScale(10),
        paddingTop: verticalScale(25),
        maxHeight: height - verticalScale(50),
    },
    companyContainer: {
        flexDirection: 'row',
        paddingRight: verticalScale(44),
        alignItems: 'center',
        marginBottom: 8,
    },
    imageContainer: {
        width: verticalScale(44),
        height: verticalScale(44),
        borderRadius: verticalScale(50),
        marginRight: verticalScale(10),
    },
    positionContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    txtLocation: {
        flex: 0.75,
        marginLeft: verticalScale(5),
        fontSize: moderateScale(14),
        color: Colors.title,
        flexGrow: 1,
    },
    hint: {
        fontSize: moderateScale(14),
        lineHeight: verticalScale(18),
        color: Colors.blueLight2,
        textAlign: 'center',
    },
    icPosition: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    typeContainer: {
        flexBasis: verticalScale(60),
        alignItems: 'center',
        justifyContent: 'center',
    },
    icClock: {
        width: verticalScale(32),
        height: verticalScale(32),
        marginBottom: verticalScale(-8),
    },
    txtType: {
        fontSize: moderateScale(12),
    },
    appliedButton: {
        backgroundColor: Colors.grayHiring,
        paddingHorizontal: verticalScale(20),
        paddingVertical: verticalScale(12),
        borderRadius: verticalScale(3),
        marginBottom: verticalScale(15),
    },
    applyButton: {
        backgroundColor: Colors.lightGreen,
        paddingHorizontal: verticalScale(20),
        paddingVertical: verticalScale(12),
        borderRadius: verticalScale(3),
        marginBottom: verticalScale(15),
    },
    buttonText: {
        color: Colors.white,
        fontSize: moderateScale(18),
        lineHeight: verticalScale(22),
        textAlign: 'center',
    },
    divideLine: {
        height: verticalScale(1),
        width: '100%',
        backgroundColor: Colors.border,
        marginVertical: verticalScale(13),
    },
    applied: {
        paddingHorizontal: 16,
        paddingVertical: 9,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#f6f8fa',
    },
    appliedSeeAll: { fontSize: 12, color: '#929ca4' },
    appliedMember: {
        borderBottomWidth: 1,
        borderBottomColor: '#f6f8fa',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
});
