import { JobsAction } from './jobs.create.action';

export const actionGetRecommendJob = (payload, callback) => ({
    type: JobsAction.GET_RECOMMEND_JOB.PENDING,
    payload,
    callback,
});

export const actionGetUserAppliedJob = (payload, callback) => ({
    type: JobsAction.GET_USER_APPLIED_JOB.PENDING,
    payload,
    callback,
});

export const actionGetUserPostedJob = (payload, callback) => ({
    type: JobsAction.GET_USER_POSTED_JOB.PENDING,
    payload,
    callback,
});

export const actionPostJob = (payload, callback) => ({
    type: JobsAction.POST_JOB.PENDING,
    payload,
    callback,
});
