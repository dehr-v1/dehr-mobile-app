import { createActionSet } from '../../../../utils';

export const CompaniesAction = {
    GET_COMPANIES: createActionSet('GET_COMPANIES'),
};
