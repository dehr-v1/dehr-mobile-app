import { createActionSet } from '../../../../utils';

export const JobsAction = {
    GET_RECOMMEND_JOB: createActionSet('GET_RECOMMEND_JOB'),
    GET_USER_APPLIED_JOB: createActionSet('GET_USER_APPLIED_JOB'),
    GET_USER_POSTED_JOB: createActionSet('GET_USER_POSTED_JOB'),
    POST_JOB: createActionSet('POST_JOB'),
};
