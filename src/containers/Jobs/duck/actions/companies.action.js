import { CompaniesAction } from './companies.create.action';

export const actionGetCompanies = (payload, callback) => ({
    type: CompaniesAction.GET_COMPANIES.PENDING,
    payload,
    callback,
});
