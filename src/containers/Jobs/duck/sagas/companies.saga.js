import { put, call, takeLatest } from 'redux-saga/effects';
import Toast from '../../../../components/elements/Toast';
import { CompaniesAction } from '../actions';
import { getCompanies } from '../../../../services/remote/company.service';

function* onGetCompanies(action) {
    const { payload, callback } = action;
    let { data } = payload;

    try {
        const company = yield call(() => getCompanies(data));
        yield put({
            type: CompaniesAction.GET_COMPANIES.SUCCESS,
            payload: company.data,
        });
        callback && callback(company);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: CompaniesAction.GET_COMPANIES.ERROR,
            payload: error,
        });
    }
}

export function* watchOnGetCompanies() {
    yield takeLatest(CompaniesAction.GET_COMPANIES.PENDING, onGetCompanies);
}
