import { put, call, takeLatest } from 'redux-saga/effects';
import Toast from '../../../../components/elements/Toast';
import { JobsAction } from '../actions';
import { getJobConfigurations, getRecommendJobs, getUserAppliedJobs, getUserPostedJobs, postJob } from '../../../../services/remote/job.service';

function* onGetListRecommendJob(action) {
    const { payload, callback } = action;
    let { data } = payload;

    try {
        const job = yield call(() => getRecommendJobs(data));
        yield put({
            type: JobsAction.GET_RECOMMEND_JOB.SUCCESS,
            payload: job.data,
        });
        callback && callback(job);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: JobsAction.GET_RECOMMEND_JOB.ERROR,
            payload: error,
        });
    }
}

export function* watchOnGetRecommendJob() {
    yield takeLatest(JobsAction.GET_RECOMMEND_JOB.PENDING, onGetListRecommendJob);
}

function* onGetListUserAppliedJob(action) {
    const { payload, callback } = action;
    let { data } = payload;

    try {
        const job = yield call(() => getUserAppliedJobs(data));
        yield put({
            type: JobsAction.GET_USER_APPLIED_JOB.SUCCESS,
            payload: job.data,
        });
        callback && callback(job);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: JobsAction.GET_USER_APPLIED_JOB.ERROR,
            payload: error,
        });
    }
}

export function* watchOnGetUserAppliedJob() {
    yield takeLatest(JobsAction.GET_USER_APPLIED_JOB.PENDING, onGetListUserAppliedJob);
}

function* onGetListUserPostedJob(action) {
    const { payload, callback } = action;
    let { data } = payload;

    try {
        const job = yield call(() => getUserPostedJobs(data));
        yield put({
            type: JobsAction.GET_USER_POSTED_JOB.SUCCESS,
            payload: job.data,
        });
        callback && callback(job);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: JobsAction.GET_USER_POSTED_JOB.ERROR,
            payload: error,
        });
    }
}

export function* watchOnGetUserPostedJob() {
    yield takeLatest(JobsAction.GET_USER_POSTED_JOB.PENDING, onGetListUserPostedJob);
}

function* onPostJob(action) {
    const { payload, callback } = action;
    let { data } = payload;
    try {
        const response = yield call(() => postJob(data));
        Toast('New Job has been posted');
        yield put({
            type: JobsAction.POST_JOB.SUCCESS,
        });
        callback && callback(response);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: JobsAction.POST_JOB.ERROR,
            payload: error,
        });
    }
}

export function* watchPostJob() {
    yield takeLatest(JobsAction.POST_JOB.PENDING, onPostJob);
}

