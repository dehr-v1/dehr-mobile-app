import { all } from 'redux-saga/effects';
import { watchOnGetCompanies } from './companies.saga';
import {
    watchOnGetRecommendJob,
    watchOnGetUserAppliedJob,
    watchOnGetUserPostedJob,
    watchPostJob,
} from './jobs.saga';

export function* jobsSaga() {
    yield all([
        watchOnGetRecommendJob(),
        watchOnGetUserAppliedJob(),
        watchOnGetUserPostedJob(),
        watchPostJob()
    ]);
}
export function* companiesSaga() {
    yield all([
        watchOnGetCompanies(),
    ]);
}
