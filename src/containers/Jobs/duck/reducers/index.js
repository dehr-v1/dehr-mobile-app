import { CompaniesAction, JobsAction } from '../actions';

const jobs = {
    jobs: [],
    userAppliedJobs: [],
    userPostedJobs: [],
    isLoading: false,
};
const jobsReducer = (state = jobs, action) => {
    const { type, payload } = action;
    switch (type) {
        case JobsAction.GET_RECOMMEND_JOB.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case JobsAction.GET_RECOMMEND_JOB.SUCCESS:
            return {
                ...state,
                isLoading: false,
                jobs: payload,
            };
        case JobsAction.GET_RECOMMEND_JOB.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case JobsAction.GET_USER_APPLIED_JOB.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case JobsAction.GET_USER_APPLIED_JOB.SUCCESS:
            return {
                ...state,
                isLoading: false,
                userAppliedJobs: payload,
            };
        case JobsAction.GET_USER_APPLIED_JOB.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case JobsAction.GET_USER_POSTED_JOB.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case JobsAction.GET_USER_POSTED_JOB.SUCCESS:
            return {
                ...state,
                isLoading: false,
                userPostedJobs: payload,
            };
        case JobsAction.GET_USER_POSTED_JOB.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case JobsAction.POST_JOB.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case JobsAction.POST_JOB.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case JobsAction.POST_JOB.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
};

const companies = {
    companies: [],
    isLoading: false,
};

const companiesReducer = (state = companies, action) => {
    const { type, payload } = action;
    switch (type) {
        case CompaniesAction.GET_COMPANIES.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case CompaniesAction.GET_COMPANIES.SUCCESS:
            return {
                ...state,
                isLoading: false,
                companies: payload,
            };
        case CompaniesAction.GET_COMPANIES.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
};

export { jobsReducer, companiesReducer };
