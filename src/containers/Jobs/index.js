import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { ActivityIndicator, FlatList, Image, ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import _get from 'lodash/get';
import _find from 'lodash/find';
import _findIndex from 'lodash/findIndex';
import _set from 'lodash/set';
import _isEmpty from 'lodash/isEmpty';
import { BASE_IMAGE_URL, ROLE_INFO } from '../../constant/value.constant';
import { UIHeader, UIModal } from '../../components/common';
import { UIButtonChoose, UIText } from '../../components/elements';
import { SCREEN } from '../../constant';
import { moderateScale, verticalScale } from '../../utils';
import { useTheme } from '@/theme';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Colors } from '../../theme/Variables';
import { actionApplyJob } from './../Home/duck/actions';
import { actionGetRecommendJob, actionGetUserAppliedJob, actionGetUserPostedJob } from './duck/actions';
import { UIJobComponent } from '../Home/components';
import JobDetails from './components/JobDetails';
import Toast from '../../components/elements/Toast';
import CustomAlert from '../../components/Alert';
import { getCurrentUser, setCurrentUser } from '../../services/persistence/store.config';

const ListJobCards = (props) => {
    const { data = [], renderItem, onLoadMore, ListFooterComponent } = props;
    const [onEndReachedCalledDuringMomentum, setOnEndReachedCalledDuringMomentum] = useState(true);

    return (
        <FlatList
            contentContainerStyle={{ paddingHorizontal: 20, paddingBottom: 15 }}
            data={data}
            extraData={data}
            renderItem={renderItem}
            keyExtractor={(item, index) => index.toString()}
            onMomentumScrollBegin={() => setOnEndReachedCalledDuringMomentum(false)}
            onEndReached={() => {
                if (!onEndReachedCalledDuringMomentum) {
                    onLoadMore();
                    setOnEndReachedCalledDuringMomentum(true);
                }
            }}
            onEndReachedThreshold={0}
            ListFooterComponent={ListFooterComponent}
        />
    );
};

const IndexJobsContainer = (props) => {
    const { profileReducer, authReducer, actionApplyJob, actionGetRecommendJob, actionGetUserAppliedJob, actionGetUserPostedJob } =
        props || {};
    const { accountInfo } = profileReducer || {};
    const { user } = authReducer || {};
    const { Images } = useTheme();
    const route = useRoute();
    const navigation = useNavigation();
    const { t } = useTranslation();
    const limit = 5;
    let modalRef = useRef();

    const [filter, setFilter] = useState(0);
    const [isJobPoster, setIsJobPoster] = useState();
    const [job, setJob] = useState(null);
    const [isLoading, setIsLoading] = useState(false);

    // Phone Verify
    const [showPopupVerify, setShowPopupVerify] = useState(false);
    const [phoneVerify, setPhoneVerify] = useState('');

    // Recommend Jobs
    const [currentRecommendJobPage, setCurrentRecommendJobPage] = useState(1);
    const [listRecommendJob, setListRecommendJob] = useState([]);

    // User Applied Jobs
    const [currentUserAppliedJobPage, setCurrentUserAppliedJobPage] = useState(1);
    const [listUserAppliedJob, setListUserAppliedJob] = useState([]);

    // User Posted Jobs
    const [currentUserPostedJobPage, setCurrentUserPostedJobPage] = useState(1);
    const [listUserPostedJob, setListUserPostedJob] = useState({});

    const getRecommendJob = (page = currentRecommendJobPage) => {
        setIsLoading(true);
        const data = { page, limit };
        actionGetRecommendJob({ data }, (listItems) => {
            const result = {
                ...listItems,
                data: [..._get(listRecommendJob, 'data', []), ..._get(listItems, 'data', [])],
            };
            setCurrentRecommendJobPage(listItems.page);
            setListRecommendJob(result);
            setIsLoading(false);
        });
    };

    const getUserAppliedJob = (page = currentUserAppliedJobPage) => {
        setIsLoading(true);
        const data = { page, limit };
        actionGetUserAppliedJob({ data }, (listItems) => {
            const result = {
                ...listItems,
                data: [..._get(listUserAppliedJob, 'data', []), ..._get(listItems, 'data', [])],
            };
            setCurrentUserAppliedJobPage(listItems.page);
            setListUserAppliedJob(result);
            setIsLoading(false);
        });
    };

    const getUserPostedJob = (page = currentUserPostedJobPage) => {
        setIsLoading(true);
        const data = { page, limit };
        actionGetUserPostedJob({ data }, (listItems) => {
            const result = {
                ...listItems,
                data: [..._get(listUserPostedJob, 'data', []), ..._get(listItems, 'data', [])],
            };
            setCurrentUserPostedJobPage(listItems.page);
            setListUserPostedJob(result);
            setIsLoading(false);
        });
    };

    useEffect(() => {
        switch (filter) {
            case 1:
                if (_isEmpty(_get(listUserAppliedJob, 'data', [])) === true) {
                    getUserAppliedJob();
                }
                break;
            case 2:
                if (_isEmpty(_get(listUserPostedJob, 'data', [])) === true) {
                    getUserPostedJob();
                }
                break;
            default:
                if (_isEmpty(_get(listRecommendJob, 'data', [])) === true) {
                    getRecommendJob();
                }
                break;
        }
    }, [filter]);

    const _renderFooterPage = () => (
        <>
            {isLoading && (
                <View style={{ padding: verticalScale(20) }}>
                    <ActivityIndicator />
                </View>
            )}
            <View style={{ height: 60 }} />
        </>
    );

    const renderAvatar = () => {
        return (
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
                <Image
                    style={styles.avatar}
                    source={accountInfo.avatar ? { uri: BASE_IMAGE_URL + accountInfo.avatar } : Images.profile.ic_avatar}
                />
            </TouchableOpacity>
        );
    };

    const renderTitle = () => {
        return (
            <View>
                <UIText font="medium" style={styles.txtWallet} text={`${t('jobs.welcome_home')},`} />
                <UIText
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    font="bold"
                    style={styles.txtUsername}
                    text={user.username || 'Anthony Pham'}
                />
            </View>
        );
    };

    const renderNotiIcon = () => {
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <TouchableOpacity onPress={() => navigation.navigate(SCREEN.PROFILE_VIEW_PAGE)}>
                    <View style={styles.redDot} />
                    <Image style={[styles.icNoti, { marginRight: verticalScale(6) }]} source={Images.home.ic_email_2} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate(SCREEN.NOTIFICATION)}>
                    <View style={[styles.redDot, { right: verticalScale(6) }]} />
                    <Image style={styles.icNoti} source={Images.wallet.ic_noti} />
                </TouchableOpacity>
            </View>
        );
    };

    const renderJob = ({ item, index }) => (
        <UIJobComponent
            onPress={() => onPressJob(item, 0)}
            type={_get(item, 'job.type')}
            location={_get(item, 'job.location')}
            position={_get(item, 'job.name')}
            companyName={_get(item, 'job.name')}
            logo={_get(item, 'job.imgLandscape')}
            index={index}
            date={_get(item, 'createdAt')}
            isApplied={_get(item, 'isApplied')}
            applicants={_get(item, 'job.numberOfApplied')}
            ableToApply
        />
    );

    const renderAppliedJob = ({ item, index }) => (
        <UIJobComponent
            onPress={() => onPressJob(item, 2)}
            type={_get(item, 'job.type')}
            location={_get(item, 'job.location')}
            position={_get(item, 'job.name')}
            companyName={_get(item, 'job.name')}
            logo={_get(item, 'job.imgLandscape')}
            index={index}
            date={_get(item, 'createdAt')}
            applicants={_get(item, 'job.numberOfApplied')}
            status={_get(item, 'status')}
        />
    );

    const renderPostedJob = ({ item, index }) => (
        <UIJobComponent
            onPress={() => onPressJob(item, 1)}
            type={_get(item, 'job.type')}
            location={_get(item, 'job.location')}
            position={_get(item, 'job.name')}
            companyName={_get(item, 'job.name')}
            logo={_get(item, 'job.imgLandscape')}
            index={index}
            date={_get(item, 'createdAt')}
            isApplied={_get(item, 'isApplied')}
            viewCount={_get(item, 'job.numberOfViewer', 0)}
            applicants={_get(item, 'job.numberOfApplied')}
            // for demo without data from api
            visibility={index % 2 === 0 ? 'hide' : 'show'}
        />
    );

    const _renderJobCategory = () => (
        <ScrollView
            horizontal
            contentContainerStyle={{ flexDirection: 'row', marginBottom: verticalScale(16), paddingHorizontal: verticalScale(16) }}
        >
            <UIButtonChoose onPress={() => setFilter(0)} isChoose={filter == 0} text={t('jobs.recommend')} />
            <UIButtonChoose onPress={() => setFilter(2)} isChoose={filter == 2} text={t('jobs.posted_jobs')} />
            <UIButtonChoose onPress={() => setFilter(1)} isChoose={filter == 1} text={t('jobs.applied_jobs')} />
        </ScrollView>
    );

    const onPressJob = (item, type) => {
        setIsJobPoster(type);
        setJob(item);
        modalRef.open();
    };

    const applyJob = async (modelJobRef) => {
        const userInfo = JSON.parse(await getCurrentUser());
        if (userInfo.phoneNumber) {
            actionApplyJob(
                {
                    data: {
                        job: _get(job, 'job._id', ''),
                        email: userInfo.email,
                        phoneNumber: userInfo.phoneNumber,
                    },
                },
                (result) => {
                    setListUserAppliedJob({
                        ...listUserAppliedJob,
                        data: [...listUserAppliedJob.data, job],
                    });
                    const jobIdx = _findIndex(listRecommendJob.data, { _id: job._id });
                    const arr = listRecommendJob.data;
                    arr[jobIdx] = { ...arr[jobIdx], isApplied: true };
                    setListRecommendJob({ ...listRecommendJob, data: arr });
                    modelJobRef.close();
                }
            );
        } else {
            modelJobRef.close();
            if (user.kycStatus === 'verified') {
                if (!user.phoneNumber || !user.email) {
                    setShowPopupVerify(true);
                }
            } else {
                Toast(t('wallet.kyc_verify'), 'error');
            }
        }
    };

    const onUpdateListPostedJob = (data) => {
        getUserAppliedJob(1);
    };

    const onPressPostJob = () => {
        if (accountInfo.role === ROLE_INFO.MEMBER) {
            Toast(t('wallet.role_pro'), 'error');
        } else {
            navigation.navigate(SCREEN.POST_JOB_PAGE, {
                onUpdateListPostedJob,
            });
        }
    };

    const inputData = async () => {
        let user = JSON.parse(await getCurrentUser());
        const newUser = {
            ...user,
            phoneNumber: phoneVerify,
        };
        setCurrentUser(JSON.stringify(newUser));
        setShowPopupVerify(false);
        applyJob();
    };

    const alertVerify = () => (
        <CustomAlert
            isEdittext={true}
            displayTitle={t('login.verify_account')}
            displayMsg={t('login.confirm_email')}
            displayName={(user && user.username) ?? ''}
            visibility={showPopupVerify}
            dismissAlert={setShowPopupVerify}
            resultPhone={setPhoneVerify}
            displayOk={t('login.verify_now')}
            onPressOK={inputData}
        />
    );

    return (
        <View style={styles.container}>
            {alertVerify()}
            <UIHeader
                centerStyle={styles.center}
                leftComponent={renderAvatar}
                centerComponent={renderTitle}
                rightComponent={renderNotiIcon}
            />
            <View>{_renderJobCategory()}</View>
            <View style={styles.contentWrapper}>
                {filter == 0 && (
                    <ListJobCards
                        data={listRecommendJob.data}
                        renderItem={renderJob}
                        onLoadMore={() =>
                            currentRecommendJobPage < listRecommendJob.totalPages && getRecommendJob(currentRecommendJobPage + 1)
                        }
                        ListFooterComponent={_renderFooterPage()}
                    />
                )}
                {filter == 1 && (
                    <ListJobCards
                        data={listUserAppliedJob.data}
                        renderItem={renderAppliedJob}
                        onLoadMore={() =>
                            currentUserAppliedJobPage < listUserAppliedJob.totalPages && getUserAppliedJob(currentUserAppliedJobPage + 1)
                        }
                        ListFooterComponent={_renderFooterPage()}
                    />
                )}
                {filter == 2 && (
                    <>
                        <ListJobCards
                            data={listUserPostedJob.data}
                            renderItem={renderPostedJob}
                            onLoadMore={() =>
                                currentUserPostedJobPage < listUserPostedJob.totalPages && getUserPostedJob(currentUserPostedJobPage + 1)
                            }
                            ListFooterComponent={_renderFooterPage()}
                        />
                    </>
                )}
            </View>
            <UIModal ref={(ref) => (modalRef = ref)}>
                <JobDetails type={isJobPoster} job={job} onApply={() => applyJob(modalRef)} />
            </UIModal>
            <View style={styles.postJobWrapper}>
                <TouchableOpacity style={styles.postJobButton} onPress={onPressPostJob}>
                    <Image source={Images.jobs.ic_pencil} />
                    <UIText style={styles.postJobText} font="bold">
                        Post Your Jobs
                    </UIText>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
    authReducer: state.authReducer,
});

export default connect(mapStateToProps, {
    actionApplyJob,
    actionGetRecommendJob,
    actionGetUserAppliedJob,
    actionGetUserPostedJob,
})(IndexJobsContainer);

const styles = StyleSheet.create({
    postJobWrapper: {
        position: 'absolute',
        bottom: verticalScale(18),
        width: '100%',
        alignItems: 'center',
    },
    postJobText: {
        fontSize: moderateScale(16),
        color: Colors.white,
        letterSpacing: verticalScale(-0.32),
        lineHeight: verticalScale(19),
        paddingLeft: verticalScale(6),
    },
    postJobButton: {
        paddingVertical: verticalScale(10),
        paddingHorizontal: verticalScale(35),
        backgroundColor: Colors.lightGreen,
        borderRadius: verticalScale(24),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: 'rgba(51, 0, 175, 0.51)',
        shadowOffset: {
            width: verticalScale(0),
            height: verticalScale(3),
        },
        shadowOpacity: verticalScale(0.2),
        shadowRadius: verticalScale(6),
        elevation: verticalScale(10),
    },
    contentWrapper: {
        flex: 1,
        // paddingHorizontal: verticalScale(16),
    },
    container: {
        backgroundColor: Colors.white,
        flex: 1,
    },
    center: {
        alignItems: 'flex-start',
        marginLeft: verticalScale(12),
    },
    txtWallet: {
        fontSize: moderateScale(12),
        color: Colors.title,
        lineHeight: verticalScale(18),
        textTransform: 'uppercase',
    },
    redDot: {
        width: verticalScale(10),
        height: verticalScale(10),
        backgroundColor: 'red',
        position: 'absolute',
        borderRadius: verticalScale(10),
        right: verticalScale(12),
        top: verticalScale(6),
        zIndex: 1,
    },
    txtUsername: {
        width: verticalScale(220),
        fontSize: moderateScale(24),
        color: Colors.title,
    },
    icNoti: {
        width: verticalScale(36),
        height: verticalScale(36),
    },
    avatar: {
        width: verticalScale(40),
        height: verticalScale(40),
        borderRadius: verticalScale(20),
    },
});
