import React, { useState, useEffect } from 'react';
import { View, StyleSheet, StatusBar, Image, FlatList, TouchableOpacity } from 'react-native';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import { UIHeader } from '../../components/common';
import { UITextInput } from '../../components/elements';
import { Colors } from '../../theme/Variables';
import { useNavigation } from '@react-navigation/native';
import { SCREEN } from '../../constant';
import { UISwiper } from './components';
import { BASE_IMAGE_URL } from '../../constant/value.constant';
import { verticalScale } from '../../utils/ScalingUtils';
import { getCurrentUser } from '../../services/persistence/store.config';
import { connect } from 'react-redux';
import { actionListConversation } from './duck/actions';

const IndexChatContainer = (props) => {
    const { Layout, Gutters, Images, Fonts } = useTheme();
    const { t } = useTranslation();
    const navigation = useNavigation();
    const [data, setRenderData] = useState([]);
    const [dataFull, setRenderDataFull] = useState([]);
    const [search, setSearch] = useState('');
    const [userId, setUserId] = useState('');
    const { chatReducer, actionListConversation } = props;
    const { isLoading } = chatReducer || false;

    useEffect(async () => {
        const user = (await getCurrentUser()) || null;
        setUserId(JSON.parse(user)._id);
        actionListConversation({}, (listUser) => {
            setRenderData(listUser);
            setRenderDataFull(listUser);
        });
    }, []);

    const onChangeSearch = (text) => {
        if (text == '') {
            setRenderData(dataFull);
        } else {
            setRenderData(dataFull.filter((e, i) => e.usersObj[1].username.toLocaleLowerCase().includes(text.toLocaleLowerCase())));
        }
        setSearch(text);
    };

    const onUpdate = (item, index) => {
        const newData = data.map((e, i) => {
            if (i != index) {
                return e;
            }
            return item;
        });
        setRenderData(newData);
    };

    const onDelete = (index) => {
        setRenderData(data.filter((e, i) => i != index));
    };

    const moveToChatBot = () => {
        navigation.navigate(SCREEN.MESSAGE, { name: null, avatar: null });
    };

    const moveToClick = (itemId, channelID, itemName, itemAvatar) => {
        navigation.navigate(SCREEN.MESSAGE, {
            id: itemId,
            channelId: channelID,
            name: itemName,
            avatar: itemAvatar ? BASE_IMAGE_URL + itemAvatar : '',
        });
    };

    const renderItem = ({ item, index }) => {
        const user = item.usersObj.filter((item) => item._id != userId)[0] ?? {};
        return (
            <UISwiper
                onDelete={() => onDelete(index)}
                onEdit={() => moveToEdit(index)}
                onclick={() => moveToClick(item._id, item._id, user.username, user.avatar_url)}
                avatar={user.avatar_url ? { uri: BASE_IMAGE_URL + user.avatar_url } : null}
                name={user.username}
                message={item.metalastchat}
                date={item.updated_at}
                status={item.is_active == 1}
                messageCount={0}
            />
        );
    };

    const renderChat = () => {
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <TouchableOpacity onPress={() => navigation.navigate(SCREEN.CONNECT_NETWORK)}>
                    <Image style={[styles.icIcon, { marginRight: verticalScale(6) }]} source={Images.chat.ic_connect} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate(SCREEN.NEW_MESSAGE)}>
                    <Image style={styles.icIcon} source={Images.header.ic_edit} />
                </TouchableOpacity>
            </View>
        );
    };

    return (
        <View style={[Layout.fill, Layout.column]}>
            <Image style={styles.headerBg} resizeMode="cover" source={Images.chat.bg_chat} />
            <UIHeader
                LeftIcon="chatbot"
                style={styles.header}
                onPress={moveToChatBot}
                rightComponent={renderChat}
                title={t('chat.mesage')}
                titleStyle={{ color: Colors.white }}
            />
            <View style={[Layout.colCenter, styles.searchParent]}>
                <UITextInput
                    placeholder={t('chat.search_conversation')}
                    onChangeText={(text) => onChangeSearch(text)}
                    underline
                    searchStyle={styles.search}
                    inputStyle={styles.editSearch}
                    value={search}
                />
                <Image style={styles.iconSearch} source={Images.home.ic_search} />
                <Image style={styles.iconSetting} source={Images.home.ic_edit} />
            </View>
            <View style={styles.contentContainer}>
                <FlatList renderItem={renderItem} extraData={data} data={data} />
            </View>
        </View>
    );
};

const mapStateToProps = (state) => ({
    chatReducer: state.chatReducer,
});
export default connect(mapStateToProps, {
    actionListConversation,
})(IndexChatContainer);

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.gray,
    },
    contentContainer: {
        flex: 1,
        backgroundColor: Colors.white,
        marginLeft: verticalScale(16),
        marginRight: verticalScale(16),
    },
    headerBg: {
        width: '100%',
        height: verticalScale(70),
    },
    header: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
    },
    icIcon: {
        width: verticalScale(36),
        height: verticalScale(36),
    },
    searchParent: {
        width: '100%',
        height: verticalScale(70),
        marginTop: verticalScale(10),
    },
    search: {
        width: '100%',
        height: verticalScale(50),
        paddingHorizontal: verticalScale(30),
    },
    editSearch: {
        backgroundColor: Colors.grayInput,
        paddingLeft: verticalScale(50),
    },
    iconSearch: {
        position: 'absolute',
        left: verticalScale(45),
    },
    iconSetting: {
        position: 'absolute',
        right: verticalScale(45),
    },
});
