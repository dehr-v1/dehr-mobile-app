import React from 'react';
import { View, StyleSheet, Animated, TouchableOpacity, Image } from 'react-native';
import { UIText } from '../../../components/elements';
import { UIAvatar } from './index';
import { Colors } from '../../../theme/Variables';
import { verticalScale } from '../../../utils/ScalingUtils';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import moment from 'moment';

const UISwiper = (props) => {
    const { avatar, name, message, date, status, messageCount, onEdit, onDelete, onclick } = props;
    const { Layout, Gutters, Colors, Images } = useTheme();
    const { t } = useTranslation();

    const leftSwipe = (progress, dragX) => {
        return (
            <TouchableOpacity onPress={onDelete} activeOpacity={0.6}>
                <View style={styles.deleteBox}>
                    <Image style={styles.icTrash} source={Images.profile.ic_trash} />
                    <UIText style={styles.txtDelete} text={t('profile.delete')} />
                </View>
            </TouchableOpacity>
        );
    };

    return (
        <Swipeable renderLeftActions={leftSwipe}>
            <TouchableOpacity onPress={onclick} activeOpacity={0.6}>
                <View style={[Layout.fill, Layout.row, Gutters.smallHPadding, styles.container]}>
                    <UIAvatar url={avatar} isOnline={status} />
                    <View style={[Layout.fill, Layout.colum, Gutters.smallHPadding]}>
                        <UIText numberOfLines={1} ellipsizeMode="tail" style={styles.txtName} font="bold" text={name} />
                        <UIText
                            numberOfLines={1}
                            ellipsizeMode="tail"
                            style={status == '0' ? styles.txtContent : styles.txtContentBlue}
                            font="regular"
                            text={message}
                        />
                    </View>
                    <View style={[Layout.column, Layout.alignItemsEnd]}>
                        <UIText style={styles.txtDate} font="regular" text={moment(moment.unix(date)).fromNow()} />
                        {status == '0' ? (
                            <Image style={styles.imgCheck} resizeMode="cover" source={Images.chat.ic_check} />
                        ) : (
                            messageCount != 0 && (
                                <View style={styles.bgMessageInbox}>
                                    <UIText style={styles.txtMessageInbox} font="regular" text={messageCount} />
                                </View>
                            )
                        )}
                    </View>
                </View>
            </TouchableOpacity>
        </Swipeable>
    );
};

export default UISwiper;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        height: verticalScale(100),
    },
    txtName: {
        fontSize: verticalScale(16),
        color: Colors.title,
    },
    txtContent: {
        fontSize: verticalScale(14),
        color: Colors.message,
    },
    txtContentBlue: {
        fontSize: verticalScale(14),
        color: Colors.blueLight,
    },
    txtDate: {
        fontSize: verticalScale(12),
        color: Colors.date,
    },
    bgMessageInbox: {
        width: verticalScale(20),
        height: verticalScale(20),
        borderRadius: verticalScale(20) / 2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.blueLight,
    },
    txtMessageInbox: {
        alignItems: 'flex-end',
        marginTop: verticalScale(-4),
        fontSize: verticalScale(10),
        color: Colors.white,
    },
    imgCheck: {
        width: verticalScale(30),
        height: verticalScale(30),
    },
    deleteBox: {
        width: verticalScale(80),
        height: verticalScale(100),
        backgroundColor: Colors.error,
        justifyContent: 'center',
        alignItems: 'center',
    },
    icTrash: {
        width: verticalScale(32),
        height: verticalScale(32),
    },
    txtDelete: {
        fontSize: verticalScale(12),
        color: Colors.white,
    },
    btnEdit: {
        position: 'absolute',
        right: verticalScale(16),
    },
});
