import React from 'react';
import { View, Image, ImageBackground, TouchableOpacity, StyleSheet } from 'react-native';
import { useTheme } from '@/theme';
import { verticalScale } from '../../../utils/ScalingUtils';
import { Colors } from '../../../theme/Variables';
import { BASE_IMAGE_URL } from '../../../constant/value.constant';

const Avatar = (props) => {
    const { Images } = useTheme();
    const { url, style, isOnline = false, onPressAvatar, onPressCamera } = props;

    return (
        <TouchableOpacity activeOpacity={onPressAvatar ? 0.7 : 1} onPress={onPressAvatar && onPressAvatar()}>
            <ImageBackground
                style={[styles.container, style]}
                imageStyle={[styles.container, styles.imageContainer]}
                source={url || Images.profile.ic_avatar}
            >
                {onPressCamera && (
                    <TouchableOpacity onPress={onPressCamera && onPressCamera} style={styles.cameraContainer}>
                        <Image style={styles.camera} source={Images.profile.ic_camera} />
                    </TouchableOpacity>
                )}

                {isOnline && (
                    <View style={styles.bgCircleShapeView}>
                        <View style={styles.circleShapeView} />
                    </View>
                )}
            </ImageBackground>
        </TouchableOpacity>
    );
};

export default Avatar;

const styles = StyleSheet.create({
    container: {
        width: verticalScale(54),
        height: verticalScale(54),
    },
    imageContainer: {
        borderRadius: verticalScale(45),
    },
    bgCircleShapeView: {
        width: verticalScale(12),
        height: verticalScale(12),
        borderRadius: verticalScale(12) / 2,
        backgroundColor: Colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: verticalScale(1),
        right: verticalScale(2),
    },
    circleShapeView: {
        width: verticalScale(8),
        height: verticalScale(8),
        borderRadius: verticalScale(8) / 2,
        backgroundColor: Colors.blueLight,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
