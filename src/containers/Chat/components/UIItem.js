import React from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { verticalScale } from '../../../utils/ScalingUtils';
import { useTheme } from '@/theme';

const UIItem = (props) => {
    const { name, description, onPress, startDate, endDate, title, location } = props;
    const { Images } = useTheme();
    let timeLocation = `${startDate} - ${endDate}`;
    if (location) {
        timeLocation += ` at ${location}`;
    }
    return (
        <View style={styles.container}>
            <UIText style={styles.txtName} font="bold" text={name} />
            {title && <UIText style={styles.txtTitle} text={title} />}
            {startDate && endDate && <UIText style={styles.txtTime} text={timeLocation} />}
            {description && <UIText style={styles.txtDesciption} text={description} />}
            <TouchableOpacity style={styles.icPencilContainer} onPress={onPress}>
                <Image source={Images.profile.ic_pencil} style={styles.icPencil} />
            </TouchableOpacity>
        </View>
    );
};

export default UIItem;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderTopColor: Colors.border,
        borderTopWidth: 1,
        paddingHorizontal: verticalScale(16),
        paddingVertical: verticalScale(15),
        justifyContent: 'center',
        backgroundColor: Colors.white,
    },
    txtName: {
        fontSize: verticalScale(16),
        color: Colors.blueDark,
    },
    txtDesciption: {
        fontSize: verticalScale(12),
        color: Colors.hint,
    },
    icPencil: {
        width: verticalScale(36),
        height: verticalScale(36),
    },
    icPencilContainer: {
        position: 'absolute',
        right: verticalScale(16),
    },
    txtTitle: {
        fontSize: verticalScale(14),
        color: Colors.hint,
    },
    txtTime: {
        fontSize: verticalScale(12),
        color: Colors.lightGray,
    },
});
