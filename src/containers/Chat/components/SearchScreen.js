import React, { useState } from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import { UIHeader } from '../../../components/common';
import { Colors } from '../../../theme/Variables';
import { useNavigation, useRoute } from '@react-navigation/native';
import { UIText, UITextInput } from '../../../components/elements';
import { verticalScale } from '../../../utils/ScalingUtils';

const SearchScreen = () => {
    const navigation = useNavigation();
    const { params } = useRoute();
    const { title, data, onChoose, placeholder } = params;
    const [renderData, setRenderData] = useState(data);
    const [search, setSearch] = useState('');

    const onChangeSearch = (text) => {
        if (text == '') {
            setRenderData(data);
        } else {
            setRenderData(renderData.filter((e, i) => e.name.toLocaleLowerCase().includes(text.toLocaleLowerCase())));
        }
        setSearch(text);
    };

    const onChooseItem = (item, index) => {
        onChoose && onChoose(item);
        navigation.goBack();
    };

    const renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity
                onPress={() => onChooseItem(item, index)}
                style={[styles.itemContainer, index == 0 && { paddingTop: verticalScale(5) }]}
            >
                <UIText font="medium" style={styles.txtItem} text={item.name} />
            </TouchableOpacity>
        );
    };

    return (
        <View style={styles.container}>
            <UIHeader LeftIcon="close" title={title} onPress={() => navigation.goBack()} />
            <UITextInput placeholder={placeholder} onChangeText={(text) => onChangeSearch(text)} value={search} />
            <FlatList data={renderData} extraData={renderData} renderItem={renderItem} />
        </View>
    );
};

export default SearchScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    itemContainer: {
        width: '100%',
        padding: verticalScale(15),
        borderBottomColor: Colors.border,
        borderBottomWidth: 1,
    },
    txtItem: {
        fontSize: verticalScale(14),
        color: Colors.blueDark,
    },
});
