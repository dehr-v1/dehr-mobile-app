import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, FlatList } from 'react-native';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { verticalScale } from '../../../../utils/ScalingUtils';
import { SCREEN } from '../../../../constant';
import { UIHeader } from '../../../../components/common';
import { UIText, UITextInput } from '../../../../components/elements';
import { UIAvatar } from '../../components';
import { BASE_IMAGE_URL } from '../../../../constant/value.constant';
import { useNavigation, useRoute } from '@react-navigation/native';
import { connect } from 'react-redux';
import { actionListUser, actionCreateConversation } from '../../duck/actions';

const IndexNewMessageContainer = (props) => {
    const { Layout, Gutters, Colors, Fonts } = useTheme();
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { chatReducer, actionListUser, actionCreateConversation } = props;
    const { isLoading } = chatReducer || false;
    const [search, setSearch] = useState('');
    const [data, setRenderData] = useState([]);
    const [dataFull, setRenderDataFull] = useState([]);
    const [isShowEmail, setShowEmail] = useState(false);

    useEffect(() => {
        actionListUser({}, (listUser) => {
            setRenderData(listUser);
            setRenderDataFull(listUser);
        });
    }, []);

    const onChangeSearch = (text) => {
        if (text == '') {
            setRenderData(dataFull);
        } else {
            setRenderData(dataFull.filter((e, i) => e.username.toLocaleLowerCase().includes(text.toLocaleLowerCase())));
        }
        setSearch(text);
    };

    const onClick = (item) => {
        const data = {
            userId: item._id,
        };
        actionCreateConversation({ data }, (channelID) => {
            navigation.navigate(SCREEN.MESSAGE, {
                id: item._id,
                channelId: channelID,
                name: item.username,
                avatar: item.avatar_url ? BASE_IMAGE_URL + item.avatar_url : '',
            });
        });
    };

    const renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => onClick(item)} activeOpacity={0.6}>
                <View style={[Layout.fill, Layout.row, Gutters.smallHPadding, styles.container]}>
                    <UIAvatar url={item.avatar_url ? { uri: BASE_IMAGE_URL + item.avatar_url } : null} isOnline />
                    <View style={[Layout.fill, Layout.colum, Gutters.smallHPadding]}>
                        <UIText numberOfLines={1} ellipsizeMode="tail" style={styles.txtName} font="bold" text={item.username} />
                        {isShowEmail && (
                            <UIText
                                style={item.is_active == '0' ? styles.txtContent : styles.txtContentBlue}
                                font="regular"
                                text={item.email}
                            />
                        )}
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    return (
        <View style={[Layout.fill]}>
            <UIHeader LeftIcon="close" title={t('chat.new_message')} onPress={() => navigation.goBack()} />
            <View style={styles.viewDivider} />
            <View style={[Layout.rowHCenter, styles.search, Gutters.largeHPadding]}>
                <UIText style={styles.txtName} font="bold" text={t('chat.to_search')} />
                <UITextInput
                    placeholder={t('chat.search_user')}
                    onChangeText={(text) => onChangeSearch(text)}
                    underline
                    searchStyle={styles.search}
                    inputStyle={styles.editSearch}
                    value={search}
                />
            </View>
            <View style={styles.viewDivider} />
            <FlatList renderItem={renderItem} extraData={data} data={data} />
        </View>
    );
};

const mapStateToProps = (state) => ({
    chatReducer: state.chatReducer,
});
export default connect(mapStateToProps, {
    actionListUser,
    actionCreateConversation,
})(IndexNewMessageContainer);

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        height: verticalScale(100),
    },
    viewDivider: {
        height: verticalScale(6),
        backgroundColor: Colors.gray,
    },
    txtName: {
        fontSize: verticalScale(16),
        color: Colors.title,
    },
    txtSearch: {
        fontSize: verticalScale(14),
        color: Colors.title,
    },
    txtContent: {
        fontSize: verticalScale(14),
        color: Colors.message,
    },
    txtContentBlue: {
        fontSize: verticalScale(14),
        color: Colors.blueLight,
    },
    search: {
        width: '100%',
        height: verticalScale(50),
    },
    editSearch: {
        backgroundColor: Colors.white,
        borderRadius: 0,
        borderWidth: 0,
    },
});
