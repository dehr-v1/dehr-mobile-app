import React, { useState, useCallback, useEffect } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { verticalScale } from '../../../../utils/ScalingUtils';
import { UIHeader } from '../../../../components/common';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Bubble, InputToolbar, Composer, Send, GiftedChat } from 'react-native-gifted-chat';
import { connect } from 'react-redux';
import { getCurrentUser } from '../../../../services/persistence/store.config';
import { actionListMessageChat, actionCreateMessageChat, actionCreateMessageChatBot } from '../../duck/actions';
import moment from 'moment';

const IndexMessageContainer = (props) => {
    const { Layout, Images, Colors, Fonts } = useTheme();
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { params } = useRoute();
    const { chatReducer, actionListMessageChat, actionCreateMessageChat, actionCreateMessageChatBot } = props;
    const { isLoading } = chatReducer || false;
    const { id, channelId, name, avatar } = params || {};
    const [listMessages, setListMessages] = useState([]);
    const [userID, setUserID] = useState(null);

    useEffect(async () => {
        const listMessageChat = [];
        setUserID(JSON.parse(await getCurrentUser())._id);
        if (channelId != null) {
            const data = {
                channelID: channelId,
            };
            actionListMessageChat({ data }, (listMessage) => {
                listMessage.map((item) => {
                    const itemNew = {
                        _id: item._id,
                        text: item.message,
                        createdAt: moment.unix(item.created_at),
                        user: {
                            _id: item.user,
                            name: name,
                            avatar: avatar,
                        },
                    };
                    listMessageChat.push(itemNew);
                });
                setListMessages(listMessageChat);
            });
        }
    }, []);

    const renderBubble = (props) => {
        if (!props.currentMessage.sent) {
            return (
                <Bubble
                    {...props}
                    wrapperStyle={{
                        left: {
                            backgroundColor: Colors.grayMessage,
                        },
                        right: {
                            backgroundColor: Colors.blueDark,
                        },
                    }}
                />
            );
        }
        return (
            <Bubble
                {...props}
                wrapperStyle={{
                    left: {
                        backgroundColor: Colors.grayMessage,
                    },
                    right: {
                        backgroundColor: Colors.blueDark,
                    },
                }}
            />
        );
    };

    const renderInputToolbar = (props) => {
        return <InputToolbar {...props} containerStyle={styles.inputText} />;
    };

    const renderSend = (props) => {
        return (
            <Send {...props} containerStyle={styles.bgCircleShapeView}>
                <Image style={styles.iconSend} source={Images.home.ic_search} />
            </Send>
        );
    };

    const onSend = (messagesText) => {
        if (id == null) {
            const data = {
                message: messagesText[0].text,
            };
            const messageChat = [];
            actionCreateMessageChatBot({ data }, (returnMessage) => {
                const newMessage = {
                    ...messagesText[0],
                    _id: Math.round(Math.random() * 1000000) + messagesText[0]._id,
                    user: {
                        _id: Math.round(Math.random() * 1000000) + messagesText[0].user._id,
                    },
                    text: returnMessage.text,
                };
                messageChat.push(newMessage);
                messageChat.push(messagesText[0]);
                setListMessages((previousMessages) => GiftedChat.append(previousMessages, messageChat));
            });
        } else {
            const data = {
                message: messagesText[0].text,
                channelID: channelId,
            };
            actionCreateMessageChat({ data }, () => {
                setListMessages((previousMessages) => GiftedChat.append(previousMessages, messagesText));
            });
        }
    };

    const clickAvatar = () => {
        navigation.navigate(SCREEN.SEARCH_SCREEN, {
            title: t('profile.edit_industrial_knowledge'),
            data: [{ name: 'Developer Design' }, { name: 'Officer Design' }],
            placeholder: t('profile.industry_ex'),
            onChoose: (item) => {
                onCreate(item);
            },
        });
    };

    return (
        <View style={[Layout.fill]}>
            <UIHeader
                LeftIcon="back"
                RightIcon="edit"
                avatar={avatar}
                onPress={() => navigation.goBack()}
                onPressRight={clickAvatar}
                title={name ?? t('chat.chat_bot')}
            />
            <View style={styles.viewDivider} />
            <GiftedChat
                messages={listMessages}
                onSend={(messages) => onSend(messages)}
                user={{
                    _id: userID,
                }}
                renderBubble={renderBubble}
                renderSend={renderSend}
                renderInputToolbar={renderInputToolbar}
                placeholder={t('chat.write_to_message')}
                isKeyboardInternallyHandled
                scrollToBottom
                alwaysShowSend
                wrapInSafeArea
                infiniteScroll
            />
        </View>
    );
};

const mapStateToProps = (state) => ({
    chatReducer: state.chatReducer,
});
export default connect(mapStateToProps, {
    actionListMessageChat,
    actionCreateMessageChat,
    actionCreateMessageChatBot,
})(IndexMessageContainer);

const styles = StyleSheet.create({
    viewDivider: {
        height: verticalScale(6),
        backgroundColor: Colors.gray,
    },
    inputText: {
        height: verticalScale(40),
        backgroundColor: Colors.gray,
        alignContent: 'center',
        justifyContent: 'center',
        marginStart: verticalScale(26),
        marginTop: verticalScale(6),
        marginBottom: verticalScale(10),
        marginEnd: verticalScale(26),
        borderRadius: verticalScale(32),
    },
    bgCircleShapeView: {
        width: verticalScale(34),
        height: verticalScale(34),
        borderRadius: verticalScale(40) / 2,
        margin: verticalScale(6),
        backgroundColor: Colors.blueLight,
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconSend: {
        width: verticalScale(30),
        height: verticalScale(30),
    },
});
