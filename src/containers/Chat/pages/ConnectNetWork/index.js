import React, { useState, useEffect } from 'react';
import { View, StyleSheet, FlatList, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import { UIHeader } from '../../../../components/common';
import { Colors } from '../../../../theme/Variables';
import { useTranslation } from 'react-i18next';
import { useNavigation, useRoute } from '@react-navigation/native';
import { moderateScale, verticalScale } from '../../../../utils/ScalingUtils';
import { SCREEN } from '../../../../constant';
import Toast from '../../../../components/elements/Toast';
import { useTheme } from '@/theme';
import { UIText, UIButtonChoose } from '../../../../components/elements';
import { UIAvatar } from '../../components';
import { BASE_IMAGE_URL } from '../../../../constant/value.constant';
import { connect } from 'react-redux';
import {
    actionListNetworkRecommended,
    actionListNetworkNetwork,
    actionListNetworkRequest,
    actionNetworkRequest,
    actionNetworkAccept,
    actionCreateConversation,
} from '../../duck/actions';

const IndexConnectNetWorkContainer = (props) => {
    const { Layout, Gutters, Images, Colors } = useTheme();
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { params } = useRoute();
    const {
        networkReducer,
        actionListNetworkNetwork,
        actionListNetworkRecommended,
        actionListNetworkRequest,
        actionNetworkRequest,
        actionNetworkAccept,
        actionCreateConversation,
    } = props;
    const { isLoading } = networkReducer || false;
    const [listNetWork, setListNetWork] = useState([]);
    const [listRecommeneded, setListRecommeneded] = useState([]);
    const [listRequest, setListRequest] = useState([]);
    const [filter, setFilter] = useState(params?.isFromNotification == true ? 2 : 0);

    useEffect(() => {
        actionListNetworkNetwork((listData) => {
            setListNetWork(listData);
        });
    }, []);

    const selectItem = (index) => {
        setFilter(index);
        if (index == 0) {
            actionListNetworkNetwork((listData) => {
                setListNetWork(listData);
            });
        } else if (index == 1) {
            actionListNetworkRecommended((listData) => {
                setListRecommeneded(listData);
            });
        } else if (index == 2) {
            actionListNetworkRequest((listData) => {
                setListRequest(listData);
            });
        }
    };

    const openChat = (item) => {
        const data = {
            userId: item._id,
        };
        actionCreateConversation({ data }, (channelID) => {
            navigation.navigate(SCREEN.MESSAGE, {
                id: item._id,
                channelId: channelID,
                name: item.username,
                avatar: item.avatar ? BASE_IMAGE_URL + item.avatar : '',
            });
        });
    };

    const networkRequest = (memberId) => {
        const data = {
            memberId: memberId,
        };
        actionNetworkRequest({ data }, () => {
            Toast(t('common.success'));
        });
    };

    const requestCancel = (item) => {
        const data = {
            _id: item._id,
            isActive: false,
        };
        actionNetworkAccept({ data }, () => {
            Toast(t('common.success'));
            actionListNetworkRequest((listData) => {
                setListRequest(listData);
            });
        });
    };

    const requestAccept = (item) => {
        const data = {
            _id: item._id,
            isActive: true,
        };
        actionNetworkAccept({ data }, () => {
            Toast(t('common.success'));
            actionListNetworkRequest((listData) => {
                setListRequest(listData);
            });
        });
    };

    const renderItem = ({ item, index }) => {
        return (
            <View style={[Layout.fill, Layout.row, Gutters.smallHPadding, styles.containerItem]}>
                <UIAvatar url={item.avatar ? { uri: BASE_IMAGE_URL + item.avatar } : null} />
                <View style={[Layout.fill, Layout.colum, Gutters.smallHPadding]}>
                    <UIText numberOfLines={1} ellipsizeMode="tail" style={styles.txtName} font="bold" text={item.username} />
                    <UIText
                        numberOfLines={1}
                        ellipsizeMode="tail"
                        style={styles.txtContent}
                        font="regular"
                        text={item.resume?.intro?.currentPosition ?? 'Unknown'}
                    />
                </View>
                {filter == 0 && (
                    <TouchableOpacity onPress={() => openChat(item)}>
                        <Image source={Images.chat.ic_chat} style={styles.icIcon} />
                    </TouchableOpacity>
                )}
                {filter == 1 && (
                    <TouchableOpacity onPress={() => networkRequest(item._id)}>
                        <Image source={Images.home.ic_network_recommended} style={styles.icIcon} />
                    </TouchableOpacity>
                )}
                {filter == 2 && (
                    <View style={{ flexDirection: 'row', alignItems: 'center', width: verticalScale(90) }}>
                        <TouchableOpacity onPress={() => requestCancel(item)} style={[styles.icAction, { borderColor: Colors.error }]}>
                            <Image style={[styles.imgAction, { tintColor: Colors.error }]} source={Images.home.ic_x} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => requestAccept(item)} style={[styles.icAction, { borderColor: Colors.lightBlue }]}>
                            <Image style={[styles.imgAction, { tintColor: Colors.lightBlue }]} source={Images.home.ic_approve} />
                        </TouchableOpacity>
                    </View>
                )}
            </View>
        );
    };

    const FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: '100%',
                    backgroundColor: Colors.grayInput,
                }}
            />
        );
    };

    return (
        <View style={[Layout.fill]}>
            <UIHeader onPress={() => navigation.goBack()} LeftIcon="back" title={t('chat.my_network')} />
            <View style={styles.contentContainer}>
                <View style={styles.row}>
                    <UIButtonChoose
                        containerStyle={{ flex: 0.3 }}
                        text={t('chat.my_network')}
                        isChoose={filter == 0}
                        onPress={() => selectItem(0)}
                    />
                    <UIButtonChoose
                        containerStyle={{ flex: 0.4 }}
                        text={t('chat.recommend')}
                        isChoose={filter == 1}
                        onPress={() => selectItem(1)}
                    />
                    <UIButtonChoose
                        containerStyle={{ flex: 0.4 }}
                        text={t('chat.requested')}
                        isChoose={filter == 2}
                        onPress={() => selectItem(2)}
                    />
                </View>
                {isLoading ? <ActivityIndicator size="small" color={Colors.error} /> : null}
                {filter == 0 && (
                    <View style={styles.viewList}>
                        {listNetWork.length ? (
                            <FlatList
                                renderItem={renderItem}
                                extraData={listNetWork}
                                data={listNetWork}
                                ItemSeparatorComponent={FlatListItemSeparator}
                                keyExtractor={(_, index) => index.toString()}
                            />
                        ) : (
                            <UIText font="bold" style={styles.txtEmptyList} text={t('common.empty')} />
                        )}
                    </View>
                )}
                {filter == 1 && (
                    <View style={styles.viewList}>
                        {listRecommeneded.length ? (
                            <FlatList
                                renderItem={renderItem}
                                extraData={listRecommeneded}
                                data={listRecommeneded}
                                ItemSeparatorComponent={FlatListItemSeparator}
                                keyExtractor={(_, index) => index.toString()}
                            />
                        ) : (
                            <UIText font="bold" style={styles.txtEmptyList} text={t('common.empty')} />
                        )}
                    </View>
                )}
                {filter == 2 && (
                    <View style={styles.viewList}>
                        {listRequest.length ? (
                            <FlatList
                                renderItem={renderItem}
                                extraData={listRequest}
                                data={listRequest}
                                ItemSeparatorComponent={FlatListItemSeparator}
                                keyExtractor={(_, index) => index.toString()}
                            />
                        ) : (
                            <UIText font="bold" style={styles.txtEmptyList} text={t('common.empty')} />
                        )}
                    </View>
                )}
            </View>
        </View>
    );
};

const mapStateToProps = (state) => ({
    networkReducer: state.networkReducer,
});
export default connect(mapStateToProps, {
    actionListNetworkNetwork,
    actionListNetworkRecommended,
    actionListNetworkRequest,
    actionNetworkAccept,
    actionNetworkRequest,
    actionCreateConversation,
})(IndexConnectNetWorkContainer);

const styles = StyleSheet.create({
    contentContainer: {
        marginBottom: verticalScale(60),
        alignItems: 'center',
        flex: 1,
    },
    containerItem: {
        height: verticalScale(64),
        alignItems: 'center',
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: verticalScale(16),
        marginBottom: verticalScale(20),
        justifyContent: 'space-around',
    },
    viewList: {
        width: '100%',
    },
    txtName: {
        fontSize: moderateScale(14),
        lineHeight: moderateScale(21),
        color: Colors.blueDark,
    },
    txtContent: {
        fontSize: moderateScale(14),
        color: Colors.title,
        lineHeight: moderateScale(16),
        marginTop: verticalScale(6),
    },
    txtEmptyList: {
        fontSize: moderateScale(16),
        color: Colors.blueDark,
        textAlignVertical: 'center',
        textAlign: 'center',
        marginTop: verticalScale(50),
    },
    icIcon: {
        width: verticalScale(30),
        height: verticalScale(30),
    },
    icAction: {
        width: verticalScale(36),
        height: verticalScale(36),
        borderRadius: verticalScale(20),
        borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: verticalScale(10),
    },
    imgAction: {
        width: verticalScale(15),
        height: verticalScale(15),
    },
});
