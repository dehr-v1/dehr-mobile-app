import { ChatAction } from '../actions';

const message = {
    message: {},
    isLoading: false,
};
const chatReducer = (state = message, action) => {
    const { type, payload } = action;
    switch (type) {
        case ChatAction.LIST_USER.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ChatAction.LIST_USER.SUCCESS:
            return {
                ...state,
                isLoading: false,
                message: payload,
            };
        case ChatAction.LIST_USER.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ChatAction.LIST_CONVERSATION.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ChatAction.LIST_CONVERSATION.SUCCESS:
            return {
                ...state,
                isLoading: false,
                message: payload,
            };
        case ChatAction.LIST_CONVERSATION.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ChatAction.LIST_MESSAGE.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ChatAction.LIST_MESSAGE.SUCCESS:
            return {
                ...state,
                isLoading: false,
                message: payload,
            };
        case ChatAction.LIST_MESSAGE.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ChatAction.CREATE_MESSAGE.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ChatAction.CREATE_MESSAGE.SUCCESS:
            return {
                ...state,
                isLoading: false,
                message: payload,
            };
        case ChatAction.CREATE_MESSAGE.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ChatAction.CREATE_CONVERSATION.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ChatAction.CREATE_CONVERSATION.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case ChatAction.CREATE_CONVERSATION.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ChatAction.CREATE_MESSAGE_CHAT_BOT.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ChatAction.CREATE_MESSAGE_CHAT_BOT.SUCCESS:
            return {
                ...state,
                isLoading: false,
                message: payload,
            };
        case ChatAction.CREATE_MESSAGE_CHAT_BOT.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
};

const network = {
    list: {},
    isLoading: false,
};

const networkReducer = (state = network, action) => {
    const { type, payload } = action;
    switch (type) {
        case ChatAction.LIST_NETWORK_RECOMMENDED.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ChatAction.LIST_NETWORK_RECOMMENDED.SUCCESS:
            return {
                ...state,
                isLoading: false,
                user: payload,
            };
        case ChatAction.LIST_NETWORK_RECOMMENDED.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ChatAction.LIST_NETWORK_NETWORK.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ChatAction.LIST_NETWORK_NETWORK.SUCCESS:
            return {
                ...state,
                isLoading: false,
                user: payload,
            };
        case ChatAction.LIST_NETWORK_NETWORK.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ChatAction.LIST_NETWORK_REQUEST.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ChatAction.LIST_NETWORK_REQUEST.SUCCESS:
            return {
                ...state,
                isLoading: false,
                user: payload,
            };
        case ChatAction.LIST_NETWORK_REQUEST.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ChatAction.NETWORK_REQUEST.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ChatAction.NETWORK_REQUEST.SUCCESS:
            return {
                ...state,
                isLoading: false,
                user: payload,
            };
        case ChatAction.NETWORK_REQUEST.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ChatAction.CANCEL_NETWORK_REQUEST.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ChatAction.CANCEL_NETWORK_REQUEST.SUCCESS:
            return {
                ...state,
                isLoading: false,
                user: payload,
            };
        case ChatAction.CANCEL_NETWORK_REQUEST.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ChatAction.NETWORK_ACCEPT.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ChatAction.NETWORK_ACCEPT.SUCCESS:
            return {
                ...state,
                isLoading: false,
                user: payload,
            };
        case ChatAction.NETWORK_ACCEPT.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
};

export { chatReducer, networkReducer };
