import { put, call, takeLatest } from 'redux-saga/effects';
import Toast from '../../../../components/elements/Toast';
import { getOAuthToken } from '../../../../services/persistence/store.config';
import { ChatAction } from '../actions';
import {
    createtMessageChat,
    createMessageChatBot,
    getListConversation,
    getLisUser,
    getListMessageChat,
    createConversation,
} from '../../../../services/remote/chat.service';

function* onGetListUser(action) {
    const { payload, callback } = action;
    try {
        const token = yield call(() => getOAuthToken());
        const response = yield call(() => getLisUser(token, new Date().getTime()));
        const listUser = response.data.member;
        if (response.success == 1) {
            yield put({
                type: ChatAction.LIST_USER.SUCCESS,
                payload: listUser,
            });
            callback && callback(listUser);
        } else {
            Toast(`${response.msg}`, 'error');
            yield put({
                type: ChatAction.LIST_USER.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        yield put({
            type: ChatAction.LIST_USER.ERROR,
            payload: error,
        });
    }
}

export function* watchonListUser() {
    yield takeLatest(ChatAction.LIST_USER.PENDING, onGetListUser);
}

function* onGetListConversation(action) {
    const { payload, callback } = action;
    try {
        const token = yield call(() => getOAuthToken());
        const response = yield call(() => getListConversation(token, new Date().getTime()));
        const listChat = response.data.chat;
        if (response.success == 1) {
            yield put({
                type: ChatAction.LIST_CONVERSATION.SUCCESS,
                payload: listChat,
            });
            callback && callback(listChat);
        } else {
            yield put({
                type: ChatAction.LIST_CONVERSATION.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        yield put({
            type: ChatAction.LIST_CONVERSATION.ERROR,
            payload: error,
        });
    }
}

export function* watchonListConversation() {
    yield takeLatest(ChatAction.LIST_CONVERSATION.PENDING, onGetListConversation);
}

function* onListMessageChat(action) {
    try {
        const { payload, callback } = action;
        const channelId = payload.data.channelID;
        const token = yield call(() => getOAuthToken());
        const response = yield call(() => getListMessageChat(channelId, token, new Date().getTime()));
        const listMessage = response.data.message;
        if (response.success == 1) {
            yield put({
                type: ChatAction.LIST_MESSAGE.SUCCESS,
            });
            yield put({
                type: ChatAction.LIST_MESSAGE.ERROR,
            });
            callback && callback(listMessage);
        } else {
            yield put({
                type: ChatAction.LIST_MESSAGE.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        yield put({
            type: ChatAction.LIST_MESSAGE.ERROR,
            payload: error,
        });
    }
}

export function* watchonListMessageChat() {
    yield takeLatest(ChatAction.LIST_MESSAGE.PENDING, onListMessageChat);
}

function* onCreateMessageChat(action) {
    try {
        const { payload, callback } = action;
        const token = yield call(() => getOAuthToken());
        const data = payload;
        const messages = data.data.message;
        const channelID = data.data.channelID;
        const response = yield call(() => createtMessageChat(token, channelID, messages, new Date().getTime()));
        if (response.success == 1) {
            yield put({
                type: ChatAction.CREATE_MESSAGE.SUCCESS,
            });
            yield put({
                type: ChatAction.CREATE_MESSAGE.ERROR,
            });
            callback && callback();
        } else {
            Toast(`${response.msg}`, 'error');
            yield put({
                type: ChatAction.CREATE_MESSAGE.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        yield put({
            type: ChatAction.CREATE_MESSAGE.ERROR,
            payload: error,
        });
    }
}

export function* watchonCreateMessageChat() {
    yield takeLatest(ChatAction.CREATE_MESSAGE.PENDING, onCreateMessageChat);
}

function* onCreateMessageChatBot(action) {
    try {
        const { payload, callback } = action;
        const { data } = payload || {};
        const response = yield call(() => createMessageChatBot(data));
        if (response[0] != null) {
            yield put({
                type: ChatAction.CREATE_MESSAGE_CHAT_BOT.SUCCESS,
            });
            callback && callback(response[0]);
        } else {
            Toast(`${response.msg}`, 'error');
            yield put({
                type: ChatAction.CREATE_MESSAGE_CHAT_BOT.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        yield put({
            type: ChatAction.CREATE_MESSAGE_CHAT_BOT.ERROR,
            payload: error,
        });
    }
}

export function* watchonCreateMessageChatBot() {
    yield takeLatest(ChatAction.CREATE_MESSAGE_CHAT_BOT.PENDING, onCreateMessageChatBot);
}

function* onCreateConversation(action) {
    try {
        const { payload, callback } = action;
        const token = yield call(() => getOAuthToken());
        const userId = payload.data.userId;
        const response = yield call(() => createConversation(userId, token, new Date().getTime()));
        if (response.success == 1) {
            yield put({
                type: ChatAction.CREATE_CONVERSATION.SUCCESS,
            });
            callback && callback(response.data.chat._id);
        } else {
            Toast(`${response.msg}`, 'error');
            yield put({
                type: ChatAction.CREATE_CONVERSATION.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        yield put({
            type: ChatAction.CREATE_CONVERSATION.ERROR,
            payload: error,
        });
    }
}

export function* watchonCreateConversation() {
    yield takeLatest(ChatAction.CREATE_CONVERSATION.PENDING, onCreateConversation);
}
