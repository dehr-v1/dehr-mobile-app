import { all } from 'redux-saga/effects';
import {
    watchonListUser,
    watchonListConversation,
    watchonListMessageChat,
    watchonCreateConversation,
    watchonCreateMessageChat,
    watchonCreateMessageChatBot,
} from './chat.saga';

import {
    watchonListNetworkRecommended,
    watchonListNetwork,
    watchonListNetworkRequest,
    watchonNetworkRequest,
    watchonNetworkAccept,
    watchCancelNetworkRequest
} from './network.saga';

export function* chatSaga() {
    yield all([
        watchonListUser(),
        watchonListConversation(),
        watchonListMessageChat(),
        watchonCreateMessageChatBot(),
        watchonCreateMessageChat(),
        watchonCreateConversation(),
    ]);
}

export function* networkSaga() {
    yield all([
        watchonListNetworkRecommended(),
        watchonListNetwork(),
        watchonListNetworkRequest(),
        watchonNetworkRequest(),
        watchonNetworkAccept(),
        watchCancelNetworkRequest(),
    ]);
}
