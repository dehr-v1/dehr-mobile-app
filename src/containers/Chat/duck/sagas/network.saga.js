import { put, call, takeLatest } from 'redux-saga/effects';
import Toast from '../../../../components/elements/Toast';
import { ChatAction } from '../actions';
import {
    getLisNetworkRecommened,
    getLisNetwork,
    getLisNetworkRequest,
    networkRequest,
    networkAccept,
    cancelNetworkRequest,
} from '../../../../services/remote/network.service';

//Network
function* onListNetworkRecommended(action) {
    try {
        const { callback } = action;
        const response = yield call(() => getLisNetworkRecommened());
        const data = response.data;
        if (data != null) {
            yield put({
                type: ChatAction.LIST_NETWORK_RECOMMENDED.SUCCESS,
            });
            callback && callback(data);
        } else {
            yield put({
                type: ChatAction.LIST_NETWORK_RECOMMENDED.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        yield put({
            type: ChatAction.LIST_NETWORK_RECOMMENDED.ERROR,
            payload: error,
        });
    }
}

function* onListNetwork(action) {
    try {
        const { callback } = action;
        const response = yield call(() => getLisNetwork());
        const data = response.data;
        if (data != null) {
            yield put({
                type: ChatAction.LIST_NETWORK_NETWORK.SUCCESS,
            });
            callback && callback(data);
        } else {
            yield put({
                type: ChatAction.LIST_NETWORK_NETWORK.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        yield put({
            type: ChatAction.LIST_NETWORK_NETWORK.ERROR,
            payload: error,
        });
    }
}

function* onListNetworkRequest(action) {
    try {
        const { callback } = action;
        const response = yield call(() => getLisNetworkRequest());
        const data = response.data;
        if (data != null) {
            yield put({
                type: ChatAction.LIST_NETWORK_REQUEST.SUCCESS,
            });
            callback && callback(data);
        } else {
            yield put({
                type: ChatAction.LIST_NETWORK_REQUEST.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        yield put({
            type: ChatAction.LIST_NETWORK_REQUEST.ERROR,
            payload: error,
        });
    }
}

function* onNetworkRequest(action) {
    try {
        const { payload, callback } = action;
        const response = yield call(() => networkRequest(payload));
        if (response != null) {
            yield put({
                type: ChatAction.NETWORK_REQUEST.SUCCESS,
            });
            callback && callback();
        } else {
            Toast(`${response.message}`, 'error');
            yield put({
                type: ChatAction.NETWORK_REQUEST.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        Toast(`${error}`, 'error');
        yield put({
            type: ChatAction.NETWORK_REQUEST.ERROR,
            payload: error,
        });
    }
}

function* onCancelNetworkRequest(action) {
    try {
        const { payload, callback } = action;
        const response = {}; // yield call(() => cancelNetworkRequest(payload)); call api here
        if (response != null) {
            yield put({
                type: ChatAction.CANCEL_NETWORK_REQUEST.SUCCESS,
            });
            callback && callback();
        } else {
            Toast(`${response.message}`, 'error');
            yield put({
                type: ChatAction.CANCEL_NETWORK_REQUEST.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        Toast(`${error}`, 'error');
        yield put({
            type: ChatAction.CANCEL_NETWORK_REQUEST.ERROR,
            payload: error,
        });
    }
}

function* onNetworkAccept(action) {
    try {
        const { payload, callback } = action;
        const data = {
            isActive: payload.data.isActive,
        };
        const response = yield call(() => networkAccept(payload.data._id, data));
        if (response != null) {
            yield put({
                type: ChatAction.NETWORK_ACCEPT.SUCCESS,
            });
            callback && callback();
        } else {
            Toast(`${response.message}`, 'error');
            yield put({
                type: ChatAction.NETWORK_ACCEPT.ERROR,
                payload: response.message,
            });
        }
    } catch (error) {
        Toast(`${error}`, 'error');
        yield put({
            type: ChatAction.NETWORK_ACCEPT.ERROR,
            payload: error,
        });
    }
}

export function* watchonListNetworkRecommended() {
    yield takeLatest(ChatAction.LIST_NETWORK_RECOMMENDED.PENDING, onListNetworkRecommended);
}

export function* watchonListNetwork() {
    yield takeLatest(ChatAction.LIST_NETWORK_NETWORK.PENDING, onListNetwork);
}

export function* watchonListNetworkRequest() {
    yield takeLatest(ChatAction.LIST_NETWORK_REQUEST.PENDING, onListNetworkRequest);
}

export function* watchonNetworkAccept() {
    yield takeLatest(ChatAction.NETWORK_ACCEPT.PENDING, onNetworkAccept);
}

export function* watchonNetworkRequest() {
    yield takeLatest(ChatAction.NETWORK_REQUEST.PENDING, onNetworkRequest);
}

export function* watchCancelNetworkRequest() {
    yield takeLatest(ChatAction.CANCEL_NETWORK_REQUEST.PENDING, onCancelNetworkRequest);
}
