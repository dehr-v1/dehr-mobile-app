import { createActionSet } from '../../../../utils';

export const ChatAction = {
    LIST_NETWORK_RECOMMENDED: createActionSet('LIST_NETWORK_RECOMMENDED'),
    LIST_NETWORK_NETWORK: createActionSet('LIST_NETWORK_NETWORK'),
    LIST_NETWORK_REQUEST: createActionSet('LIST_NETWORK_REQUEST'),
    NETWORK_REQUEST: createActionSet('NETWORK_REQUEST'),
    NETWORK_ACCEPT: createActionSet('NETWORK_ACCEPT'),
    LIST_USER: createActionSet('LIST_USER'),
    LIST_CONVERSATION: createActionSet('LIST_CONVERSATION'),
    LIST_MESSAGE: createActionSet('LIST_MESSAGE'),
    CREATE_CONVERSATION: createActionSet('CREATE_CONVERSATION'),
    CREATE_MESSAGE: createActionSet('CREATE_MESSAGE'),
    CREATE_MESSAGE_CHAT_BOT: createActionSet('CREATE_MESSAGE_CHAT_BOT'),
    CANCEL_NETWORK_REQUEST: createActionSet('CANCEL_NETWORK_REQUEST'),
};
