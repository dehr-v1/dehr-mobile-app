import { ChatAction } from './chat.create.action';

export const actionListNetworkNetwork = (callback) => ({
    type: ChatAction.LIST_NETWORK_NETWORK.PENDING,
    callback,
});

export const actionListNetworkRecommended = (callback) => ({
    type: ChatAction.LIST_NETWORK_RECOMMENDED.PENDING,
    callback,
});

export const actionListNetworkRequest = (callback) => ({
    type: ChatAction.LIST_NETWORK_REQUEST.PENDING,
    callback,
});

export const actionNetworkRequest = (payload, callback) => ({
    type: ChatAction.NETWORK_REQUEST.PENDING,
    payload,
    callback,
});

export const actionNetworkAccept = (payload, callback) => ({
    type: ChatAction.NETWORK_ACCEPT.PENDING,
    payload,
    callback,
});

export const actionListConversation = (payload, callback) => ({
    type: ChatAction.LIST_CONVERSATION.PENDING,
    payload,
    callback,
});

export const actionListUser = (payload, callback) => ({
    type: ChatAction.LIST_USER.PENDING,
    payload,
    callback,
});

export const actionListMessageChat = (payload, callback) => ({
    type: ChatAction.LIST_MESSAGE.PENDING,
    payload,
    callback,
});

export const actionCreateConversation = (payload, callback) => ({
    type: ChatAction.CREATE_CONVERSATION.PENDING,
    payload,
    callback,
});

export const actionCreateMessageChatBot = (payload, callback) => ({
    type: ChatAction.CREATE_MESSAGE_CHAT_BOT.PENDING,
    payload,
    callback,
});

export const actionCreateMessageChat = (payload, callback) => ({
    type: ChatAction.CREATE_MESSAGE.PENDING,
    payload,
    callback,
});

export const actionCancelNetworkRequest = (payload, callback) => ({
    type: ChatAction.CANCEL_NETWORK_REQUEST.PENDING,
    payload,
    callback,
});
