import { createActionSet } from '../../../../utils';

export const HomeAction = {
    GET_JOB: createActionSet('GET_JOB'),
    GET_HOT_JOB: createActionSet('GET_HOT_JOB'),
    GET_LATEST_JOB: createActionSet('GET_LATEST_JOB'),
    GET_APPLIED_JOB: createActionSet('GET_APPLIED_JOB'),
    APPLY_JOB: createActionSet('APPLE_JOB'),
    PROFILE_VIEW: createActionSet('PROFILE_VIEW'),
    ACCEPT_UNLOCK: createActionSet('ACCEPT_UNLOCK'),
    REJECT_UNCLOCK: createActionSet('REJECT_UNCLOCK'),
    GET_INFO: createActionSet('GET_INFO'),
    GET_POPUP: createActionSet('GET_POPUP'),
    UPGRADE_PRO: createActionSet('UPGRADE_PRO'),
    GET_PROFILE: createActionSet('GET_PROFILE'),
    CREATE_RESUME: createActionSet('CREATE_RESUME'),
    GET_APPLIED_RESUME: createActionSet('GET_APPLIED_RESUME'),
};

export const SearchAction = {
    SEARCH_JOB: createActionSet('SEARCH_JOB'),
    SEARCH_CONFIG: createActionSet('SEARCH_CONFIG'),
    SEARCH_ADVANCE: createActionSet('SEARCH_ADVANCE'),
    SEARCH_RESUME: createActionSet('SEARCH_RESUME'),
    SEARCH_JOB: createActionSet('SEARCH_JOB'),
    SEARCH_PEOPLE: createActionSet('SEARCH_PEOPLE'),
    SEARCH_RESUME: createActionSet('SEARCH_RESUME'),
    GET_JOB_SEARCH: createActionSet('GET_JOB_SEARCH'),
    GET_PEOPLE_SEARCH: createActionSet('GET_PEOPLE_SEARCH')
};
