import { HomeAction } from './home.create.action';

export const actionGetJob = (payload, callback) => ({
    type: HomeAction.GET_JOB.PENDING,
    payload,
    callback,
});

export const actionGetHotJob = (payload, callback) => ({
    type: HomeAction.GET_HOT_JOB.PENDING,
    payload,
    callback,
});

export const actionGetLatestJob = (payload, callback) => ({
    type: HomeAction.GET_LATEST_JOB.PENDING,
    payload,
    callback,
});

export const actionGetAppliedJob = (payload, callback) => ({
    type: HomeAction.GET_APPLIED_JOB.PENDING,
    payload,
    callback,
});

export const actionApplyJob = (payload, callback) => ({
    type: HomeAction.APPLY_JOB.PENDING,
    payload,
    callback,
});

export const actionGetProfileView = (payload, callback) => ({
    type: HomeAction.PROFILE_VIEW.PENDING,
    payload,
    callback,
});

export const actionGetInfo = (payload, callback) => ({
    type: HomeAction.GET_INFO.PENDING,
    payload,
    callback,
});

export const actionGetPopup = (callback) => ({
    type: HomeAction.GET_POPUP.PENDING,
    callback,
});

export const actionAcceptUnlock = (payload, callback) => ({
    type: HomeAction.ACCEPT_UNLOCK.PENDING,
    payload,
    callback,
});

export const actionRejectUnlock = (payload, callback) => ({
    type: HomeAction.REJECT_UNCLOCK.PENDING,
    payload,
    callback,
});

export const actionUpGradePro = (callback) => ({
    type: HomeAction.UPGRADE_PRO.PENDING,
    callback,
});

export const actionGetProfile = (payload, callback) => ({
    type: HomeAction.GET_PROFILE.PENDING,
    payload,
    callback,
});

export const actionCreateResume = (payload, callback) => ({
    type: HomeAction.CREATE_RESUME.PENDING,
    payload,
    callback,
});

export const actionAppliedJob = (payload, callback) => ({
    type: HomeAction.GET_APPLIED_RESUME.PENDING,
    payload,
    callback,
});
