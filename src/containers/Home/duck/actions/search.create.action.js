import { SearchAction } from './home.create.action';

export const actionGetJobSearch = (payload, callback) => ({
    type: SearchAction.GET_JOB_SEARCH.PENDING,
    payload,
    callback
})

export const actionSearchJob = (payload, callback) => ({
    type: SearchAction.SEARCH_JOB.PENDING,
    payload,
    callback,
});

export const actionGetPeopleSearch = (payload, callback) => ({
    type: SearchAction.GET_PEOPLE_SEARCH.PENDING,
    payload,
    callback
})

export const actionSearchResume = (payload, callback) => ({
    type: SearchAction.SEARCH_RESUME.PENDING,
    payload,
    callback,
});

export const actionSearchConfig = (payload, callback) => ({
    type: SearchAction.SEARCH_CONFIG.PENDING,
    payload,
    callback,
});


export const actionSearchAdvance = (payload, callback) => ({
    type: SearchAction.SEARCH_ADVANCE.PENDING,
    payload,
    callback,
});

export const actionSearchPeople = (payload, callback) => ({
    type: SearchAction.SEARCH_PEOPLE.PENDING,
    payload,
    callback,
});
