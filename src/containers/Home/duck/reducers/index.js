import { SearchAction, HomeAction } from '../../duck/actions';

const home = {
    jobs: [],
    isLoading: false,
    appliedJobs: [],
};
const homeReducer = (state = home, action) => {
    const { type, payload } = action;
    switch (type) {
        case HomeAction.GET_JOB.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case HomeAction.GET_JOB.SUCCESS:
            return {
                ...state,
                isLoading: false,
                jobs: payload,
            };
        case HomeAction.GET_JOB.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.GET_HOT_JOB.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case HomeAction.GET_HOT_JOB.SUCCESS:
            return {
                ...state,
                isLoading: false,
                jobs: payload,
            };
        case HomeAction.GET_HOT_JOB.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.GET_APPLIED_JOB.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case HomeAction.GET_APPLIED_JOB.SUCCESS:
            return {
                ...state,
                isLoading: false,
                jobs: payload,
            };
        case HomeAction.GET_APPLIED_JOB.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.GET_LATEST_JOB.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case HomeAction.GET_LATEST_JOB.SUCCESS:
            return {
                ...state,
                isLoading: false,
                jobs: payload,
            };
        case HomeAction.GET_LATEST_JOB.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.APPLY_JOB.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case HomeAction.APPLY_JOB.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.APPLY_JOB.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.PROFILE_VIEW.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case HomeAction.PROFILE_VIEW.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.PROFILE_VIEW.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.ACCEPT_UNLOCK.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case HomeAction.ACCEPT_UNLOCK.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.ACCEPT_UNLOCK.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.REJECT_UNCLOCK.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case HomeAction.REJECT_UNCLOCK.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.REJECT_UNCLOCK.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.GET_INFO.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case HomeAction.GET_INFO.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.GET_INFO.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.GET_POPUP.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case HomeAction.GET_POPUP.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.GET_POPUP.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.UPGRADE_PRO.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case HomeAction.UPGRADE_PRO.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.UPGRADE_PRO.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.GET_PROFILE.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case HomeAction.GET_PROFILE.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.GET_PROFILE.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.CREATE_RESUME.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case HomeAction.CREATE_RESUME.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case HomeAction.CREATE_RESUME.ERROR:
            return {
                ...state,
                isLoading: false,
            };

        case HomeAction.GET_APPLIED_RESUME.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case HomeAction.GET_APPLIED_RESUME.SUCCESS:
            return {
                ...state,
                isLoading: false,
                appliedJobs: payload,
            };
        case HomeAction.GET_APPLIED_RESUME.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
};

const search = {
    search: [],
    isLoading: false
};

const searchReducer = (state = search, action) => {
    const { type, payload } = action;
    switch (type) {
        case SearchAction.SEARCH_JOB.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case SearchAction.SEARCH_JOB.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case SearchAction.SEARCH_JOB.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case SearchAction.SEARCH_CONFIG.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case SearchAction.SEARCH_CONFIG.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case SearchAction.SEARCH_CONFIG.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case SearchAction.SEARCH_ADVANCE.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case SearchAction.SEARCH_ADVANCE.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case SearchAction.SEARCH_ADVANCE.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case SearchAction.SEARCH_RESUME.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case SearchAction.SEARCH_RESUME.SUCCESS:
            return {
                ...state,
                isLoading: false,
                jobs: payload,
            };
        case SearchAction.SEARCH_RESUME.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case SearchAction.GET_JOB_SEARCH.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case SearchAction.GET_JOB_SEARCH.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case SearchAction.GET_JOB_SEARCH.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case SearchAction.GET_PEOPLE_SEARCH.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case SearchAction.GET_PEOPLE_SEARCH.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case SearchAction.GET_PEOPLE_SEARCH.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
};

export { homeReducer, searchReducer };
