import { all } from 'redux-saga/effects';
import {
    watchOnApplyJob,
    watchOnGetResume,
    watchOnGetJob,
    watchonProfileView,
    watchonAcceptUnlock,
    watchonRejectUnlock,
    watchOnGetInfo,
    watchonUpgradePro,
    watchonGetPopup,
    watchonGetProfile,
    watchonCreateResume,
    watchGetAppliedJob,
} from './home.saga';

import {
    watchOnSearchJob,
    watchonSearchConfig,
    watchonSearchAdvance,
    watchOnSearchResume,
    watchOnGetJobSearch,
    watchOnGetPeoPleSearch,
} from './search.saga';

export function* homeSaga() {
    yield all([
        watchOnGetJob(),
        watchOnApplyJob(),
        watchonProfileView(),
        watchonAcceptUnlock(),
        watchonRejectUnlock(),
        watchOnGetInfo(),
        watchonUpgradePro(),
        watchonGetPopup(),
        watchonGetProfile(),
        watchonCreateResume(),
        watchGetAppliedJob(),
    ]);
}

export function* searchSaga() {
    yield all([
        watchOnSearchJob(),
        watchonSearchConfig(),
        watchonSearchAdvance(),
        watchOnSearchResume(),
        watchOnGetJobSearch(),
        watchOnGetPeoPleSearch()
    ]);
}
