import { put, call, takeLatest } from 'redux-saga/effects';
import Toast from '../../../../components/elements/Toast';
import { getOAuthToken, getSeedPhase, setPopUp, getPopUp } from '../../../../services/persistence/store.config';
import { HomeAction } from '../actions';
import { upgradePro, acceptUnlock, rejectUnlock, decryptWithPrivateKey, getPopup } from '../../../../services/remote/home.service';
import {
    applyJob,
    getJob,
    searchJobs,
    getAppliedJob,
    getHotJob,
    getLatestJob,
    getAppliedListJob,
    searchPeople,
} from '../../../../services/remote/job.service';
import { getResumeList, searchResume, getProfile, getInfo, createResume } from '../../../../services/remote/resume.service';

function* onGetListJob(action) {
    const { payload, callback } = action;
    try {
        const job = yield call(() => getJob());
        yield put({
            type: HomeAction.GET_JOB.SUCCESS,
            payload: job.data,
        });
        callback && callback(job.data);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: HomeAction.GET_JOB.ERROR,
            payload: error,
        });
    }
}

export function* watchOnGetJob() {
    yield takeLatest(HomeAction.GET_JOB.PENDING, onGetListJob);
}

function* onGetListHotJob(action) {
    const { payload, callback } = action;
    try {
        const job = yield call(() => getHotJob(payload));
        yield put({
            type: HomeAction.GET_HOT_JOB.SUCCESS,
            payload: job,
        });
        callback && callback(job);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: HomeAction.GET_HOT_JOB.ERROR,
            payload: error,
        });
    }
}

export function* watchOnGetHotJob() {
    yield takeLatest(HomeAction.GET_HOT_JOB.PENDING, onGetListHotJob);
}

function* onGetListLatestJob(action) {
    const { payload, callback } = action;
    try {
        const job = yield call(() => getLatestJob(payload));
        yield put({
            type: HomeAction.GET_LATEST_JOB.SUCCESS,
            payload: job,
        });
        callback && callback(job);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: HomeAction.GET_LATEST_JOB.ERROR,
            payload: error,
        });
    }
}

export function* watchOnGetLatestJob() {
    yield takeLatest(HomeAction.GET_LATEST_JOB.PENDING, onGetListLatestJob);
}

function* onGetListAppliedJob(action) {
    const { payload, callback } = action;
    try {
        const job = yield call(() => getAppliedListJob(payload));
        yield put({
            type: HomeAction.GET_APPLIED_JOB.SUCCESS,
            payload: job,
        });
        callback && callback(job);
    } catch (error) {
        // Toast(`${error.message}`, 'error');
        yield put({
            type: HomeAction.GET_APPLIED_JOB.ERROR,
            payload: error,
        });
    }
}

export function* watchOnGetAppliedJob() {
    yield takeLatest(HomeAction.GET_APPLIED_JOB.PENDING, onGetListAppliedJob);
}

function* onApplyJob(action) {
    const { payload, callback } = action;
    let { data } = payload;
    try {
        const response = yield call(() => applyJob(data));
        Toast('Apply job successfully');
        yield put({
            type: HomeAction.APPLY_JOB.SUCCESS,
        });
        callback && callback();
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: HomeAction.APPLY_JOB.ERROR,
            payload: error,
        });
    }
}

export function* watchOnApplyJob() {
    yield takeLatest(HomeAction.APPLY_JOB.PENDING, onApplyJob);
}

function* onGetProfileView(action) {
    try {
        const { payload, callback } = action;
        const data = {
            value: payload.request.type,
        };
        const response = yield call(() => getResumeList(data));
        yield put({
            type: HomeAction.PROFILE_VIEW.SUCCESS,
        });
        callback && callback(response.data.reverse());
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: HomeAction.PROFILE_VIEW.ERROR,
            payload: error,
        });
    }
}

export function* watchonProfileView() {
    yield takeLatest(HomeAction.PROFILE_VIEW.PENDING, onGetProfileView);
}

function* onAcceptUnlock(action) {
    const { payload, callback } = action;
    try {
        const formData = new FormData();
        formData.append('requestId', payload.data.requestId);
        formData.append('requesterAddress', payload.data.requesterAddress);
        formData.append('ciphertext', payload.data.encryptedData.ciphertext);
        formData.append('ephemPublicKey', payload.data.encryptedData.ephemPublicKey);
        formData.append('iv', payload.data.encryptedData.iv);
        formData.append('mac', payload.data.encryptedData.mac);
        const response = yield call(() => acceptUnlock(formData));
        yield put({
            type: HomeAction.ACCEPT_UNLOCK.SUCCESS,
            payload: response.data,
        });
        callback && callback(true);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: HomeAction.ACCEPT_UNLOCK.ERROR,
            payload: error,
        });
    }
}

export function* watchonAcceptUnlock() {
    yield takeLatest(HomeAction.ACCEPT_UNLOCK.PENDING, onAcceptUnlock);
}

function* onRejectUnlock(action) {
    const { payload, callback } = action;
    try {
        const formData = new FormData();
        formData.append('requestId', payload.data.requestId);
        formData.append('requesterAddress', payload.data.requesterAddress);
        const response = yield call(() => rejectUnlock(formData));
        yield put({
            type: HomeAction.REJECT_UNCLOCK.SUCCESS,
            payload: response.data,
        });
        callback && callback(true);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: HomeAction.REJECT_UNCLOCK.ERROR,
            payload: error,
        });
    }
}

export function* watchonRejectUnlock() {
    yield takeLatest(HomeAction.REJECT_UNCLOCK.PENDING, onRejectUnlock);
}

function* onGetInfo(action) {
    const { payload, callback } = action;
    try {
        const data = {
            value: payload.request.resume,
        };
        const res = yield call(() => getInfo(data));
        const getWallet = yield call(() => getSeedPhase());
        const wallet = JSON.parse(getWallet);
        const newData = res.encryptedData;
        const encryptedData = {
            iv: newData.iv,
            ephemPublicKey: newData.ephemPublicKey,
            ciphertext: newData.ciphertext,
            mac: newData.mac,
        };
        const decrypted = yield call(() => decryptWithPrivateKey(wallet.privateKey, encryptedData));
        yield put({
            type: HomeAction.GET_INFO.SUCCESS,
            payload: decrypted,
        });
        callback && callback(decrypted);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: HomeAction.GET_INFO.ERROR,
            payload: error,
        });
    }
}

export function* watchOnGetInfo() {
    yield takeLatest(HomeAction.GET_INFO.PENDING, onGetInfo);
}

function* onUpgradePro(action) {
    const { callback } = action;
    try {
        const token = yield call(() => getOAuthToken());
        const formData = new FormData();
        formData.append('authorization', token);
        formData.append('role', 'pro');
        const response = yield call(() => upgradePro(formData));
        if (response.success == 1) {
            yield put({
                type: HomeAction.UPGRADE_PRO.SUCCESS,
                payload: response.data,
            });
            callback && callback();
        } else {
            Toast(`${response.msg}`, 'error');
            yield put({
                type: HomeAction.UPGRADE_PRO.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        yield put({
            type: HomeAction.UPGRADE_PRO.ERROR,
            payload: error,
        });
    }
}

export function* watchonUpgradePro() {
    yield takeLatest(HomeAction.UPGRADE_PRO.PENDING, onUpgradePro);
}

function* onGetPopup(action) {
    const { callback } = action;
    try {
        const data = yield call(() => getPopup());
        let homePopup;
        const home = data.find((item) => item.screen === 'home') ?? null;
        const listPopup = data.filter((item) => item.screen != 'home');
        if (home != null && home.type === 'onetime') {
            // all
            const checkTime = yield call(() => getPopUp());
            const dateNow = new Date().getDate().toString();
            if (checkTime == null || dateNow != checkTime) {
                setPopUp(dateNow);
                homePopup = home;
            }
        } else {
            homePopup = home;
        }
        yield put({
            type: HomeAction.GET_POPUP.SUCCESS,
        });
        callback && callback({ list: listPopup, home: homePopup });
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: HomeAction.GET_POPUP.ERROR,
            payload: error,
        });
        callback && callback(null);
    }
}

export function* watchonGetPopup() {
    yield takeLatest(HomeAction.GET_POPUP.PENDING, onGetPopup);
}

function* onGetProfile(action) {
    const { payload, callback } = action;
    try {
        const data = {
            value: payload.request.resume,
        };
        const response = yield call(() => getProfile(data));
        yield put({
            type: HomeAction.GET_PROFILE.SUCCESS,
            payload: response,
        });
        callback && callback(response);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: HomeAction.GET_PROFILE.ERROR,
            payload: error,
        });
    }
}

export function* watchonGetProfile() {
    yield takeLatest(HomeAction.GET_PROFILE.PENDING, onGetProfile);
}

function* onCreateResume(action) {
    const { payload, callback } = action;
    try {
        const data = {
            value: payload.request.resume,
        };
        const response = yield call(() => createResume(data));
        yield put({
            type: HomeAction.CREATE_RESUME.SUCCESS,
            payload: response,
        });
        callback && callback();
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: HomeAction.CREATE_RESUME.ERROR,
            payload: error,
        });
    }
}

export function* watchonCreateResume() {
    yield takeLatest(HomeAction.CREATE_RESUME.PENDING, onCreateResume);
}

function* onGetAppliedJob(action) {
    const { payload, callback } = action;
    try {
        const job = yield call(() => getAppliedJob());
        yield put({
            type: HomeAction.GET_APPLIED_RESUME.SUCCESS,
            payload: job.data,
        });
        const appliedJobs = job.data.map((e) => {
            return e.job;
        });
        callback && callback(appliedJobs);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: HomeAction.GET_APPLIED_RESUME.ERROR,
            payload: error,
        });
    }
}

export function* watchGetAppliedJob() {
    yield takeLatest(HomeAction.GET_APPLIED_RESUME.PENDING, onGetAppliedJob);
}
