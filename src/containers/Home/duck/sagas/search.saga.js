import { put, call, takeLatest } from 'redux-saga/effects';
import Toast from '../../../../components/elements/Toast';
import { SearchAction } from '../actions';
import {
    searchJobs,
    searchConfig,
    searchAdvance,
    searchResume,
    getJobSearch,
    getPeopleSearch,
    searchPeoples,
} from '../../../../services/remote/search.service';

function* onSearchConfig(action) {
    const { payload, callback } = action;
    try {
        const config = yield call(() => searchConfig());
        const listData = config;
        if (listData != null) {
            yield put({
                type: SearchAction.SEARCH_CONFIG.SUCCESS,
            });
            callback && callback(listData);
        } else {
            Toast(`${error.message}`, 'error');
            yield put({
                type: SearchAction.SEARCH_CONFIG.ERROR,
                payload: error,
            });
        }
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: SearchAction.SEARCH_CONFIG.ERROR,
            payload: error,
        });
    }
}

function* onSearchAdvance(action) {
    const { payload, callback } = action;
    try {
        const search = yield call(() => searchAdvance(payload.data));
        yield put({
            type: SearchAction.SEARCH_ADVANCE.SUCCESS,
        });
        callback && callback(search);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: SearchAction.SEARCH_ADVANCE.ERROR,
            payload: error,
        });
    }
}

function* onGetJobSearch(action) {
    const { payload, callback } = action;
    try {
        const job = yield call(() => getJobSearch(payload));
        yield put({
            type: SearchAction.GET_JOB_SEARCH.SUCCESS,
        });
        callback && callback(job);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: SearchAction.GET_JOB_SEARCH.ERROR,
            payload: error,
        });
    }
}

function* onSearchJob(action) {
    const { payload, callback } = action;
    try {
        const job = yield call(() => searchJobs(payload));
        yield put({
            type: SearchAction.SEARCH_JOB.SUCCESS,
        });
        callback && callback(job);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: SearchAction.SEARCH_JOB.ERROR,
            payload: error,
        });
    }
}

function* onGetPeopleSearch(action) {
    const { payload, callback } = action;
    try {
        const people = yield call(() => getPeopleSearch(payload));
        yield put({
            type: SearchAction.GET_PEOPLE_SEARCH.SUCCESS,
        });
        callback && callback(people);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: SearchAction.GET_PEOPLE_SEARCH.ERROR,
            payload: error,
        });
    }
}

function* onSearchPeople(action) {
    const { payload, callback } = action;
    let { data } = payload;
    try {
        const people = yield call(() => searchPeoples(payload));
        yield put({
            type: SearchAction.SEARCH_PEOPLE.SUCCESS,
        });
        callback && callback(people);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: SearchAction.SEARCH_PEOPLE.ERROR,
            payload: error,
        });
    }
}

function* onSearchResume(action) {
    const { payload, callback } = action;
    let { data } = payload;
    try {
        const res = yield call(() => searchResume(data));
        const listData = res.data;
        yield put({
            type: SearchAction.SEARCH_RESUME.SUCCESS,
        });
        callback && callback(listData);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: SearchAction.SEARCH_RESUME.ERROR,
            payload: error,
        });
    }
}

export function* watchOnSearchPeople() {
    yield takeLatest(SearchAction.SEARCH_PEOPLE.PENDING, onSearchPeople);
}

export function* watchOnSearchResume() {
    yield takeLatest(SearchAction.SEARCH_RESUME.PENDING, onSearchResume);
}

export function* watchonSearchConfig() {
    yield takeLatest(SearchAction.SEARCH_CONFIG.PENDING, onSearchConfig);
}

export function* watchonSearchAdvance() {
    yield takeLatest(SearchAction.SEARCH_ADVANCE.PENDING, onSearchAdvance);
}

export function* watchOnSearchJob() {
    yield takeLatest(SearchAction.SEARCH_JOB.PENDING, onSearchJob);
}

export function* watchOnGetJobSearch() {
    yield takeLatest(SearchAction.GET_JOB_SEARCH.PENDING, onGetJobSearch);
}

export function* watchOnGetPeoPleSearch() {
    yield takeLatest(SearchAction.GET_PEOPLE_SEARCH.PENDING, onGetPeopleSearch);
}
