import React, { useState, useRef, useEffect } from 'react';
import {
    View,
    TouchableOpacity,
    StyleSheet,
    Image,
    FlatList,
    Dimensions,
    ImageBackground,
    ActivityIndicator,
} from 'react-native';
import { useTheme } from '@/theme';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';

import { useTranslation } from 'react-i18next';
import { UIHeader, UIModal, UISearchInput } from '../../components/common';
import { Colors } from '../../theme/Variables';
import { moderateScale, verticalScale } from '../../utils/ScalingUtils';
import { UIButtonChoose, UIText } from '../../components/elements';
import LinearGradient from 'react-native-linear-gradient';
import { BASE_IMAGE_URL } from '../../constant/value.constant';
import { JobDetail, Popup, UIJobComponent, UIJobPosted, UIPeopleComponent } from './components';
import { useNavigation } from '@react-navigation/native';
import { SCREEN } from '../../constant';
import RNPickerSelect from 'react-native-picker-select';
import { fontFamily } from '../../theme/Fonts';
import {
    actionGetJob,
    actionSearchResume,
    actionApplyJob,
    actionGetPopup,
    actionAppliedJob,
    actionGetHotJob,
    actionGetLatestJob,
    actionGetAppliedJob,
} from './duck/actions';
import { actionGetAccountInfo } from '../Profile/duck/actions';
import { actionBalanceWallet } from '../Wallet/duck/actions';
import { connect } from 'react-redux';
import { LineChart } from 'react-native-chart-kit';
import { getFirstPopUp, setFirstPopUp } from '../../services/persistence/store.config';
import Modal from 'react-native-modal';

const { width } = Dimensions.get('window');

const IndexHomeContainer = (props) => {
    const { t } = useTranslation();
    const { Layout, Gutters, Fonts, Images } = useTheme();
    const navigation = useNavigation();
    const limit = 5;
    const {
        walletReducer,
        authReducer,
        profileReducer,
        notificationReducer,
        actionGetAccountInfo,
        actionGetJob,
        actionGetHotJob,
        actionGetLatestJob,
        actionGetAppliedJob,
        actionSearchResume,
        actionBalanceWallet,
        actionApplyJob,
        actionGetPopup,
        actionAppliedJob,
        homeReducer
    } = props || {};
    const { user } = authReducer || {};
    const { accountInfo } = profileReducer || {};
    const { isHasNew, isLoading } = notificationReducer || {};
    // const { coinDeHRCache } = walletReducer || 0
    const [role, setRole] = useState('member');

    const [filter, setFilter] = useState(0);
    const [job, setJob] = useState(null);
    const [isJobPoster, setIsJobPoster] = useState(false);
    const [listJob, setListJob] = useState([]);
    const [listResume, setListResume] = useState([]);
    const [listJobHot, setListJobHot] = useState([]);
    const [listJobLatest, setListJobLatest] = useState([]);
    const [listJobApplied, setListJobApplied] = useState([]);
    const [listPopup, setListPopUp] = useState([]);
    const [homePopup, setHomePopUp] = useState({});
    const [listApply, setListApply] = useState([]);
    const [listViewed, setListViewed] = useState([]);
    const [listInterview, setListInterview] = useState([]);
    const [isShowPopup, setisShowPopUp] = useState(false);
    const [onEndReachedCalledDuringMomentum, setOnEndReachedCalledDuringMomentum] = useState(true);

    const [currentLatestPage, setCurrentLatestPage] = useState(1);
    const [currentHotPage, setCurrentHotPage] = useState(1);
    const [currentAppliedPage, setCurrentAppliedPage] = useState(1);

    let modalRef = useRef();

    useEffect(() => {
        (async () => {
           
            actionGetAccountInfo((user) => {
                setRole(user.role);
                if (role == 'pro') {
                    actionSearchResume({}, (listResume) => {
                        setListResume(listResume);
                        setListApply(listResume);
                        setListViewed(listResume);
                        setListInterview(listResume);
                    });
                }
            });
   
        })();
        return () => {};
    }, []);

    useEffect(() => {
        switch (filter) {
            case 1:
                if (_isEmpty(_get(listJobLatest, 'data', [])) === true) {
                    getLatestJob();
                }
                break;
            case 2:
                if (_isEmpty(_get(listJobApplied, 'data', [])) === true) {
                    getAppliedJob();
                }
                break;
            default:
                if (_isEmpty(_get(listJobHot, 'data', [])) === true) {
                    getHotJob();
                }
                break;
        }
    }, [filter]);

    const getHotJob = (page = currentHotPage) => {
        actionGetHotJob({ page, limit }, (listJobs) => {
            const result = {
                ...listJobs,
                data: [..._get(listJobHot, 'data', []), ..._get(listJobs, 'data', [])],
            };
            setCurrentHotPage(listJobs.page);
            setListJobHot(result);
        });
    };

    const getLatestJob = (page = currentLatestPage) => {
        actionGetLatestJob({ page, limit }, (listJobs) => {
            const result = {
                ...listJobs,
                data: [..._get(listJobLatest, 'data', []), ..._get(listJobs, 'data', [])],
            };
            setCurrentLatestPage(listJobs.page);
            setListJobLatest(result);
        });
    };

    const getAppliedJob = (page = currentAppliedPage) => {
        actionGetAppliedJob({ page, limit }, (listJobs) => {
            const result = {
                ...listJobs,
                data: [..._get(listJobApplied, 'data', []), ..._get(listJobs, 'data', [])],
            };
            currentAppliedPage(listJobs.page);
            setListJobApplied(result);
        });
    };

    const onGoPeopleDetail = (userProfile) => {
        navigation.navigate(SCREEN.PEOPLE_DETAIL_PAGE, { userProfile });
    };

    const renderAvatar = () => {
        return (
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
                <Image
                    style={styles.avatar}
                    source={accountInfo.avatar ? { uri: BASE_IMAGE_URL + accountInfo.avatar } : Images.profile.ic_avatar}
                />
            </TouchableOpacity>
        );
    };

    const renderTitle = () => {
        return (
            <View>
                <UIText font="medium" style={styles.txtWallet} text={`${t('home.welcome_home')},`} />
                <UIText
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    font="bold"
                    style={styles.txtUsername}
                    text={user.username || 'Anthony Pham'}
                />
            </View>
        );
    };

    const renderNotiIcon = () => {
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <TouchableOpacity onPress={() => navigation.navigate(SCREEN.PROFILE_VIEW_PAGE)}>
                    <View style={styles.redDot} />
                    <Image style={[styles.icNoti, { marginRight: verticalScale(6) }]} source={Images.home.ic_email_2} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate(SCREEN.NOTIFICATION)}>
                    {isHasNew && <View style={[styles.redDot, { right: 6 }]} />}
                    <Image style={styles.icNoti} source={Images.wallet.ic_noti} />
                </TouchableOpacity>
            </View>
        );
    };

    const renderJob = ({ item, index }) => {
        return (
            <UIJobComponent
                onPress={() => onPressJob(item)}
                onPressAvatar={() => onGoPeopleDetail(item)}
                type={item.type}
                location={item.company.address}
                position={item.name}
                companyName={item.company.name}
                logo={item.company.imgLandscape}
                matched={75}
                index={index}
                date={item.createdAt}
                ableToApply
            />
        );
    };

    const renderJobPosted = ({ item, index }) => {
        return (
            <View style={{ marginRight: verticalScale(20) }}>
                <UIJobPosted
                    onPress={() => onPressJobPoster(item)}
                    containerStyle={{ width: width - verticalScale(70) }}
                    logo={item.company.imgLandscape}
                    position={item.name}
                    companyName={item.company.name}
                    des={item.description}
                    time={item.expired_date}
                    people={(item.job_apply && item.job_apply.length) || 0}
                />
            </View>
        );
    };

    const renderMatchPeople = ({ item, index }) => {
        return (
            <View style={{ marginRight: verticalScale(20) }}>
                <UIPeopleComponent
                    onPressAvatar={() => onGoPeopleDetail(item)}
                    isMatch
                    containerStyle={{ width: width - verticalScale(70) }}
                    stylesDes={{ maxWidth: width - verticalScale(140) }}
                    avatar={item.user_id && item.user_id.avatar_url}
                    ratingsSumary={item.ratingsSumary ?? 0}
                    name={item.username}
                    position={item.intro.headline}
                    location={item.intro.location}
                    price={item.desired_salary}
                    exp={item.years_of_experience}
                />
            </View>
        );
    };

    const renderHiringPeople = ({ item, index }) => {
        return (
            <UIPeopleComponent
                containerStyle={{ backgroundColor: Colors.white }}
                stylesDes={{ maxWidth: width - verticalScale(190) }}
                onPressAvatar={() => onGoPeopleDetail(item)}
                isHiring
                ratingsSumary={item.ratingsSumary ?? 0}
                avatar={item.user_id && item.user_id.avatar_url}
                name={item.username}
                position={item.intro.headline}
                location={item.intro.location}
                price={item.desired_salary}
                exp={item.years_of_experience}
            />
        );
    };

    const renderButton = () => {
        return (
            <View style={styles.touchableOpacityStyle}>
                <TouchableOpacity activeOpacity={0.7} onPress={() => navigation.navigate(SCREEN.LIST_POPUP_PAGE, { listPopUp: listPopup })}>
                    <Image source={Images.home.ic_farm} style={styles.floatingButtonStyle} />
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.7} onPress={() => navigation.navigate(SCREEN.BEST_FOR_YOU)}>
                    <Image source={Images.home.ic_ess} style={styles.floatingButtonStyle} />
                </TouchableOpacity>
            </View>
        );
    };

    const renderSearch = () => {
        return (
            <View>
                <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    colors={[Colors.blueDark, Colors.blue]}
                    style={styles.fastSearchContainer}
                >
                    <UIText font="bold" style={styles.txtFastSearch} text={t('home.fast_search')} />
                    <UIText font="regular" style={styles.txtFastSearchDes} text={t('home.fast_search_des')} />
                    <TouchableOpacity onPress={() => navigation.navigate(SCREEN.SEARCH_JOB)}>
                        <UISearchInput editable={false} containerStyle={styles.searchInput} />
                    </TouchableOpacity>
                    <View style={[Layout.fullWidth, Layout.alignItemsCenter]}>
                        <TouchableOpacity onPress={() => navigation.navigate(SCREEN.SEARCH_ADVANCE)}>
                            <View style={styles.viewSearch}>
                                <Image style={styles.icSearch} source={Images.home.ic_search_advance} resizeMode="cover" />
                                <UIText font="bold" style={styles.txtSearchAdvance} text={t('search.advance')} />
                            </View>
                        </TouchableOpacity>
                    </View>
                </LinearGradient>
            </View>
        );
    };

    const renderChart = () => {
        return hasWallet == true ? (
            <ImageBackground source={Images.home.bg_chart} style={styles.containerChart} imageStyle={{ borderRadius: 16 }}>
                <LineChart
                    bezier
                    data={chart}
                    width={width - verticalScale(30)}
                    height={verticalScale(260)}
                    yAxisLabel=""
                    yAxisSuffix=" DeHR"
                    chartConfig={{
                        backgroundColor: 'transparent',
                        backgroundGradientTo: 'transparent',
                        backgroundGradientFromOpacity: 0,
                        backgroundGradientFrom: 'transparent',
                        backgroundGradientToOpacity: 0,
                        decimalPlaces: 0,
                        color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        style: {
                            borderRadius: 16,
                        },
                        propsForDots: {
                            r: '4',
                            strokeWidth: '2',
                            stroke: '#93ff00',
                        },
                        propsForLabels: {
                            fontSize: moderateScale(9),
                        },
                    }}
                    style={{
                        borderRadius: 16,
                    }}
                    verticalLabelRotation={20}
                    onDataPointClick={}
                    formatXLabel={(label) => label.toUpperCase()}
                />
                <View style={[Layout.column, styles.containerTextChart]}>
                    <Image style={styles.logoDeHR} source={Images.logo} resizeMode="cover" />
                    <View style={[Layout.row, styles.containerTextChartTotal]}>
                        <UIText font="bold" style={styles.txtDeHR} text={coinDeHR} />
                    </View>
                </View>
            </ImageBackground>
        ) : (
            <View />
        );
    };

    const onPressJobPoster = (item) => {
        setIsJobPoster(true);
        setJob(item);
        modalRef.open();
    };

    const onPressJob = (item) => {
        setIsJobPoster(false);
        setJob(item);
        modalRef.open();
    };

    const applyJob = () => {
        actionApplyJob({
            data: {
                job: job._id,
            },
        });
        modalRef.close();
    };

    const _renderHeadPage = () => (
        <>
            <View style={{ marginHorizontal: -verticalScale(16), width: '100%' }}>
                <UIHeader
                    centerStyle={styles.center}
                    leftComponent={renderAvatar}
                    centerComponent={renderTitle}
                    rightComponent={renderNotiIcon}
                />
            </View>
            {renderSearch()}
            {_renderJobCategory()}
        </>
    );

    const _renderJobCategory = () => (
        <View style={{ flexDirection: 'row', marginBottom: verticalScale(20) }}>
            <View style={{ flexDirection: 'row' }}>
                <UIButtonChoose onPress={() => setFilter(0)} isChoose={filter == 0} text={t('home.hot')} />
                <UIButtonChoose onPress={() => setFilter(1)} isChoose={filter == 1} icon={Images.home.ic_popular} text={t('home.latest')} />
                <UIButtonChoose onPress={() => setFilter(2)} isChoose={filter == 2} text={t('home.applied')} />
            </View>
            <TouchableOpacity onPress={() => navigation.navigate(SCREEN.ALL_JOB_PAGE)} style={{ flexGrow: 1, alignItems: 'flex-end', justifyContent: 'center' }}>
                <UIText style={{ color: Colors.lightGray, fontSize: moderateScale(12), textTransform: 'capitalize' }}>{t('home.see_all')}</UIText>
            </TouchableOpacity>
        </View>
    );

    const _renderFooterPage = () => (
        <>{homeReducer.isLoading && <View style={{ padding: verticalScale(20) }}><ActivityIndicator /></View>}</>
    )

    return (
        <View style={styles.container}>
            <View contentContainerStyle={styles.contentContainer}>
                {/* <View style={styles.container}> */}
                {/* <UIHeader
                        centerStyle={styles.center}
                        leftComponent={renderAvatar}
                        centerComponent={renderTitle}
                        rightComponent={renderNotiIcon}
                    /> */}
                {/* {role == 'pro' ? (
                        <View style={styles.contentContainer}>
                            {renderSearch()}
                            <View style={[styles.row, { marginBottom: verticalScale(10) }]}>
                                <UIText font="medium" style={styles.txtSeeAll} text={t('home.job_posted')} />
                                <TouchableOpacity onPress={() => navigation.navigate(SCREEN.JOB_POSTED, { listJob: listJob })}>
                                    <UIText font="medium" style={styles.txtSeeAll} text={t('home.see_all')} />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <FlatList
                                    horizontal
                                    data={listJob}
                                    extraData={listJob}
                                    renderItem={renderJobPosted}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                            <View style={[styles.row, { marginTop: verticalScale(20), marginBottom: verticalScale(10) }]}>
                                <UIText font="medium" style={styles.txtSeeAll} text={t('home.people_match')} />
                                <TouchableOpacity onPress={() => navigation.navigate(SCREEN.PEOPLE_MATCHED)}>
                                    <UIText font="medium" style={styles.txtSeeAll} text={t('home.see_all')} />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <FlatList
                                    horizontal
                                    data={listResume}
                                    extraData={listResume}
                                    renderItem={renderMatchPeople}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                            <View style={[styles.row, { marginTop: verticalScale(20), marginBottom: verticalScale(10) }]}>
                                <UIText font="medium" style={styles.txtSeeAll} text={t('home.hiring_process')} />
                                <TouchableOpacity onPress={() => navigation.navigate(SCREEN.HIRING_PROCESS)}>
                                    <UIText font="medium" style={styles.txtSeeAll} text={t('home.see_all')} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.pickerHiring}>
                                <RNPickerSelect
                                    textInputProps={styles.picker}
                                    onValueChange={() => { }}
                                    style={{
                                        inputAndroid: {
                                            color: Colors.blueDark,
                                            width: '100%',
                                            height: verticalScale(50),
                                            paddingTop: verticalScale(10),
                                            lineHeight: verticalScale(20),
                                        },
                                    }}
                                    useNativeAndroidPickerStyle
                                    items={[
                                        { label: 'IT', value: 'IT' },
                                        { label: 'Software', value: 'Software' },
                                        { label: 'Senior Sales & Marketing', value: 'Senior Sales & Marketing' },
                                    ]}
                                />
                            </View>
                            <View style={{ flex: 1 }}>
                                <ScrollView horizontal nestedScrollEnabled contentContainerStyle={{ flexGrow: 1 }}>
                                    <View style={styles.contentHiringContainer}>
                                        <View style={styles.contentContainerItem}>
                                            <UIText
                                                style={styles.txtName}
                                                font="bold"
                                                text={t('home.applied_count', { count: listApply.length || 0 })}
                                            />
                                            <FlatList data={listApply} extraData={listApply} renderItem={renderHiringPeople} />
                                        </View>
                                        <View style={styles.contentContainerItem}>
                                            <UIText
                                                style={styles.txtName}
                                                font="bold"
                                                text={t('home.viewed_count', { count: listViewed.length || 0 })}
                                            />
                                            <FlatList data={listViewed} extraData={listViewed} renderItem={renderHiringPeople} />
                                        </View>
                                        <View style={styles.contentContainerItem}>
                                            <UIText
                                                style={styles.txtName}
                                                font="bold"
                                                text={t('home.interview_count', { count: listInterview.length || 0 })}
                                            />
                                            <FlatList data={listInterview} extraData={listInterview} renderItem={renderHiringPeople} />
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                    ) : (
                    )} */}
                <>
                    {filter == 0 && (
                        <FlatList
                            contentContainerStyle={{ paddingHorizontal: verticalScale(20), paddingBottom: verticalScale(15) }}
                            nestedScrollEnabled
                            data={listJobHot.data}
                            extraData={listJobHot.data}
                            renderItem={renderJob}
                            keyExtractor={(item, index) => index.toString()}
                            onMomentumScrollBegin={() => setOnEndReachedCalledDuringMomentum(false)}
                            onEndReached={() => {
                                if (!onEndReachedCalledDuringMomentum && currentHotPage < listJobHot.totalPages) {
                                    getHotJob(currentHotPage + 1);
                                    setOnEndReachedCalledDuringMomentum(true);
                                }
                            }}
                            onEndReachedThreshold={0.5}
                            ListHeaderComponent={_renderHeadPage()}
                            ListFooterComponent={_renderFooterPage()}
                        />
                    )}
                    {filter == 1 && (
                        <FlatList
                            contentContainerStyle={{ paddingHorizontal: verticalScale(20), paddingBottom: verticalScale(15) }}
                            nestedScrollEnabled
                            data={listJobLatest.data}
                            extraData={listJobLatest.data}
                            renderItem={renderJob}
                            keyExtractor={(item, index) => index.toString()}
                            onMomentumScrollBegin={() => setOnEndReachedCalledDuringMomentum(false)}
                            onEndReached={() => {
                                if (!onEndReachedCalledDuringMomentum && currentLatestPage < listJobLatest.totalPages) {
                                    getLatestJob(currentLatestPage + 1);
                                    setOnEndReachedCalledDuringMomentum(true);
                                }
                            }}
                            onEndReachedThreshold={0.5}
                            ListHeaderComponent={_renderHeadPage()}
                            ListFooterComponent={_renderFooterPage()}
                        />
                    )}
                    {filter == 2 && (
                        <FlatList
                            contentContainerStyle={{ paddingHorizontal: verticalScale(20), paddingBottom: verticalScale(15) }}
                            nestedScrollEnabled
                            data={listJobApplied.data}
                            extraData={listJobApplied.data}
                            renderItem={renderJob}
                            keyExtractor={(item, index) => index.toString()}
                            onMomentumScrollBegin={() => setOnEndReachedCalledDuringMomentum(false)}
                            onEndReached={() => {
                                if (!onEndReachedCalledDuringMomentum && currentAppliedPage < listJobApplied.totalPages) {
                                    getAppliedJob(currentAppliedPage + 1);
                                    setOnEndReachedCalledDuringMomentum(true);
                                }
                            }}
                            onEndReachedThreshold={0.5}
                            ListHeaderComponent={_renderHeadPage()}
                            ListFooterComponent={_renderFooterPage()}
                        />
                    )}
                </>
                {/* </View> */}
                <UIModal ref={(ref) => (modalRef = ref)}>
                    <JobDetail isJobPosted={isJobPoster} onPress={applyJob} job={job} />
                </UIModal>
                <Modal animationIn="fadeIn" onBackdropPress={() => setisShowPopUp(false)} animationInTiming={0} isVisible={isShowPopup}>
                    <Popup popup={homePopup} onPress={() => setisShowPopUp(false)} />
                </Modal>
            </View>
            {listPopup.length > 0 && renderButton()}
        </View>
    );
};

const mapStateToProps = (state) => ({
    walletReducer: state.walletReducer,
    authReducer: state.authReducer,
    profileReducer: state.profileReducer,
    notificationReducer: state.notificationReducer,
    homeReducer: state.homeReducer,
});
export default connect(mapStateToProps, {
    actionGetAccountInfo,
    actionGetJob,
    actionGetHotJob,
    actionGetLatestJob,
    actionGetAppliedJob,
    actionSearchResume,
    actionApplyJob,
    actionBalanceWallet,
    actionGetPopup,
    actionAppliedJob,
})(IndexHomeContainer);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
        flexDirection: 'column',
        // paddingHorizontal: verticalScale(16),
    },
    redDot: {
        width: verticalScale(10),
        height: verticalScale(10),
        backgroundColor: 'red',
        position: 'absolute',
        borderRadius: verticalScale(10),
        right: verticalScale(12),
        top: verticalScale(6),
        zIndex: 1,
    },
    containerChart: {
        flex: 1,
        width: null,
        height: null,
    },
    avatar: {
        width: verticalScale(40),
        height: verticalScale(40),
        borderRadius: verticalScale(20),
    },
    containerTextChart: {
        position: 'absolute',
        left: verticalScale(70),
        top: verticalScale(20),
    },
    containerTextChartTotal: {
        height: verticalScale(45),
        marginTop: verticalScale(15),
    },
    logoDeHR: {
        width: verticalScale(80),
        height: verticalScale(25),
    },
    logoDeHRSmall: {
        marginStart: verticalScale(6),
        width: verticalScale(40),
        height: verticalScale(12),
    },
    txtWallet: {
        fontSize: moderateScale(12),
        color: Colors.title,
        lineHeight: verticalScale(18),
        textTransform: 'uppercase',
    },
    txtName: {
        fontSize: verticalScale(18),
        color: Colors.blueDark,
        marginTop: verticalScale(8),
        marginBottom: verticalScale(8),
    },
    txtUsername: {
        width: verticalScale(220),
        fontSize: moderateScale(24),
        color: Colors.title,
    },
    center: {
        alignItems: 'flex-start',
        marginLeft: verticalScale(12),
    },
    icNoti: {
        width: verticalScale(36),
        height: verticalScale(36),
    },
    contentContainer: {},
    contentHiringContainer: {
        flexDirection: 'row',
        flex: 1,
    },
    contentContainerItem: {
        marginTop: verticalScale(20),
        marginEnd: verticalScale(16),
        paddingHorizontal: verticalScale(16),
        width: width - verticalScale(80),
        backgroundColor: Colors.grayHiring,
    },
    fastSearchContainer: {
        width: '100%',
        padding: verticalScale(20),
        borderRadius: verticalScale(10),
        marginTop: verticalScale(20),
        marginBottom: verticalScale(20),
    },
    txtDeHR: {
        fontSize: moderateScale(24),
        color: Colors.white,
    },
    txtDeHRPercent: {
        marginStart: verticalScale(6),
        fontSize: moderateScale(12),
        color: '#00e626',
    },
    txtDeHRCoin: {
        fontSize: moderateScale(14),
        color: Colors.white,
    },
    txtFastSearch: {
        fontSize: moderateScale(18),
        color: Colors.white,
    },
    txtFastSearchDes: {
        fontSize: moderateScale(14),
        color: '#9fa4be',
    },
    searchInput: {
        marginTop: verticalScale(15),
        marginBottom: verticalScale(5),
    },
    picker: {
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
    },
    pickerContainer: {
        width: '100%',
        height: verticalScale(44),
        backgroundColor: Colors.white,
        borderRadius: verticalScale(6),
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: verticalScale(15),
        marginTop: verticalScale(15),
    },
    btnSearch: {
        marginTop: verticalScale(15),
        height: verticalScale(44),
        marginBottom: verticalScale(5),
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    txtSeeAll: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
    },
    viewSearch: {
        flexDirection: 'row',
        width: verticalScale(200),
        height: verticalScale(40),
        marginTop: verticalScale(10),
        borderRadius: verticalScale(10),
        backgroundColor: Colors.bgSearch,
        justifyContent: 'center',
        alignItems: 'center',
    },
    txtSearchAdvance: {
        fontSize: moderateScale(14),
        color: Colors.white,
    },
    icSearch: {
        width: verticalScale(30),
        height: verticalScale(30),
        color: Colors.white,
    },
    pickerHiring: {
        backgroundColor: Colors.white,
        height: verticalScale(44),
        borderRadius: verticalScale(10),
        borderColor: Colors.border,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: verticalScale(15),
        marginBottom: verticalScale(15),
    },
    touchableOpacityStyle: {
        position: 'absolute',
        width: verticalScale(70),
        height: verticalScale(70),
        alignItems: 'center',
        justifyContent: 'center',
        right: verticalScale(14),
        bottom: verticalScale(32),
    },
    floatingButtonStyle: {
        resizeMode: 'contain',
        width: verticalScale(60),
        height: verticalScale(60),
    },
});
