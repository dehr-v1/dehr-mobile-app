import React, { useEffect, useRef, useState } from 'react';
import { useNavigation } from '@react-navigation/core';
import { useTranslation } from 'react-i18next';
import { ActivityIndicator, FlatList, StyleSheet, View } from 'react-native';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';

import { useTheme } from '@/theme';
import { UIHeader, UIModal } from '../../../components/common';
import { moderateScale, verticalScale } from '../../../utils';
import { Colors } from '../../../theme/Variables';
import { connect } from 'react-redux';
import { actionGetHotJob, actionGetLatestJob, actionGetAppliedJob, actionApplyJob } from './../duck/actions';
import { UIButtonChoose } from '../../../components/elements';
import { JobDetail, UIJobComponent } from '../components';

const ListJobCards = ({ data = [], renderItem, onLoadMore, ListFooterComponent }) => {
    const [onEndReachedCalledDuringMomentum, setOnEndReachedCalledDuringMomentum] = useState(true);

    return (
        <FlatList
            contentContainerStyle={{ paddingHorizontal: verticalScale(20), paddingBottom: verticalScale(15) }}
            data={data}
            extraData={data}
            renderItem={renderItem}
            keyExtractor={(item, index) => index.toString()}
            onMomentumScrollBegin={() => setOnEndReachedCalledDuringMomentum(false)}
            onEndReached={() => {
                if (!onEndReachedCalledDuringMomentum) {
                    onLoadMore();
                    setOnEndReachedCalledDuringMomentum(true);
                }
            }}
            onEndReachedThreshold={0}
            ListFooterComponent={ListFooterComponent}
        />
    );
};

const AllJobPage = (props) => {
    const { actionGetHotJob, actionGetLatestJob, actionGetAppliedJob, actionApplyJob } = props;
    const navigation = useNavigation();
    const { t } = useTranslation();
    const { Images } = useTheme();
    const limit = 5;

    const [filter, setFilter] = useState(0);
    const [listJobHot, setListJobHot] = useState([]);
    const [listJobLatest, setListJobLatest] = useState([]);
    const [listJobApplied, setListJobApplied] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [currentLatestPage, setCurrentLatestPage] = useState(1);
    const [currentHotPage, setCurrentHotPage] = useState(1);
    const [currentAppliedPage, setCurrentAppliedPage] = useState(1);
    const [isJobPoster, setIsJobPoster] = useState(false);
    const [job, setJob] = useState(null);

    let modalRef = useRef();

    useEffect(() => {
        switch (filter) {
            case 1:
                if (_isEmpty(_get(listJobLatest, 'data', [])) === true) {
                    getLatestJob();
                }
                break;
            case 2:
                if (_isEmpty(_get(listJobApplied, 'data', [])) === true) {
                    getAppliedJob();
                }
                break;
            default:
                if (_isEmpty(_get(listJobHot, 'data', [])) === true) {
                    getHotJob();
                }
                break;
        }
    }, [filter]);

    const getHotJob = (page = currentHotPage) => {
        setIsLoading(true);
        actionGetHotJob({ page, limit }, (listJobs) => {
            const result = {
                ...listJobs,
                data: [..._get(listJobHot, 'data', []), ..._get(listJobs, 'data', [])],
            };
            setCurrentHotPage(listJobs.page);
            setListJobHot(result);
            setIsLoading(false);
        });
    };

    const getLatestJob = (page = currentLatestPage) => {
        setIsLoading(true);
        actionGetLatestJob({ page, limit }, (listJobs) => {
            const result = {
                ...listJobs,
                data: [..._get(listJobLatest, 'data', []), ..._get(listJobs, 'data', [])],
            };
            setCurrentLatestPage(listJobs.page);
            setListJobLatest(result);
            setIsLoading(false);
        });
    };

    const getAppliedJob = (page = currentAppliedPage) => {
        setIsLoading(true);
        actionGetAppliedJob({ page, limit }, (listJobs) => {
            const result = {
                ...listJobs,
                data: [..._get(listJobApplied, 'data', []), ..._get(listJobs, 'data', [])],
            };
            currentAppliedPage(listJobs.page);
            setListJobApplied(result);
            setIsLoading(false);
        });
    };

    const _renderFooterPage = () => (
        <>
            {isLoading && (
                <View style={{ padding: 20 }}>
                    <ActivityIndicator />
                </View>
            )}
        </>
    );

    const renderJob = ({ item, index }) => {
        return (
            <UIJobComponent
                onPress={() => onPressJob(item)}
                type={item.type}
                location={item.company.address}
                position={item.name}
                companyName={item.company.name}
                logo={item.company.imgLandscape}
                index={index}
                date={item.createdAt}
                matched={75}
            />
        );
    };

    const onPressJob = (item) => {
        setIsJobPoster(false);
        setJob(item);
        modalRef.open();
    };

    const applyJob = () => {
        actionApplyJob({
            data: {
                job: job._id,
            },
        });
        modalRef.close();
    };

    return (
        <View style={{ backgroundColor: Colors.white, flex: 1 }}>
            <UIHeader onPress={() => navigation.goBack()} LeftIcon="back_invert" title={t('home.jobs')} titleStyle={styles.txtTitle} />
            <View style={{ flexDirection: 'row', marginBottom: verticalScale(20), paddingHorizontal: verticalScale(16) }}>
                <View style={{ flexDirection: 'row' }}>
                    <UIButtonChoose onPress={() => setFilter(0)} isChoose={filter == 0} text={t('home.hot')} />
                    <UIButtonChoose
                        onPress={() => setFilter(1)}
                        isChoose={filter == 1}
                        icon={Images.home.ic_popular}
                        text={t('home.latest')}
                    />
                    <UIButtonChoose onPress={() => setFilter(2)} isChoose={filter == 2} text={t('home.applied')} />
                </View>
            </View>
            {filter == 0 && (
                <ListJobCards
                    data={listJobHot.data}
                    renderItem={renderJob}
                    onLoadMore={() => currentHotPage < listJobHot.totalPages && getHotJob(currentHotPage + 1)}
                    ListFooterComponent={_renderFooterPage()}
                />
            )}
            {filter == 1 && (
                <ListJobCards
                    data={listJobLatest.data}
                    renderItem={renderJob}
                    onLoadMore={() => currentLatestPage < listJobLatest.totalPages && getLatestJob(currentLatestPage + 1)}
                    ListFooterComponent={_renderFooterPage()}
                />
            )}
            {filter == 2 && (
                <ListJobCards
                    data={listJobApplied.data}
                    renderItem={renderJob}
                    onLoadMore={() => currentAppliedPage < listJobApplied.totalPages && getAppliedJob(currentAppliedPage + 1)}
                    ListFooterComponent={_renderFooterPage()}
                />
            )}
            <UIModal ref={(ref) => (modalRef = ref)}>
                <JobDetail isJobPosted={isJobPoster} onPress={applyJob} job={job} />
            </UIModal>
        </View>
    );
};

const styles = StyleSheet.create({
    txtTitle: {
        fontSize: moderateScale(20),
        color: Colors.title,
    },
});

const mapStateToProps = (state) => ({
    walletReducer: state.walletReducer,
    authReducer: state.authReducer,
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {
    actionGetHotJob,
    actionGetLatestJob,
    actionGetAppliedJob,
    actionApplyJob,
})(AllJobPage);
