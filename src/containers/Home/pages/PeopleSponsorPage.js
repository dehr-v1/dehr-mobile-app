import React from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { UIHeader } from '../../../components/common';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import { Colors } from '../../../theme/Variables';
import { UIPeopleSponsor } from '../components';
import { verticalScale } from '../../../utils/ScalingUtils';

const people = [{}, {}, {}, {}];

const PeopleSponserPage = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();

    const renderClass = ({ item, index }) => {
        return <UIPeopleSponsor />;
    };

    return (
        <View style={styles.container}>
            <UIHeader LeftIcon="back" onPress={() => navigation.goBack()} title={t('home.people_sponsor')} />
            <View style={styles.contentContainer}>
                <FlatList data={people} extraData={people} renderItem={renderClass} />
            </View>
        </View>
    );
};

export default PeopleSponserPage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
        alignItems: 'center',
    },
    contentContainer: {
        paddingHorizontal: verticalScale(16),
    },
});
