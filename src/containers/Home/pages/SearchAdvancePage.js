import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Image, ScrollView, TouchableOpacity } from 'react-native';
import { UIHeader } from '../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../theme/Variables';
import { useNavigation, useRoute } from '@react-navigation/native';
import { verticalScale } from '../../../utils/ScalingUtils';
import RNPickerSelect from 'react-native-picker-select';
import { UIButton, UIText, UIRadio } from '../../../components/elements';
import { fontFamily } from '../../../theme/Fonts';
import { useTheme } from '@/theme';
import { connect } from 'react-redux';
import { actionSearchConfig, actionSearchAdvance } from '../duck/actions';
import Slider from '@react-native-community/slider';
import { SCREEN } from '../../../constant';
import { CommonUtils } from '../../../utils';

const SearchAdvancePage = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { params } = useRoute() || {};
    const { Images, Layout } = useTheme();
    const { actionSearchConfig, actionSearchAdvance } = props || {};
    const [listConfig, setListConfig] = useState({});
    const [sortBy, setSortBy] = useState(0);
    const [datePosted, setDatePosted] = useState('any');
    const [industry, setIndustry] = useState('');
    const [listIndustry, setListIndustry] = useState([]);
    const [experience, setExperience] = useState('');
    const [listExperience, setListExperience] = useState([]);
    const [jobType, setJobType] = useState('');
    const [listJobType, setListJobType] = useState([]);
    const [location, setLocation] = useState('');
    const [listLocation, setListLocation] = useState([]);
    const [salary, setSalary] = useState('');
    const [listSalary, setListSalary] = useState([]);
    const [contactPrice, setContactPrice] = useState(0);

    useEffect(() => {
        actionSearchConfig({}, (listConfig) => {
            setListConfig(listConfig);
            initConfig(listConfig);
        });
    }, []);

    const initConfig = (config) => {
        setSortBy(0)
        setDatePosted('any')
        //Industry
        setIndustry(config.industry.default)
        setListIndustry(config.industry.values)
        //Level
        setExperience(config.levelRequired.default)
        const experienceNew = [];
        config.levelRequired.values.map((item) => {
            experienceNew.push({ label: item, value: item })
        });
        setListExperience(experienceNew)
        //Type
        setJobType(config.type.default)
        const jobTypeNew = [];
        config.type.values.map((item) => {
            jobTypeNew.push({ label: item, value: item })
        });
        setListJobType(jobTypeNew)
        //Salary
        setSalary(config.salaryRange.default)
        const salaryRangeNew = [];
        config.salaryRange.values.map((item) => {
            salaryRangeNew.push({ label: item, value: item })
        });
        setListSalary(salaryRangeNew)
        //Location
        setLocation(config.location.default.code)
        setListLocation(config.location.values)
    }

    const onReset = () => {
        initConfig(listConfig);
    };

    const onSearch = () => {
        const data = `sort=${'createdAt:-1'}&industry=${industry}&levelRequired=${experience}&type=${jobType}&location=${location}&salaryRange=${salary}&time=${datePosted}`
        actionSearchAdvance({ data }, (listSearch) => {
            navigation.navigate(SCREEN.SEARCH_JOB, { listSearch: listSearch })
        });
    };

    const onPressIndustry = () => {
        navigation.navigate(SCREEN.SEARCH_SCREEN, {
            title: t('search.advance_industry'),
            data: listIndustry,
            placeholder: t('search.advance_industry_search'),
            onChoose: (item) => {
                setIndustry(item);
            },
        });
    };

    const onPressLocation = () => {
        navigation.navigate(SCREEN.SEARCH_SCREEN, {
            title: t('search.advance_location'),
            data: listLocation,
            placeholder: t('search.advance_location_search'),
            onChoose: (item) => {
                setLocation(item.name);
            },
        });
    };

    const viewSortBy = () => {
        return <View style={[Layout.row]}>
            <UIRadio text={t('search.advance_relevant')} isCheck={sortBy === 0} onPressRadio={() => setSortBy(0)} />
            <UIRadio text={t('search.advance_recent')} isCheck={sortBy === 1} onPressRadio={() => setSortBy(1)} />
        </View>
    };

    const viewDatePosted = () => {
        return <View>
            <View style={[Layout.row]}>
                <UIRadio text={t('search.advance_date_any_time')} isCheck={datePosted === 'any'} onPressRadio={() => setDatePosted('any')} />
                <UIRadio text={t('search.advance_date_past_month')} isCheck={datePosted === 'month'} onPressRadio={() => setDatePosted('month')} />
            </View>
            <View style={[Layout.row]}>
                <UIRadio text={t('search.advance_date_past_week')} isCheck={datePosted === 'week'} onPressRadio={() => setDatePosted('week')} />
                <UIRadio text={t('search.advance_date_past_24')} isCheck={datePosted === '24h'} onPressRadio={() => setDatePosted('24h')} />
            </View>
        </View>
    };

    return (
        <View style={styles.container}>
            <UIHeader
                LeftIcon="close"
                onPress={() => navigation.goBack()}
                title={t('search.advance_title')}
            />
            <ScrollView contentContainerStyle={styles.scrollContentContainer} style={styles.contentContainer}>
                <UIText font="bold" style={styles.txtGender} text={t('search.advance_sort')} />
                {viewSortBy()}
                <UIText font="bold" style={styles.txtGender} text={t('search.advance_date')} />
                {viewDatePosted()}
                <UIText style={styles.titlePicker} font="bold" text={t('search.advance_industry')} />
                <TouchableOpacity onPress={onPressIndustry}>
                    <View style={styles.pickerContainer}>
                        <UIText text={industry} />
                        <View style={styles.icDownContainer}>
                            <Image style={styles.icDown} source={Images.profile.ic_down} />
                        </View>
                    </View>
                </TouchableOpacity>
                <UIText style={styles.titlePicker} font="bold" text={t('search.advance_experience')} />
                <View style={styles.pickerContainer}>
                    <RNPickerSelect
                        textInputProps={styles.picker}
                        onValueChange={(value) => setExperience(value)}
                        style={{ ...pickerSelectStyles }}
                        useNativeAndroidPickerStyle={false}
                        value={experience}
                        items={listExperience}
                    />
                    <View style={styles.icDownContainer}>
                        <Image style={styles.icDown} source={Images.profile.ic_down} />
                    </View>
                </View>
                <UIText style={styles.titlePicker} font="bold" text={t('search.advance_job_type')} />
                <View style={styles.pickerContainer}>
                    <RNPickerSelect
                        textInputProps={styles.picker}
                        onValueChange={(value) => setJobType(value)}
                        style={{ ...pickerSelectStyles }}
                        useNativeAndroidPickerStyle={false}
                        value={jobType}
                        items={listJobType}
                    />
                    <View style={styles.icDownContainer}>
                        <Image style={styles.icDown} source={Images.profile.ic_down} />
                    </View>
                </View>
                <UIText style={styles.titlePicker} font="bold" text={t('search.advance_location')} />
                <TouchableOpacity onPress={onPressLocation}>
                    <View style={styles.pickerContainer}>
                        <UIText text={location} />
                        <View style={styles.icDownContainer}>
                            <Image style={styles.icDown} source={Images.profile.ic_down} />
                        </View>
                    </View>
                </TouchableOpacity>
                <UIText style={styles.titlePicker} font="bold" text={t('search.advance_salary')} />
                <View style={styles.pickerContainer}>
                    <RNPickerSelect
                        textInputProps={styles.picker}
                        onValueChange={(value) => setSalary(value)}
                        style={{ ...pickerSelectStyles }}
                        useNativeAndroidPickerStyle={false}
                        value={salary}
                        items={listSalary}
                    />
                    <View style={styles.icDownContainer}>
                        <Image style={styles.icDown} source={Images.profile.ic_down} />
                    </View>
                </View>
                <UIText style={styles.titlePicker} font="bold" text={t('search.advance_contact_price')} />
                <Slider
                    style={{ width: '100%', height: verticalScale(60) }}
                    minimumValue={0}
                    maximumValue={100}
                    thumbTintColor={Colors.blueDark}
                    minimumTrackTintColor={Colors.blueDark}
                    maximumTrackTintColor={Colors.disable}
                    onValueChange={value => setContactPrice(value)}
                />
                <View style={[Layout.row, Layout.justifyContentBetween]}>
                    <UIText font="bold" text={CommonUtils.convertPrice(contactPrice) || '0.00'} />
                    <UIText font="bold" text={t('search.advance_max')} />
                </View>
            </ScrollView>
            <View style={styles.btnContainer}>
                <UIButton
                    style={{ width: verticalScale(150) }}
                    type={'BgWhite'}
                    onPress={onReset}
                    text={t('search.advance_reset')}
                />
                <UIButton
                    style={{ width: verticalScale(150) }}
                    onPress={onSearch}
                    text={t('search.advance_search_now')}
                />
            </View>
        </View>
    );
};

const mapStateToProps = (state) => ({
    searchReducer: state.searchReducer,
});

export default connect(mapStateToProps, {
    actionSearchConfig,
    actionSearchAdvance
})(SearchAdvancePage);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    scrollContentContainer: {
        paddingBottom: verticalScale(20),
        flexGrow: 1,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        marginTop: verticalScale(30),
    },
    pickerContainer: {
        height: verticalScale(50),
        width: '100%',
        backgroundColor: Colors.gray,
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(6),
        justifyContent: 'center',
        paddingHorizontal: verticalScale(15),
        marginBottom: verticalScale(15),
    },
    picker: {
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
    },
    icDown: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    icDownContainer: {
        position: 'absolute',
        right: verticalScale(15),
    },
    btnContainer: {
        flexDirection: 'row',
        marginTop: 'auto',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: verticalScale(10),
        borderTopWidth: 1,
        borderTopColor: Colors.border,
        paddingHorizontal: verticalScale(30),
        width: '100%',
    },
    icCheck: {
        width: verticalScale(16),
        height: verticalScale(16),
        tintColor: Colors.blueDark,
    },
    icCheckContainer: {
        width: verticalScale(24),
        height: verticalScale(24),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.gray,
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: verticalScale(6),
    },
    txtCurrentWork: {
        fontSize: verticalScale(16),
        color: Colors.blueDark,
        marginLeft: verticalScale(13),
    },
    currentWorkContainer: {
        flexDirection: 'row',
        marginTop: verticalScale(15),
    },
    dateContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: verticalScale(15),
    },
    inputDateContainer: {
        flex: 0.48,
    },
    inputDescription: {
        width: '100%',
        backgroundColor: Colors.gray,
        height: verticalScale(175),
        borderRadius: verticalScale(6),
        borderWidth: 1,
        borderColor: Colors.border,
        paddingHorizontal: verticalScale(15),
        paddingTop: verticalScale(15),
        paddingBottom: verticalScale(15),
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
        color: Colors.blueDark,
    },
    txtCount: {
        fontSize: verticalScale(14),
        fontFamily: fontFamily.regular,
        color: '#878eae',
        marginTop: verticalScale(10),
    },
    countContainer: {
        width: '100%',
        alignItems: 'flex-end',
    },
    btnMedia: {
        height: verticalScale(42),
    },
    btnBlue: {
        backgroundColor: '#4286cb',
    },
    image: {
        width: verticalScale(60),
        height: verticalScale(60),
    },
    imgContainer: {
        marginTop: verticalScale(20),
        marginRight: verticalScale(20),
    },
    icDelete: {
        width: verticalScale(20),
        height: verticalScale(20),
        borderRadius: verticalScale(10),
    },
    deleteContainer: {
        top: -verticalScale(10),
        right: -verticalScale(10),
        position: 'absolute',
    },
    icDown: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    icDownContainer: {
        position: 'absolute',
        right: verticalScale(15),
    },
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        color: Colors.blueDark,
        width: '100%',
        height: verticalScale(50),
        paddingTop: verticalScale(10),
        lineHeight: verticalScale(20),
    },
    inputAndroid: {
        color: Colors.blueDark,
        width: '100%',
        height: verticalScale(50),
        paddingTop: verticalScale(10),
        lineHeight: verticalScale(20),
    },
});