import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { useTheme } from '@/theme';
import { Colors } from '../../../theme/Variables';
import { UIHeader } from '../../../components/common';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import { navigateAndSimpleReset } from '@/navigators/Root';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { UIButton, UIText } from '../../../components/elements';
import moment from 'moment';
import { connect } from 'react-redux';
import { actionUpGradePro } from '../duck/actions';
import { SCREEN } from '../../../constant';
import { getSeedPhase } from '../../../services/persistence/store.config';

const Summary = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { Images } = useTheme();
    const { actionUpGradePro } = props || {};

    const upgradePro = async () => {
        const getWallet = await getSeedPhase();
        const wallet = JSON.parse(getWallet);
        const address = wallet?.address;
        if (address) {
            actionUpGradePro(() => {
                navigateAndSimpleReset(SCREEN.MAIN);
            });
        } else {
            navigation.navigate('Wallet');
        }
    };

    return (
        <View style={styles.container}>
            <UIHeader LeftIcon="back" onPress={() => navigation.goBack()} title={t('home.summary')} />
            <View style={styles.contentContainer}>
                <Image source={Images.home.bg_package} style={styles.packageContainer} />
                <View style={[styles.row, styles.availableContainer]}>
                    <UIText font="medium" style={styles.txtAvailable} text={t('home.available')} />
                    <View style={styles.row}>
                        <UIText font="bold" style={styles.txtPoint} text="5641" />
                        <Image style={styles.icLogo} source={Images.wallet.ic_logo} />
                    </View>
                </View>
                <View style={[styles.availableContainer]}>
                    <UIText font="medium" style={styles.txtAvailable} text={t('home.summary')} />
                    <View style={[styles.row, styles.dateContainer, { marginTop: verticalScale(15) }]}>
                        <UIText style={styles.txtDate} text={t('home.stake_date')} />
                        <UIText style={styles.txtDate} text={moment(new Date()).format('YYYY-MM-DD hh:mm')} />
                    </View>
                    <View style={[styles.row, styles.dateContainer]}>
                        <UIText style={styles.txtDate} text={t('home.value_date')} />
                        <UIText style={styles.txtDate} text={moment(new Date()).format('YYYY-MM-DD hh:mm')} />
                    </View>
                    <View style={[styles.row, styles.dateContainer]}>
                        <UIText style={styles.txtDate} text={t('home.redemption_date')} />
                        <UIText style={styles.txtDate} text={moment(new Date()).format('YYYY-MM-DD hh:mm')} />
                    </View>
                </View>
                <View style={[styles.row, styles.availableContainer]}>
                    <UIText font="medium" style={styles.txtAvailable} text={t('home.lock_amount')} />
                    <View style={styles.row}>
                        <UIText font="bold" style={styles.txtPoint} text="3264" />
                        <Image style={styles.icLogo} source={Images.wallet.ic_logo} />
                    </View>
                </View>
                <View style={styles.txtBottom}>
                    <UIText style={{ fontSize: moderateScale(12), color: Colors.blueDark }} text={t('home.you_agree')} />
                    <UIText style={{ fontSize: moderateScale(12), color: Colors.blue }} text={t('home.service_agreement')} />
                    <UIButton onPress={() => upgradePro()} style={styles.btn} text={t('home.confirm_purchare')} />
                </View>
            </View>
        </View>
    );
};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {
    actionUpGradePro,
})(Summary);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        flex: 1,
        paddingHorizontal: verticalScale(16),
    },
    packageContainer: {
        width: '100%',
        height: verticalScale(140),
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    icLogo: {
        width: verticalScale(44),
        height: verticalScale(14.5),
        tintColor: Colors.blueDark,
        marginLeft: verticalScale(5),
    },
    availableContainer: {
        borderColor: Colors.border,
        borderWidth: 1,
        paddingHorizontal: verticalScale(16),
        paddingVertical: verticalScale(15),
        justifyContent: 'space-between',
        borderRadius: verticalScale(10),
        marginTop: verticalScale(20),
    },
    txtPoint: {
        fontSize: moderateScale(18),
        color: Colors.blueDark,
    },
    txtAvailable: {
        fontSize: moderateScale(14),
        color: '#5e5e5e',
    },
    dateContainer: {
        justifyContent: 'space-between',
        marginVertical: verticalScale(8),
    },
    txtDate: {
        fontSize: moderateScale(12),
        color: '#5e5e5e',
    },
    txtBottom: {
        marginTop: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: verticalScale(40),
    },
    btn: {
        height: verticalScale(56),
        marginTop: verticalScale(15),
    },
});
