import React, { useState, useRef, useEffect } from 'react';
import { View, StyleSheet, ImageBackground, FlatList } from 'react-native';
import { useTheme } from '@/theme';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { UIHeader, UIModal, UISearchInput } from '../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../theme/Variables';
import { JobDetail, UIJobPosted } from '../components';
import { useNavigation, useRoute } from '@react-navigation/native';
import { actionGetJob } from '../duck/actions';
import { connect } from 'react-redux';

const JobPostedPage = (props) => {
    const { Images } = useTheme();
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { params } = useRoute();
    const { actionGetJob } = props || {};
    const { listJob, jobId } = params || [];
    const [listJobs, setListJob] = useState([]);
    const [job, setJob] = useState(null);

    let modalRef = useRef();

    useEffect(() => {
        if (listJob) {
            setListJob(listJob);
        } else {
            actionGetJob({}, (listJobs) => {
                const allJobs = [...listJobs.popular, ...listJobs.featured, ...listJobs.apply];
                setListJob(allJobs);
                if (jobId != null) {
                    const selectedJob = allJobs.find((item) => item._id === jobId) ?? null;
                    if (selectedJob != null) {
                        setJob(selectedJob);
                        modalRef.open();
                    }
                }
            });
        }
    }, []);

    const onPressJob = (item) => {
        setJob(item);
        modalRef.open();
    };

    const renderJob = ({ item, index }) => {
        return (
            <UIJobPosted
                onPress={() => onPressJob(item)}
                logo={item.company.img_landscape}
                position={item.name}
                companyName={item.company.name}
                des={item.description}
                time={item.expired_date}
                people={(item.job_apply && item.job_apply.length) || 0}
            />
        );
    };

    return (
        <View style={styles.container}>
            <ImageBackground style={styles.bgHeader} resizeMode="stretch" imageStyle={styles.bgHeader} source={Images.chat.bg_chat}>
                <UIHeader
                    onPress={() => navigation.goBack()}
                    RightIcon="more"
                    LeftIcon="back_white"
                    title={t('home.job_posted')}
                    titleStyle={styles.txtTitle}
                />
                <View style={{ paddingHorizontal: verticalScale(16) }}>
                    <UISearchInput />
                </View>
            </ImageBackground>
            <View style={styles.contentContainer}>
                <FlatList data={listJobs} extraData={listJobs} renderItem={renderJob} keyExtractor={(_, index) => index.toString()} />
            </View>
            <UIModal ref={(ref) => (modalRef = ref)}>
                <JobDetail isJobPosted onPress={() => modalRef.close()} job={job} />
            </UIModal>
        </View>
    );
};

const mapStateToProps = (state) => ({
    homeReducer: state.homeReducer,
});
export default connect(mapStateToProps, {
    actionGetJob,
})(JobPostedPage);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    bgHeader: {
        width: '100%',
        height: verticalScale(130),
        borderBottomLeftRadius: verticalScale(15),
        borderBottomRightRadius: verticalScale(15),
    },
    txtTitle: {
        fontSize: moderateScale(20),
        color: Colors.white,
    },
    txtHotJob: {
        color: Colors.white,
        fontSize: moderateScale(12),
    },
    txtHotJobDes: {
        fontSize: moderateScale(12),
        color: '#9fa4be',
    },
    contentContainer: {
        marginTop: verticalScale(20),
        paddingHorizontal: verticalScale(16),
        flex: 1,
        backgroundColor: Colors.white,
    },
});
