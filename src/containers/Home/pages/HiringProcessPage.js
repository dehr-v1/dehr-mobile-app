import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Dimensions, ImageBackground, FlatList } from 'react-native';
import { useTheme } from '@/theme';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { UIHeader } from '../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../theme/Variables';
import { UIPeopleComponent } from '../components';
import { useNavigation } from '@react-navigation/native';
import { fontFamily } from '../../../theme/Fonts';
import RNPickerSelect from 'react-native-picker-select';
import { actionSearchResume } from '../duck/actions';
import { connect } from 'react-redux';
import { ScrollView } from 'react-native-gesture-handler';
import { UIText } from '../../../components/elements';
const { width } = Dimensions.get('window');

const HiringProcessPage = (props) => {
    const { Images } = useTheme();
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { actionSearchResume } = props || {};
    const [listApply, setListApply] = useState([]);
    const [listViewed, setListViewed] = useState([]);
    const [listInterview, setListInterview] = useState([]);

    useEffect(() => {
        actionSearchResume({}, (listResume) => {
            setListApply(listResume);
            setListViewed(listResume);
            setListInterview(listResume);
        });
    }, []);

    const renderPeople = ({ item, index }) => {
        return (
            <UIPeopleComponent
                containerStyle={{ backgroundColor: Colors.white }}
                stylesDes={{ maxWidth: width - verticalScale(200) }}
                isHiring
                avatar={item.user_id && item.user_id.avatar_url}
                name={item.username}
                ratingsSumary={item.ratingsSumary ?? 0}
                position={item.intro.headline}
                location={item.intro.location}
                price={item.desired_salary}
                exp={item.years_of_experience}
            />
        );
    };

    return (
        <View style={styles.container}>
            <ImageBackground style={styles.bgHeader} resizeMode="stretch" imageStyle={styles.bgHeader} source={Images.chat.bg_chat}>
                <UIHeader
                    onPress={() => navigation.goBack()}
                    RightIcon="more"
                    LeftIcon="back_white"
                    title={t('home.hiring_process')}
                    titleStyle={styles.txtTitle}
                />
                <View style={{ paddingHorizontal: verticalScale(16) }}>
                    <View style={styles.pickerContainer}>
                        <RNPickerSelect
                            textInputProps={styles.picker}
                            onValueChange={() => { }}
                            style={{
                                inputAndroid: {
                                    color: Colors.blueDark,
                                    width: '100%',
                                    height: verticalScale(50),
                                    paddingTop: verticalScale(10),
                                    lineHeight: verticalScale(20),
                                },
                            }}
                            useNativeAndroidPickerStyle
                            items={[
                                { label: 'IT', value: 'IT' },
                                { label: 'Software', value: 'Software' },
                                { label: 'Software', value: 'Software' },
                            ]}
                        />
                    </View>
                </View>
            </ImageBackground>
            <ScrollView horizontal nestedScrollEnabled contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.contentContainer}>
                    <View style={styles.contentContainerItem}>
                        <UIText style={styles.txtName} font="bold" text={t('home.applied_count', { count: listApply.length || 0 })} />
                        <FlatList data={listApply} extraData={listApply} renderItem={renderPeople} />
                    </View>
                    <View style={styles.contentContainerItem}>
                        <UIText style={styles.txtName} font="bold" text={t('home.viewed_count', { count: listViewed.length || 0 })} />
                        <FlatList data={listViewed} extraData={listViewed} renderItem={renderPeople} />
                    </View>
                    <View style={styles.contentContainerItem}>
                        <UIText style={styles.txtName} font="bold" text={t('home.interview_count', { count: listInterview.length || 0 })} />
                        <FlatList data={listInterview} extraData={listInterview} renderItem={renderPeople} />
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};
// isChoose = { hiringFilter == 1} onPress = {() => setHiringFilter(1)}
const mapStateToProps = (state) => ({
    homeReducer: state.homeReducer,
});
export default connect(mapStateToProps, {
    actionSearchResume,
})(HiringProcessPage);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    bgHeader: {
        width: '100%',
        height: verticalScale(130),
        borderBottomLeftRadius: verticalScale(15),
        borderBottomRightRadius: verticalScale(15),
    },
    txtTitle: {
        fontSize: moderateScale(20),
        color: Colors.white,
    },
    txtName: {
        fontSize: verticalScale(18),
        color: Colors.blueDark,
        marginTop: verticalScale(8),
        marginBottom: verticalScale(8),
    },
    txtHotJob: {
        color: Colors.white,
        fontSize: moderateScale(12),
    },
    txtHotJobDes: {
        fontSize: moderateScale(12),
        color: '#9fa4be',
    },
    contentContainer: {
        flexDirection: 'row',
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainerItem: {
        marginTop: verticalScale(20),
        marginStart: verticalScale(16),
        paddingHorizontal: verticalScale(16),
        width: width - verticalScale(80),
        backgroundColor: Colors.grayHiring,
    },
    picker: {
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
    },
    pickerContainer: {
        width: '100%',
        height: verticalScale(44),
        backgroundColor: Colors.white,
        borderRadius: verticalScale(6),
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: verticalScale(15),
        marginBottom: verticalScale(10),
    },
});
