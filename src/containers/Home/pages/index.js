import SearchJobPage from './SearchJobPage';
import JobPostedPage from './JobPostedPage';
import PeopleMatchedPage from './PeopleMatchedPage';
import HiringProcessPage from './HiringProcessPage';
import BestOfYouPage from './BestOfYouPage';
import EssDetailPage from './EssDetailPage';
import ClassPage from './ClassPage';
import OrganisationPage from './OrganisationPage';
import PeopleSponserPage from './PeopleSponsorPage';
import ActiveProAccount from './ActiveProAccount';
import Summary from './Summary';
import PeopleDetailPage from './PeopleDetailPage';
import ProfileViewPage from './ProfileViewPage';
import ListPopUpPage from './ListPopUpPage';
import SearchAdvancePage from './SearchAdvancePage';
import AllJobPage from './AllJobPage';

export {
    SearchJobPage,
    SearchAdvancePage,
    JobPostedPage,
    PeopleMatchedPage,
    HiringProcessPage,
    BestOfYouPage,
    EssDetailPage,
    ClassPage,
    OrganisationPage,
    PeopleSponserPage,
    ActiveProAccount,
    Summary,
    PeopleDetailPage,
    ProfileViewPage,
    ListPopUpPage,
    AllJobPage,
};
