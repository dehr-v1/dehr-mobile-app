import React, { useState } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { UIHeader, UIRadioButtom } from '../../../components/common';
import { Colors } from '../../../theme/Variables';
import { useTranslation } from 'react-i18next';
import { useNavigation, useRoute } from '@react-navigation/native';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { UIButton, UIText } from '../../../components/elements';
import { useTheme } from '@/theme';

const EssDetailPage = (props) => {
    const { t } = useTranslation();
    const { Images } = useTheme();
    const navigation = useNavigation();
    const { params } = useRoute() || {};
    const { ess } = params || {};

    return (
        <View style={styles.container}>
            <UIHeader onPress={() => navigation.goBack()} LeftIcon="back" />
            <View style={styles.contentContainer}>
                <UIText style={styles.txtTitle} font="bold" text={ess.title} />
                <Image source={Images.home.bg_ess} style={styles.bg} />
                <UIText style={styles.txtQuestion} text={`${t('home.question')} 1/20`} />
                <UIText font="bold" style={styles.txtQues} text="How meaningful is your work?" />
                <View style={styles.row}>
                    <UIRadioButtom />
                    <UIText style={styles.txtAnw} text="Extremely meaningful" />
                </View>
                <View style={styles.row}>
                    <UIRadioButtom />
                    <UIText style={styles.txtAnw} text="Extremely meaningful" />
                </View>
                <View style={styles.row}>
                    <UIRadioButtom />
                    <UIText style={styles.txtAnw} text="Extremely meaningful" />
                </View>
                <View style={styles.row}>
                    <UIRadioButtom />
                    <UIText style={styles.txtAnw} text="Extremely meaningful" />
                </View>

                <View style={styles.btnContainer}>
                    <UIButton style={styles.btn} type="BgGrey" text={t('home.previous')} />
                    <UIButton style={styles.btn} type="BgBlue" text={t('wallet.next')} />
                </View>
            </View>
        </View>
    );
};

export default EssDetailPage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        marginHorizontal: verticalScale(20),
        flex: 1,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        marginVertical: verticalScale(14),
    },
    txtTitle: {
        fontSize: moderateScale(24),
        color: '#323853',
        lineHeight: moderateScale(28),
    },
    bg: {
        width: '100%',
        height: verticalScale(140),
        marginTop: verticalScale(32),
        marginBottom: verticalScale(15),
        borderRadius: verticalScale(8),
    },
    txtQuestion: {
        fontSize: moderateScale(12),
        color: Colors.lightGray,
        textTransform: 'uppercase',
    },
    txtQues: {
        color: Colors.blueDark,
    },
    txtAnw: {
        fontSize: moderateScale(15),
        color: Colors.blueDark,
        marginLeft: verticalScale(15),
    },
    btn: {
        height: verticalScale(42),
        flex: 0.45,
    },
    btnContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: verticalScale(50),
    },
});
