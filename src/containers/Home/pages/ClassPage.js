import React from 'react';
import { View, StyleSheet, FlatList, Dimensions } from 'react-native';
import { UIHeader } from '../../../components/common';
import { useTranslation } from 'react-i18next';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Colors } from '../../../theme/Variables';
import UIClass from '../components/UIClass';
import { verticalScale } from '../../../utils/ScalingUtils';

const { width } = Dimensions.get('window');

const ClassPage = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { params } = useRoute() || {};
    const { classes } = params || [];

    const renderClass = ({ item, index }) => {
        const { title, times, startDate, price, mentor, progress } = item || {};
        return (
            <View style={{ marginBottom: verticalScale(20) }}>
                <UIClass
                    containerStyle={{ width: width - verticalScale(32) }}
                    title={title}
                    times={times}
                    startDate={startDate}
                    price={price}
                    mentor={mentor}
                    isViewAll
                    progress={progress || null}
                />
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <UIHeader LeftIcon="back" onPress={() => navigation.goBack()} title={t('home.classes')} />
            <FlatList data={classes} extraData={classes} renderItem={renderClass} />
        </View>
    );
};

export default ClassPage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
        alignItems: 'center',
    },
});
