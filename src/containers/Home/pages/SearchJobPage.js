import React, { useEffect, useRef, useState } from 'react';
import { useNavigation, useRoute } from '@react-navigation/core';
import { useTranslation } from 'react-i18next';
import { ActivityIndicator, FlatList, ImageBackground, Image, KeyboardAvoidingView, Platform, StyleSheet, Text, View } from 'react-native';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';
import { useTheme } from '@/theme';
import { UIHeader, UIModal, UISearchInput } from '../../../components/common';
import { moderateScale, verticalScale } from '../../../utils';
import { Colors } from '../../../theme/Variables';
import { connect } from 'react-redux';
import { actionApplyJob, actionSearchJob, actionSearchPeople, actionGetJobSearch, actionGetPeopleSearch } from './../duck/actions';
import { JobDetail, UIJobComponent, UIPeopleComponent } from '../components';
import { UIText, UIButtonChoose } from '../../../components/elements';
import { fontFamily } from '../../../theme/Fonts';
import { SCREEN } from '../../../constant';
import UIPeopleV2Component from '../components/UIPeopleV2Component';

const ListJobCards = ({ data = [], renderItem, onLoadMore, ListFooterComponent }) => {
    const [onEndReachedCalledDuringMomentum, setOnEndReachedCalledDuringMomentum] = useState(true);

    return (
        <FlatList
            contentContainerStyle={{ paddingHorizontal: 20, paddingBottom: 15 }}
            data={data}
            extraData={data}
            renderItem={renderItem}
            keyExtractor={(item, index) => index.toString()}
            onMomentumScrollBegin={() => setOnEndReachedCalledDuringMomentum(false)}
            onEndReached={() => {
                if (!onEndReachedCalledDuringMomentum) {
                    onLoadMore();
                    setOnEndReachedCalledDuringMomentum(true);
                }
            }}
            onEndReachedThreshold={0}
            ListFooterComponent={ListFooterComponent}
        />
    );
};

const mapStateToProps = (state) => ({
    walletReducer: state.walletReducer,
    authReducer: state.authReducer,
    profileReducer: state.profileReducer,
    homeReducer: state.homeReducer,
});

const mapStateToActions = {
    actionApplyJob,
    actionSearchJob,
    actionSearchPeople,
    actionGetJobSearch,
    actionGetPeopleSearch,
};

const AllJobPage = (props) => {
    const navigation = useNavigation();
    const { t } = useTranslation();
    const { Images } = useTheme();
    const limit = 5;

    const { actionSearchJob, actionApplyJob, actionSearchPeople, homeReducer, actionGetJobSearch, actionGetPeopleSearch } = props;

    const { isLoading } = homeReducer || false;
    const { params } = useRoute() || {};
    const { listSearch } = params || [];
    const [filter, setFilter] = useState(0);
    const [listJob, setListJob] = useState(listSearch || []);
    const [listPeople, setListPeople] = useState([]);
    const [listJobSearch, setListJobSearch] = useState([]);
    const [listPeopleSearch, setListPeopleSearch] = useState([]);
    const [onEndReachedCalledDuringMomentum, setOnEndReachedCalledDuringMomentum] = useState(true);
    const [isJobPoster, setIsJobPoster] = useState(false);
    const [job, setJob] = useState(null);
    const [searchValue, setSearchValue] = useState('');
    const [isSearch, setIsSearch] = useState(false);

    let currentPeoplePage = 1;
    let currentJobPage = 1;

    let modalRef = useRef();

    useEffect(() => {
        setSearchValue('');
        setIsSearch(false);
        switch (filter) {
            case 1:
                if (_isEmpty(_get(listPeople, 'data', [])) === true) {
                    getPeople(currentPeoplePage);
                }
                break;
            default:
                if (_isEmpty(_get(listJob, 'data', [])) === true) {
                    getJob(currentJobPage);
                }
                break;
        }
    }, [filter]);

    const getJob = (page = currentJobPage, reset = false) => {
        actionGetJobSearch({ page, limit }, (listJobs) => {
            const result = {
                ...listJobs,
                data: reset === true ? _get(listJobs, 'data', []) : [..._get(listJob, 'data', []), ..._get(listJobs, 'data', [])],
            };
            currentJobPage = listJobs.page;
            setListJob(result);
        });
    };

    const getPeople = (page = currentPeoplePage, reset = false) => {
        actionGetPeopleSearch({ page, limit }, (listPeoples) => {
            const result = {
                ...listPeoples,
                data: reset === true ? _get(listPeoples, 'data', []) : [..._get(listPeople, 'data', []), ..._get(listPeoples, 'data', [])],
            };
            currentPeoplePage = listPeople.page;
            setListPeople(result);
        });
    };

    const getSearchJob = async (page = currentJobPage, keyword = searchValue, reset = false) => {
        await actionSearchJob({ page, limit, keyword }, (listJobs) => {
            const result = {
                ...listJobs,
                data: reset === true ? _get(listJobs, 'data', []) : [..._get(listJobSearch, 'data', []), ..._get(listJobs, 'data', [])],
            };
            currentJobPage = listJobs.page;
            setListJobSearch(result);
        });
    };

    const getSearchPeople = async (page = currentPeoplePage, keyword = searchValue, reset = false) => {
        await actionSearchPeople({ page, limit, keyword }, (listPeople) => {
            const result = {
                ...listPeople,
                data:
                    reset === true
                        ? _get(listPeople, 'data', [])
                        : [..._get(listPeopleSearch, 'data', []), ..._get(listPeople, 'data', [])],
            };
            currentPeoplePage = listPeople.page;
            setListPeopleSearch(result);
        });
    };

    const _renderFooterPage = () => (
        <>
            {isLoading && (
                <View style={{ padding: 20 }}>
                    <ActivityIndicator />
                </View>
            )}
        </>
    );

    const renderJob = ({ item, index }) => {
        return (
            <>
                {/* <Text>{item._id}</Text> */}
                <UIJobComponent
                    onPress={() => onPressJob(item)}
                    type={item.type}
                    location={item.company.address}
                    position={item.name}
                    companyName={item.company.name}
                    logo={item.company.imgLandscape}
                    index={index}
                    date={item.createdAt}
                />
            </>
        );
    };

    const renderPeople = ({ item, index }) => {
        return (
            <UIPeopleV2Component
                item={item}
                name={`${_get(item, 'firstName', '')} 
            ${_get(item, 'lastName', '')}`}
                onPressAvatar={() => navigation.navigate(SCREEN.PEOPLE_DETAIL_PAGE, { userProfile: item })}
            />
        );
    };

    const onPressJob = (item) => {
        setIsJobPoster(false);
        setJob(item);
        modalRef.open();
    };

    const applyJob = () => {
        actionApplyJob({
            data: {
                job: job._id,
            },
        });
        modalRef.close();
    };

    const onSearch = (value) => {
        setSearchValue(value);
        if (value == '') {
            setIsSearch(false);
        }
        {
            setIsSearch(true);
            if (filter === 1) {
                getSearchPeople(1, value, true);
            } else {
                getSearchJob(1, value, true);
            }
        }
    };

    const onLoadMore = () => {
        if (filter == 0) {
            if (isSearch) {
                if (currentJobPage < listJobSearch.totalPages) getSearchJob(currentJobPage + 1);
            } else {
                if (currentJobPage < listJob.totalPages) getJob(currentJobPage + 1);
            }
        } else {
            if (isSearch) {
                if (currentPeoplePage < listPeopleSearch.totalPages) getSearchPeople(currentPeoplePage + 1);
            } else {
                if (currentPeoplePage < listPeople.totalPages) getPeople(currentPeoplePage + 1);
            }
        }
    };

    const searchAdvance = () => {
        return <Image style={styles.icSearch} source={Images.home.ic_search_advance} resizeMode="cover" />;
    };

    const clickSearchAdvance = () => {
        navigation.navigate(SCREEN.SEARCH_ADVANCE);
    };

    return (
        <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} style={{ backgroundColor: Colors.white, flex: 1 }}>
            <ImageBackground style={styles.bgHeader} resizeMode="stretch" imageStyle={styles.bgHeader} source={Images.chat.bg_chat}>
                <UIHeader
                    onPress={() => navigation.goBack()}
                    rightComponent={searchAdvance}
                    onPressRight={clickSearchAdvance}
                    LeftIcon="back_white"
                    title={t('home.search')}
                    titleStyle={styles.txtTitle}
                />
                <View style={{ paddingHorizontal: verticalScale(16) }}>
                    <UISearchInput value={searchValue} onSearch={(value) => onSearch(value)} />
                    <View style={{ flexDirection: 'row', marginTop: verticalScale(10) }}>
                        <UIText font="bold" style={styles.txtHotJob} text={`${t('home.hot_job')}: `} />
                        <UIText font="medium" style={styles.txtHotJobDes} text="UI/UX designer, iOS developer…" />
                    </View>
                </View>
            </ImageBackground>
            <View style={styles.tabs}>
                <View style={{ flexDirection: 'row' }}>
                    <UIButtonChoose onPress={() => setFilter(0)} isChoose={filter == 0} text={t('home.hot_jobs')} />
                    <UIButtonChoose onPress={() => setFilter(1)} isChoose={filter == 1} text={t('home.people')} />
                </View>
            </View>
            <Text style={styles.foundTitle}>
                At your request, we found{' '}
                {_get(filter == 0 ? (isSearch ? listJobSearch : listJob) : isSearch ? listPeopleSearch : listPeople, 'totalRecords', 0)}{' '}
                {filter === 1 ? 'talents' : 'jobs'}
            </Text>
            {/* <Text>{JSON.stringify(listJobSearch)}</Text> */}
            <ListJobCards
                data={filter == 0 ? (isSearch ? listJobSearch.data : listJob.data) : isSearch ? listPeopleSearch.data : listPeople.data}
                renderItem={filter == 0 ? renderJob : renderPeople}
                onLoadMore={onLoadMore}
                ListFooterComponent={_renderFooterPage()}
            />
            <UIModal ref={(ref) => (modalRef = ref)}>
                <JobDetail isJobPosted={isJobPoster} onPress={applyJob} job={job} />
            </UIModal>
        </KeyboardAvoidingView>
    );
};

const styles = StyleSheet.create({
    txtTitle: {
        fontSize: moderateScale(20),
        color: Colors.title,
    },
    bgHeader: {
        width: '100%',
        height: verticalScale(170),
        borderBottomLeftRadius: verticalScale(15),
        borderBottomRightRadius: verticalScale(15),
        marginBottom: verticalScale(16),
    },
    txtTitle: {
        fontSize: moderateScale(20),
        color: Colors.white,
    },
    txtHotJob: {
        color: Colors.white,
        fontSize: moderateScale(12),
    },
    txtHotJobDes: {
        fontSize: moderateScale(12),
        color: '#9fa4be',
    },
    contentContainer: {
        marginTop: verticalScale(20),
        paddingHorizontal: verticalScale(26),
        flex: 1,
        backgroundColor: Colors.white,
    },
    tabs: { flexDirection: 'row', marginBottom: verticalScale(20), paddingHorizontal: verticalScale(16) },
    foundTitle: {
        color: Colors.title,
        fontSize: moderateScale(14),
        marginBottom: verticalScale(20),
        paddingHorizontal: verticalScale(20),
        fontFamily: fontFamily.bold,
    },
    icSearch: {
        width: verticalScale(30),
        height: verticalScale(30),
        color: Colors.white,
    },
});

export default connect(mapStateToProps, mapStateToActions)(AllJobPage);
