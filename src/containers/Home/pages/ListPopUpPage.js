import React, { useState, useRef } from 'react';
import { View, StyleSheet, FlatList, ScrollView, Image, ImageBackground, TouchableOpacity } from 'react-native';
import { useTheme } from '@/theme';
import { Colors } from '../../../theme/Variables';
import { UIHeader, UIModal } from '../../../components/common';
import { UIButton, UIText } from '../../../components/elements';
import { useTranslation } from 'react-i18next';
import { useNavigation, useRoute } from '@react-navigation/native';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import moment from 'moment';
import { BASE_IMAGE_URL } from '../../../constant/value.constant';

const ListPopUpPage = () => {
    const { t } = useTranslation();
    const { Images } = useTheme();
    const navigation = useNavigation();
    const { params } = useRoute() || {};
    const { listPopUp } = params || [];
    const [popup, setPopUp] = useState({});

    let detailsModalRef = useRef();

    const onPressPopup = (item) => {
        setPopUp(item);
        detailsModalRef.open();
    };

    const renderPopup = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => onPressPopup(item)}>
                <View style={styles.containerItem}>
                    <ImageBackground
                        resizeMode="center"
                        style={styles.bg}
                        imageStyle={styles.backgroundImage}
                        source={
                            item.img_landscape != '' && item.img_landscape != null
                                ? { uri: BASE_IMAGE_URL + item.img_landscape }
                                : Images.profile.ic_avatar
                        }
                    />
                    <View style={styles.infoContainer}>
                        <UIText font="bold" style={styles.txtTitle} text={item.name} />
                        <View style={[styles.row]}>
                            <View style={styles.dotGreen} />
                            <UIText style={styles.txtAvailable} text={`${moment(item.startdate).format('HH:mm - MMM DD,YYYY')}`} />
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    return (
        <View style={styles.container}>
            <ScrollView>
                <UIHeader LeftIcon="back" onPress={() => navigation.goBack()} title={t('home.farm')} />
                <FlatList data={listPopUp} extraData={listPopUp} renderItem={renderPopup} keyExtractor={(_, index) => index.toString()} />
            </ScrollView>
            <UIModal ref={(ref) => (detailsModalRef = ref)}>
                <View style={styles.topUpContainer}>
                    <UIText font="bold" style={styles.txtTitle} text={popup.name} />
                    <ImageBackground
                        style={styles.bgDetail}
                        imageStyle={styles.backgroundImage}
                        source={
                            popup.img_landscape != '' && popup.img_landscape != null
                                ? { uri: BASE_IMAGE_URL + popup.img_landscape }
                                : Images.profile.ic_avatar
                        }
                    />
                    <UIText style={styles.txtDesc} text={popup.description} />
                    <UIButton
                        onPress={() => detailsModalRef.close()}
                        style={{ width: '100%', marginTop: verticalScale(30) }}
                        text={t('home.join_now')}
                    />
                    <TouchableOpacity style={styles.leftButton} onPress={() => detailsModalRef.close()}>
                        <Image source={Images.header.ic_close} />
                    </TouchableOpacity>
                </View>
            </UIModal>
        </View>
    );
};

export default ListPopUpPage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    containerItem: {
        backgroundColor: Colors.white,
        borderRadius: verticalScale(10),
        borderWidth: 1,
        borderColor: Colors.border,
        width: '92%',
        marginTop: verticalScale(10),
        marginHorizontal: verticalScale(20),
    },
    topUpContainer: {
        position: 'absolute',
        bottom: 0,
        height: '95%',
        backgroundColor: Colors.white,
        borderTopLeftRadius: verticalScale(20),
        borderTopRightRadius: verticalScale(20),
        justifyContent: 'center',
        padding: verticalScale(30),
        width: '100%',
    },
    bg: {
        flex: 1,
        height: verticalScale(200),
        borderTopLeftRadius: verticalScale(10),
        borderTopRightRadius: verticalScale(10),
    },
    bgDetail: {
        width: '100%',
        height: verticalScale(180),
        marginTop: verticalScale(20),
    },
    backgroundImage: {
        width: '100%',
        height: verticalScale(180),
        borderRadius: verticalScale(10),
    },
    infoContainer: {
        padding: verticalScale(15),
    },
    txtTitle: {
        fontSize: moderateScale(20),
        color: Colors.blueDark,
    },
    txtDesc: {
        flex: 1,
        fontSize: moderateScale(14),
        color: Colors.title,
        marginTop: verticalScale(6),
        marginBottom: verticalScale(15),
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    dotGreen: {
        width: verticalScale(12),
        height: verticalScale(12),
        borderRadius: verticalScale(6),
        backgroundColor: Colors.green,
    },
    txtAvailable: {
        marginLeft: verticalScale(7),
        fontSize: moderateScale(12),
        color: Colors.lightGray,
    },
    leftButton: {
        position: 'absolute',
        top: 0,
        right: 0,
        margin: verticalScale(12),
        justifyContent: 'center',
        alignItems: 'center',
    },
});
