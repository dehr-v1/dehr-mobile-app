import React from 'react';
import { View, StyleSheet, FlatList, Dimensions } from 'react-native';
import { UIHeader } from '../../../components/common';
import { useTranslation } from 'react-i18next';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Colors } from '../../../theme/Variables';
import UIClass from '../components/UIClass';
import { verticalScale } from '../../../utils/ScalingUtils';
import { UIOrganisation } from '../components';

const { width } = Dimensions.get('window');

const OrganisationPage = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { params } = useRoute() || {};
    const { organisation } = params || [];

    const renderClass = ({ item, index }) => {
        const { name, desc, available } = item || {};
        return (
            <View style={{ marginBottom: verticalScale(20) }}>
                <UIOrganisation name={name} desc={desc} available={available} />
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <UIHeader LeftIcon="back" onPress={() => navigation.goBack()} title={t('home.organisation')} />
            <View style={styles.contentContainer}>
                <FlatList data={organisation} extraData={organisation} renderItem={renderClass} />
            </View>
        </View>
    );
};

export default OrganisationPage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
        alignItems: 'center',
    },
    contentContainer: {
        paddingHorizontal: verticalScale(16),
    },
});
