import React from 'react';
import { View, StyleSheet, ImageBackground, TouchableOpacity } from 'react-native';
import { useTheme } from '@/theme';
import { Colors } from '../../../theme/Variables';
import { UIHeader } from '../../../components/common';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import { verticalScale } from '../../../utils/ScalingUtils';
import { SCREEN } from '../../../constant';

const ActiveProAccount = () => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { Images } = useTheme();

    const renderPackage = (type) => {
        return (
            <TouchableOpacity onPress={() => navigation.navigate(SCREEN.SUMMARY)}>
                <ImageBackground
                    resizeMode="contain"
                    imageStyle={styles.packageContainer}
                    style={styles.packageContainer}
                    source={type == 'b' ? Images.home.package_bronze : type == 's' ? Images.home.package_silver : Images.home.package_gold}
                />
            </TouchableOpacity>
        );
    };

    return (
        <View style={styles.container}>
            <UIHeader LeftIcon="back" onPress={() => navigation.goBack()} title={t('home.active_pro_account')} />
            <View style={styles.contentContainer}>
                {renderPackage('b')}
                {renderPackage('s')}
                {renderPackage('g')}
            </View>
        </View>
    );
};

export default ActiveProAccount;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        flex: 1,
    },
    packageContainer: {
        width: '100%',
        height: verticalScale(220),
    },
});
