import React, { useRef, useState, useEffect } from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    FlatList,
    ImageBackground,
    Image,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Text,
} from 'react-native';
import { UIHeader, UIModal, CustomTabbar } from '../../../components/common';
import { UIButton, UIText } from '../../../components/elements';
import { CATEGORY, SCREEN } from '../../../constant';
import _get from 'lodash/get';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import { useRoute, useNavigation } from '@react-navigation/native';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import { moderateScale, verticalScale, CommonUtils, width, scale } from '../../../utils';
import { Colors } from '../../../theme/Variables';
import { UIAvatar } from '../../Profile/components';
import { fontFamily } from '../../../theme/Fonts';
import { UIButtonEdit, UIItem } from '../../Profile/components';
import * as KardiaClient from 'kardia-js-sdk';
import Web3 from 'web3';
import {
    DEHR_CONTRACT_ADDRESS,
    SKILL_LEVEL,
    EXCHANGE_DATA_CONTRACT_ADDRESS,
    RPC_ENDPOINT,
    BASE_IMAGE_URL,
    LOCK_INFO,
    STATUS_INFO,
    NETWORK_STATUS,
} from '../../../constant/value.constant';
import { connect } from 'react-redux';
import { actionGetInfo, actionGetProfile, actionCreateResume } from '../duck/actions';
import { actionBalanceWallet } from '../../Wallet/duck/actions';
import moment from 'moment';
import { getSeedPhase } from '../../../services/persistence/store.config';
import Toast from '../../../components/elements/Toast';
import ExchangeData from '../../../abis/ExchangeData.json';
import DeHR from '../../../abis/DeHR.json';
import { actionNetworkRequest, actionCancelNetworkRequest } from '../../Chat/duck/actions';

const web3 = new Web3();
const kardiaClient = new KardiaClient.default({ endpoint: RPC_ENDPOINT });
const transactionModule = kardiaClient.transaction;
const contractInstance = kardiaClient.contract;
const membershipColorPattern = {
    levelOne: {
        color: Colors.blueDark,
        gradient: [],
    },
    levelTwo: {
        color: Colors.blueLight2,
        gradient: [Colors.blueLight2, Colors.blueLight3],
    },
    levelThree: {
        color: Colors.lightBlue,
        gradient: [Colors.blueLight3, Colors.blueLight4],
        backgroundColor: Colors.blueLight5,
    },
};

const PeopleDetailPage = (props) => {
    const { authReducer } = props || {};
    const { Layout, Images } = useTheme();
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { params } = useRoute() || {};
    const { userProfile } = params || {};
    const { user } = authReducer || {};
    const { resume, avatar_url, _id, username, contact, trustedObj, email } = userProfile || {};

    const { intro, about, ratingsSumary, background, skills, addtionalInfo } = resume || {};
    const { price } = contact || {};
    const { summary } = about || {};
    const { headline, current_position, location, industrial_knowledge } = intro || {};
    const { experience, education } = background || [];
    const { socialLinks } = addtionalInfo || [];
    const [loading, isLoading] = useState(false);
    const { actionBalanceWallet, actionGetInfo, actionGetProfile, actionCreateResume, actionNetworkRequest, actionCancelNetworkRequest } =
        props || {};
    const [coin, setCoin] = useState(0);
    //const [price, setPrice] = useState(0);
    const [profileInfoEmail, setProfileInfoEmail] = useState('');
    const [profileInfoPhone, setProfileInfoPhone] = useState('');
    const [isProcessing, setProcessing] = useState(false);
    const [hasWallet, setHasWallet] = useState(false);
    const [isLock, setIsLock] = useState(0);
    const [status, setStatus] = useState(0);
    const [statusNetwork, setStatusNetwork] = useState('');
    const [membershipOption, setMembershipOption] = useState(membershipColorPattern.levelOne);
    const membershipLevel = 1; // 1 or 2 or 3

    let topUpModalRef = useRef();
    let transactionModalRef = useRef();
    let profileModalRef = useRef();
    let confirmModalRef = useRef();
    let cancelNetworkRef = useRef();

    useEffect(() => {
        (async () => {
            setIsLock(userProfile.is_unlocked);
            setStatus(userProfile.status);
            //setPrice(contact_price ? web3.utils.fromWei(contact_price, 'Ether') : 0);
            const request = {
                resume: _id,
            };
            // setNetWork(NETWORK_STATUS.ACCEPTED)
            // setIsLock(LOCK_INFO.NO_BUY);
            // setStatus(STATUS_INFO.OPEN);
            //Test
            actionGetProfile({ request }, (data) => {
                setIsLock(data.is_unlocked);
                setStatus(data.status);
                setStatusNetwork(data.statusNetwork);
            });
            const getWallet = await getSeedPhase();
            const wallet = JSON.parse(getWallet);

            if (wallet) {
                const address = wallet?.address;
                if (address) {
                    setHasWallet(true);
                    actionBalanceWallet({ address }, (data) => {
                        setCoin(data);
                    });
                }
            }
        })();
        return () => {};
    }, []);

    useEffect(() => {
        switch (membershipLevel) {
            case 2:
                setMembershipOption(membershipColorPattern.levelTwo);
                break;
            case 3:
                setMembershipOption(membershipColorPattern.levelThree);
                break;
            default:
                setMembershipOption(membershipColorPattern.levelOne);
                break;
        }
    }, [membershipLevel]);

    const cancelRequest = () => {
        cancelNetworkRef.open();
    };

    const requestUnlockProfile = () => {
        // Processing
        if ((isLock == LOCK_INFO.BUY && status == STATUS_INFO.OPEN) || isProcessing) {
            Toast(t('common.process_toast'));
        } else {
            if (parseInt(coin) < parseInt(CommonUtils.convertFromWei(price))) {
                topUpModalRef.open();
            } else {
                transactionModalRef.open();
            }
        }
    };

    const requestInfoProfile = () => {
        profileModalRef.open();
        const request = {
            resume: _id,
        };
        actionGetInfo({ request }, (decrypted) => {
            const listData = decrypted.split('-');
            if (listData.length > 1) {
                setProfileInfoEmail(listData[0]);
                setProfileInfoPhone(listData[1]);
            }
        });
    };

    const transactionRequest = async (transactionModal, confirmModalRef, topUpModalRef) => {
        isLoading(true);
        const amountUnlock = CommonUtils.convertFromWei(price);
        const recipient = userProfile.wallet;
        const getWallet = await getSeedPhase();
        const wallet = JSON.parse(getWallet);
        const address = wallet?.address;
        const privateKey = wallet?.privateKey;
        if (address) {
            actionBalanceWallet({ address }, async (coin) => {
                if (parseInt(coin) >= parseInt(amountUnlock)) {
                    if (wallet) {
                        const callbackResult = async function (result) {
                            if (result) {
                                contractInstance.updateAbi(ExchangeData.abi);
                                contractInstance.updateByteCode(ExchangeData.bytecode);
                                const invocation = contractInstance.invokeContract('request1', [recipient]);
                                const txHash = await invocation.send(privateKey, EXCHANGE_DATA_CONTRACT_ADDRESS);
                                const interval = setInterval(async function () {
                                    const receipt = await transactionModule.getTransactionReceipt(txHash);
                                    if (receipt) {
                                        clearInterval(interval);
                                        isLoading(false);
                                        if (receipt.status === 1) {
                                            setProcessing(true);
                                            Toast('SUCCESS!');
                                            confirmModalRef.open();
                                            const request = {
                                                resume: _id,
                                            };
                                            actionCreateResume({ request }, () => {
                                                transactionModal.close();
                                            });
                                        } else {
                                            Toast('ERROR!', 'error');
                                        }
                                    }
                                }, 1000);
                            } else {
                                Toast('ERROR!', 'error');
                                isLoading(false);
                                transactionModal.close();
                            }
                        };
                        await dehrApproveExchangedata(amountUnlock, privateKey, callbackResult);
                    } else {
                        isLoading(false);
                        Toast(t('wallet.setup_wallet'), 'error');
                    }
                } else {
                    transactionModal.close();
                    topUpModalRef.open();
                    isLoading(false);
                    // Toast(t('home.no_coin'));
                }
            });
        } else {
            isLoading(false);
            transactionModal.close();
            Toast(t('wallet.setup_wallet'), 'error');
        }
    };

    const dehrApproveExchangedata = async (amount, privateKey, callbackResult) => {
        contractInstance.updateAbi(DeHR.abi);
        contractInstance.updateByteCode(DeHR.bytecode);
        const invocation = contractInstance.invokeContract('approve', [EXCHANGE_DATA_CONTRACT_ADDRESS, web3.utils.toBN(amount)]);
        const txHash = await invocation.send(privateKey, DEHR_CONTRACT_ADDRESS);

        /**
         * waiting for transaction accept to block
         * time may later 20(s)
         * */
        const interval = setInterval(async function () {
            const receipt = await transactionModule.getTransactionReceipt(txHash);
            if (receipt) {
                clearInterval(interval);
                /**
                 * Transaction status. 1 if transaction is executed successfully, otherwise 0.
                 * */
                if (receipt.status === 1) {
                    callbackResult(true);
                } else {
                    callbackResult(false);
                }
            }
        }, 1000);
    };

    const onConnectNetWork = () => {
        const data = {
            memberId: _id,
        };
        actionNetworkRequest(data, () => {
            setStatusNetwork(NETWORK_STATUS.REQUESTED);
        });
    };

    //Status Info
    const renderButtonNetWorkStatus = () => {
        switch (statusNetwork) {
            case NETWORK_STATUS.REQUESTED:
                return (
                    <View style={[styles.viewBtn, { marginRight: scale(11) }]}>
                        <UIButton
                            text={t('profile.cancel_request')}
                            type="BgGrey3"
                            style={{ height: verticalScale(44) }}
                            textStyle={{ fontSize: moderateScale(16) }}
                            onPress={cancelRequest}
                        />
                    </View>
                );
            case NETWORK_STATUS.ACCEPTED:
                return (
                    <View style={[styles.viewBtn, { marginRight: scale(11) }]}>
                        <UIButton
                            text={t('profile.connected')}
                            type="BgGreen"
                            style={{ height: verticalScale(44) }}
                            textStyle={{ fontSize: moderateScale(16) }}
                        />
                    </View>
                );
            default:
                return (
                    <View style={[styles.viewBtn, { marginRight: scale(11) }]}>
                        <UIButton
                            text={t('profile.connect_network')}
                            type="BgGreen"
                            onPress={onConnectNetWork}
                            style={{ height: verticalScale(44) }}
                            textStyle={{ fontSize: moderateScale(16) }}
                        />
                    </View>
                );
        }
    };

    const renderButtonInfoStatus = () => {
        if (statusNetwork == NETWORK_STATUS.ACCEPTED && isLock == LOCK_INFO.BUY && status == STATUS_INFO.ACCEPTED) {
            return (
                <View style={[styles.viewBtn, { marginRight: scale(11) }]}>
                    <UIButton
                        text={t('chat.mesage')}
                        type="BgOrange"
                        onPress={() => requestInfoProfile()}
                        style={{ height: verticalScale(44) }}
                        textStyle={{ fontSize: moderateScale(16) }}
                    />
                </View>
            );
        } else if (isLock == LOCK_INFO.BUY && status == STATUS_INFO.OPEN) {
            return (
                <View style={[styles.viewBtn, { marginRight: scale(11) }]}>
                    <UIButton
                        text={t('profile.waiting')}
                        type="BgGrey2"
                        onPress={() => {}}
                        disable
                        style={{ height: verticalScale(44) }}
                        textStyle={{ fontSize: moderateScale(16) }}
                    />
                </View>
            );
        } else {
            return (
                <View style={[styles.viewBtn, { marginRight: scale(11) }]}>
                    <UIButton
                        text={t('profile.unlock_info')}
                        type="BgOrange"
                        onPress={() => transactionModalRef.open()}
                        style={{ height: verticalScale(44) }}
                        textStyle={{ fontSize: moderateScale(16) }}
                    />
                </View>
            );
        }
    };

    //Item
    const renderItemExperience = ({ item, index }) => {
        return (
            <UIItem
                name={item.title}
                media={item.media}
                startDate={new Date(item.startDate)}
                startDate={new Date(item.endDate)}
                title={item.companyName}
                description={item.description}
                stylesName={styles.textName}
                hideAction
            />
        );
    };

    const renderItemEducation = ({ item, index }) => {
        return (
            <UIItem
                name={item.school}
                title={item.description}
                media={item.media}
                startDate={new Date(item.startDate)}
                startDate={new Date(item.endDate)}
                stylesName={styles.textName}
                hideAction
            />
        );
    };

    const renderItemSkill = ({ item, index }) => {
        return (
            <UIItem
                name={item.title}
                title={SKILL_LEVEL[item.proficiency - 1].label}
                stylesName={styles.textName}
                hideAction
                style={styles.item}
            />
        );
    };

    const renderProfile = () => {
        return (
            <View style={styles.viewContainer}>
                <ScrollView contentContainerStyle={styles.profileContainer}>
                    <>
                        <ImageBackground
                            style={styles.profileImageContainer}
                            imageStyle={styles.profileImage}
                            source={Images.profile.bg_work}
                        >
                            <UIHeader LeftIcon={'back'} RightIcon="more" onPress={() => navigation.goBack()} />
                            <TouchableOpacity style={styles.btnCamera}>
                                <Image style={styles.camera} source={Images.profile.ic_camera} />
                            </TouchableOpacity>
                            {membershipLevel > 1 && (
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    colors={membershipOption.gradient}
                                    style={{
                                        width: '100%',
                                        height: membershipLevel === 2 ? 8 : 25,
                                        position: 'absolute',
                                        bottom: membershipLevel === 2 ? 0 : 5,
                                    }}
                                />
                            )}
                            {membershipLevel === 3 && (
                                <View
                                    style={{
                                        opacity: 1,
                                        position: 'absolute',
                                        height: 5,
                                        width: '100%',
                                        bottom: 0,
                                        backgroundColor: '#30e3eb',
                                    }}
                                ></View>
                            )}
                        </ImageBackground>
                        <View
                            style={[styles.contentContainer, { backgroundColor: _get(membershipOption, 'backgroundColor', Colors.white) }]}
                        >
                            <UIAvatar
                                membershipLevel={membershipLevel}
                                url={avatar_url ? BASE_IMAGE_URL + avatar_url : null}
                                style={{ marginTop: verticalScale(-50) }}
                            />
                            <UIText
                                style={[styles.txtName, { color: membershipOption.color }]}
                                font="black"
                                numberOfLines={2}
                                ellipsizeMode="tail"
                                text={userProfile.fullname || userProfile.username}
                            />
                            <View style={styles.viewRate}>
                                <View style={styles.rowHeader}>
                                    <Image
                                        style={[styles.icStar, ratingsSumary >= 1 && { tintColor: Colors.yellow }]}
                                        source={Images.home.ic_star_vote}
                                    />
                                    <Image
                                        style={[styles.icStar, ratingsSumary >= 2 && { tintColor: Colors.yellow }]}
                                        source={Images.home.ic_star_vote}
                                    />
                                    <Image
                                        style={[styles.icStar, ratingsSumary >= 3 && { tintColor: Colors.yellow }]}
                                        source={Images.home.ic_star_vote}
                                    />
                                    <Image
                                        style={[styles.icStar, ratingsSumary >= 4 && { tintColor: Colors.yellow }]}
                                        source={Images.home.ic_star_vote}
                                    />
                                    <Image
                                        style={[styles.icStar, styles.icLastStar, ratingsSumary >= 5 && { tintColor: Colors.yellow }]}
                                        source={Images.home.ic_star_vote}
                                    />
                                </View>
                                <Text style={styles.textInfoPrice}>
                                    {t('profile.info_price')}
                                    <Text style={[styles.textDeHR, { color: membershipOption.color }]}>
                                        {' '}
                                        {CommonUtils.convertFromWei(price)} DeHR
                                    </Text>
                                </Text>
                                {/* <TouchableOpacity onPress={setContactPrice}>
                                    <Image style={styles.icEditDeHR} source={Images.profile.ic_edit} />
                                </TouchableOpacity> */}
                            </View>
                            <UIText numberOfLines={2} ellipsizeMode="tail" style={styles.txtHeadline} text={`${headline || ''}`} />
                            <View style={styles.viewLocation}>
                                <Image style={styles.icLocation} source={Images.profile.ic_position_2} />
                                <UIText
                                    numberOfLines={2}
                                    style={[styles.txtHeadline, { paddingHorizontal: 0 }]}
                                    // text={`${headline}`}
                                    text={experience[0]?.location}
                                />
                            </View>
                            <View style={styles.btnContainer}>
                                {/* =============== */}
                                {/* {isUnlocking ? (
                                    <>
                                        <View style={[styles.viewBtn, { marginRight: scale(11) }]}>
                                            <UIButton
                                                text={t('profile.cancel_request')}
                                                type="BgGrey2"
                                                Icon={Images.profile.cancel_person}
                                                onPress={() => setIsUnlocking(false)}
                                            />
                                        </View>
                                        <View style={styles.viewBtn}>
                                            <UIButton text={t('profile.waiting')} type="BgGrey3" />
                                        </View>
                                    </>
                                ) : (
                                    <>
                                        <View style={[styles.viewBtn, { marginRight: scale(11) }]}>
                                            <UIButton
                                                text={t('profile.edit_profile')}
                                                type="BgGrey2"
                                                style={{ height: verticalScale(44) }}
                                                textStyle={{ fontSize: moderateScale(16), color: Colors.blueDark }}
                                                onPress={onPressEdit}
                                            />
                                        </View>
                                        <View style={styles.viewBtn}>
                                            <UIButton
                                                text={t('profile.my_network')}
                                                type="BgBlue"
                                                onPress={() => navigation.navigate(SCREEN.CONNECT_NETWORK)}
                                                //onPress={() => modalUnlockInfoRef.open()}
                                                style={{ height: verticalScale(44) }}
                                                textStyle={{ fontSize: moderateScale(16) }}
                                            />
                                        </View> 
                                    </>*/}
                                {renderButtonNetWorkStatus()}
                                {renderButtonInfoStatus()}
                            </View>
                        </View>
                        <ScrollableTabView renderTabBar={(props) => <CustomTabbar {...props} />}>
                            <View style={styles.viewContent} tabLabel={t('profile.about')}>
                                <UIText text={summary || ''} style={styles.textContent} />
                            </View>
                            <View tabLabel={t('profile.experience')}>
                                {/* <UIButtonEdit text={t('profile.experience_endorsements')} onPress={onPressEdit} /> */}
                                <FlatList
                                    keyExtractor={(_, i) => i.toString()}
                                    data={experience}
                                    extraData={experience}
                                    renderItem={renderItemExperience}
                                />
                            </View>
                            <View tabLabel={t('profile.education')}>
                                {/* <UIButtonEdit text={t('profile.education')} onPress={onPressEdit} /> */}
                                <FlatList
                                    keyExtractor={(_, i) => i.toString()}
                                    data={education}
                                    extraData={education}
                                    renderItem={renderItemEducation}
                                />
                            </View>
                            <View tabLabel={t('profile.skills')}>
                                {/* <UIButtonEdit text={t('profile.skills_endorsements')} onPress={onPressEdit} /> */}
                                <FlatList
                                    data={skills}
                                    extraData={skills}
                                    keyExtractor={(_, i) => i.toString()}
                                    renderItem={renderItemSkill}
                                />
                            </View>
                            {LOCK_INFO.BUY === isLock && (
                                <View tabLabel={t('profile.contact')}>
                                    {/* <UIButtonEdit text={t('profile.contact_info')} onPress={onPressEdit} /> */}
                                    <FlatList
                                        data={[{ name: 'Email', value: email }]}
                                        keyExtractor={(_, i) => i.toString()}
                                        renderItem={({ item }) => (
                                            <UIItem
                                                name={item?.name}
                                                title={item?.value}
                                                stylesName={styles.textName}
                                                hideAction
                                                style={styles.item}
                                            />
                                        )}
                                    />
                                </View>
                            )}
                        </ScrollableTabView>
                    </>
                </ScrollView>
            </View>
        );
    };

    const onGotIt = () => {
        setProcessing(true);
        confirmModalRef.close();
    };

    const onCancelRequest = () => {
        actionCancelNetworkRequest(
            {
                memberId: _id,
            },
            () => {
                cancelNetworkRef.close();
                setStatusNetwork(NETWORK_STATUS.REJECTED);
            }
        );
    };

    return (
        <>
            {renderProfile()}
            <UIModal ref={(ref) => (topUpModalRef = ref)}>
                <TouchableWithoutFeedback>
                    <View style={styles.topUpContainer}>
                        <UIText font="bold" style={styles.txtTitleModal} text={t('home.notification')} />
                        <Image source={Images.wallet.bg_wallet_setup} style={styles.bgTopup} />
                        <UIText style={styles.txtDesc} text={t('home.top_up_desc')} />
                        <View style={[styles.row, { marginTop: 0, justifyContent: 'space-between' }]}>
                            <UIButton
                                onPress={() => topUpModalRef.close()}
                                style={{ flex: 0.47, height: verticalScale(44) }}
                                type="BgWhite"
                                text={t('profile.cancel')}
                            />
                            <UIButton
                                onPress={() => navigation.navigate('Wallet')}
                                style={{ flex: 0.47, height: verticalScale(44) }}
                                text={t('home.top_up')}
                            />
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </UIModal>
            <UIModal ref={(ref) => (transactionModalRef = ref)}>
                <>
                    <View style={{ flex: 1 }} />
                    <View style={styles.unlockContainer}>
                        <UIText font="bold" style={styles.txtTitleModal} text={t('home.transaction_unlock_info')} />
                        <View style={styles.rowSimpleModal}>
                            <UIText font="bold" style={styles.txtTransactionAmount} text="-" />
                            <UIText font="bold" style={styles.txtTransactionAmount} text={CommonUtils.convertFromWei(price)} />
                            <Image style={styles.icDehr} source={Images.wallet.ic_logo} />
                        </View>

                        <View style={styles.rowModal}>
                            <UIText text={t('wallet.type')} />
                            <UIText font="medium" text={t('wallet.send')} />
                        </View>
                        <View style={styles.rowModal}>
                            <UIText text={t('wallet.transaction_code')} />
                            <UIText font="medium" text="1355678900" />
                        </View>
                        <View style={styles.rowModal}>
                            <UIText text={t('wallet.time')} />
                            <UIText font="medium" text={moment(new Date()).format('hh:mm - MMM DD, YYYY')} />
                        </View>
                        <View style={styles.rowModal}>
                            <UIText text={t('wallet.fund')} />
                            <UIText font="medium" text="DeHR Wallet" />
                        </View>
                        <View style={styles.rowModal}>
                            <UIText text={t('wallet.wallet_balance')} />
                            <View style={[Layout.rowCenter]}>
                                <Image style={styles.icDehrTotal} source={Images.wallet.ic_logo} />
                                <UIText style={{ marginTop: verticalScale(4) }} font="medium" text={CommonUtils.convertFromWei(coin)} />
                            </View>
                        </View>
                        <UIButton
                            isLoading={loading}
                            onPress={() => transactionRequest(transactionModalRef, confirmModalRef, topUpModalRef)}
                            style={{ marginTop: verticalScale(50) }}
                            text={t('wallet.confirm')}
                        />
                    </View>
                </>
            </UIModal>
            <UIModal ref={(ref) => (profileModalRef = ref)}>
                <>
                    <View style={{ flex: 1 }} />
                    <View style={styles.unlockContainer}>
                        <UIText font="bold" style={styles.txtTitleModal} text={t('home.infomation_personal')} />
                        <UIAvatar url={(avatar_url && BASE_IMAGE_URL + avatar_url) || null} style={styles.imgAvatar} />
                        <View style={styles.rowModal}>
                            <UIText text={t('home.name')} />
                            <UIText
                                numberOfLines={1}
                                ellipsizeMode="tail"
                                style={{ marginHorizontal: verticalScale(16) }}
                                font="medium"
                                text={username}
                            />
                        </View>
                        <View style={styles.rowModal}>
                            <UIText text={t('home.email')} />
                            <UIText font="medium" text={profileInfoEmail} />
                        </View>
                        <View style={styles.rowModal}>
                            <UIText text={t('home.phone')} />
                            <UIText font="medium" text={profileInfoPhone} />
                        </View>
                        {!hasWallet && <UIText style={{ textAlign: 'center' }} text={t('profile.profile_nowallet')} />}
                        <UIButton
                            onPress={() => profileModalRef.close()}
                            style={{ marginTop: verticalScale(50) }}
                            text={t('wallet.confirm')}
                        />
                    </View>
                </>
            </UIModal>
            <UIModal ref={(ref) => (confirmModalRef = ref)}>
                <TouchableWithoutFeedback>
                    <View style={styles.topUpContainer}>
                        <UIText font="bold" style={styles.txtTitleModal} text={t('home.notification')} />
                        <UIText style={styles.txtDesc} text={`${t('profile.please_waitting_confirm')} @${username}`} />
                        <View style={[styles.row, { marginTop: 0, justifyContent: 'space-between' }]}>
                            <UIButton
                                onPress={() => confirmModalRef.close()}
                                style={{ flex: 0.47, height: verticalScale(44) }}
                                type="BgWhite"
                                text={t('profile.cancel')}
                            />
                            <UIButton onPress={onGotIt} style={{ flex: 0.47, height: verticalScale(44) }} text={t('wallet.got_it')} />
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </UIModal>
            <UIModal ref={(ref) => (cancelNetworkRef = ref)}>
                <TouchableWithoutFeedback>
                    <View style={styles.modalCancelRequest}>
                        <UIText font="bold" style={styles.txtTitleModal} text={t('profile.cancel_network_request')} />
                        <View style={styles.viewLine} />
                        <TouchableOpacity onPress={onCancelRequest}>
                            <UIText style={{ paddingVertical: verticalScale(12) }} text={t('profile.cancel_request')} />
                        </TouchableOpacity>
                        <View style={styles.viewLine} />
                        <TouchableOpacity onPress={() => cancelNetworkRef.close()}>
                            <UIText style={{ paddingVertical: verticalScale(12) }} text={t('home.close')} />
                        </TouchableOpacity>
                        <View style={styles.viewLine} />
                    </View>
                </TouchableWithoutFeedback>
            </UIModal>
        </>
    );

    // return (
    //     <View style={{ flex: 1, backgroundColor: 'white' }}>
    //         <ScrollView contentContainerStyle={styles.profileContainer}>
    //             <>
    //                 <ImageBackground style={styles.profileImageContainer} imageStyle={styles.profileImage} source={Images.profile.bg_work}>
    //                     <UIHeader LeftIcon="back" onPress={() => navigation.goBack()} RightIcon="more" />
    //                 </ImageBackground>
    //                 <View style={styles.contentContainer}>
    //                     <UIAvatar
    //                         url={(avatar && BASE_IMAGE_URL + avatar) || null}
    //                         style={{ marginTop: verticalScale(-50) }}
    //                     />
    //                     <UIText numberOfLines={2} ellipsizeMode="tail" style={styles.txtName} font="black" text={username} />
    //                     <View style={styles.row}>
    //                         <Image
    //                             style={[styles.icStar, ratingsSumary >= 1 && { tintColor: Colors.green }]}
    //                             source={Images.home.ic_star_vote}
    //                         />
    //                         <Image
    //                             style={[styles.icStar, ratingsSumary >= 2 && { tintColor: Colors.green }]}
    //                             source={Images.home.ic_star_vote}
    //                         />
    //                         <Image
    //                             style={[styles.icStar, ratingsSumary >= 3 && { tintColor: Colors.green }]}
    //                             source={Images.home.ic_star_vote}
    //                         />
    //                         <Image
    //                             style={[styles.icStar, ratingsSumary >= 4 && { tintColor: Colors.green }]}
    //                             source={Images.home.ic_star_vote}
    //                         />
    //                         <Image
    //                             style={[styles.icStar, ratingsSumary >= 5 && { tintColor: Colors.green }]}
    //                             source={Images.home.ic_star_vote}
    //                         />
    //                     </View>
    //                     <UIText style={styles.txtHeadline} text={`${headline || ''}`} />
    //                     <View style={styles.btnContainer}>
    //                         <View style={styles.btn}>
    //                             <UIButton
    //                                 onPress={user.role == 'pro' ? requesHiringProfile : null}
    //                                 text={t('profile.connect_network')}
    //                                 type="BgGreen"
    //                                 style={{height: verticalScale(44)}}
    //                                 textStyle={{fontSize: moderateScale(16)}}
    //                             />
    //                         </View>
    //                         {isLock == 1 && status == 2 ? (
    //                             <View style={styles.btn}>
    //                                 <TouchableOpacity onPress={requestInfoProfile} style={styles.btnContactInfo}>
    //                                     <UIText font="bold" style={{ color: Colors.white }} text={t('profile.contact_profile_info')} />
    //                                 </TouchableOpacity>
    //                             </View>
    //                         ) : (
    //                             <View style={styles.btn}>
    //                                 <TouchableOpacity onPress={requestUnlockProfile} style={styles.btnContactPrice}>
    //                                     <View>
    //                                         {(isLock == 1 && status == 0) || isProcessing ? (
    //                                             <UIText font="bold" style={{ color: Colors.white }} text={t('common.process')} />
    //                                         ) : (
    //                                             <UIText font="bold" style={{ color: Colors.white }} text={t('profile.unlock_info')} />
    //                                         )}
    //                                         {price != 0 && (
    //                                             <View style={[Layout.rowCenter]}>
    //                                                 <UIText font="bold" style={styles.txtContactPrice} text={price} />
    //                                                 <Image style={styles.icLogo} source={Images.wallet.ic_logo} />
    //                                             </View>
    //                                         )}
    //                                     </View>
    //                                 </TouchableOpacity>
    //                             </View>
    //                         )}
    //                     </View>
    //                 </View>
    //                 <ScrollableTabView renderTabBar={(props) => <CustomTabbar {...props} />}>
    //                     <View tabLabel={t('profile.about')}>
    //                         <UIButtonEdit text={t('profile.summary')} />
    //                         <View style={styles.viewContent}>
    //                             <UIText
    //                                 text={summary || ''}
    //                                 style={styles.textContent}
    //                             />
    //                         </View>
    //                     </View>
    //                     <View tabLabel={t('profile.experience')}>
    //                         <UIButtonEdit text={t('profile.experience_endorsements')} />
    //                         <FlatList
    //                             data={[{}, {}, {}]}
    //                             keyExtractor={(_, i) => i.toString()}
    //                             renderItem={() => (
    //                                 <UIItem
    //                                     name="Senior Web Designer"
    //                                     title="VNG Coporation"
    //                                     startDate={new Date('2020-07-01')}
    //                                     endDate={0}
    //                                     location="at Ho Chi Minh City, Vietnam"
    //                                     description="Major working on mobile & web design, create UI/UX for all projects, creative & impressive…. See more"
    //                                     stylesName={styles.textName}
    //                                     hideAction
    //                                     style={styles.item}
    //                                 />
    //                             )}
    //                         />
    //                     </View>
    //                     <View tabLabel={t('profile.education')}>
    //                         <UIButtonEdit text={t('profile.education')} />
    //                         <FlatList
    //                             data={[{}, {}, {}]}
    //                             keyExtractor={(_, i) => i.toString()}
    //                             renderItem={() => (
    //                                 <UIItem
    //                                     name="Senior Web Designer"
    //                                     title="Bachelor’s degree, English literature (British and Commonwealth"
    //                                     startDate={new Date('2020-07-01')}
    //                                     endDate={0}
    //                                     stylesName={styles.textName}
    //                                     hideAction
    //                                     style={styles.item}
    //                                 />
    //                             )}
    //                         />
    //                     </View>
    //                     <View tabLabel={t('profile.skills')}>
    //                         <UIButtonEdit text={t('profile.skills_endorsements')} />
    //                         <FlatList
    //                             data={[
    //                                 { name: 'Adobe Photoshop', value: 'Good' },
    //                                 { name: 'Adobe Photoshop', value: 'Good' },
    //                             ]}
    //                             keyExtractor={(_, i) => i.toString()}
    //                             renderItem={({ item }) => (
    //                                 <UIItem
    //                                     name={item?.name}
    //                                     title={item?.value}
    //                                     stylesName={styles.textName}
    //                                     hideAction
    //                                     style={styles.item}
    //                                 />
    //                             )}
    //                         />
    //                     </View>
    //                     <View tabLabel={t('profile.contact')}>
    //                         <UIButtonEdit text={t('profile.contact_info')} />
    //                         <FlatList
    //                             data={[
    //                                 { name: 'Email', value: 'thuynt@wisory.vn' },
    //                                 { name: 'Điện thoại', value: '0938 283 124' },
    //                             ]}
    //                             keyExtractor={(_, i) => i.toString()}
    //                             renderItem={({ item }) => (
    //                                 <UIItem
    //                                     name={item?.name}
    //                                     title={item?.value}
    //                                     stylesName={styles.textName}
    //                                     hideAction
    //                                     style={styles.item}
    //                                 />
    //                             )}
    //                         />
    //                     </View>
    //                 </ScrollableTabView>
    //             </>
    //         </ScrollView>
    //     <UIModal ref={(ref) => (topUpModalRef = ref)}>
    //         <TouchableWithoutFeedback>
    //             <View style={styles.topUpContainer}>
    //                 <UIText font="bold" style={styles.txtTitleModal} text={t('home.notification')} />
    //                 <Image source={Images.wallet.bg_wallet_setup} style={styles.bgTopup} />
    //                 <UIText style={styles.txtDesc} text={t('home.top_up_desc')} />
    //                 <View style={[styles.row, { marginTop: 0, justifyContent: 'space-between' }]}>
    //                     <UIButton
    //                         onPress={() => topUpModalRef.close()}
    //                         style={{ flex: 0.47 }}
    //                         type="BgWhite"
    //                         text={t('profile.cancel')}
    //                     />
    //                     <UIButton onPress={() => navigation.navigate('Wallet')} style={{ flex: 0.47 }} text={t('home.top_up')} />
    //                 </View>
    //             </View>
    //         </TouchableWithoutFeedback>
    //     </UIModal>
    //     <UIModal ref={(ref) => (transactionModalRef = ref)}>
    //         <>
    //             <View style={{ flex: 1 }} />
    //             <View style={styles.unlockContainer}>
    //                 <UIText font="bold" style={styles.txtTitleModal} text={t('home.transaction_unlock_info')} />
    //                 <View style={styles.rowSimpleModal}>
    //                     <UIText font="bold" style={styles.txtTransactionAmount} text="-" />
    //                     <UIText font="bold" style={styles.txtTransactionAmount} text={price} />
    //                     <Image style={styles.icDehr} source={Images.wallet.ic_logo} />
    //                 </View>

    //                 <View style={styles.rowModal}>
    //                     <UIText text={t('wallet.type')} />
    //                     <UIText font="medium" text={t('wallet.send')} />
    //                 </View>
    //                 <View style={styles.rowModal}>
    //                     <UIText text={t('wallet.transaction_code')} />
    //                     <UIText font="medium" text="1355678900" />
    //                 </View>
    //                 <View style={styles.rowModal}>
    //                     <UIText text={t('wallet.time')} />
    //                     <UIText font="medium" text={moment(new Date()).format('hh:mm - MMM DD, YYYY')} />
    //                 </View>
    //                 <View style={styles.rowModal}>
    //                     <UIText text={t('wallet.fund')} />
    //                     <UIText font="medium" text="DeHR Wallet" />
    //                 </View>
    //                 <View style={styles.rowModal}>
    //                     <UIText text={t('wallet.wallet_balance')} />
    //                     <View style={[Layout.rowCenter]}>
    //                         <Image style={styles.icDehrTotal} source={Images.wallet.ic_logo} />
    //                         <UIText style={{ marginTop: verticalScale(4) }} font="medium" text={CommonUtils.convertFromWei(coin)} />
    //                     </View>
    //                 </View>
    //                 <UIButton
    //                     isLoading={loading}
    //                     onPress={() => transactionRequest(transactionModalRef)}
    //                     style={{ marginTop: verticalScale(50) }}
    //                     text={t('wallet.confirm')}
    //                 />
    //             </View>
    //         </>
    //     </UIModal>
    //     <UIModal ref={(ref) => (profileModalRef = ref)}>
    //         <>
    //             <View style={{ flex: 1 }} />
    //             <View style={styles.unlockContainer}>
    //                 <UIText font="bold" style={styles.txtTitleModal} text={t('home.infomation_personal')} />
    //                 <UIAvatar url={(avatar && BASE_IMAGE_URL + avatar) || null} style={styles.imgAvatar} />
    //                 <View style={styles.rowModal}>
    //                     <UIText text={t('home.name')} />
    //                     <UIText
    //                         numberOfLines={1}
    //                         ellipsizeMode="tail"
    //                         style={{ marginHorizontal: verticalScale(16) }}
    //                         font="medium"
    //                         text={username}
    //                     />
    //                 </View>
    //                 <View style={styles.rowModal}>
    //                     <UIText text={t('home.email')} />
    //                     <UIText font="medium" text={profileInfoEmail} />
    //                 </View>
    //                 <View style={styles.rowModal}>
    //                     <UIText text={t('home.phone')} />
    //                     <UIText font="medium" text={profileInfoPhone} />
    //                 </View>
    //                 {!hasWallet && <UIText style={{ textAlign: 'center' }} text={t('profile.profile_nowallet')} />}
    //                 <UIButton
    //                     onPress={() => profileModalRef.close()}
    //                     style={{ marginTop: verticalScale(50) }}
    //                     text={t('wallet.confirm')}
    //                 />
    //             </View>
    //         </>
    //     </UIModal>
    //     <UIModal ref={(ref) => (confirmModalRef = ref)}>
    //         <TouchableWithoutFeedback>
    //             <View style={styles.topUpContainer}>
    //                 <UIText font="bold" style={styles.txtTitleModal} text={t('home.notification')} />
    //                 <UIText style={styles.txtDesc} text={(t('home.please_waitting_confirm'), { name: username })} />
    //                 <View style={[styles.row, { marginTop: 0, justifyContent: 'space-between' }]}>
    //                     <UIButton
    //                         onPress={() => confirmModalRef.close()}
    //                         style={{ flex: 0.47 }}
    //                         type="BgWhite"
    //                         text={t('profile.cancel')}
    //                     />
    //                     <UIButton onPress={() => confirmModalRef.close()} style={{ flex: 0.47 }} text={t('wallet.got_it')} />
    //                 </View>
    //             </View>
    //         </TouchableWithoutFeedback>
    //     </UIModal>
    // </View>
    // );
};

const mapStateToProps = (state) => ({
    homeReducer: state.homeReducer,
    authReducer: state.authReducer,
});

export default connect(mapStateToProps, {
    actionBalanceWallet,
    actionGetInfo,
    actionGetProfile,
    actionCreateResume,
    actionNetworkRequest,
    actionCancelNetworkRequest,
})(PeopleDetailPage);

const styles = StyleSheet.create({
    viewContainer: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    profileContainer: {
        alignItems: 'center',
        paddingBottom: verticalScale(30),
    },
    profileImage: {
        width: '100%',
        height: verticalScale(200),
    },
    imgAvatar: {
        width: verticalScale(100),
        height: verticalScale(100),
        marginBottom: verticalScale(10),
    },
    profileImageContainer: {
        position: 'absolute',
        top: 0,
        width: '100%',
        alignItems: 'center',
        backgroundColor: Colors.white,
    },
    contentContainer: {
        marginTop: verticalScale(200),
        alignItems: 'center',
        width: '100%',
        backgroundColor: Colors.white,
    },
    viewContent: {
        padding: scale(16),
    },
    viewLocation: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        marginBottom: verticalScale(16),
    },
    icLocation: {
        width: scale(20),
        height: scale(20),
    },
    txtName: {
        marginHorizontal: verticalScale(16),
        fontSize: moderateScale(24),
        color: Colors.blueDark,
    },
    txtHeadline: {
        fontSize: verticalScale(14),
        color: Colors.blueDark,
        marginTop: verticalScale(8),
        textAlign: 'center',
    },
    btnContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingHorizontal: verticalScale(16),
        width: '100%',
        marginTop: verticalScale(15),
        marginBottom: verticalScale(20),
    },
    viewBtn: {
        flex: 0.45,
    },
    textInfoPrice: {
        color: Colors.title,
        fontFamily: fontFamily.regular,
        fontSize: moderateScale(14),
        lineHeight: moderateScale(18),
        marginRight: scale(11),
    },
    textDeHR: {
        color: Colors.blueLight2,
        fontFamily: fontFamily.bold,
        fontSize: moderateScale(14),
        lineHeight: moderateScale(18),
    },
    icEdit: {
        width: verticalScale(22),
        height: verticalScale(22),
        tintColor: Colors.blueDark,
    },
    viewModalTop: {
        padding: verticalScale(12),
        backgroundColor: Colors.white,
        borderRadius: verticalScale(10),
    },
    viewModalBot: {
        marginBottom: verticalScale(20),
        paddingVertical: verticalScale(12),
        backgroundColor: Colors.white,
        borderRadius: verticalScale(10),
    },
    textSelectImg: {
        alignSelf: 'center',
        marginBottom: verticalScale(10),
        borderRadius: verticalScale(10),
    },
    textCancel: {
        textTransform: 'capitalize',
        lineHeight: verticalScale(20),
        alignSelf: 'center',
        borderRadius: verticalScale(10),
    },
    icStar: {
        width: verticalScale(16),
        height: verticalScale(16),
        tintColor: '#d3dae1',
        marginRight: verticalScale(6),
    },
    viewRate: {
        marginTop: verticalScale(8),
        flexDirection: 'row',
    },
    rowHeader: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    row: {
        width: '100%',
        marginTop: verticalScale(8),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    icLogo: {
        width: verticalScale(42),
        height: verticalScale(13),
        marginLeft: verticalScale(5),
        marginBottom: verticalScale(5),
        tintColor: Colors.white,
    },
    icDehr: {
        width: verticalScale(36),
        height: verticalScale(12),
        marginLeft: verticalScale(5),
        tintColor: Colors.green,
    },
    icDehrTotal: {
        width: verticalScale(36),
        height: verticalScale(12),
        marginEnd: verticalScale(5),
        tintColor: Colors.blueDark,
    },
    txtContactPrice: {
        fontSize: moderateScale(18),
        color: Colors.white,
    },
    btnContactPrice: {
        width: '100%',
        paddingHorizontal: verticalScale(10),
        paddingVertical: verticalScale(10),
        backgroundColor: Colors.orange,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: verticalScale(4),
        height: verticalScale(44),
        justifyContent: 'center',
    },
    btnContactInfo: {
        width: '100%',
        paddingHorizontal: verticalScale(10),
        paddingVertical: verticalScale(10),
        backgroundColor: Colors.blueDark,
        alignItems: 'center',
        borderRadius: verticalScale(4),
        height: verticalScale(63),
        justifyContent: 'center',
    },
    txtAddHiring: {
        fontFamily: fontFamily.bold,
        fontSize: moderateScale(16),
        textAlign: 'center',
    },
    topUpContainer: {
        width: '86%',
        borderRadius: verticalScale(10),
        padding: verticalScale(20),
        backgroundColor: Colors.white,
        alignItems: 'center',
    },
    modalCancelRequest: {
        width: '86%',
        borderRadius: verticalScale(10),
        backgroundColor: Colors.white,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: verticalScale(16),
    },
    txtTitleModal: {
        fontSize: moderateScale(20),
        lineHeight: moderateScale(26),
        width: '100%',
        textAlign: 'center',
        marginBottom: verticalScale(15),
    },
    bgTopup: {
        width: verticalScale(199),
        height: verticalScale(135),
        marginBottom: verticalScale(20),
    },
    txtDesc: {
        textAlign: 'center',
        marginBottom: verticalScale(30),
    },
    unlockContainer: {
        backgroundColor: Colors.white,
        borderTopLeftRadius: verticalScale(20),
        borderTopRightRadius: verticalScale(20),
        alignItems: 'center',
        padding: verticalScale(30),
        width: '100%',
    },
    txtTransactionAmount: {
        fontSize: moderateScale(30),
        color: Colors.green,
        lineHeight: moderateScale(32),
    },
    rowModal: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: verticalScale(20),
        width: '100%',
    },
    rowSimpleModal: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: verticalScale(20),
    },
    textContent: {
        paddingHorizontal: scale(12),
        paddingTop: verticalScale(10),
    },
    viewLine: {
        width: '100%',
        height: 1,
        backgroundColor: Colors.border,
        paddingHorizontal: scale(12),
    },
});
