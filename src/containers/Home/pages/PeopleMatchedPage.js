import React, { useState, useRef, useEffect } from 'react';
import { View, StyleSheet, Dimensions, ImageBackground, FlatList } from 'react-native';
import { useTheme } from '@/theme';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { UIHeader } from '../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../theme/Variables';
import { UIPeopleComponent } from '../components';
import { useNavigation } from '@react-navigation/native';
import RNPickerSelect from 'react-native-picker-select';
import { fontFamily } from '../../../theme/Fonts';
import { actionSearchResume } from '../duck/actions';
import { connect } from 'react-redux';
import { SCREEN } from '../../../constant';
const { width } = Dimensions.get('window');

const PeopleMatchedPage = (props) => {
    const { Images } = useTheme();
    const { t } = useTranslation();
    const navigation = useNavigation();
    const modalRef = useRef();
    const { actionSearchResume } = props || {};
    const [listResume, setListResume] = useState([]);

    useEffect(() => {
        actionSearchResume({}, (listResume) => {
            setListResume(listResume);
        });
    }, []);

    const onPressJob = (item) => {
        setJob(item);
        modalRef.open();
    };

    const onGoPeopleDetail = (userProfile) => {
        navigation.navigate(SCREEN.PEOPLE_DETAIL_PAGE, { userProfile });
    };

    const renderPeople = ({ item, index }) => {
        return (
            <UIPeopleComponent
                onPressAvatar={() => onGoPeopleDetail(item)}
                stylesDes={{ maxWidth: width - verticalScale(130) }}
                isMatch
                avatar={item.user_id && item.user_id.avatar_url}
                name={item.username}
                ratingsSumary={item.ratingsSumary ?? 0}
                position={item.intro.headline}
                location={item.intro.location}
                price={item.desired_salary}
                exp={item.years_of_experience}
            />
        );
    };

    return (
        <View style={styles.container}>
            <ImageBackground style={styles.bgHeader} resizeMode="stretch" imageStyle={styles.bgHeader} source={Images.chat.bg_chat}>
                <UIHeader
                    onPress={() => navigation.goBack()}
                    RightIcon="more"
                    LeftIcon="back_white"
                    title={t('home.people_matched')}
                    titleStyle={styles.txtTitle}
                />
                <View style={{ paddingHorizontal: verticalScale(16) }}>
                    <View style={styles.pickerContainer}>
                        <RNPickerSelect
                            textInputProps={styles.picker}
                            onValueChange={() => {}}
                            style={{
                                inputAndroid: {
                                    color: Colors.blueDark,
                                    width: '100%',
                                    height: verticalScale(50),
                                    paddingTop: verticalScale(10),
                                    lineHeight: verticalScale(20),
                                },
                            }}
                            useNativeAndroidPickerStyle
                            items={[
                                { label: 'IT', value: 'IT' },
                                { label: 'Software', value: 'Software' },
                                { label: 'Software', value: 'Software' },
                            ]}
                        />
                    </View>
                </View>
            </ImageBackground>
            <View style={styles.contentContainer}>
                <FlatList data={listResume} extraData={listResume} renderItem={renderPeople} />
            </View>
        </View>
    );
};

const mapStateToProps = (state) => ({
    homeReducer: state.homeReducer,
});
export default connect(mapStateToProps, {
    actionSearchResume,
})(PeopleMatchedPage);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    bgHeader: {
        width: '100%',
        height: verticalScale(130),
        borderBottomLeftRadius: verticalScale(15),
        borderBottomRightRadius: verticalScale(15),
    },
    txtTitle: {
        fontSize: moderateScale(20),
        color: Colors.white,
    },
    txtHotJob: {
        color: Colors.white,
        fontSize: moderateScale(12),
    },
    txtHotJobDes: {
        fontSize: moderateScale(12),
        color: '#9fa4be',
    },
    contentContainer: {
        marginTop: verticalScale(20),
        paddingHorizontal: verticalScale(16),
        flex: 1,
        backgroundColor: Colors.white,
    },
    picker: {
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
    },
    pickerContainer: {
        width: '100%',
        height: verticalScale(44),
        backgroundColor: Colors.white,
        borderRadius: verticalScale(6),
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: verticalScale(15),
        marginBottom: verticalScale(10),
    },
});
