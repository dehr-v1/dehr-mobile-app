import React, { useState } from 'react';
import { View, StyleSheet, FlatList, ScrollView, Image, TouchableOpacity } from 'react-native';
import { UIHeader } from '../../../components/common';
import { Colors } from '../../../theme/Variables';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import { moderateScale, verticalScale, width } from '../../../utils/ScalingUtils';
import { UIEss, UIOrganisation, UIPeopleSponsor } from '../components';
import { SCREEN } from '../../../constant';
import LinearGradient from 'react-native-linear-gradient';
import { useTheme } from '@/theme';
import { UIText, UIButtonChoose } from '../../../components/elements';
import UIClass from '../components/UIClass';

const fakeEss = [
    {
        coin: 100,
        title: 'DeHR JSC would like to make ESS',
        desc: 'Here are 11 things I wish I knew when I started my business. I hope they will save you some time and some anguish because (experience is a good teacher here)...',
        user: {
            fullname: 'Theresa Webb',
            avatar: null,
        },
    },
    {
        coin: 100,
        title: 'DeHR JSC would like to make ESS',
        desc: 'Here are 11 things I wish I knew when I started my business. I hope they will save you some time and some anguish because (experience is a good teacher here)...',
        user: {
            fullname: 'Theresa Webb',
            avatar: null,
        },
    },
    {
        coin: 100,
        title: 'DeHR JSC would like to make ESS',
        desc: 'Here are 11 things I wish I knew when I started my business. I hope they will save you some time and some anguish because (experience is a good teacher here)...',
        user: {
            fullname: 'Theresa Webb',
            avatar: null,
        },
    },
    {
        coin: 100,
        title: 'DeHR JSC would like to make ESS',
        desc: 'Here are 11 things I wish I knew when I started my business. I hope they will save you some time and some anguish because (experience is a good teacher here)...',
        user: {
            fullname: 'Theresa Webb',
            avatar: null,
        },
    },
];

const fakeClass = [
    {
        title: 'Adobe  XD in advance',
        times: 30,
        startDate: new Date(),
        price: 3213,
        mentor: {
            avatar: null,
        },
        progress: 80,
    },
    {
        title: 'Adobe  XD in advance',
        times: 30,
        startDate: new Date(),
        price: 3213,
        mentor: {
            avatar: null,
        },
        progress: 80,
    },
    {
        title: 'Adobe  XD in advance',
        times: 30,
        startDate: new Date(),
        price: 3213,
        mentor: {
            avatar: null,
        },
        progress: 80,
    },
];

const fakeLatestClass = [
    {
        title: 'Adobe  XD in advance',
        times: 30,
        startDate: new Date(),
        price: 3213,
        mentor: {
            avatar: null,
        },
    },
    {
        title: 'Adobe  XD in advance',
        times: 30,
        startDate: new Date(),
        price: 3213,
        mentor: {
            avatar: null,
        },
    },
    {
        title: 'Adobe  XD in advance',
        times: 30,
        startDate: new Date(),
        price: 3213,
        mentor: {
            avatar: null,
        },
        progress: 80,
    },
];

const fakeOrganisation = [
    {
        name: 'PNJ recruit Junior Front-End in next year 2021',
        desc: 'PNJ sponsors students who study Software Development. PNJ pays fee to University directly and canndidate works for us after graduated',
        available: new Date(),
    },
    {
        name: 'PNJ recruit Junior Front-End in next year 2021',
        desc: 'PNJ sponsors students who study Software Development. PNJ pays fee to University directly and canndidate works for us after graduated',
        available: new Date(),
    },
    {
        name: 'PNJ recruit Junior Front-End in next year 2021',
        desc: 'PNJ sponsors students who study Software Development. PNJ pays fee to University directly and canndidate works for us after graduated',
        available: new Date(),
    },
    {
        name: 'PNJ recruit Junior Front-End in next year 2021',
        desc: 'PNJ sponsors students who study Software Development. PNJ pays fee to University directly and canndidate works for us after graduated',
        available: new Date(),
    },
];

const BestOfYouPage = (props) => {
    const [filter, setFilter] = useState(0);

    const { t } = useTranslation();
    const { Images } = useTheme();
    const navigation = useNavigation();

    renderEss = ({ item, index }) => {
        return (
            <UIEss
                onPress={() => navigation.navigate(SCREEN.ESS_DETAIL_PAGE, { ess: item })}
                title={item.title}
                coin={item.coin}
                desc={item.description}
                user={item.user}
            />
        );
    };

    renderProgressClass = ({ item, index }) => {
        const { title, times, startDate, price, mentor, progress } = item || {};
        return (
            <View style={{ marginRight: verticalScale(20) }}>
                <UIClass
                    containerStyle={{ width: width - verticalScale(75) }}
                    title={title}
                    times={times}
                    startDate={startDate}
                    price={price}
                    mentor={mentor}
                    progress={progress || null}
                />
            </View>
        );
    };

    renderLatestClass = ({ item, index }) => {
        const { title, times, startDate, price, mentor, progress } = item || {};
        return (
            <View style={{ marginBottom: verticalScale(20) }}>
                <UIClass
                    containerStyle={{ width: width - verticalScale(32) }}
                    title={title}
                    times={times}
                    startDate={startDate}
                    price={price}
                    mentor={mentor}
                    progress={progress || null}
                />
            </View>
        );
    };

    renderOrganisation = ({ item, index }) => {
        const { name, desc, available } = item || {};
        return (
            <View style={{ marginRight: verticalScale(20) }}>
                <UIOrganisation containerStyle={{ width: width - verticalScale(75) }} name={name} desc={desc} available={available} />
            </View>
        );
    };

    renderPeopleSponser = ({ item, index }) => {
        return <UIPeopleSponsor />;
    };

    return (
        <View style={styles.container}>
            <UIHeader onPress={() => navigation.goBack()} LeftIcon="back" title={t('home.best_for_you')} />
            <View style={styles.contentContainer}>
                <View style={[styles.row, { justifyContent: 'space-around', marginBottom: verticalScale(20) }]}>
                    <UIButtonChoose
                        containerStyle={{ flex: 0.3 }}
                        text={t('home.ess')}
                        isChoose={filter == 0}
                        onPress={() => setFilter(0)}
                    />
                    <UIButtonChoose
                        containerStyle={{ flex: 0.3 }}
                        text={t('home.learning')}
                        isChoose={filter == 1}
                        onPress={() => setFilter(1)}
                    />
                    <UIButtonChoose
                        containerStyle={{ flex: 0.3 }}
                        text={t('home.sponsor')}
                        isChoose={filter == 2}
                        onPress={() => setFilter(2)}
                    />
                </View>
                {filter == 0 && (
                    <View style={{ width: '100%' }}>
                        <FlatList data={fakeEss} extraData={fakeEss} renderItem={renderEss} />
                    </View>
                )}
                {filter == 1 && (
                    <ScrollView style={{ width: '100%' }}>
                        <View style={styles.overviewContainer}>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                colors={['#00af83', '#1ccea1']}
                                style={styles.linear}
                            >
                                <View style={styles.icContainer}>
                                    <Image style={styles.ic} source={Images.home.ic_class} />
                                    <UIText font="bold" style={styles.txtTitle} text={t('home.class_completed')} />
                                </View>
                                <UIText font="bold" style={styles.txtContent} text="12" />
                            </LinearGradient>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                colors={['#2599db', '#57c2ff']}
                                style={styles.linear}
                            >
                                <View style={styles.icContainer}>
                                    <Image style={styles.ic} source={Images.home.ic_course} />
                                    <UIText font="bold" style={styles.txtTitle} text={t('home.time_duration')} />
                                </View>
                                <UIText font="bold" style={styles.txtContent} text="12h 30m" />
                            </LinearGradient>
                        </View>
                        <View style={[styles.row, { justifyContent: 'space-between', marginBottom: verticalScale(10) }]}>
                            <UIText style={styles.txtSeeAll} text={t('home.available_class')} />
                            <TouchableOpacity onPress={() => navigation.navigate(SCREEN.CLASS_PAGE, { classes: fakeLatestClass })}>
                                <UIText style={styles.txtSeeAll} text={t('home.see_all')} />
                            </TouchableOpacity>
                        </View>
                        <FlatList horizontal renderItem={renderProgressClass} data={fakeClass} extraData={fakeClass} />
                        <View
                            style={[
                                styles.row,
                                { justifyContent: 'space-between', marginBottom: verticalScale(10), marginTop: verticalScale(30) },
                            ]}
                        >
                            <UIText style={styles.txtSeeAll} text={t('home.available_class')} />
                            <TouchableOpacity onPress={() => navigation.navigate(SCREEN.CLASS_PAGE, { classes: fakeLatestClass })}>
                                <UIText style={styles.txtSeeAll} text={t('home.see_all')} />
                            </TouchableOpacity>
                        </View>
                        <FlatList renderItem={renderLatestClass} data={fakeLatestClass} extraData={fakeLatestClass} />
                    </ScrollView>
                )}
                {filter == 2 && (
                    <ScrollView style={{ width: '100%' }}>
                        <View style={[styles.row, { justifyContent: 'space-between', marginBottom: verticalScale(10) }]}>
                            <UIText style={styles.txtSeeAll} text={t('home.organisation')} />
                            <TouchableOpacity
                                onPress={() => navigation.navigate(SCREEN.ORGANISATION_PAGE, { organisation: fakeOrganisation })}
                            >
                                <UIText style={styles.txtSeeAll} text={t('home.see_all')} />
                            </TouchableOpacity>
                        </View>
                        <FlatList horizontal renderItem={renderOrganisation} data={fakeOrganisation} extraData={fakeOrganisation} />
                        <View
                            style={[
                                styles.row,
                                { justifyContent: 'space-between', marginBottom: verticalScale(10), marginTop: verticalScale(30) },
                            ]}
                        >
                            <UIText style={styles.txtSeeAll} text={t('home.people')} />
                            <TouchableOpacity onPress={() => navigation.navigate(SCREEN.PEOPLE_SPONSOR_PAGE)}>
                                <UIText style={styles.txtSeeAll} text={t('home.see_all')} />
                            </TouchableOpacity>
                        </View>
                        <FlatList renderItem={renderPeopleSponser} data={fakeLatestClass} extraData={fakeLatestClass} />
                    </ScrollView>
                )}
            </View>
        </View>
    );
};

export default BestOfYouPage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        marginHorizontal: verticalScale(16),
        marginBottom: verticalScale(40),
        alignItems: 'center',
        flex: 1,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
    },
    linear: {
        width: (width - verticalScale(45)) / 2,
        height: verticalScale(120),
        borderRadius: verticalScale(8),
    },
    overviewContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: verticalScale(20),
    },
    ic: {
        width: verticalScale(32),
        height: verticalScale(32),
    },
    icContainer: {
        flexDirection: 'row',
        marginTop: verticalScale(15),
        marginLeft: verticalScale(17),
        alignItems: 'center',
    },
    txtTitle: {
        fontSize: moderateScale(12),
        color: Colors.white,
        marginLeft: verticalScale(10),
        lineHeight: moderateScale(22),
    },
    txtContent: {
        fontSize: moderateScale(26),
        color: Colors.white,
        lineHeight: moderateScale(36),
        width: '100%',
        textAlign: 'center',
        marginTop: verticalScale(6),
    },
    txtSeeAll: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
    },
});
