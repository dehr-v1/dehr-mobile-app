import React, { useEffect, useState } from 'react';
import { View, StyleSheet, FlatList, Image, TouchableOpacity } from 'react-native';
import { UIHeader } from '../../../components/common';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { useTheme } from '@/theme';
import { UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { connect } from 'react-redux';
import { actionGetProfileView, actionAcceptUnlock, actionRejectUnlock } from '../duck/actions';
import { actionBalanceWallet } from '../../Wallet/duck/actions';
import { BASE_IMAGE_URL, EXCHANGE_DATA_CONTRACT_ADDRESS, DEHR_CONTRACT_ADDRESS, RPC_ENDPOINT } from '../../../constant/value.constant';
import { getSeedPhase, getCurrentUser } from '../../../services/persistence/store.config';
import Toast from '../../../components/elements/Toast';
import { SCREEN } from '../../../constant';
// Test
import ExchangeData from '../../../abis/ExchangeData';
import DeHR from '../../../abis/DeHR';
import * as KardiaClient from 'kardia-js-sdk';
import Web3 from 'web3';
import * as EthCrypto from 'eth-crypto';

import { ScrollView } from 'react-native-gesture-handler';

const web3 = new Web3();
const kardiaClient = new KardiaClient.default({ endpoint: RPC_ENDPOINT });
const transactionModule = kardiaClient.transaction;
const contractInstance = kardiaClient.contract;

const ProfileViewPage = (props) => {
    const navigation = useNavigation();
    const { t } = useTranslation();
    const { Images } = useTheme();
    const { actionGetProfileView, actionBalanceWallet, actionAcceptUnlock, actionRejectUnlock } = props;
    const [profileViewSender, setProfileViewSender] = useState([]);
    const [profileViewRecipient, setProfileViewRecipient] = useState([]);

    useEffect(() => {
        let request = {
            type: 1,
        };
        actionGetProfileView({ request }, (data) => {
            setProfileViewSender(data);
        });
        request = {
            type: 0,
        };
        actionGetProfileView({ request }, (data) => {
            setProfileViewRecipient(data);
        });
    }, []);

    const requestUnlock = async (item) => {
        setProfileViewSender(profileViewSender.filter((data) => data._id !== item._id));
        const amountUnlock = item.contact_price;
        const recipient = item.wallet; // lấy từ server
        const getWallet = await getSeedPhase();
        const wallet = JSON.parse(getWallet);
        const address = wallet?.address;
        const callbackResult = async function (result) {
            if (result) {
                contractInstance.updateAbi(ExchangeData.abi);
                contractInstance.updateByteCode(ExchangeData.bytecode);
                const invocation = contractInstance.invokeContract('request1', [recipient]);
                const txHash = await invocation.send(privateKey, EXCHANGE_DATA_CONTRACT_ADDRESS);
                const interval = setInterval(async function () {
                    const receipt = await transactionModule.getTransactionReceipt(txHash);
                    if (receipt) {
                        clearInterval(interval);
                        if (receipt.status === 1) {
                            Toast('SUCCESS!');
                        } else {
                            Toast('ERROR!', 'error');
                        }
                    }
                }, 1000);
            } else {
                Toast('ERROR!', 'error');
            }
        };
        if (address) {
            actionBalanceWallet({ address }, async (coin) => {
                if (coin >= amountUnlock) {
                    if (wallet) {
                        const privateKey = wallet?.privateKey;
                        await dehrApproveExchangedata(amountUnlock, privateKey, callbackResult);
                    } else {
                        Toast(t('wallet.setup_wallet'), 'error');
                    }
                } else {
                    Toast(t('home.no_coin'));
                }
            });
        } else {
            Toast(t('wallet.setup_wallet'), 'error');
        }
    };

    const cancelRequestUnlock = async (item) => {
        setProfileViewSender(profileViewSender.filter((data) => data._id !== item._id));
        const requestId = item.requestId;
        contractInstance.updateAbi(ExchangeData.abi);
        contractInstance.updateByteCode(ExchangeData.bytecode);
        const getWallet = await getSeedPhase();
        const wallet = JSON.parse(getWallet);
        if (wallet) {
            const privateKey = wallet?.privateKey;
            const invocation = contractInstance.invokeContract('cancelRequest', [requestId]);
            const txHash = await invocation.send(privateKey, EXCHANGE_DATA_CONTRACT_ADDRESS);
            const interval = setInterval(async function () {
                const receipt = await transactionModule.getTransactionReceipt(txHash);
                if (receipt) {
                    clearInterval(interval);
                    if (receipt.status === 1) {
                        Toast('SUCCESS!');
                    } else {
                        Toast('ERROR!', 'error');
                    }
                }
            }, 1000);
        } else {
            Toast(t('wallet.setup_wallet'));
        }
    };

    const acceptRequestUnlock = async (item) => {
        setProfileViewRecipient(profileViewRecipient.filter((data) => data._id !== item._id));
        const amountUnlock = item.contact_price;
        const requestId = item.requestId;
        const requesterAddress = item.wallet;
        const requesterPublicKey = item.publicKey ?? '00';
        const getWallet = await getSeedPhase();
        const wallet = JSON.parse(getWallet);
        const address = wallet?.address;
        const privateKey = wallet?.privateKey;
        if (address) {
            actionBalanceWallet({ address }, async (coin) => {
                if (parseInt(coin) >= parseInt(amountUnlock)) {
                    const user = (await getCurrentUser()) || null;
                    const userData = JSON.parse(user);
                    const encrypted = await EthCrypto.encryptWithPublicKey(
                        requesterPublicKey.substring(2),
                        `${userData.email}-${userData.phoneNumber}` // đây là cái mình lấy từ storage lên
                    );
                    const encryptedData = {
                        iv: encrypted.iv,
                        ephemPublicKey: encrypted.ephemPublicKey,
                        ciphertext: encrypted.ciphertext,
                        mac: encrypted.mac,
                    };

                    const data = {
                        requestId: item.requestId,
                        requesterAddress: item.wallet,
                        encryptedData: encryptedData,
                    };
                    actionAcceptUnlock({ data }, async (isSuccess) => {
                        if (isSuccess) {
                            contractInstance.updateAbi(ExchangeData.abi);
                            contractInstance.updateByteCode(ExchangeData.bytecode);
                            const invocation = contractInstance.invokeContract('accept', [requestId]);
                            const txHash = await invocation.send(privateKey, EXCHANGE_DATA_CONTRACT_ADDRESS);
                            const interval = setInterval(async function () {
                                const receipt = await transactionModule.getTransactionReceipt(txHash);
                                if (receipt) {
                                    clearInterval(interval);

                                    if (receipt.status === 1) {
                                        Toast('SUCCESS!');
                                    } else {
                                        Toast('ERROR!', 'error');
                                    }
                                }
                            }, 1000);
                        }
                    });
                } else {
                    Toast(t('home.no_coin'));
                }
            });
        } else {
            Toast(t('wallet.setup_wallet'), 'error');
        }
    };

    const rejectRequestUnlock = async (item) => {
        setProfileViewRecipient(profileViewRecipient.filter((data) => data._id !== item._id));
        const data = {
            requestId: item.requestId,
            requesterAddress: item.wallet,
        };
        actionRejectUnlock({ data }, (isSuccess) => {
            if (isSuccess) {
                Toast('SUCCESS!');
            }
        });
    };

    const dehrApproveExchangedata = async (amount, privateKey, callbackResult) => {
        contractInstance.updateAbi(DeHR.abi);
        contractInstance.updateByteCode(DeHR.bytecode);
        const invocation = contractInstance.invokeContract('approve', [EXCHANGE_DATA_CONTRACT_ADDRESS, web3.utils.toBN(amount)]);
        const txHash = await invocation.send(privateKey, DEHR_CONTRACT_ADDRESS);

        /**
         * waiting for transaction accept to block
         * time may later 20(s)
         * */
        const interval = setInterval(async function () {
            const receipt = await transactionModule.getTransactionReceipt(txHash);
            if (receipt) {
                clearInterval(interval);
                /**
                 * Transaction status. 1 if transaction is executed successfully, otherwise 0.
                 * */
                if (receipt.status === 1) {
                    callbackResult(true);
                } else {
                    callbackResult(false);
                }
            }
        }, 1000);
    };

    const renderItemSender = ({ item, index }) => {
        return (
            <View style={styles.itemContainer}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => navigation.navigate(SCREEN.PEOPLE_DETAIL_PAGE, { userProfile: item })}>
                        <Image
                            style={styles.icAvatar}
                            source={
                                item?.user_id?.avatar_url ? { uri: BASE_IMAGE_URL + item.user_id.avatar_url } : Images.profile.ic_avatar
                            }
                        />
                    </TouchableOpacity>
                    <View>
                        <UIText numberOfLines={2} ellipsizeMode="tail" font="medium" style={styles.txtName} text={item.username} />
                        <UIText numberOfLines={4} ellipsizeMode="tail" style={styles.txtTitle} text={item.intro.headline} />
                    </View>
                </View>
                {/* <View style={{ flexDirection: 'row', alignItems: 'center', flex: 0.3 }} >
                    <TouchableOpacity onPress={() => cancelRequestUnlock(item)} style={[styles.icAction, { borderColor: Colors.error }]} >
                        <Image style={[styles.imgAction, { tintColor: Colors.error }]} source={Images.home.ic_x} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => requestUnlock(item)} style={[styles.icAction, { borderColor: Colors.lightBlue }]} >
                        <Image style={[styles.imgAction, { tintColor: Colors.lightBlue }]} source={Images.home.ic_approve} />
                    </TouchableOpacity>
                </View> */}
            </View>
        );
    };

    const renderItemRecipient = ({ item, index }) => {
        return (
            <View style={styles.itemContainer}>
                <View style={{ flexDirection: 'row', alignItems: 'center', flex: 0.57 }}>
                    <TouchableOpacity onPress={() => navigation.navigate(SCREEN.PEOPLE_DETAIL_PAGE, { userProfile: item })}>
                        <Image
                            style={styles.icAvatar}
                            source={
                                item?.user_id?.avatar_url ? { uri: BASE_IMAGE_URL + item.user_id.avatar_url } : Images.profile.ic_avatar
                            }
                        />
                    </TouchableOpacity>
                    <View>
                        <UIText numberOfLines={2} ellipsizeMode="tail" font="medium" style={styles.txtName} text={item.username || ''} />
                        <UIText numberOfLines={4} ellipsizeMode="tail" style={styles.txtTitle} text={item.intro.headline || ''} />
                    </View>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', flex: 0.27 }}>
                    <TouchableOpacity onPress={() => rejectRequestUnlock(item)} style={[styles.icAction, { borderColor: Colors.error }]}>
                        <Image style={[styles.imgAction, { tintColor: Colors.error }]} source={Images.home.ic_x} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => acceptRequestUnlock(item)}
                        style={[styles.icAction, { borderColor: Colors.lightBlue }]}
                    >
                        <Image style={[styles.imgAction, { tintColor: Colors.lightBlue }]} source={Images.home.ic_approve} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <UIHeader
                style={{ backgroundColor: Colors.white }}
                LeftIcon="back"
                onPress={() => navigation.goBack()}
                title={t('home.profile_views')}
            />
            <UIText font="bold" style={styles.txtTitleList} text={t('home.send')} />
            {profileViewSender.length ? (
                <View style={styles.listContainer}>
                    <FlatList
                        data={profileViewSender}
                        extraData={profileViewSender}
                        renderItem={renderItemSender}
                        keyExtractor={(_, index) => index.toString()}
                    />
                </View>
            ) : (
                <UIText font="bold" style={styles.txtEmptyList} text={t('common.empty')} />
            )}
            <UIText font="bold" style={styles.txtTitleList} text={t('home.receiver')} />
            {profileViewRecipient.length ? (
                <View style={styles.listContainer}>
                    <FlatList
                        data={profileViewRecipient}
                        extraData={profileViewRecipient}
                        renderItem={renderItemRecipient}
                        keyExtractor={(_, index) => index.toString()}
                    />
                </View>
            ) : (
                <UIText font="bold" style={styles.txtEmptyList} text={t('common.empty')} />
            )}
        </View>
    );
};

const mapStateToProps = (state) => ({
    homeReducer: state.homeReducer,
});
export default connect(mapStateToProps, {
    actionGetProfileView,
    actionBalanceWallet,
    actionAcceptUnlock,
    actionRejectUnlock,
})(ProfileViewPage);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    listContainer: {
        height: '40%',
        flex: 1,
    },
    itemContainer: {
        width: '100%',
        padding: verticalScale(16),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: Colors.white,
        marginTop: verticalScale(1),
    },
    icAvatar: {
        width: verticalScale(40),
        height: verticalScale(40),
        borderRadius: verticalScale(22),
        marginRight: verticalScale(12),
    },
    txtName: {
        fontSize: moderateScale(16),
        lineHeight: moderateScale(21),
        maxWidth: verticalScale(200),
    },
    txtTitleList: {
        width: '100%',
        height: verticalScale(50),
        backgroundColor: Colors.gray,
        fontSize: moderateScale(16),
        paddingLeft: verticalScale(30),
        paddingTop: verticalScale(12),
        textAlignVertical: 'center',
        justifyContent: 'center',
    },
    txtEmptyList: {
        fontSize: moderateScale(16),
        paddingVertical: verticalScale(12),
        alignItems: 'center',
        justifyContent: 'center',
        textAlignVertical: 'center',
        textAlign: 'center',
    },
    txtTitle: {
        fontSize: moderateScale(14),
        lineHeight: moderateScale(16),
        color: Colors.message,
        maxWidth: verticalScale(260),
    },
    icAction: {
        width: verticalScale(36),
        height: verticalScale(36),
        borderRadius: verticalScale(20),
        borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: verticalScale(10),
    },
    imgAction: {
        width: verticalScale(15),
        height: verticalScale(15),
    },
});
