import React from 'react';
import { View, ImageBackground, StyleSheet } from 'react-native';
import { UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import moment from 'moment';

const UIOrganisation = (props) => {
    const { Images } = useTheme();
    const { t } = useTranslation();

    const { name, desc, available, containerStyle } = props;

    return (
        <View style={[styles.container, containerStyle]}>
            <ImageBackground style={styles.bg} imageStyle={styles.backgroundImage} source={Images.demo.bg_ess_sponsor} />
            <View style={styles.infoContainer}>
                <UIText font="bold" style={styles.txtTitle} text={name} />
                <UIText style={styles.txtDesc} text={desc} />
                <View style={[styles.row]}>
                    <View style={styles.dotGreen} />
                    <UIText style={styles.txtAvailable} text={`${t('home.available_to')} ${moment(available).format('YYYY, MMM Do')}`} />
                </View>
            </View>
        </View>
    );
};

export default UIOrganisation;

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        borderRadius: verticalScale(10),
        borderWidth: 1,
        borderColor: Colors.border,
        width: '100%',
    },
    bg: {
        flex: 1,
        height: verticalScale(180),
        borderTopLeftRadius: verticalScale(10),
        borderTopRightRadius: verticalScale(10),
    },
    backgroundImage: {
        width: null,
        height: null,
    },
    infoContainer: {
        padding: verticalScale(15),
    },
    txtTitle: {
        color: Colors.blueDark,
    },
    txtDesc: {
        fontSize: moderateScale(14),
        color: Colors.title,
        marginTop: verticalScale(6),
        marginBottom: verticalScale(15),
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    dotGreen: {
        width: verticalScale(12),
        height: verticalScale(12),
        borderRadius: verticalScale(6),
        backgroundColor: Colors.green,
    },
    txtAvailable: {
        marginLeft: verticalScale(7),
        fontSize: moderateScale(12),
        color: Colors.lightGray,
    },
});
