import React from 'react';
import { View, ImageBackground, StyleSheet, Image } from 'react-native';
import { UIButton, UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';

const UIEss = (props) => {
    const { Images } = useTheme();
    const { t } = useTranslation();

    const { coin, title, desc, user, onPress } = props;

    return (
        <View style={styles.container}>
            <ImageBackground style={styles.bg} imageStyle={styles.backgroundImage} source={Images.demo.bg_ess}>
                {/* <View style={styles.priceContainer} >
                    <UIText font="bold" style={styles.txtCoin} text={coin} />
                    <Image source={Images.wallet.ic_logo} style={styles.icLogo} />
                </View> */}
            </ImageBackground>
            <View style={styles.infoContainer}>
                <UIText style={styles.txtBusiness} text={t('home.business')} />
                <UIText font="bold" style={styles.txtTitle} text={title} />
                <UIText style={styles.txtDesc} text={desc} />
                <View style={[styles.row, { justifyContent: 'space-between' }]}>
                    <View style={styles.row}>
                        <Image style={styles.icAvatar} source={user.avatar ? { uri: user.avatar } : Images.profile.ic_avatar} />
                        <UIText font="bold" style={styles.txtFullname} text={user.fullname} />
                    </View>
                    <UIButton onPress={onPress} style={styles.btn} type="BgGreen" text={t('home.join')} />
                </View>
            </View>
        </View>
    );
};

export default UIEss;

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        borderRadius: verticalScale(10),
        borderWidth: 1,
        borderColor: Colors.border,
        width: '100%',
        marginBottom: verticalScale(20),
    },
    bg: {
        flex: 1,
        height: verticalScale(180),
        borderTopLeftRadius: verticalScale(10),
        borderTopRightRadius: verticalScale(10),
    },
    backgroundImage: {
        width: null,
        height: null,
    },
    txtCoin: {
        fontSize: moderateScale(14),
        color: Colors.white,
    },
    icLogo: {
        width: verticalScale(30),
        height: verticalScale(10),
    },
    priceContainer: {
        flexDirection: 'row',
        paddingHorizontal: verticalScale(8),
        backgroundColor: Colors.blueDark,
        position: 'absolute',
        right: verticalScale(4),
        bottom: verticalScale(6),
        borderRadius: verticalScale(3),
        alignItems: 'center',
    },
    txtBusiness: {
        fontSize: moderateScale(12),
        color: Colors.lightGray,
        textTransform: 'uppercase',
    },
    infoContainer: {
        padding: verticalScale(15),
    },
    txtTitle: {
        color: Colors.blueDark,
    },
    txtDesc: {
        fontSize: moderateScale(14),
        color: Colors.title,
        marginTop: verticalScale(6),
        marginBottom: verticalScale(15),
    },
    icAvatar: {
        width: verticalScale(32),
        height: verticalScale(32),
        borderRadius: verticalScale(16),
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    txtFullname: {
        fontSize: verticalScale(14),
        color: Colors.blueDark,
        marginLeft: verticalScale(12),
    },
    btn: {
        width: verticalScale(98),
        height: verticalScale(31),
    },
});
