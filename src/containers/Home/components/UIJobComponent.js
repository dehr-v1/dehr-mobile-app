import React, { useMemo } from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import moment from 'moment';
import _isNumber from 'lodash/isNumber';
import { UIText, UIAvatar } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@/theme';
import { BASE_IMAGE_URL } from '../../../constant/value.constant';
import { fontFamily } from '../../../theme/Fonts';

const UIJobComponent = (props) => {
    const { t } = useTranslation();
    const { Images } = useTheme();
    const {
        logo,
        companyName,
        position,
        location,
        des,
        type,
        containerStyle,
        onPress,
        onPressAvatar,
        date,
        matched,
        isApplied,
        ableToApply,
        status,
        applicants,
        viewCount,
        visibility,
    } = props;

    const renderButtons = () => {
        if (ableToApply === true) {
            return (
                <TouchableOpacity
                    onPress={onPress}
                    style={[
                        styles.button,
                        {
                            backgroundColor: !!isApplied === false ? Colors.lightGreen : Colors.grayHiring,
                        },
                    ]}
                >
                    <UIText
                        style={[
                            styles.buttonText,
                            {
                                color: !!isApplied === false ? Colors.white : Colors.lightGray4,
                            },
                        ]}
                        font="bold"
                        text={!isApplied? t('home.apply_now') : t('home.applied')}
                    />
                </TouchableOpacity>
            );
        } else if (visibility === 'show') {
            return (
                <TouchableOpacity
                    onPress={onPress}
                    style={[
                        styles.button,
                        {
                            backgroundColor: Colors.lightYellow,
                            paddingHorizontal: verticalScale(15)
                        },
                    ]}
                >
                    <UIText
                        style={[
                            styles.buttonText,
                            {
                                color: Colors.white,
                            },
                        ]}
                        font="bold"
                        text="Show"
                    />
                </TouchableOpacity>
            );
        } else if (visibility === 'hide') {
            return (
                <TouchableOpacity
                    onPress={onPress}
                    style={[
                        styles.button,
                        {
                            backgroundColor: Colors.lightGray5,
                            paddingHorizontal: verticalScale(15)
                        },
                    ]}
                >
                    <UIText
                        style={[
                            styles.buttonText,
                            {
                                color: Colors.white,
                            },
                        ]}
                        font="bold"
                        text="Hide"
                    />
                </TouchableOpacity>
            );
        } else {
            return <></>;
        }
    };

    return (
        <TouchableOpacity onPress={onPress} >
            <View style={[styles.container, containerStyle, matched && { marginVertical: verticalScale(12) }]}>
                {matched && (
                    <View style={styles.tagContainer}>
                        <View style={styles.match}>
                            <View style={styles.percentContainer}>
                                <UIText font="bold" style={styles.txtPercent} text={`${matched}%`} />
                            </View>
                            <View style={styles.matchedContainer}>
                                <UIText font="bold" style={styles.txtPercent} text={t('home.matched')} />
                            </View>
                        </View>
                        {/* <View style={styles.applyContainer}>
                        <UIText font="bold" style={styles.txtPercent} text={t('home.apply_now')} />
                    </View> */}
                    </View>
                )}
                <View style={styles.companyContainer}>
                    <UIAvatar url={logo ? BASE_IMAGE_URL + logo : null} onPressAvatar={onPressAvatar} style={styles.imageContainer} />
                    <UIText
                        numberOfLines={1}
                        ellipsizeMode="tail"
                        style={{ color: Colors.blueDark }}
                        font="bold"
                        text={companyName ?? ''}
                    />
                </View>
                {/* <UIText font="bold" style={styles.txtPosition} text={position} /> */}
                <View style={styles.positionContainer}>
                    <View style={{ flexGrow: 1 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={Images.profile.ic_position_2} style={styles.icPosition} />
                            <UIText style={styles.txtLocation} text={companyName} />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={Images.home.ic_building} style={styles.icPosition} />
                            <UIText style={styles.txtLocation} text={location} />
                        </View>
                        <UIText numberOfLines={3} ellipsizeMode="tail" style={styles.txtDes} text={des} />
                    </View>
                    <View style={styles.typeContainer}>
                        <Image style={styles.icClock} source={Images.home.ic_clock} />
                        <UIText style={styles.txtType} text={type == 'fulltime' ? t('home.full_time') : t('home.part_time')} />
                    </View>
                </View>
                <View style={{ flexDirection: 'row', marginBottom: verticalScale(15), alignItems: 'center' }}>
                    <View style={{ flexGrow: 1, flexDirection: 'row' }}>
                        {date && (
                            <UIText
                                font="medium"
                                style={{ color: Colors.lightGray, fontSize: moderateScale(12) }}
                                text={moment(date).fromNow()}
                            />
                        )}
                        <UIText
                            style={{ marginHorizontal: verticalScale(5), fontSize: moderateScale(12), color: Colors.lightGray }}
                            text="•"
                        />
                        <UIText
                            font="medium"
                            style={{ color: Colors.blueLight2, fontSize: moderateScale(12) }}
                            text={`${applicants || 0} ${t('home.applicants')}`}
                        />
                        {_isNumber(viewCount) && (
                            <>
                                <UIText
                                    style={{ marginHorizontal: verticalScale(5), fontSize: moderateScale(12), color: Colors.lightGray }}
                                    text="•"
                                />
                                <UIText
                                    font="medium"
                                    style={{ color: Colors.blueLight2, fontSize: moderateScale(12) }}
                                    text={`${viewCount || 0} ${t('jobs.viewers')}`}
                                />
                            </>
                        )}
                        {status &&
                            <>
                                <UIText
                                    style={{ marginHorizontal: verticalScale(5), fontSize: moderateScale(12), color: Colors.lightGray }}
                                    text="•"
                                />
                                {/* <UIText
                                    style={{ color: Colors.lightGreen, fontSize: moderateScale(12) }}
                                    font="bold"
                                    text={t('home.ongoing')}
                                /> */}
                                <UIText
                                    font="medium"
                                    style={{ color: status=="ongoing"? Colors.lightGreen: status=="pending"? Colors.lightYellow: Colors.lightGray, fontSize: moderateScale(12),textTransform: 'capitalize' }}
                                    font="bold"
                                    text={status}
                                />
                                {/* <UIText style={{ color: Colors.lightGray, fontSize: moderateScale(12) }} font="bold" text={t('home.done')} /> */}
                            </>
                        }
                    </View>
                    {renderButtons()}
                </View>
            </View>
        </TouchableOpacity>
    );
};

export default UIJobComponent;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(10),
        paddingHorizontal: verticalScale(15),
        marginVertical: verticalScale(5),
        shadowColor: 'rgba(15, 15, 29, 0.25)',
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowRadius: 15,
        shadowOpacity: 0.37,
        backgroundColor: 'white'
    },
    button: {
        paddingHorizontal: verticalScale(10),
        paddingVertical: verticalScale(6),
        borderRadius: verticalScale(3),
    },
    buttonText: {
        fontSize: moderateScale(13),
        lineHeight: verticalScale(22),
    },
    txtPercent: {
        color: Colors.white,
        fontFamily: fontFamily.bold,
        fontSize: moderateScale(12),
        textTransform: 'uppercase',
    },
    match: {
        flexDirection: 'row',
        height: verticalScale(24),
    },
    percentContainer: {
        width: verticalScale(42),
        height: '100%',
        backgroundColor: Colors.green,
        borderTopLeftRadius: verticalScale(4),
        borderBottomLeftRadius: verticalScale(4),
        justifyContent: 'center',
        alignItems: 'center',
    },
    matchedContainer: {
        width: verticalScale(81),
        height: '100%',
        backgroundColor: '#3e4a7e',
        borderTopRightRadius: verticalScale(4),
        borderBottomRightRadius: verticalScale(4),
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: -1,
    },
    applyContainer: {
        width: verticalScale(86),
        height: '100%',
        backgroundColor: Colors.green,
        borderRadius: verticalScale(4),
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: verticalScale(10),
    },
    tagContainer: {
        flexDirection: 'row',
        height: verticalScale(24),
        marginTop: -verticalScale(12),
        width: '100%',
    },
    logo: {
        width: verticalScale(44),
        height: verticalScale(44),
        borderRadius: verticalScale(22),
        marginRight: verticalScale(15),
    },
    imageContainer: {
        marginRight: verticalScale(10),
    },
    companyContainer: {
        flexDirection: 'row',
        marginTop: verticalScale(14),
        paddingRight: verticalScale(44),
        alignItems: 'center',
        marginBottom: 8,
    },
    txtPosition: {
        color: Colors.blueDark,
        marginTop: verticalScale(10),
        marginBottom: verticalScale(15),
    },
    icPosition: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    positionContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    txtLocation: {
        flex: 0.75,
        marginLeft: verticalScale(5),
        fontSize: moderateScale(14),
        color: Colors.title,
        flexGrow: 1,
    },
    icClock: {
        width: verticalScale(32),
        height: verticalScale(32),
        marginBottom: -8,
    },
    txtType: {
        fontSize: moderateScale(12),
    },
    typeContainer: {
        flex: 0.25,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
