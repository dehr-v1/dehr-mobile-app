import React from 'react';
import { View, StyleSheet, Image, ImageBackground, TouchableOpacity } from 'react-native';
import { UIButton, UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@/theme';
import { BASE_IMAGE_URL } from '../../../constant/value.constant';

const UIPeopleComponent = (props) => {
    const { t } = useTranslation();
    const { Images } = useTheme();
    const {
        avatar,
        name,
        position,
        ratingsSumary,
        location,
        exp,
        containerStyle,
        stylesDes,
        price,
        isMatch = false,
        isHiring = false,
        onPressAvatar,
    } = props;

    return (
        <View style={[styles.container, containerStyle]}>
            {(isMatch || isHiring) && (
                <View style={styles.tagContainer}>
                    <View style={styles.match}>
                        <View style={styles.percentContainer}>
                            <UIText font="bold" style={styles.txtPercent} text="80%" />
                        </View>
                        <View style={styles.matchedContainer}>
                            <UIText style={styles.txtPercent} text={t('home.matched')} />
                        </View>
                        <View style={styles.matchedImg}>
                            <Image style={styles.icLocation} source={Images.home.ic_match} />
                        </View>
                    </View>
                </View>
            )}
            <View style={styles.companyContainer}>
                <TouchableOpacity onPress={onPressAvatar}>
                    <ImageBackground
                        style={styles.imageContainer}
                        imageStyle={styles.imageContainer}
                        source={
                            avatar != '' && avatar != null && avatar != BASE_IMAGE_URL
                                ? { uri: BASE_IMAGE_URL + avatar }
                                : Images.profile.ic_avatar
                        }
                    />
                </TouchableOpacity>
                <View>
                    <UIText
                        numberOfLines={1}
                        ellipsizeMode="tail"
                        style={[{ color: Colors.blueDark }, stylesDes]}
                        font="bold"
                        text={name}
                    />
                    <UIText
                        numberOfLines={2}
                        ellipsizeMode="tail"
                        style={[{ color: Colors.title, fontSize: moderateScale(14), minHeight: verticalScale(60) }, stylesDes]}
                        font="regular"
                        text={position}
                    />
                </View>
            </View>
            <View style={styles.secondContainer}>
                <View>
                    <View style={styles.row}>
                        <Image style={styles.icLocation} source={Images.home.ic_location_blue} />
                        <UIText style={styles.txtLocation} text={location || 'Ho Chi Minh City'} />
                    </View>
                    <View style={[{ marginTop: verticalScale(10) }, styles.row]}>
                        <Image
                            style={[styles.icStar, ratingsSumary >= 1 && { tintColor: Colors.green }]}
                            source={Images.home.ic_star_vote}
                        />
                        <Image
                            style={[styles.icStar, ratingsSumary >= 2 && { tintColor: Colors.green }]}
                            source={Images.home.ic_star_vote}
                        />
                        <Image
                            style={[styles.icStar, ratingsSumary >= 3 && { tintColor: Colors.green }]}
                            source={Images.home.ic_star_vote}
                        />
                        <Image
                            style={[styles.icStar, ratingsSumary >= 4 && { tintColor: Colors.green }]}
                            source={Images.home.ic_star_vote}
                        />
                        <Image
                            style={[styles.icStar, ratingsSumary >= 5 && { tintColor: Colors.green }]}
                            source={Images.home.ic_star_vote}
                        />
                    </View>
                </View>
                <View>
                    <View style={styles.row}>
                        <Image style={styles.icLocation} source={Images.home.ic_years} />
                        <UIText style={styles.txtLocation} font="bold" text={`${exp || 0} ` + t('home.years')} />
                    </View>
                    <View style={styles.row}>
                        <Image style={styles.icLocation} source={Images.home.ic_money} />
                        <UIText style={styles.txtLocation} font="bold" text={`$ ${price || 0}`} />
                    </View>
                </View>
            </View>
            {isHiring || isMatch ? (
                <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                    <UIButton
                        text={isHiring ? t('home.reject') : t('home.ignore')}
                        type={isHiring ? 'BorderRed' : 'BgGreyText'}
                        style={styles.btn}
                    />
                    <UIButton text={t('home.contact')} type="BgBlue" style={styles.btn} />
                </View>
            ) : (
                <UIButton text={t('home.connect')} type="BgBlue" style={styles.btn} />
            )}
        </View>
    );
};

export default UIPeopleComponent;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(10),
        paddingHorizontal: verticalScale(15),
        marginVertical: verticalScale(10),
    },
    imageContainer: {
        width: verticalScale(44),
        height: verticalScale(44),
        borderRadius: verticalScale(50),
        marginRight: verticalScale(10),
    },
    logo: {
        width: verticalScale(44),
        height: verticalScale(44),
        borderRadius: verticalScale(22),
        marginRight: verticalScale(15),
    },
    secondContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: verticalScale(5),
    },
    companyContainer: {
        flexDirection: 'row',
        marginTop: verticalScale(14),
        alignItems: 'center',
    },
    icLocation: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    txtLocation: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
        marginLeft: verticalScale(5),
    },
    icStar: {
        width: verticalScale(16),
        height: verticalScale(16),
        marginRight: verticalScale(6),
        tintColor: Colors.disable,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    txtExp: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
        marginLeft: verticalScale(5),
    },
    icExp: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    btn: {
        width: verticalScale(98),
        height: verticalScale(31),
        marginVertical: verticalScale(15),
    },
    txtPercent: {
        color: Colors.white,
        fontSize: moderateScale(12),
    },
    match: {
        flexDirection: 'row',
        height: verticalScale(30),
    },
    percentContainer: {
        width: verticalScale(42),
        height: '100%',
        backgroundColor: Colors.green,
        borderTopLeftRadius: verticalScale(4),
        borderBottomLeftRadius: verticalScale(4),
        justifyContent: 'center',
        alignItems: 'center',
    },
    matchedContainer: {
        width: verticalScale(76),
        height: '100%',
        backgroundColor: Colors.blueDark,
        marginLeft: -1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    matchedImg: {
        width: verticalScale(25),
        height: '100%',
        borderTopRightRadius: verticalScale(4),
        borderBottomRightRadius: verticalScale(4),
        backgroundColor: Colors.blueDark,
        justifyContent: 'center',
        alignItems: 'center',
    },
    applyContainer: {
        width: verticalScale(86),
        height: '100%',
        backgroundColor: Colors.green,
        borderRadius: verticalScale(4),
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: verticalScale(10),
    },
    tagContainer: {
        flexDirection: 'row',
        height: verticalScale(24),
        marginTop: -verticalScale(12),
        width: '100%',
    },
});
