import React from 'react';
import { View, StyleSheet, Image, ImageBackground, TouchableOpacity } from 'react-native';
import { UIButton, UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@/theme';
import { BASE_IMAGE_URL } from '../../../constant/value.constant';

const UIFreelancer = (props) => {
    const { t } = useTranslation();
    const { Images } = useTheme();
    const { avatar, onPressAvatar, name, des, price, bids, containerStyle } = props;

    return (
        <View style={[styles.container, containerStyle]}>
            <View style={styles.companyContainer}>
                <TouchableOpacity onPress={onPressAvatar}>
                    <ImageBackground
                        style={styles.imageContainer}
                        imageStyle={styles.imageContainer}
                        source={
                            avatar != '' && avatar != null && avatar != BASE_IMAGE_URL
                                ? { uri: BASE_IMAGE_URL + avatar }
                                : Images.profile.ic_avatar
                        }
                    />
                </TouchableOpacity>
                <UIText numberOfLines={1} ellipsizeMode="tail" style={{ color: Colors.blueDark }} font="bold" text={name} />
            </View>
            <UIText numberOfLines={4} ellipsizeMode="tail" style={{ marginTop: verticalScale(10) }} text={des} font="bold" />
            <View style={styles.infoContainer}>
                <View style={{ flex: 0.7 }}>
                    <View style={styles.row}>
                        <Image style={styles.icLocation} source={Images.home.ic_location_blue} />
                        <UIText style={styles.txtLocation} text={`${price}/hours`} />
                    </View>
                    <View style={styles.row}>
                        <Image style={styles.icLocation} source={Images.home.ic_price} />
                        <UIText style={styles.txtLocation} text={`${bids} bids`} />
                    </View>
                </View>
                <View style={{ flex: 0.3 }}>
                    <UIButton text={t('home.bid_now')} type="BgOrange" style={styles.btn} />
                </View>
            </View>
        </View>
    );
};

export default UIFreelancer;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(10),
        paddingHorizontal: verticalScale(15),
        marginVertical: verticalScale(10),
    },
    imageContainer: {
        width: verticalScale(44),
        height: verticalScale(44),
        borderRadius: verticalScale(50),
        marginRight: verticalScale(10),
    },
    logo: {
        width: verticalScale(44),
        height: verticalScale(44),
        borderRadius: verticalScale(22),
        marginRight: verticalScale(15),
    },
    companyContainer: {
        flexDirection: 'row',
        marginTop: verticalScale(14),
        alignItems: 'center',
    },
    icLocation: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    txtLocation: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
        marginLeft: verticalScale(5),
    },
    icStar: {
        width: verticalScale(16),
        height: verticalScale(16),
        marginRight: verticalScale(6),
        tintColor: Colors.disable,
    },
    infoContainer: {
        marginTop: verticalScale(15),
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: verticalScale(8),
    },
    txtExp: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
        marginLeft: verticalScale(5),
    },
    icExp: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    btn: {
        width: verticalScale(98),
        height: verticalScale(31),
        marginVertical: verticalScale(15),
    },
});
