import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { UIButton, UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import { useTheme } from '@/theme';
import moment from 'moment';
import ProgressCircle from 'react-native-progress-circle';

const UIClass = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { Images } = useTheme();

    const { title, times, startDate, price, mentor, progress, containerStyle, isViewAll = false } = props;

    return (
        <View style={[styles.container, containerStyle, progress && !isViewAll && styles.progressContainer]}>
            <UIText style={styles.txtTitle} font="bold" text={title} />
            {progress && !isViewAll && <UIText style={styles.txtAvailableSkill} text={t('home.available_your_skill')} />}
            <View style={[styles.row, { marginBottom: verticalScale(5) }]}>
                <Image style={styles.ic} source={Images.home.ic_hourglass} />
                <UIText style={styles.txtInfo} text={`${times} minutes`} />
            </View>
            <View style={[styles.row, { marginBottom: verticalScale(5) }]}>
                <Image style={styles.ic} source={Images.home.ic_calendar} />
                <UIText style={styles.txtInfo} text={moment(startDate).format('DD MMMM YYYY')} />
            </View>
            <View style={[styles.row, { marginBottom: verticalScale(5) }]}>
                <Image style={styles.ic} source={Images.home.ic_price} />
                <UIText style={styles.txtInfo} text={price} />
                <Image source={Images.wallet.ic_logo} style={styles.icLogo} />
            </View>
            <Image style={styles.icAvatar} source={mentor.avatar ? { uri: mentor.avatar } : Images.profile.ic_avatar} />
            <View style={styles.progress}>
                {progress && !isViewAll ? (
                    <ProgressCircle percent={progress} radius={verticalScale(20)} borderWidth={verticalScale(3)} color={Colors.green}>
                        <UIText text={progress} />
                    </ProgressCircle>
                ) : (
                    <UIButton
                        style={styles.btn}
                        type={progress && isViewAll ? 'BgGreen' : 'BgBlue'}
                        text={isViewAll && progress ? t('mission.in_progress') : t('home.join')}
                    />
                )}
            </View>
        </View>
    );
};

export default UIClass;

const styles = StyleSheet.create({
    container: {
        padding: verticalScale(15),
        backgroundColor: Colors.white,
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: verticalScale(10),
        width: '100%',
    },
    txtTitle: {
        color: Colors.blueDark,
    },
    progressContainer: {
        backgroundColor: '#f0faff',
        borderWidth: 0,
    },
    txtAvailableSkill: {
        fontSize: moderateScale(14),
        color: '#fc7f47',
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    ic: {
        width: verticalScale(20),
        height: verticalScale(20),
        marginRight: verticalScale(5),
    },
    txtInfo: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
    },
    icLogo: {
        width: verticalScale(30),
        height: verticalScale(10),
        tintColor: Colors.blueDark,
        marginLeft: verticalScale(5),
    },
    icAvatar: {
        width: verticalScale(46),
        height: verticalScale(46),
        borderRadius: verticalScale(23),
        position: 'absolute',
        top: verticalScale(15),
        right: verticalScale(15),
    },
    progress: {
        position: 'absolute',
        right: verticalScale(15),
        bottom: verticalScale(15),
    },
    btn: {
        width: verticalScale(98),
        height: verticalScale(31),
    },
});
