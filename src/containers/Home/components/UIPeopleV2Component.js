import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useTheme } from '@/theme';
import { BASE_IMAGE_URL } from '../../../constant/value.constant';
import { moderateScale, verticalScale } from '../../../utils';
import { Colors } from '../../../theme/Variables';
import { fontFamily } from '../../../theme/Fonts';
import { UIText, UIAvatar } from '../../../components/elements';

const UIPeopleV2Component = ({ name, avatar, rating = 3, location = 'Ho Chi Minh City, Vietnam', onPressAvatar, item }) => {
    const { Images } = useTheme();
    const { resume, username } = item || {};
    const { intro } = resume || {};
    const { headline } = intro || {};

    return (
        <View style={{ marginTop: 15, marginBottom: 10 }}>
            <View style={styles.card}>
                <View style={styles.head}>
                    <View>
                        <UIAvatar url={avatar ? BASE_IMAGE_URL + avatar : null} onPressAvatar={onPressAvatar} />
                    </View>
                    <View style={{ paddingLeft: 10, flexGrow: 1 }}>
                        <Text style={styles.name}>{username || 'user' }</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Image style={[styles.icStar, rating >= 1 && { tintColor: Colors.yellow }]} source={Images.home.ic_star_vote} />
                            <Image style={[styles.icStar, rating >= 2 && { tintColor: Colors.yellow }]} source={Images.home.ic_star_vote} />
                            <Image style={[styles.icStar, rating >= 3 && { tintColor: Colors.yellow }]} source={Images.home.ic_star_vote} />
                            <Image style={[styles.icStar, rating >= 4 && { tintColor: Colors.yellow }]} source={Images.home.ic_star_vote} />
                            <Image style={[styles.icStar, rating >= 5 && { tintColor: Colors.yellow }]} source={Images.home.ic_star_vote} />
                        </View>
                    </View>
                    <TouchableOpacity style={{ alignItems: 'flex-end', justifyContent: 'center' }}>
                        <Image source={Images.home.ic_add_friend} style={styles.icAddFriend} />
                    </TouchableOpacity>
                </View>
                <Text style={styles.description}>{headline || ''}</Text>
                <View style={styles.location}>
                    <Image source={Images.profile.ic_position_2} style={styles.icPosition} />
                    <UIText style={styles.txtLocation} text={location} />
                </View>
            </View>
            <Text style={styles.hint}>Maybe your colleages at VNG from 2008 to 2012</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    icStar: {
        width: verticalScale(16),
        height: verticalScale(16),
        marginRight: verticalScale(6),
        tintColor: Colors.disable2,
    },
    txtLocation: {
        flex: 0.75,
        marginLeft: verticalScale(5),
        fontSize: moderateScale(14),
        color: Colors.title,
        flexGrow: 1,
    },
    icPosition: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    icAddFriend: {
        width: verticalScale(30),
        height: verticalScale(30),
    },
    card: {
        borderColor: '#e7e8ee',
        borderWidth: 1,
        padding: 15,
        borderRadius: 10,
        marginBottom: 8,
        shadowColor: 'rgba(15, 15, 29, 0.37)',
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowRadius: 13,
        shadowOpacity: 0.37,
        backgroundColor: 'white',
    },
    head: { flexDirection: 'row', marginBottom: 10 },
    name: {
        fontSize: 16,
        lineHeight: 20,
        marginBottom: 4,
        fontFamily: fontFamily.bold,
        color: Colors.title,
    },
    description: { color: Colors.title, fontSize: 14, fontFamily: fontFamily.regular },
    location: { flexDirection: 'row', alignItems: 'center' },
    hint: { fontSize: 12, fontFamily: fontFamily.medium, color: Colors.lightGray },
});

export default UIPeopleV2Component;
