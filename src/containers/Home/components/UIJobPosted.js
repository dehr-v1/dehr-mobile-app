import React from 'react';
import { View, StyleSheet, Image, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import { UIButton, UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@/theme';
import { BASE_IMAGE_URL } from '../../../constant/value.constant';
const { width } = Dimensions.get('window');

const UIJobPosted = (props) => {
    const { Images } = useTheme();
    const { companyName, position, des, time, people, containerStyle, logo, onPress } = props;

    return (
        <TouchableOpacity onPress={onPress}>
            <View style={[styles.container, containerStyle]}>
                <View style={styles.companyContainer}>
                    <ImageBackground
                        style={styles.imageContainer}
                        imageStyle={styles.imageContainer}
                        source={
                            logo != '' && logo != null && logo != BASE_IMAGE_URL ? { uri: BASE_IMAGE_URL + logo } : Images.profile.ic_avatar
                        }
                    />
                    <UIText
                        numberOfLines={1}
                        ellipsizeMode="tail"
                        style={{ marginTop: verticalScale(5), color: Colors.blueDark, maxWidth: width - verticalScale(200) }}
                        text={companyName}
                        font="bold"
                    />
                </View>
                <UIText numberOfLines={1} ellipsizeMode="tail" style={styles.txtPosition} font="bold" text={position} />
                <UIText numberOfLines={3} ellipsizeMode="tail" style={styles.txtDes} text={des} />
                <View style={[styles.row, { justifyContent: 'space-between' }]}>
                    <View style={styles.row}>
                        <Image style={styles.icTime} source={Images.home.ic_time} />
                        <UIText style={styles.txtTime} text={(time != 0 && time) || ''} />
                    </View>
                    <View style={styles.row}>
                        <Image style={styles.icTime} source={Images.home.ic_people} />
                        <UIText style={styles.txtTime} text={`${people} applicants`} />
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    );
};

export default UIJobPosted;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(10),
        paddingHorizontal: verticalScale(15),
        marginTop: verticalScale(10),
    },
    companyContainer: {
        flexDirection: 'row',
        marginTop: verticalScale(5),
        alignItems: 'center',
    },
    logo: {
        width: verticalScale(44),
        height: verticalScale(44),
        borderRadius: verticalScale(22),
        marginRight: verticalScale(15),
    },
    imageContainer: {
        width: verticalScale(44),
        height: verticalScale(44),
        borderRadius: verticalScale(50),
        marginRight: verticalScale(10),
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: verticalScale(2),
    },
    txtPosition: {
        maxWidth: width - verticalScale(80),
        fontSize: moderateScale(16),
        color: Colors.blueDark,
        marginTop: verticalScale(12),
        marginBottom: verticalScale(6),
    },
    txtDes: {
        maxWidth: width - verticalScale(80),
        fontSize: moderateScale(14),
        color: '#707070',
        marginBottom: verticalScale(10),
    },
    txtTime: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
    },
    icTime: {
        width: verticalScale(24),
        height: verticalScale(24),
        marginLeft: verticalScale(5),
    },
});
