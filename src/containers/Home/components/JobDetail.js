import React from 'react';
import { View, StyleSheet, Image, ScrollView, Dimensions, ImageBackground, TouchableWithoutFeedback } from 'react-native';
import { UIButton, UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@/theme';
import { BASE_IMAGE_URL } from '../../../constant/value.constant';

const { width } = Dimensions.get('window');

const JobDetail = (props) => {
    const { t } = useTranslation();
    const { Images } = useTheme();

    const { job, onPress } = props || {};
    const { logo, company, position, location, requirement, type, desc, expired_date, job_apply, name, address } = job || {};

    return (
        <>
            <View style={{ flex: 1 }} />
            <TouchableWithoutFeedback onPress={() => {}}>
                <View style={styles.container}>
                    <View style={styles.tagContainer}>
                        <View style={styles.match}>
                            <View style={styles.percentContainer}>
                                <UIText font="bold" style={styles.txtPercent} text="80%" />
                            </View>
                            <View style={styles.matchedContainer}>
                                <UIText style={styles.txtPercent} text={t('home.matched')} />
                            </View>
                        </View>
                    </View>
                    <View style={styles.companyContainer}>
                        <ImageBackground
                            style={styles.imageContainer}
                            imageStyle={styles.imageContainer}
                            source={
                                company.img_landscape != '' && company.img_landscape != null && company.img_landscape != BASE_IMAGE_URL
                                    ? { uri: BASE_IMAGE_URL + company.img_landscape }
                                    : Images.profile.ic_avatar
                            }
                        />
                        <UIText style={{ color: Colors.blueDark, maxWidth: width - verticalScale(70) }} font="bold" text={company.name} />
                    </View>
                    <UIText numberOfLines={1} ellipsizeMode="tail" font="bold" style={styles.txtPosition} text={name} />
                    <View style={styles.desContainer}>
                        <ScrollView nestedScrollEnabled contentContainerStyle={{ flexGrow: 1 }}>
                            <View onStartShouldSetResponder={() => true}>
                                <View>
                                    <View style={styles.row}>
                                        <View style={styles.row}>
                                            <Image style={styles.icTime} source={Images.home.ic_time} />
                                            <UIText style={styles.txtTime} text={expired_date} />
                                        </View>
                                        <View style={styles.row}>
                                            <Image style={styles.icTime} source={Images.home.ic_people} />
                                            <UIText style={styles.txtTime} text={`${(job_apply && job_apply.length) || 0} applicants`} />
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.positionContainer}>
                                    <Image source={Images.profile.ic_position} style={styles.icPosition} />
                                    <UIText style={styles.txtLocation} text={company.address} />
                                    <View style={styles.typeContainer}>
                                        <Image style={styles.icClock} source={Images.home.ic_clock} />
                                        <UIText style={styles.txtType} text={type == 1 ? t('home.full_time') : t('home.part_time')} />
                                    </View>
                                </View>
                                <UIText style={styles.txtDes} text={desc} />
                                <UIText font="bold" style={styles.txtPosition} text={t('mission.requirements')} />
                                <View style={{ marginLeft: verticalScale(20) }}>
                                    <UIText style={styles.txtReqDes} text={requirement} />
                                </View>
                            </View>
                        </ScrollView>
                    </View>

                    <UIButton onPress={onPress} style={styles.btn} type="BgBlue" text={t('home.apply_now')} />
                    <UIText style={styles.txtUpdate} text={t('home.update_your_skill')} />
                </View>
            </TouchableWithoutFeedback>
        </>
    );
};

export default JobDetail;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: Colors.white,
        borderTopLeftRadius: verticalScale(10),
        borderTopRightRadius: verticalScale(10),
        paddingHorizontal: verticalScale(16),
        paddingTop: verticalScale(25),
    },
    desContainer: {
        width: '100%',
        height: '50%',
    },
    scrollContainer: {
        flexGrow: 1,
    },
    txtPercent: {
        color: Colors.white,
        fontSize: moderateScale(12),
    },
    match: {
        flexDirection: 'row',
        height: verticalScale(24),
    },
    percentContainer: {
        width: verticalScale(42),
        height: '100%',
        backgroundColor: Colors.green,
        borderTopLeftRadius: verticalScale(4),
        borderBottomLeftRadius: verticalScale(4),
        justifyContent: 'center',
        alignItems: 'center',
    },
    matchedContainer: {
        width: verticalScale(81),
        height: '100%',
        backgroundColor: '#3e4a7e',
        borderTopRightRadius: verticalScale(4),
        borderBottomRightRadius: verticalScale(4),
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: -1,
    },
    applyContainer: {
        width: verticalScale(86),
        height: '100%',
        backgroundColor: Colors.green,
        borderRadius: verticalScale(4),
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: verticalScale(10),
    },
    tagContainer: {
        flexDirection: 'row',
        height: verticalScale(24),
        width: '100%',
    },
    imageContainer: {
        width: verticalScale(44),
        height: verticalScale(44),
        borderRadius: verticalScale(50),
        marginRight: verticalScale(10),
    },
    companyContainer: {
        flexDirection: 'row',
        marginTop: verticalScale(14),
        alignItems: 'center',
    },
    txtPosition: {
        color: Colors.blueDark,
        marginTop: verticalScale(12),
        marginBottom: verticalScale(10),
    },
    icPosition: {
        width: verticalScale(32),
        height: verticalScale(32),
    },
    positionContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginBottom: verticalScale(8),
    },
    txtLocation: {
        flex: 0.85,
        marginLeft: verticalScale(5),
        fontSize: moderateScale(14),
        color: Colors.title,
    },
    icClock: {
        width: verticalScale(32),
        height: verticalScale(32),
    },
    txtType: {
        fontSize: moderateScale(12),
        marginTop: verticalScale(-8),
    },
    typeContainer: {
        flex: 0.2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    dot: {
        width: verticalScale(6),
        height: verticalScale(6),
        borderRadius: verticalScale(3),
        backgroundColor: '#e2e2e2',
        marginTop: verticalScale(8),
    },
    txtReqDes: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
        marginLeft: verticalScale(10),
    },
    btn: {
        marginTop: verticalScale(50),
        width: verticalScale(350),
    },
    txtUpdate: {
        fontSize: moderateScale(14),
        color: Colors.blue,
        textAlign: 'center',
        marginBottom: verticalScale(25),
        marginTop: verticalScale(10),
    },
    txtDes: {
        fontSize: moderateScale(14),
        color: '#707070',
    },
    txtTime: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
    },
    icTime: {
        width: verticalScale(24),
        height: verticalScale(24),
        marginLeft: verticalScale(5),
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: verticalScale(8),
    },
});
