import UIJobComponent from './UIJobComponent';
import UIPeopleComponent from './UIPeopleComponent';
import UIFreelancer from './UIFreelancer';
import JobDetail from './JobDetail';
import UIJobPosted from './UIJobPosted';
import UIEss from './UIEss';
import Popup from './Popup';
import UIOrganisation from './UIOrganisation';
import UIPeopleSponsor from './UIPeopleSponsor';

export { UIJobComponent, UIPeopleComponent, UIFreelancer, Popup, JobDetail, UIJobPosted, UIEss, UIOrganisation, UIPeopleSponsor };
