import React from 'react';
import { View, StyleSheet, Dimensions, ImageBackground, TouchableOpacity } from 'react-native';
import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { useTheme } from '@/theme';
import { BASE_IMAGE_URL } from '../../../constant/value.constant';
const { width, height } = Dimensions.get('window');

const Popup = (props) => {
    const { Images } = useTheme();
    const { popup, onPress } = props || {};

    const onPressPopup = (item) => {};

    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.container}>
                <ImageBackground
                    resizeMode="contain"
                    style={styles.bg}
                    imageStyle={styles.backgroundImage}
                    source={
                        popup.img_landscape != '' && popup.img_landscape != null
                            ? { uri: BASE_IMAGE_URL + popup.img_landscape }
                            : Images.profile.ic_avatar
                    }
                />
            </View>
        </TouchableOpacity>
    );
};

export default Popup;

const styles = StyleSheet.create({
    container: {
        minWidth: width - verticalScale(20),
        minHeight: height - verticalScale(40),
        borderRadius: verticalScale(10),
        backgroundColor: 'transparent',
    },
    bg: {
        flex: 1,
        margin: verticalScale(40),
    },
    backgroundImage: {
        width: null,
        height: null,
    },
});
