import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import { UIButton, UIText } from '../../../components/elements';

const UIPeopleSponsor = (props) => {
    const { t } = useTranslation();
    const { Images } = useTheme();
    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <View style={styles.row}>
                <Image style={styles.icAvatar} source={Images.profile.ic_avatar} />
                <UIText style={styles.txtName} font="bold" text="Alex Winson" />
            </View>
            <View style={[styles.row, { marginTop: verticalScale(10) }]}>
                <View style={{ flex: 0.5 }}>
                    <UIText style={styles.txtInfo} text={`${t('home.university')}: UEH`} />
                </View>
                <View style={{ flex: 0.5 }}>
                    <UIText style={styles.txtInfo} text={`${t('home.last_ranking')}: A`} />
                </View>
            </View>
            <View style={styles.row}>
                <View style={{ flex: 0.5 }}>
                    <UIText style={styles.txtInfo} text={`${t('home.major')}: Marketing`} />
                </View>
                <View style={{ flex: 0.5 }}>
                    <UIText style={styles.txtInfo} text={`${t('home.current_fee')}: $ 3.000/yr`} />
                </View>
            </View>
            <UIText
                style={styles.txtDesc}
                text="I would like to be sponsored the tuition fee for rest 2 yrs in UEH. Guarantee the graduation rank and work for company "
            />
            <View style={[styles.row, { justifyContent: 'space-between' }]}>
                <View style={styles.row}>
                    <Image style={styles.icPrice} source={Images.home.ic_price} />
                    <UIText font="bold" style={styles.txtPrice} text="3.268" />
                    <Image style={styles.icLogo} source={Images.wallet.ic_logo} />
                </View>
                <UIButton textStyle={{ fontSize: moderateScale(13) }} style={styles.btn} type="BgBlue" text={t('home.view_profile')} />
            </View>
        </View>
    );
};

export default UIPeopleSponsor;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderColor: Colors.border,
        borderWidth: 1,
        backgroundColor: Colors.white,
        padding: verticalScale(15),
        marginBottom: verticalScale(15),
        borderRadius: verticalScale(10),
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    icAvatar: {
        width: verticalScale(44),
        height: verticalScale(44),
        borderRadius: verticalScale(22),
    },
    txtName: {
        color: Colors.blueDark,
        marginLeft: verticalScale(12),
    },
    txtInfo: {
        fontSize: moderateScale(12),
        color: Colors.title,
    },
    txtDesc: {
        fontSize: moderateScale(14),
        color: Colors.title,
        marginTop: verticalScale(12),
        marginBottom: verticalScale(14),
    },
    icPrice: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    txtPrice: {
        fontSize: moderateScale(14),
        color: Colors.title,
        marginLeft: verticalScale(4),
    },
    icLogo: {
        width: verticalScale(30),
        height: verticalScale(10),
        marginLeft: verticalScale(5),
    },
    btn: {
        width: verticalScale(104),
        height: verticalScale(31),
        borderRadius: verticalScale(3),
    },
});
