import React, { useState } from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { UIButton, UIText } from '../../../components/elements';
import { useTheme } from '@/theme';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { Colors } from '../../../theme/Variables';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { fontFamily } from '../../../theme/Fonts';
import { MISSION_TYPE } from '../../../constant/value.constant';

const UIMission = (props) => {
    const { Images } = useTheme();
    const { t } = useTranslation();
    const { data, onDoMission, onGetReward } = props || {};
    const { name, desc, created_at, status, misson_id } = data || {};
    const { _id, reward, template } = misson_id || {};
    const [progress, setProgress] = useState(status == 0 || status == 3 ? 0 : status == 1 ? 0.3 : 1);
    const isVerify = template == MISSION_TYPE.FIREND || template == MISSION_TYPE.ENDORSE || false;

    const txtAction =
        isVerify && status == 1
            ? t('mission.verify_it')
            : status == 0
            ? t('mission.do_it')
            : status == 1
            ? t('mission.in_progress')
            : status == 2
            ? t('mission.get_reward')
            : t('mission.received');

    const onPressAction = () => {
        if (status == 2) {
            onGetReward();
        } else {
            onDoMission();
        }
    };

    return (
        <View style={styles.container}>
            <View style={styles.titleContainer}>
                <UIText font="bold" style={styles.txtTitle} text={name} />
                <TouchableOpacity style={styles.icMoreContainer}>
                    <Image style={styles.icMore} source={Images.header.ic_more} />
                </TouchableOpacity>
            </View>
            <View style={[styles.titleContainer, styles.actionContainer]}>
                <View>
                    <UIText style={styles.txtDate} text={moment.unix(created_at).format('hh:mm - MMM DD, YYYY')} />
                    <View style={styles.titleContainer}>
                        <UIText style={styles.txtDate} text={`${t('mission.reward')}: `} />
                        <UIText font="bold" style={styles.txtReward} text={reward} />
                        <Image style={styles.icLogo} source={Images.wallet.ic_logo} />
                    </View>
                </View>
                <UIButton
                    disable={status == 3}
                    onPress={onPressAction}
                    textStyle={[styles.txtAction, status == 3 && styles.textReward]}
                    style={[
                        styles.btnAction,
                        {
                            backgroundColor:
                                status != 2 && isVerify ? '#30a7eb' : status == 0 ? '30a7eb' : status == 1 ? Colors.green : Colors.orange,
                        },
                        status == 3 && styles.rewardBtn,
                    ]}
                    text={txtAction}
                />
            </View>
            {progress > 0 && status != 3 && !isVerify && (
                <View style={styles.progressContainer}>
                    <View style={styles.progress}>
                        <View
                            style={{
                                flex: progress,
                                backgroundColor: status == 1 ? Colors.green : Colors.orange,
                                borderRadius: verticalScale(20),
                            }}
                        />
                    </View>
                    <UIText
                        font="bold"
                        style={{ color: status == 1 ? Colors.green : Colors.orange, width: verticalScale(100), textAlign: 'center' }}
                        text={`${progress * 100}%`}
                    />
                </View>
            )}
        </View>
    );
};

export default UIMission;

const styles = StyleSheet.create({
    container: {
        padding: verticalScale(15),
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(10),
        marginVertical: verticalScale(7),
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    txtTitle: {
        fontSize: moderateScale(16),
        color: Colors.blueDark,
        flex: 0.8,
        flexWrap: 'wrap',
    },
    icMoreContainer: {
        flex: 0.2,
        alignItems: 'flex-end',
    },
    icMore: {
        width: verticalScale(30),
        height: verticalScale(30),
        tintColor: '#707070',
    },
    txtDate: {
        fontSize: moderateScale(12),
        color: Colors.lightGray,
    },
    txtReward: {
        fontSize: moderateScale(12),
        color: Colors.blueDark,
    },
    icLogo: {
        width: verticalScale(25.5),
        height: verticalScale(8.5),
        tintColor: Colors.blueDark,
        marginLeft: verticalScale(5),
    },
    btnAction: {
        width: verticalScale(98),
        height: verticalScale(25),
        borderRadius: verticalScale(3),
    },
    txtAction: {
        fontSize: moderateScale(13),
        color: Colors.white,
        fontFamily: fontFamily.bold,
    },
    actionContainer: {
        justifyContent: 'space-between',
    },
    progressContainer: {
        marginTop: verticalScale(10),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    progress: {
        width: verticalScale(200),
        height: verticalScale(5),
        backgroundColor: Colors.border,
        borderRadius: verticalScale(20),
        flexDirection: 'row',
    },
    rewardBtn: {
        backgroundColor: Colors.white,
        borderColor: Colors.orange,
        borderWidth: 1,
    },
    textReward: {
        color: Colors.orange,
    },
});
