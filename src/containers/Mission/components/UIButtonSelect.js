import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import { UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { verticalScale } from '../../../utils/ScalingUtils';

const UIButtonSelect = (props) => {
    const { text, isSelect, onPress, style, textStyle } = props;
    return (
        <TouchableOpacity
            onPress={onPress}
            style={[styles.container, isSelect && { backgroundColor: Colors.green, borderStyle: 'solid' }, style]}
        >
            <UIText font="bold" style={[{ color: isSelect ? Colors.white : Colors.blueDark }, textStyle]} text={text} />
        </TouchableOpacity>
    );
};

export default UIButtonSelect;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: verticalScale(40),
        borderColor: Colors.border,
        borderWidth: 1,
        borderStyle: 'dashed',
        borderRadius: verticalScale(6),
        justifyContent: 'center',
        alignItems: 'center',
    },
});
