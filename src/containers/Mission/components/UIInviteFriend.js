import React from 'react';
import { View, Image, TouchableOpacity, StyleSheet, Share } from 'react-native';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import { UIText } from '../../../components/elements';
import moment from 'moment';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { Colors } from '../../../theme/Variables';
import { BASE_IMAGE_URL } from '../../../constant/value.constant';

const UIInviteFriend = (props) => {
    const { Images } = useTheme();
    const { t } = useTranslation();
    const { user, dateInvite } = props;

    const onInvite = async () => {
        try {
            const result = await Share.share({
                message: 'DeHR',
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };

    return (
        <View style={styles.container}>
            <View style={{ flex: 0.15 }}>
                <Image style={styles.avatar} source={{ uri: BASE_IMAGE_URL + user.avatar_url }} />
            </View>
            <View style={{ flex: 0.65 }}>
                <UIText font="medium" style={styles.txtName} text={user.fullname} />
                <UIText style={styles.txtPosition} text="Doodle brands Founder" />
            </View>
            {dateInvite ? (
                <UIText style={styles.txtJoin} text={`${t('mission.joined_in')}\n${moment.unix(dateInvite).format('DD/MM/YYYY')}`} />
            ) : (
                <TouchableOpacity onPress={onInvite} style={styles.btnInvite}>
                    <UIText font="bold" style={styles.txtInvite} text={t('mission.invite')} />
                </TouchableOpacity>
            )}
        </View>
    );
};

export default UIInviteFriend;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: verticalScale(12),
    },
    txtName: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
    },
    txtPosition: {
        fontSize: moderateScale(14),
        color: Colors.message,
    },
    txtInvite: {
        fontSize: moderateScale(12),
        color: Colors.blueDark,
    },
    btnInvite: {
        width: verticalScale(60),
        height: verticalScale(30),
        borderColor: Colors.blueDark,
        borderWidth: 1,
        borderRadius: verticalScale(6),
        justifyContent: 'center',
        alignItems: 'center',
        flex: 0.2,
    },
    txtJoin: {
        fontSize: moderateScale(12),
        color: Colors.message,
        flex: 0.2,
    },
    avatar: {
        width: verticalScale(44),
        height: verticalScale(44),
        borderRadius: verticalScale(22),
    },
});
