import React, { useState, useRef, useEffect } from 'react';
import {
    View,
    TouchableOpacity,
    StyleSheet,
    Image,
    Platform,
    ScrollView,
    FlatList,
    StatusBar,
    TouchableWithoutFeedback,
    Share,
} from 'react-native';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import { UIHeader, UIModal } from '../../components/common';
import Toast from '../../components/elements/Toast';
import { Colors } from '../../theme/Variables';
import { connect } from 'react-redux';
import { moderateScale, verticalScale } from '../../utils/ScalingUtils';
import { UIButton, UIText } from '../../components/elements';
import { BASE_IMAGE_URL, MISSION_TYPE, DEHR_WEBSITE } from '../../constant/value.constant';
import { SCREEN } from '../../constant/screen.constant';
import { UIButtonSelect, UIInviteFriend, UIMission } from './components';
import moment from 'moment';
import QRCode from 'react-native-qrcode-svg';
import { actionGetMissions, actionAcceptMission, actionGetReward, actionDoneMission } from './duck/actions';
import { useNavigation, useRoute } from '@react-navigation/native';
import { navigateAndSimpleReset } from '@/navigators/Root';
import { getSeedPhase } from '../../services/persistence/store.config';

const friend = [
    {
        fullname: 'Christ Phan',
        position: 'Doodle brands Founder',
        avatar_url: '1628230605GERdqOtL.png',
    },
    {
        fullname: 'Christ Phan',
        position: 'Doodle brands Founder',
        avatar_url: '1628230605GERdqOtL.png',
    },
];

const friendInvite = [
    {
        fullname: 'Christ Phan',
        position: 'Doodle brands Founder',
        avatar_url: '1628230605GERdqOtL.png',
        dateJoined: '1628346109',
    },
    {
        fullname: 'Christ Phan',
        position: 'Doodle brands Founder',
        avatar_url: '1628230605GERdqOtL.png',
        dateJoined: '1628346109',
    },
    {
        fullname: 'Christ Phan',
        position: 'Doodle brands Founder',
        avatar_url: '1628230605GERdqOtL.png',
        dateJoined: '1628346109',
    },
];

const IndexMissionContainer = (props) => {
    const { t } = useTranslation();
    const { Layout, Gutters, Fonts, Images } = useTheme();
    const navigation = useNavigation();
    const { params } = useRoute();

    const { profileReducer, missionReducer, actionGetMissions, actionAcceptMission, actionGetReward, actionDoneMission } = props || {};
    const { accountInfo } = profileReducer || {};
    const { listMission, isLoading } = missionReducer || {};

    const [isMission, setIsMission] = useState(params?.isMission ?? true);
    const [missions, setMissions] = useState(listMission);
    const [mission, setMission] = useState({});
    const [isWorkWith, setIsWorkWith] = useState(null);
    const [department, setDepartment] = useState(null);
    const [page, setPage] = useState(1);
    const [size, setSize] = useState(10);
    const [isDone, setIsDone] = useState(0);
    const [isQuestion, setIsQuestion] = useState(false);
    const [step, setStep] = useState(0);
    const [vote, setVote] = useState(0);

    let missionRef = useRef();
    const referralLink = `${DEHR_WEBSITE}/invite/${accountInfo.username}`;

    useEffect(() => {
        actionGetMissions();
    }, []);

    useEffect(() => {
        setMissions(listMission);
    }, [listMission]);

    const renderAvatar = () => {
        return (
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
                <Image
                    style={styles.avatar}
                    source={accountInfo.avatar_url ? { uri: BASE_IMAGE_URL + accountInfo.avatar_url } : Images.profile.ic_avatar}
                />
            </TouchableOpacity>
        );
    };

    const renderTitle = () => {
        return (
            <View>
                <UIText style={styles.txtWallet} text={`${t('mission.mission')},`} />
                <UIText
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    font="bold"
                    style={styles.txtUsername}
                    text={accountInfo.username || 'Anthony pham'}
                />
            </View>
        );
    };

    const renderNotiIcon = () => {
        return <Image style={styles.icNoti} source={Images.wallet.ic_noti} />;
    };

    const renderTab = () => {
        return (
            <View style={styles.tabContainer}>
                <UIButton
                    type={isMission ? '' : 'BgGrey'}
                    textStyle={{ color: isMission ? Colors.white : Colors.blueDark }}
                    onPress={() => setIsMission(true)}
                    style={[styles.btnTab]}
                    text={t('mission.all_missions')}
                />
                <UIButton
                    textStyle={{ color: isMission ? Colors.blueDark : Colors.white }}
                    onPress={() => setIsMission(false)}
                    iconStyle={[styles.icLink, { tintColor: isMission ? Colors.blueDark : Colors.white }]}
                    Icon={Images.mission.ic_link}
                    style={styles.btnTab}
                    type={isMission ? 'BgGrey' : ''}
                    text={t('mission.referral')}
                />
            </View>
        );
    };

    const onDoMission = (item, index) => {
        const { misson_id } = item;
        const { template } = misson_id;
        setIsQuestion(template == MISSION_TYPE.FRIEND || template == MISSION_TYPE.ENDORSE);
        setMission(item);
        missionRef.open();
    };

    const onGetReward = async (item, missionref) => {
        const getWallet = await getSeedPhase();
        const wallet = JSON.parse(getWallet);
        const address = wallet?.address;
        if (address) {
            actionGetReward(
                {
                    missiondo_id: item._id,
                    wallet: address,
                },
                () => {
                    Toast(t('mission.reward_success'));
                    setIsDone(0);
                    missionref.close();
                }
            );
        } else {
            Toast(t('wallet.setup_wallet'), 'error');
        }
    };

    const renderMission = ({ item, index }) => {
        return (
            <UIMission
                key={item._id}
                data={item}
                onDoMission={() => onDoMission(item, index)}
                onGetReward={() => onGetReward(item, missionRef)}
            />
        );
    };

    const onPressDoMission = async () => {
        if (mission.status == 1) {
            const { misson_id } = mission || {};
            if (misson_id.template == MISSION_TYPE.KYC) {
                if (accountInfo.kycStatus === 'verified') {
                    doneMission();
                } else {
                    setIsDone(2);
                }
            } else {
                if (misson_id.template == MISSION_TYPE.WALLET) {
                    const getWallet = await getSeedPhase();
                    if (getWallet) {
                        doneMission();
                    } else {
                        setIsDone(2);
                    }
                } else {
                    if (accountInfo.is_update_profile == 1) {
                        doneMission();
                    } else {
                        setIsDone(2);
                    }
                }
            }
        } else {
            actionAcceptMission(
                {
                    missiondo_id: mission._id,
                },
                () => {
                    goToDoMission();
                }
            );
            missionRef.close();
        }
    };

    const onPressDo = () => {
        setIsDone(0);
        goToDoMission();
    };

    const doneMission = () => {
        actionDoneMission(
            {
                missiondo_id: mission._id,
            },
            () => {
                setIsDone(1);
            }
        );
    };

    const goToDoMission = () => {
        if (mission.misson_id.template == MISSION_TYPE.KYC) {
            missionRef.close();
            navigation.navigate(SCREEN.SETUP_KYC);
        } else {
            if (mission.misson_id.template == MISSION_TYPE.WALLET) {
                missionRef.close();
                navigateAndSimpleReset('Wallet');
            } else {
                if (mission.misson_id.template == MISSION_TYPE.PROFILE) {
                    missionRef.close();
                    navigation.navigate(SCREEN.CREATE_PROFILE);
                } else if (mission.misson_id.template == MISSION_TYPE.PRICE) {
                    missionRef.close();
                    navigation.navigate(SCREEN.UPDATE_CONTACT_PRICE);
                } else {
                    setStep(1);
                }
            }
        }
    };

    const onDoneQuestionMission = () => {
        actionDoneMission(
            {
                missiondo_id: mission._id,
                answer1: isWorkWith ? 1 : 0,
                answer2: mission.misson_id.template == MISSION_TYPE.FRIEND ? (department == 1 ? 1 : 0) : vote,
            },
            () => {
                setIsDone(1);
                setStep(0);
                setIsQuestion(false);
            }
        );
    };

    const onShare = async () => {
        try {
            const result = await Share.share({
                message: referralLink,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };

    const renderVote = () => {
        return (
            <View style={styles.voteContainer}>
                <TouchableOpacity onPress={() => setVote(1)} style={styles.icStarContainer}>
                    <Image style={[styles.icStar, vote >= 1 && { tintColor: Colors.green }]} source={Images.home.ic_star_vote} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setVote(2)} style={styles.icStarContainer}>
                    <Image style={[styles.icStar, vote >= 2 && { tintColor: Colors.green }]} source={Images.home.ic_star_vote} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setVote(3)} style={styles.icStarContainer}>
                    <Image style={[styles.icStar, vote >= 3 && { tintColor: Colors.green }]} source={Images.home.ic_star_vote} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setVote(4)} style={styles.icStarContainer}>
                    <Image style={[styles.icStar, vote >= 4 && { tintColor: Colors.green }]} source={Images.home.ic_star_vote} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setVote(5)} style={styles.icStarContainer}>
                    <Image style={[styles.icStar, vote >= 5 && { tintColor: Colors.green }]} source={Images.home.ic_star_vote} />
                </TouchableOpacity>
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <UIHeader
                onPressRight={() => navigation.navigate(SCREEN.NOTIFICATION)}
                centerStyle={styles.center}
                leftComponent={renderAvatar}
                centerComponent={renderTitle}
                rightComponent={renderNotiIcon}
            />
            <View style={styles.contentContainer}>
                {renderTab()}
                {isMission ? (
                    <ScrollView contentContainerStyle={{ paddingBottom: verticalScale(150) }}>
                        <UIText font="medium" style={styles.txtMissionCate} text={t('mission.for_new_users')} />
                        <FlatList
                            extraData={missions.missionForNewUser}
                            renderItem={renderMission}
                            data={missions.missionForNewUser}
                            keyExtractor={(item, index) => index.toString()}
                        />
                        <UIText font="medium" style={styles.txtMissionCate} text={t('mission.for_all_users')} />
                        <FlatList
                            extraData={missions.missionForAllUser}
                            renderItem={renderMission}
                            data={missions.missionForAllUser}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </ScrollView>
                ) : (
                    <ScrollView contentContainerStyle={{ paddingBottom: verticalScale(150) }} showsVerticalScrollIndicator={false}>
                        <Image resizeMode="stretch" style={styles.bgInvite} source={Images.mission.bg_invite} />
                        <View style={styles.referContainer}>
                            <View style={styles.referLinkContainer}>
                                <UIText font="bold" style={styles.txtDate} text={t('mission.referral_link')} />
                                <UIText
                                    font="bold"
                                    style={[styles.txtDate, { color: Colors.error, fontSize: moderateScale(16) }]}
                                    text={referralLink}
                                />
                            </View>
                            <View style={styles.shareContainer}>
                                <TouchableOpacity onPress={onShare}>
                                    <UIText font="bold" style={[styles.txtDate, styles.txtShare]} text={t('mission.share')} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        {/* <View style={[styles.referContainer, styles.referIdContainer]}>
                            <View style={{ flexDirection: 'row' }}>
                                <UIText font="bold" style={styles.txtDate} text={t('mission.referral_id')} />
                                <UIText
                                    font="bold"
                                    style={[
                                        styles.txtDate,
                                        { color: Colors.error, fontSize: moderateScale(16), marginLeft: verticalScale(10) },
                                    ]}
                                    text="123456"
                                />
                            </View>
                            <UIText font="bold" style={[styles.txtShare, { color: Colors.blueDark }]} text={t('mission.copy')} />
                        </View> */}
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: verticalScale(20) }}>
                            <View style={{ alignItems: 'center' }}>
                                <TouchableOpacity onPress={onShare}>
                                    <UIText
                                        font="bold"
                                        style={[styles.txtDate, { marginBottom: verticalScale(10) }]}
                                        text={t('mission.share')}
                                    />
                                </TouchableOpacity>
                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity>
                                        <Image style={styles.icFb} source={Images.mission.ic_fb} />
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                        <Image style={styles.icFb} source={Images.mission.ic_linkedin} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ alignItems: 'center' }}>
                                <UIText
                                    font="bold"
                                    style={[styles.txtDate, { marginBottom: verticalScale(10) }]}
                                    text={t('mission.scan_qr_code')}
                                />
                                <QRCode size={verticalScale(100)} value={referralLink} />
                            </View>
                        </View>

                        <TouchableOpacity onPress={onShare}>
                            <UIText
                                font="bold"
                                style={[styles.txtDate, { marginTop: verticalScale(10) }]}
                                text={t('mission.friends_invited')}
                            />
                        </TouchableOpacity>
                        {friendInvite.map((item, index) => {
                            return (
                                <UIInviteFriend
                                    user={{ fullname: item.fullname, position: item.position, avatar_url: item.avatar_url }}
                                    dateInvite={item.dateJoined}
                                />
                            );
                        })}
                    </ScrollView>
                )}
            </View>
            <UIModal ref={(ref) => (missionRef = ref)}>
                <>
                    <View style={{ flex: 1 }} />
                    <TouchableWithoutFeedback onPress={() => {}}>
                        <View style={styles.modalContainer}>
                            {step == 0 ? (
                                <>
                                    <UIText font="bold" style={styles.txtMissionName} text={mission.name} />
                                    <View style={styles.infoContainer}>
                                        <View style={styles.row}>
                                            <UIText font="bold" style={styles.txtDate} text={t('mission.date')} />
                                            <UIText
                                                style={styles.txtDateCreate}
                                                text={moment.unix(mission.created_at).format('hh:mm - MMM DD, YYYY')}
                                            />
                                        </View>
                                        <View style={styles.row}>
                                            <UIText font="bold" style={styles.txtDate} text={t('mission.reward')} />
                                            <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                                                <UIText
                                                    font="bold"
                                                    style={[styles.txtRewardPoint, Platform.OS != 'ios' && { marginTop: verticalScale(3) }]}
                                                    text={mission?.misson_id?.reward}
                                                />
                                                <Image resizeMode="stretch" style={styles.icLogo} source={Images.wallet.ic_logo} />
                                            </View>
                                        </View>
                                    </View>
                                    {/* <UIText font="medium" style={styles.txtReq} text={t('mission.requirements')} />
                <View style={{ flexDirection: 'row', alignItems: 'flex-start' }} >
                  <View style={styles.dot} />
                  <UIText style={styles.txtReqDes} text={t('mission.req_1')} />
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'flex-start' }} >
                  <View style={styles.dot} />
                  <UIText style={styles.txtReqDes} text={t('mission.req_2')} />
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'flex-start' }} >
                  <View style={styles.dot} />
                  <UIText style={styles.txtReqDes} text={t('mission.req_3')} />
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'flex-start' }} >
                  <View style={styles.dot} />
                  <UIText style={styles.txtReqDes} text={t('mission.req_4')} />
                </View> */}
                                    <View style={styles.bottomModalContainer}>
                                        {(isDone == 1 || isDone == 2) && (
                                            <UIText
                                                font="bold"
                                                style={{ color: isDone == 1 ? Colors.orange : '#30a7eb', textAlign: 'center' }}
                                                text={isDone == 1 ? t('mission.mission_done') : t('mission.mission_not_done')}
                                            />
                                        )}
                                        {!isQuestion ? (
                                            <UIButton
                                                isLoading={isLoading}
                                                onPress={
                                                    isDone == 1
                                                        ? () => onGetReward(mission, missionRef)
                                                        : isDone == 2
                                                        ? onPressDo
                                                        : onPressDoMission
                                                }
                                                style={[
                                                    styles.btnDoIt,
                                                    mission.status == 1 && { backgroundColor: Colors.green },
                                                    isDone == 1 && { backgroundColor: Colors.orange },
                                                    isDone == 2 && { backgroundColor: '#30a7eb' },
                                                ]}
                                                text={
                                                    isDone == 1
                                                        ? t('mission.get_reward')
                                                        : mission.status == 1 && isDone == 0
                                                        ? t('mission.check')
                                                        : t('mission.do_it')
                                                }
                                            />
                                        ) : (
                                            <UIButton
                                                isLoading={isLoading}
                                                text={t('mission.do_it')}
                                                onPress={onPressDo}
                                                style={[styles.btnDoIt, { backgroundColor: '#30a7eb' }]}
                                            />
                                        )}
                                    </View>
                                </>
                            ) : (
                                <>
                                    <UIText font="bold" style={styles.txtMissionName} text={t('mission.verify_network')} />
                                    <UIText font="bold" style={styles.txtWorkWith} text={mission.question[0]} />
                                    <View style={styles.YesNocontainer}>
                                        <UIButtonSelect
                                            onPress={() => setIsWorkWith(false)}
                                            style={{ flex: 0.3 }}
                                            text="No"
                                            isSelect={isWorkWith == null ? false : !isWorkWith}
                                        />
                                        <UIButtonSelect
                                            onPress={() => setIsWorkWith(true)}
                                            style={{ flex: 0.3 }}
                                            text="Yes"
                                            isSelect={isWorkWith == null ? false : isWorkWith}
                                        />
                                    </View>
                                    {mission.misson_id.template == MISSION_TYPE.FRIEND && isWorkWith && (
                                        <>
                                            <UIText font="bold" style={[styles.txtWorkWith, { marginTop: 0 }]} text={mission.question[1]} />
                                            <UIButtonSelect
                                                onPress={() => setDepartment(0)}
                                                style={styles.btnSelect}
                                                text={`A. ${t('mission.marketing')}`}
                                                isSelect={department == 0}
                                            />
                                            <UIButtonSelect
                                                onPress={() => setDepartment(1)}
                                                style={styles.btnSelect}
                                                text={`B. ${mission.answer[1]}`}
                                                isSelect={department == 1}
                                            />
                                            <UIButtonSelect
                                                onPress={() => setDepartment(2)}
                                                style={styles.btnSelect}
                                                text={`C. ${t('mission.hr')}`}
                                                isSelect={department == 2}
                                            />
                                        </>
                                    )}
                                    {mission.misson_id.template == MISSION_TYPE.ENDORSE && isWorkWith && (
                                        <>
                                            <UIText font="bold" style={[styles.txtWorkWith, { marginTop: 0 }]} text={mission.question[1]} />
                                            {renderVote()}
                                        </>
                                    )}

                                    <UIButton
                                        onPress={
                                            !(isWorkWith == null && department == null && vote == 0) ? onDoneQuestionMission : () => {}
                                        }
                                        style={[styles.btnComplete]}
                                        text={t('mission.complete')}
                                    />
                                </>
                            )}
                        </View>
                    </TouchableWithoutFeedback>
                </>
            </UIModal>
        </View>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
    missionReducer: state.missionReducer,
});
export default connect(mapStateToProps, {
    actionGetMissions,
    actionAcceptMission,
    actionGetReward,
    actionDoneMission,
})(IndexMissionContainer);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    avatar: {
        width: verticalScale(40),
        height: verticalScale(40),
        borderRadius: verticalScale(20),
    },
    txtWallet: {
        fontSize: moderateScale(12),
        color: '#707070',
    },
    txtUsername: {
        fontSize: moderateScale(24),
        color: Colors.title,
    },
    center: {
        alignItems: 'flex-start',
        marginLeft: verticalScale(12),
    },
    icNoti: {
        width: verticalScale(36),
        height: verticalScale(36),
    },
    tabContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },
    contentContainer: {
        paddingHorizontal: verticalScale(16),
        marginTop: verticalScale(20),
    },
    btnTab: {
        flex: 0.48,
        height: verticalScale(36),
    },
    icLink: {
        width: verticalScale(13),
        height: verticalScale(13),
        tintColor: Colors.blueDark,
    },
    modalContainer: {
        backgroundColor: Colors.white,
        width: '100%',
        paddingTop: verticalScale(32),
        paddingHorizontal: verticalScale(30),
        borderTopLeftRadius: verticalScale(20),
        borderTopRightRadius: verticalScale(20),
    },
    txtMissionName: {
        fontSize: moderateScale(20),
        color: Colors.blueDark,
        textAlign: 'center',
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: verticalScale(8),
    },
    icLogo: {
        width: verticalScale(37.8),
        height: verticalScale(12.5),
        tintColor: Colors.blueDark,
        marginLeft: verticalScale(5),
    },
    txtRewardPoint: {
        color: Colors.blueDark,
    },
    txtDate: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
    },
    txtDateCreate: {
        color: Colors.lightGray,
    },
    infoContainer: {
        paddingHorizontal: verticalScale(20),
        borderRadius: verticalScale(6),
        backgroundColor: Colors.gray,
        paddingVertical: verticalScale(8),
        marginTop: verticalScale(20),
    },
    txtReq: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
        marginTop: verticalScale(22),
    },
    dot: {
        width: verticalScale(6),
        height: verticalScale(6),
        borderRadius: verticalScale(3),
        backgroundColor: '#e2e2e2',
        marginTop: verticalScale(5),
    },
    txtReqDes: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
        marginLeft: verticalScale(10),
    },
    btnDoIt: {
        marginTop: verticalScale(20),
        marginBottom: verticalScale(44),
        backgroundColor: '#30a7eb',
    },
    bottomModalContainer: {
        marginTop: verticalScale(170),
    },
    txtWorkWith: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
        textAlign: 'center',
        marginTop: verticalScale(35),
    },
    YesNocontainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingHorizontal: verticalScale(50),
        marginTop: verticalScale(15),
        marginBottom: verticalScale(48),
    },
    btnSelect: {
        height: verticalScale(44),
        marginVertical: verticalScale(10),
    },
    btnComplete: {
        marginTop: verticalScale(150),
        marginBottom: verticalScale(44),
    },
    bgInvite: {
        width: '100%',
        marginTop: verticalScale(20),
    },
    txtShare: {
        color: Colors.white,
        textTransform: 'uppercase',
    },
    referContainer: {
        backgroundColor: Colors.border,
        borderRadius: verticalScale(6),
        flexDirection: 'row',
        marginTop: verticalScale(20),
    },
    referLinkContainer: {
        padding: verticalScale(16),
        flex: 0.7,
    },
    shareContainer: {
        flex: 0.3,
        backgroundColor: Colors.error,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopRightRadius: verticalScale(6),
        borderBottomRightRadius: verticalScale(6),
    },
    referIdContainer: {
        padding: verticalScale(17),
        justifyContent: 'space-between',
        marginBottom: verticalScale(20),
    },
    icFb: {
        width: verticalScale(32),
        height: verticalScale(32),
        marginHorizontal: verticalScale(5),
    },
    txtMissionCate: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
        marginTop: verticalScale(24),
    },
    icStar: {
        width: verticalScale(36),
        height: verticalScale(36),
        tintColor: '#d3dae1',
    },
    icStarContainer: {
        marginLeft: verticalScale(13),
    },
    voteContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        marginTop: verticalScale(22),
    },
});
