import { MissionAction } from '../actions';

const mission = {
    listMission: {
        missionForAllUser: [],
        missionForNewUser: [],
    },
    isLoading: false,
};
const missionReducer = (state = mission, action) => {
    const { type, payload } = action;
    switch (type) {
        case MissionAction.MISSION_GET_MISSIONS.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case MissionAction.MISSION_GET_MISSIONS.SUCCESS:
            return {
                ...state,
                isLoading: false,
                listMission: payload,
            };
        case MissionAction.MISSION_GET_MISSIONS.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case MissionAction.MISSION_ACCEPT_MISSION.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case MissionAction.MISSION_ACCEPT_MISSION.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case MissionAction.MISSION_ACCEPT_MISSION.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case MissionAction.MISSION_DONE_MISSION.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case MissionAction.MISSION_DONE_MISSION.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case MissionAction.MISSION_DONE_MISSION.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case MissionAction.MISSION_GET_REWARD.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case MissionAction.MISSION_GET_REWARD.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case MissionAction.MISSION_GET_REWARD.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
};

export { missionReducer };
