import { all } from 'redux-saga/effects';
import { watchOnAcceptMission, watchOnDoneMission, watchOnGetMissions, watchOnGetReward } from './mission.sagas';

export function* missionSaga() {
    yield all([watchOnGetMissions(), watchOnAcceptMission(), watchOnDoneMission(), watchOnGetReward()]);
}
