import { put, call, takeLatest } from 'redux-saga/effects';
import Toast from '../../../../components/elements/Toast';
import { getOAuthToken } from '../../../../services/persistence/store.config';
import { MissionAction } from '../actions';
import { acceptMission, doneMission, getMissions, getReward } from '../../../../services/remote/mission.service';

function* onGetMissions(action) {
    const { payload, callback } = action;
    let { data } = payload || {};
    try {
        const res = yield call(() => getMissions(data));
        const missions = res.data;
        // missions.missionForNewUser.sort((a, b) => a.status > b.status);
        // missions.missionForAllUser.sort((a, b) => a.status > b.status);
        missions.missionForNewUser = missions.map((e) => {
            return e.mission;
        });
        missions.missionForAllUser = missions.map((e) => {
            return e.mission;
        });

        yield put({
            type: MissionAction.MISSION_GET_MISSIONS.SUCCESS,
            payload: missions,
        });
        callback && callback();
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: MissionAction.MISSION_GET_MISSIONS.ERROR,
            payload: error,
        });
    }
}

export function* watchOnGetMissions() {
    yield takeLatest(MissionAction.MISSION_GET_MISSIONS.PENDING, onGetMissions);
}

function* onAcceptMission(action) {
    const { payload, callback } = action;
    const { missiondo_id } = payload || {};
    try {
        const data = {
            missiondo_id,
        };
        const response = yield call(() => acceptMission(data));
        if (response.success == 1) {
            const missions = response.data;
            yield put({
                type: MissionAction.MISSION_ACCEPT_MISSION.SUCCESS,
                payload: missions,
            });
            callback && callback();
            yield put({
                type: MissionAction.MISSION_GET_MISSIONS.PENDING,
            });
        } else {
            Toast(`${response.msg}`, 'error');
            yield put({
                type: MissionAction.MISSION_ACCEPT_MISSION.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        yield put({
            type: MissionAction.MISSION_ACCEPT_MISSION.ERROR,
            payload: error,
        });
    }
}

export function* watchOnAcceptMission() {
    yield takeLatest(MissionAction.MISSION_ACCEPT_MISSION.PENDING, onAcceptMission);
}

function* onDoneMission(action) {
    const { payload, callback } = action;
    const { missiondo_id, answer1, answer2 } = payload || {};
    try {
        const data = {
            missiondo_id,
            answer1,
            answer2,
        };
        const response = yield call(() => doneMission(data));
        if (response.success == 1) {
            const missions = response.data;
            yield put({
                type: MissionAction.MISSION_DONE_MISSION.SUCCESS,
                payload: missions,
            });
            callback && callback();
            yield put({
                type: MissionAction.MISSION_GET_MISSIONS.PENDING,
            });
        } else {
            Toast(`${response.msg}`, 'error');
            yield put({
                type: MissionAction.MISSION_DONE_MISSION.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        yield put({
            type: MissionAction.MISSION_DONE_MISSION.ERROR,
            payload: error,
        });
    }
}

export function* watchOnDoneMission() {
    yield takeLatest(MissionAction.MISSION_DONE_MISSION.PENDING, onDoneMission);
}

function* onGetReward(action) {
    const { payload, callback } = action;
    const { missiondo_id } = payload || {};
    try {
        const token = yield call(() => getOAuthToken());
        const data = {
            missiondo_id,
            token,
        };
        const response = yield call(() => getReward(data));
        if (response.success == 1) {
            const missions = response.data;
            yield put({
                type: MissionAction.MISSION_GET_REWARD.SUCCESS,
                payload: missions,
            });
            callback && callback();
            yield put({
                type: MissionAction.MISSION_GET_MISSIONS.PENDING,
            });
        } else {
            Toast(`${response.msg}`, 'error');
            yield put({
                type: MissionAction.MISSION_GET_REWARD.ERROR,
                payload: response.msg,
            });
        }
    } catch (error) {
        yield put({
            type: MissionAction.MISSION_GET_REWARD.ERROR,
            payload: error,
        });
    }
}

export function* watchOnGetReward() {
    yield takeLatest(MissionAction.MISSION_GET_REWARD.PENDING, onGetReward);
}
