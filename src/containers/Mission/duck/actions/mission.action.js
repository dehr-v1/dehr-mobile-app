import { MissionAction } from './mission.create.action';

export const actionGetMissions = (payload, callback) => ({
    type: MissionAction.MISSION_GET_MISSIONS.PENDING,
    payload,
    callback,
});

export const actionAcceptMission = (payload, callback) => ({
    type: MissionAction.MISSION_ACCEPT_MISSION.PENDING,
    payload,
    callback,
});

export const actionDoneMission = (payload, callback) => ({
    type: MissionAction.MISSION_DONE_MISSION.PENDING,
    payload,
    callback,
});

export const actionGetReward = (payload, callback) => ({
    type: MissionAction.MISSION_GET_REWARD.PENDING,
    payload,
    callback,
});
