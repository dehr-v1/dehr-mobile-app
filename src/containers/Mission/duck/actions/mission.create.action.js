import { createActionSet } from '../../../../utils';

export const MissionAction = {
    MISSION_GET_MISSIONS: createActionSet('MISSION_GET_MISSIONS'),
    MISSION_ACCEPT_MISSION: createActionSet('MISSION_ACCEPT_MISSION'),
    MISSION_DONE_MISSION: createActionSet('MISSION_DONE_MISSION'),
    MISSION_GET_REWARD: createActionSet('MISSION_GET_REWARD'),
};
