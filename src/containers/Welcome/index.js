import React, { useEffect } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@/theme';
import { Brand } from '@/components';
import { useNavigation } from '@react-navigation/native';
import { navigateAndSimpleReset } from '@/navigators/Root';
import { SCREEN } from '../../constant';
import { fontFamily } from '../../theme/Fonts';
import { setFirstTime } from '../../services/persistence/store.config';
import { connect } from 'react-redux';
import { setUser } from '../Login/duck/actions';
import { moderateScale, verticalScale } from '../../utils/ScalingUtils';

const IndexWelcomeContainer = (props) => {
    const { Layout, Images } = useTheme();
    const navigation = useNavigation();
    const { t } = useTranslation();

    const onPressStart = () => {
        setFirstTime('true');
        navigation.navigate(SCREEN.LOGIN);
    };

    return (
        <View style={[Layout.fill]}>
            <Image style={[Layout.fullSize]} resizeMode="cover" source={Images.bg_splash} />
            <View style={styles.mainView}>
                <Brand />
                <Text style={styles.textStyle}>{t('Decentralized')}</Text>
                <Text style={styles.textStyle}>{t('Human Relationship')}</Text>
                <Text style={styles.textStyle}>{t('Platform')}</Text>
                <View style={{ height: 50 }} />
                <TouchableOpacity style={styles.buttonStyle} onPress={onPressStart}>
                    <Text style={styles.buttonTextStyle}>{t('home.join_now')}</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    mainView: {
        width: '100%',
        marginStart: verticalScale(50),
        marginTop: verticalScale(100),
        position: 'absolute',
    },
    textStyle: {
        fontFamily: fontFamily.light,
        color: '#fff',
        fontSize: moderateScale(24),
    },
    buttonStyle: {
        width: verticalScale(200),
        height: verticalScale(50),
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#fff',
        borderWidth: 1,
    },
    buttonTextStyle: {
        fontFamily: fontFamily.black,
        color: '#fff',
        fontSize: moderateScale(16),
    },
});

const mapStateToProps = (state) => ({});
export default connect(mapStateToProps, {
    setUser,
})(IndexWelcomeContainer);
