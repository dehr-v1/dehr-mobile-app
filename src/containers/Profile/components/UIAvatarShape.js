import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import MaskedView from '@react-native-community/masked-view';
import _get from 'lodash/get';
import { verticalScale } from '../../../utils';

const avatarFrames = {
    levelTwo: {
        backLayer: require('../../../assets/Images/Profile/Avatar/back_layer_1.png'),
        avatarMask: require('../../../assets/Images/Profile/Avatar/mask_1.png'),
    },
    levelThree: {
        backLayer: require('../../../assets/Images/Profile/Avatar/back_layer_2.png'),
        avatarMask: require('../../../assets/Images/Profile/Avatar/mask_2.png'),
        customAvatarContainer: {
            width: verticalScale(102),
            height: verticalScale(102),
        },
    },
};

const UIAvatarShape = ({ membershipLevel = 2, source }) => {
    const selectedFrame = () => {
        switch (membershipLevel) {
            case 3:
                return avatarFrames.levelThree;
            default:
                return avatarFrames.levelTwo;
        }
    };

    return (
        <View
            style={[
                styles.container,
                {
                    top: membershipLevel === 2 ? verticalScale(-7) : verticalScale(-22),
                    marginBottom: membershipLevel === 2 ? verticalScale(-15) : verticalScale(-25),
                },
            ]}
        >
            <View style={[styles.backLayer, { top: membershipLevel === 2 ? verticalScale(4) : verticalScale(2.6) }]}>
                <Image
                    style={[styles.firstBackLayer, { transform: [{ translateY: verticalScale(-0.3) }] }]}
                    resizeMode="contain"
                    source={selectedFrame().backLayer}
                />
            </View>
            <MaskedView
                maskElement={
                    <View style={styles.maskWrapper}>
                        <Image
                            style={[styles.avatarContainer, _get(selectedFrame(), 'customAvatarContainer', {})]}
                            resizeMode="contain"
                            source={selectedFrame().avatarMask}
                        />
                    </View>
                }
            >
                <Image
                    style={[styles.avatarContainer, _get(selectedFrame(), 'customAvatarContainer', {})]}
                    resizeMode="cover"
                    source={source}
                />
            </MaskedView>
        </View>
    );
};

export default UIAvatarShape;

const styles = StyleSheet.create({
    container: {
        width: verticalScale(142),
        height: verticalScale(142),
        justifyContent: 'center',
        alignItems: 'center',
        top: verticalScale(-7),
    },
    backLayer: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        top: verticalScale(4),
        left: verticalScale(0),
        justifyContent: 'center',
        alignItems: 'center',
    },
    firstBackLayer: {
        width: '100%',
        height: '100%',
        position: 'absolute',
    },
    secondBackLayer: {
        width: verticalScale(106),
        height: verticalScale(106),
        position: 'absolute',
    },
    maskWrapper: {
        backgroundColor: 'transparent',
        width: verticalScale(100),
        height: verticalScale(100),
    },
    avatarContainer: {
        width: verticalScale(100),
        height: verticalScale(100),
    },
});
