import React from 'react';
import { View, StyleSheet, Image, TouchableOpacity, FlatList } from 'react-native';
import { UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { useTheme } from '@/theme';
import moment from 'moment';
import { BASE_IMAGE_URL } from '../../../constant/value.constant';
import { useTranslation } from 'react-i18next';
import FacePile from 'react-native-face-pile';

const UIItem = (props) => {
    const { t } = useTranslation();
    const {
        name,
        description,
        onPress,
        startDate = '',
        endDate = '',
        title = '',
        location,
        hideAction = false,
        media,
        logo,
        trust = [],
        isPadding = true,
        stylesName,
        style,
    } = props;
    const { Images } = useTheme();

    let timeLocation = `${moment(new Date(startDate)).format('MMM YYYY')} -`;
    timeLocation += endDate == 0 ? ' Present' : `${moment(new Date(startDate)).format('MMM YYYY')}`;
    if (location) {
        timeLocation += ` at ${location}`;
    }

    const renderMedia = ({ item, index }) => {
        if (index < 4) {
            return <Image style={styles.icMedia} source={{ uri: BASE_IMAGE_URL + item.value }} />;
        }
        if (index == 4 && media.length == 5) {
            return <Image style={styles.icMedia} source={{ uri: BASE_IMAGE_URL + item.value }} />;
        }
        return (
            <View style={[styles.icMedia, { borderWidth: 1, borderColor: Colors.border, backgroundColor: '#f6f8fa' }]}>
                <UIText style={{ fontSize: moderateScale(14), color: Colors.hint }} text={`+${media.length - 4}`} />
            </View>
        );
    };

    return (
        <View style={[styles.container, hideAction && { borderTopWidth: 0 }, isPadding && { paddingVertical: verticalScale(15) }, style]}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                {logo && <Image style={styles.icLogo} source={{ uri: BASE_IMAGE_URL + logo }} />}
                <View>
                    {name != '' && name && <UIText style={[styles.txtName, stylesName]} font="bold" text={name} />}
                    {title != '' && <UIText style={styles.txtTitle} text={title} />}
                </View>
            </View>
            {startDate != '' && endDate !== '' && <UIText style={styles.txtTime} text={timeLocation} />}
            {description != '' && description && <UIText style={styles.txtDesciption} text={description} />}
            {media && media != [] && (
                <View style={{ marginBottom: verticalScale(10) }}>
                    <FlatList horizontal data={media} extraData={media} renderItem={renderMedia} />
                </View>
            )}
            {trust.length > 0 && (
                <View style={styles.trustContainer}>
                    <FacePile circleSize={16} numFaces={4} faces={trust} />
                    <UIText font="bold" style={styles.txtConnections} text={t('profile.trust_connect', { name: trust.length })} />
                </View>
            )}
            {!hideAction && (
                <TouchableOpacity style={styles.icPencilContainer} onPress={onPress}>
                    <Image source={Images.profile.ic_pencil} style={styles.icPencil} />
                </TouchableOpacity>
            )}
        </View>
    );
};

export default UIItem;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderTopColor: Colors.border,
        borderTopWidth: 1,
        paddingHorizontal: verticalScale(16),
        paddingVertical: verticalScale(5),
        justifyContent: 'center',
        backgroundColor: Colors.white,
    },
    txtName: {
        fontSize: moderateScale(16),
        lineHeight: moderateScale(22),
        color: Colors.blueDark,
    },
    txtDesciption: {
        fontSize: moderateScale(12),
        lineHeight: moderateScale(20),
        color: Colors.hint,
    },
    txtConnections: {
        fontSize: moderateScale(9),
        color: Colors.hint,
        marginLeft: verticalScale(26),
    },
    icPencil: {
        width: verticalScale(36),
        height: verticalScale(36),
    },
    icPencilContainer: {
        position: 'absolute',
        right: verticalScale(16),
        top: verticalScale(15),
    },
    txtTitle: {
        fontSize: moderateScale(14),
        color: Colors.hint,
    },
    txtTime: {
        fontSize: moderateScale(12),
        color: Colors.lightGray,
    },
    icMedia: {
        width: verticalScale(60),
        height: verticalScale(60),
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: verticalScale(10),
        marginTop: verticalScale(10),
    },
    icLogo: {
        width: verticalScale(44),
        height: verticalScale(44),
        borderRadius: verticalScale(22),
        marginRight: verticalScale(10),
    },
    trustContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    txtTrust: {
        fontSize: moderateScale(12),
        lineHeight: moderateScale(14),
        color: Colors.white,
    },
    trustedContainer: {
        height: verticalScale(20),
        width: verticalScale(68),
        backgroundColor: Colors.green,
        borderRadius: verticalScale(10),
        justifyContent: 'center',
        alignItems: 'center',
    },
    icTrust: {
        width: verticalScale(32),
        height: verticalScale(32),
        borderRadius: verticalScale(16),
        marginLeft: -verticalScale(16),
    },
});
