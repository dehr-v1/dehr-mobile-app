import React, { useMemo } from 'react';
import { Image, ImageBackground, TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import { useTheme } from '@/theme';
import { verticalScale } from '../../../utils/ScalingUtils';
import { Colors } from '../../../theme/Variables';
import { BASE_IMAGE_URL } from '../../../constant/value.constant';
import UIAvatarShape from './UIAvatarShape';

const Avatar = (props) => {
    const { Images } = useTheme();
    const { style, onPressAvatar, onPressCamera, url, membershipLevel = 1 } = props;

    const _renderByMembershipLevel = () => {
        const level = Number(membershipLevel);
        if (level === 1) {
            return (
                <ImageBackground
                    style={[styles.container, style]}
                    imageStyle={[styles.container, styles.imageContainer]}
                    source={url != null && url != BASE_IMAGE_URL ? { uri: url } : Images.profile.ic_avatar}
                >
                    {onPressCamera && (
                        <TouchableOpacity onPress={onPressCamera && onPressCamera} style={styles.cameraContainer}>
                            <Image style={styles.camera} source={Images.profile.ic_camera} />
                        </TouchableOpacity>
                    )}
                </ImageBackground>
            );
        }

        return (
            <View style={style}>
                <UIAvatarShape
                    membershipLevel={membershipLevel}
                    source={url != null && url != BASE_IMAGE_URL ? { uri: url } : Images.profile.ic_avatar}
                />
            </View>
        );
    };

    return (
        <TouchableOpacity activeOpacity={onPressAvatar ? 0.8 : 1} onPress={onPressAvatar && onPressAvatar}>
            {_renderByMembershipLevel()}
        </TouchableOpacity>
    );
};

export default Avatar;

const styles = StyleSheet.create({
    container: {
        width: verticalScale(100),
        height: verticalScale(100),
    },
    imageContainer: {
        borderRadius: verticalScale(50),
        borderColor: Colors.blueDark,
        borderWidth: verticalScale(3),
    },
    camera: {
        width: verticalScale(24),
        height: verticalScale(24),
    },
    cameraContainer: {
        width: verticalScale(32),
        height: verticalScale(32),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.blueDark,
        position: 'absolute',
        bottom: 0,
        right: 0,
        borderRadius: verticalScale(16),
    },
});
