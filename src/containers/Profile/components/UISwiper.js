import React from 'react';
import { View, StyleSheet, Animated, TouchableOpacity, Image } from 'react-native';
import { UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { verticalScale } from '../../../utils/ScalingUtils';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';

const UISwiper = (props) => {
    const { name, onEdit, onDelete } = props;
    const { Images } = useTheme();
    const { t } = useTranslation();

    const leftSwipe = (progress, dragX) => {
        return (
            <TouchableOpacity onPress={onDelete} activeOpacity={0.6}>
                <View style={styles.deleteBox}>
                    <Image style={styles.icTrash} source={Images.profile.ic_trash} />
                    <UIText style={styles.txtDelete} text={t('profile.delete')} />
                </View>
            </TouchableOpacity>
        );
    };

    return (
        <Swipeable renderLeftActions={leftSwipe}>
            <View style={styles.container}>
                <UIText style={styles.txtName} font="bold" text={name} />
                <TouchableOpacity onPress={onEdit} style={styles.btnEdit}>
                    <Image style={styles.icPencil} source={Images.profile.ic_pencil} />
                </TouchableOpacity>
            </View>
        </Swipeable>
    );
};

export default UISwiper;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderTopColor: Colors.border,
        borderTopWidth: 1,
        backgroundColor: Colors.white,
        paddingHorizontal: verticalScale(16),
        justifyContent: 'center',
        height: verticalScale(66),
    },
    txtName: {
        fontSize: verticalScale(16),
        color: Colors.blueDark,
    },
    deleteBox: {
        width: verticalScale(66),
        height: verticalScale(66),
        backgroundColor: Colors.error,
        justifyContent: 'center',
        alignItems: 'center',
    },
    icTrash: {
        width: verticalScale(32),
        height: verticalScale(32),
    },
    txtDelete: {
        fontSize: verticalScale(12),
        color: Colors.white,
    },
    icPencil: {
        width: verticalScale(36),
        height: verticalScale(36),
    },
    btnEdit: {
        position: 'absolute',
        right: verticalScale(16),
    },
});
