import React, { useState, useEffect } from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import { UIHeader } from '../../../components/common';
import { Colors } from '../../../theme/Variables';
import { useNavigation, useRoute } from '@react-navigation/native';
import { UIText, UITextInput } from '../../../components/elements';
import { verticalScale } from '../../../utils/ScalingUtils';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { actionGetLocation } from '../duck/actions';

const SearchScreen = (props) => {
    const navigation = useNavigation();
    const { t } = useTranslation();
    const { params } = useRoute();
    const { title, data, onChoose, placeholder } = params;
    const [renderData, setRenderData] = useState(title == 'Location' && data == null ? [] : data);
    const [dataFull, setRenderDataFull] = useState(title == 'Location' && data == null ? [] : data);
    const [search, setSearch] = useState('');

    useEffect(() => {
        if (title == 'Location' && data == null) {
            props.actionGetLocation((data) => {
                setRenderData(data);
                setRenderDataFull(data);
            });
        }
    }, []);

    const onChangeSearch = (text) => {
        if (text == '') {
            setRenderData(dataFull);
        } else {
            setRenderData(dataFull.filter((e, i) => e.name.toLocaleLowerCase().includes(text.toLocaleLowerCase())));
        }
        setSearch(text);
    };

    const onChooseItem = (item, index) => {
        onChoose && onChoose(item);
        navigation.goBack();
    };

    const renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity
                onPress={() => onChooseItem(item, index)}
                style={[styles.itemContainer, index == 0 && { paddingTop: verticalScale(5) }]}
            >
                <UIText font="medium" style={styles.txtItem} text={item.name ?? item} />
            </TouchableOpacity>
        );
    };

    return (
        <View style={styles.container}>
            <UIHeader LeftIcon="close" title={title} onPress={() => navigation.goBack()} />
            <UITextInput placeholder={placeholder} onChangeText={(text) => onChangeSearch(text)} value={search} />
            <FlatList data={renderData} extraData={renderData} renderItem={renderItem} />
        </View>
    );
};
const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {
    actionGetLocation,
})(SearchScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    itemContainer: {
        width: '100%',
        padding: verticalScale(15),
        borderBottomColor: Colors.border,
        borderBottomWidth: 1,
    },
    txtItem: {
        fontSize: verticalScale(14),
        color: Colors.blueDark,
    },
});
