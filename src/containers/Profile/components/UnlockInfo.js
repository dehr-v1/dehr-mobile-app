import React, { useState } from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { useTheme } from '@/theme';

import { Colors } from '../../../theme/Variables';
import { useTranslation } from 'react-i18next';
import { fontFamily } from '../../../theme/Fonts';
import { UIButton, UIText } from '../../../components/elements';

const CenterModal = ({ children }) => {
    return (
        <View style={styles.containerCenter}>
            <View style={styles.card}>{children}</View>
        </View>
    );
};

const BottomModal = ({ children }) => {
    return <View style={styles.containerBottom}>{children}</View>;
};

const UnlockInfo = ({ onCancel, onFinish }) => {
    const { Images } = useTheme();
    const { t } = useTranslation();
    const [activeStep, setActiveStep] = useState(1);

    switch (activeStep) {
        case 2:
            return (
                <BottomModal>
                    <UIText style={[styles.title, { marginBottom: 20 }]} font="bold">
                        {t('profile.transaction_unlock_info')}
                    </UIText>
                    <UIText style={[styles.price, { marginBottom: 20 }]}>- $ 204</UIText>
                    <View style={{ marginBottom: 72 }}>
                        <View style={styles.transactionRow}>
                            <UIText style={styles.transactionName}>{t('profile.type')}</UIText>
                            <UIText style={styles.transactionValue} font="bold" text="Send"></UIText>
                        </View>
                        <View style={styles.transactionRow}>
                            <UIText style={styles.transactionName}>{t('profile.transaction_code')}</UIText>
                            <UIText style={styles.transactionValue} font="bold">
                                1355678900
                            </UIText>
                        </View>
                        <View style={styles.transactionRow}>
                            <UIText style={styles.transactionName}>{t('profile.time')}</UIText>
                            <UIText style={styles.transactionValue} font="bold">
                                20: 29 – Aug 19, 2021
                            </UIText>
                        </View>
                        <View style={styles.transactionRow}>
                            <UIText style={styles.transactionName}>{t('profile.fund')}</UIText>
                            <UIText style={styles.transactionValue} font="bold">
                                DeHR Wallet
                            </UIText>
                        </View>
                        <View style={styles.transactionRow}>
                            <UIText style={styles.transactionName}>{t('profile.wallet_balance')}</UIText>
                            <UIText style={styles.transactionValue} font="bold">
                                $8,452.98
                            </UIText>
                        </View>
                    </View>
                    <View>
                        <UIButton text={t('profile.complete')} onPress={() => setActiveStep(3)} />
                    </View>
                </BottomModal>
            );
        case 3:
            return (
                <CenterModal>
                    <UIText style={[styles.title, { marginBottom: 30 }]} font="bold">
                        {t('profile.notification')}
                    </UIText>
                    <UIText style={[styles.description, { textAlign: 'left' }]}>
                        {t('profile.please_waitting_confirm')} <UIText font="bold">@Brittany Brant</UIText>
                    </UIText>

                    <View style={styles.buttonRow}>
                        <View style={styles.buttonColumn}>
                            <UIButton text={t('profile.cancel')} onPress={() => onCancel()} type="BgWhite" textStyle={styles.buttonText} />
                        </View>
                        <View style={styles.buttonColumn}>
                            <UIButton text={t('profile.i_got_it')} textStyle={styles.buttonText} onPress={() => onFinish()} />
                        </View>
                    </View>
                </CenterModal>
            );
        default:
            return (
                <CenterModal>
                    <UIText style={styles.title} font="bold">
                        {t('profile.notification')}
                    </UIText>
                    <Image resizeMode="contain" source={Images.wallet.bg_wallet_setup} style={styles.bgWalletSetup} />
                    <UIText style={styles.description}>{t('profile.no_coin')}</UIText>
                    <View style={styles.buttonRow}>
                        <View style={styles.buttonColumn}>
                            <UIButton text={t('profile.cancel')} onPress={() => onCancel()} type="BgWhite" textStyle={styles.buttonText} />
                        </View>
                        <View style={styles.buttonColumn}>
                            <UIButton text={t('profile.top_up_now')} textStyle={styles.buttonText} onPress={() => setActiveStep(2)} />
                        </View>
                    </View>
                </CenterModal>
            );
    }
};

const styles = StyleSheet.create({
    bgWalletSetup: {
        maxWidth: 199,
        alignSelf: 'center',
    },
    containerBottom: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        backgroundColor: Colors.white,
        paddingTop: 30,
        paddingLeft: 30,
        paddingRight: 30,
        paddingBottom: 40,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    containerCenter: {
        width: '100%',
        padding: 20,
    },
    card: {
        padding: 20,
        borderRadius: 10,
        backgroundColor: Colors.white,
    },
    title: {
        fontSize: 20,
        lineHeight: 26,
        color: Colors.blueDark,
        textAlign: 'center',
    },
    description: {
        fontSize: 16,
        lineHeight: 20,
        color: Colors.title,
        textAlign: 'center',
        marginBottom: 30,
    },
    buttonText: {
        fontSize: 16,
    },
    buttonRow: {
        flexDirection: 'row',
        marginLeft: -9,
        marginRight: -9,
    },
    buttonColumn: {
        flexGrow: 1,
        flexShrink: 1,
        paddingHorizontal: 9,
    },
    price: {
        fontSize: 30,
        fontWeight: '700',
        color: Colors.lightGreen2,
        textAlign: 'center',
        lineHeight: 30
    },
    transactionName: {
        flexGrow: 1,
        flexShrink: 1,
        fontSize: 16,
        color: Colors.title,
    },
    transactionValue: {
        flexGrow: 1,
        flexShrink: 1,
        textAlign: 'right',
        marginBottom: 20,
        fontSize: 16,
        color: Colors.title,
    },
    transactionRow: {
        flexDirection: 'row',
    },
});

export default UnlockInfo;
