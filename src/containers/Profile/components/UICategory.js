import React, { useState } from 'react';
import { View, LayoutAnimation, TouchableOpacity, Image, StyleSheet, Platform, UIManager } from 'react-native';
import { UIText } from '../../../components/elements';
import { useTheme } from '@/theme';
import { verticalScale } from '../../../utils/ScalingUtils';
import { Colors } from '../../../theme/Variables';

if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}

const Category = (props) => {
    const { name, children } = props;
    const { Images } = useTheme();
    const [isDrop, setIsDrop] = useState(false);

    const onDrop = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        setIsDrop(!isDrop);
    };

    return (
        <View style={{ width: '100%', marginVertical: verticalScale(5) }}>
            <TouchableOpacity
                activeOpacity={1}
                onPress={onDrop}
                style={[styles.container, { backgroundColor: isDrop ? Colors.blueDark : Colors.gray }, !isDrop && styles.border]}
            >
                <UIText font="medium" style={{ color: isDrop ? Colors.white : Colors.blueDark }} text={name} />
                <Image style={isDrop ? styles.icWhite : styles.icBlue} source={Images.profile.ic_down} />
            </TouchableOpacity>
            {isDrop && <View style={styles.childrenContainer}>{children}</View>}
        </View>
    );
};

export default Category;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: verticalScale(54),
        borderRadius: verticalScale(6),
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: 'blue',
        paddingLeft: verticalScale(20),
        paddingRight: verticalScale(15),
    },
    border: {
        borderWidth: 1,
        borderColor: Colors.border,
    },
    icWhite: {
        transform: [{ rotate: '180deg' }],
        tintColor: Colors.white,
    },
    icBlue: {
        tintColor: Colors.blueDark,
    },
    childrenContainer: {
        paddingHorizontal: verticalScale(14),
        alignItems: 'flex-end',
    },
});
