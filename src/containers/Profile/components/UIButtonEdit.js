import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { moderateScale, scale, verticalScale } from '../../../utils';
import { useTheme } from '@/theme';

const UIButtonEdit = (props) => {
    const {
        onPress,
        text ,
    } = props
    const {Images} =  useTheme()
    return (
        <View style={styles.container}>
            <UIText
                text = {text}
                font = "bold"
                style = {styles.text}
            />
             <TouchableOpacity style={styles.btn} onPress = {onPress}>
                <Image resizeMode="contain" source={Images.profile.ic_pencil} style={styles.icon} />
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: scale(16),
        backgroundColor: Colors.white,
        marginBottom: verticalScale(2),
    },
    text : {
        fontSize: moderateScale(12),
        lineHeight: moderateScale(20),
        color: Colors.darkGray,
        textTransform: 'uppercase',
    },
    btn: {
        width: scale(32),
        height: scale(32),
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon : {
        width: scale(28),
        height: scale(28),
    }
});

export default UIButtonEdit;
