import React, { useState, useEffect } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { moderateScale, verticalScale } from '../../../utils/ScalingUtils';
import { useTheme } from '@/theme';
import { CATEGORY } from '../../../constant';
import UIItem from './UIItem';
import { BASE_IMAGE_URL } from '../../../constant/value.constant';

const UIMainCategory = (props) => {
    const { Images } = useTheme();
    const { data, type, category, onPress, trustedObj = [] } = props;
    const [renderData, setRenderData] = useState([...data]);
    const [listAvatar, setListAvatar] = useState([]);

    useEffect(() => {
        if (trustedObj.length > 0) {
            const listTrusted = [];
            trustedObj.map((item, index) => {
                listTrusted.push({ id: index, imageUrl: BASE_IMAGE_URL + item });
            });
            setListAvatar(listTrusted);
        }
    }, []);

    const renderItem = ({ item, index }) => {
        switch (type) {
            case CATEGORY.ABOUT:
                return <UIItem isPadding={false} hideAction name={item.title} title={item.proficiency} />;
            case CATEGORY.EXPERIENCE:
                return (
                    <UIItem
                        hideAction
                        name={item.title}
                        startDate={item.startDate}
                        endDate={item.endDate}
                        title={item.employmentType}
                        description={item.description}
                        trust={listAvatar}
                        media={item.media}
                        location={item.location}
                    />
                );
            case CATEGORY.EDUCATION:
                return (
                    <UIItem
                        hideAction
                        name={item.school}
                        startDate={item.startDate}
                        endDate={item.endDate}
                        title={item.degree}
                        media={item.media}
                        description={item.description}
                    />
                );
            case CATEGORY.SKILL:
                return (
                    <View
                        style={{
                            marginTop: index == 0 ? verticalScale(10) : 0,
                            marginBottom: index + 1 == renderData.length ? verticalScale(10) : 0,
                        }}
                    >
                        <UIItem
                            isPadding={false}
                            key={`otherSkill${index}`}
                            name={item.title}
                            description={item.proficiency}
                            trust={listAvatar}
                            hideAction
                        />
                    </View>
                );
            default:
                return (
                    <View
                        style={{
                            marginTop: index == 0 ? verticalScale(10) : 0,
                            marginBottom: index + 1 == renderData.length ? verticalScale(10) : 0,
                        }}
                    >
                        <UIItem name={item.title} isPadding={false} description={item.proficiency} hideAction />
                    </View>
                );
        }
        return <View />;
    };

    return (
        <View style={styles.container}>
            <View style={styles.categoryContainer}>
                <UIText font="bold" style={styles.txtCategory} text={category} />
                {/* <TouchableOpacity onPress={onPress} >
                    <Image source={Images.profile.ic_pencil} />
                </TouchableOpacity> */}
            </View>
            <FlatList data={renderData} extraData={renderData} renderItem={renderItem} keyExtractor={(item, index) => index.toString()} />
        </View>
    );
};

export default UIMainCategory;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: Colors.white,
        marginTop: verticalScale(5),
        paddingHorizontal: verticalScale(16),
        paddingTop: verticalScale(15),
    },
    txtCategory: {
        fontSize: moderateScale(18),
        lineHeight: moderateScale(26),
    },
    categoryContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: verticalScale(16),
    },
});
