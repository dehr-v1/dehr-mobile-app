import React from 'react';
import { View, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { UIText } from '../../../components/elements';
import { verticalScale } from '../../../utils/ScalingUtils';
import { useTheme } from '@/theme';
import { Colors } from '../../../theme/Variables';

const SubCategory = (props) => {
    const { icon, name, description, onPress, customStyle, customContainer, editable = false } = props;
    const { Images } = useTheme();
    return (
        <View style={styles.container}>
            <View style={[styles.icContainer, customContainer]}>
                <Image resizeMode="contain" source={icon} style={[styles.iconLeft, customStyle]} />
            </View>
            <View style={styles.contentContainer}>
                <UIText text={name} />
                <UIText style={styles.txtDes} text={description} />
            </View>
            {editable && (
                <TouchableOpacity onPress={onPress}>
                    <Image style={styles.iconLeft} source={Images.profile.ic_plus} />
                </TouchableOpacity>
            )}
        </View>
    );
};

export default SubCategory;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        marginVertical: verticalScale(8),
    },
    iconLeft: {
        width: verticalScale(38),
        height: verticalScale(38),
        tintColor: Colors.blueDark,
    },
    contentContainer: {
        flex: 1,
        paddingHorizontal: verticalScale(10),
    },
    txtDes: {
        fontSize: verticalScale(12),
        color: Colors.lightGray,
        flexWrap: 'wrap',
        lineHeight: verticalScale(16),
    },
    icContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
});
