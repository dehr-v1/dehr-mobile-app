import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { UIText } from '../../../components/elements';
import { Colors } from '../../../theme/Variables';
import { scale, verticalScale } from '../../../utils';

const UIButtonAdd = (props) => {
    const {
        icon,
        onPress,
        text,
    } = props
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.container}>
                <View style={styles.btn}>
                    <Image resizeMode="contain" source={icon} style={styles.icon} />
                </View>
                <UIText
                    text={text}
                    font="bold"
                />
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: verticalScale(15),
        paddingHorizontal: scale(16),
    },
    btn: {
        width: scale(40),
        height: scale(40),
        borderRadius: scale(20),
        backgroundColor: Colors.grayHiring,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: scale(14),
    },
    icon: {
        width: scale(28),
        height: scale(28),
    }
});

export default UIButtonAdd;
