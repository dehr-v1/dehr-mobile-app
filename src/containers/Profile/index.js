import React, { useEffect, useState, useRef } from 'react';
import { View, Text, Image, StyleSheet, ImageBackground, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import { useTheme } from '@/theme';
import _get from 'lodash/get';
import { useTranslation } from 'react-i18next';
import LinearGradient from 'react-native-linear-gradient';
import { useNavigation, useRoute } from '@react-navigation/native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import { UIHeader, UIModal, CustomTabbar } from '../../components/common';
import { moderateScale, verticalScale, CommonUtils, scale } from '../../utils';
import { Colors } from '../../theme/Variables';
import { UIButton, UIText } from '../../components/elements';
import { CATEGORY, SCREEN } from '../../constant';
import { UIAvatar } from './components';
import { accountUpdateAccountInfo } from './duck/actions';
import { KYC_STATUS, BASE_IMAGE_URL, LOCK_INFO, SKILL_LEVEL } from '../../constant/value.constant';
import Toast from '../../components/elements/Toast';
import { fontFamily } from '../../theme/Fonts';
import { UIButtonEdit, UIItem } from './components';
import UnlockInfo from './components/UnlockInfo';

const membershipColorPattern = {
    levelOne: {
        color: Colors.blueDark,
        gradient: [],
    },
    levelTwo: {
        color: Colors.blueLight2,
        gradient: [Colors.blueLight2, Colors.blueLight3],
    },
    levelThree: {
        color: Colors.lightBlue,
        gradient: [Colors.blueLight3, Colors.blueLight4],
        backgroundColor: Colors.blueLight5,
    },
};

const IndexProfileContainer = (props) => {
    const { t } = useTranslation();
    const { Layout, Images } = useTheme();
    const navigation = useNavigation();
    const { params } = useRoute() || {};
    const { profileReducer, accountUpdateAccountInfo } = props || {};
    const { accountInfo } = profileReducer || {};
    const { newPrice } = 0;
    const { resume, avatar: avatar_url, phoneNumber, email, kycStatus, contact } = accountInfo || {};
    const { price } = contact || {};
    const [avatar, setAvatar] = useState(avatar_url || '');
    const [membershipOption, setMembershipOption] = useState(membershipColorPattern.levelOne);
    const membershipLevel = 1; // 1 or 2 or 3
    const { is_unlocked } = false;
    const [isUnlocking, setIsUnlocking] = useState(false);
    const {
        intro,
        about,
        background,
        skills,
        ratingsSumary,
        addtionalInfo,
        contactPrice = '0',
        isProfileUpdated,
        trustedObj,
    } = resume || {};
    const { headline, current_position, location, industrial_knowledge } = intro || {};
    const { summary } = about || {};
    const { experience, education } = background || [];
    const { site_company, hobbies, social_links } = addtionalInfo || [];
    const [isUpdate, setIsUpdate] = useState(false);
    let modalRef = useRef();
    let modalUnlockInfoRef = useRef();

    useEffect(() => {
        switch (membershipLevel) {
            case 2:
                setMembershipOption(membershipColorPattern.levelTwo);
                break;
            case 3:
                setMembershipOption(membershipColorPattern.levelThree);
                break;
            default:
                setMembershipOption(membershipColorPattern.levelOne);
                break;
        }
    }, [membershipLevel]);

    useEffect(() => {
        setIsUpdate(!isUpdate);
    }, [accountInfo]);

    useEffect(() => {
        setAvatar(avatar);
    }, [avatar]);

    const onPressAvatar = () => {
        navigation.navigate(SCREEN.ACCOUNT_INFO);
    };

    const onPressEdit = (index = 0) => {
        navigation.navigate(SCREEN.CREATE_PROFILE, { indexTab: index });
    };

    const onPressModalTakePhoto = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
            includeBase64: true,
        }).then((image) => {
            modalRef.close();
            const { path } = image;
            setAvatar(path);
            updateAvatar(image);
        });
    };
    const onPressModalChooseFrom = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,
            includeBase64: true,
        }).then((image) => {
            modalRef.close();
            const { path } = image;
            setAvatar(path);
            updateAvatar(image);
        });
    };
    const onPressCancelModal = () => {
        modalRef.close();
    };

    const setContactPrice = () => {
        if (kycStatus === KYC_STATUS.VERIFIED) {
            navigation.navigate(SCREEN.UPDATE_CONTACT_PRICE, {
                oldPrice: CommonUtils.convertFromWei(price) || '0',
            });
        } else {
            Toast(t('wallet.kyc_verify'), 'error');
        }
    };

    const updateAvatar = (avatar) => {
        const data = {
            image: avatar,
            _id: accountInfo._id,
        };
        accountUpdateAccountInfo({ data }, () => { });
    };

    //Item
    const renderItemExperience = ({ item, index }) => {
        return (
            <UIItem
                name={item.title}
                media={item.media}
                startDate={new Date(item.startDate)}
                startDate={new Date(item.endDate)}
                title={item.companyName}
                description={item.description}
                stylesName={styles.textName}
                hideAction
            />
        );
    };

    const renderItemEducation = ({ item, index }) => {
        return (
            <UIItem
                name={item.school}
                title={item.description}
                media={item.media}
                startDate={new Date(item.startDate)}
                startDate={new Date(item.endDate)}
                stylesName={styles.textName}
                hideAction
            />
        );
    };

    const renderItemSkill = ({ item, index }) => {
        return (
            <UIItem
                name={item.title}
                title={SKILL_LEVEL[item.proficiency - 1].label}
                stylesName={styles.textName}
                hideAction
                style={styles.item}
            />
        );
    };

    const renderNoProfile = () => {
        return (
            <View style={styles.container}>
                <Image style={styles.imgBg} resizeMode="cover" source={Images.profile.bg_profile} />
                <UIText
                    font="black"
                    numberOfLines={2}
                    ellipsizeMode="tail"
                    style={styles.txtWelcome}
                    text={t('profile.welcome', { name: accountInfo.username })}
                />
                <UIText style={styles.txt} text={t('profile.welcome_des')} />
                <UIButton
                    onPress={() => navigation.navigate(SCREEN.CREATE_PROFILE)}
                    style={styles.btnCreate}
                    text={t('profile.create_profile')}
                />
            </View>
        );
    };

    const renderProfile = () => {
        return (
            <View style={styles.viewContainer}>
                <ScrollView contentContainerStyle={styles.profileContainer}>
                    <>
                        <ImageBackground
                            style={styles.profileImageContainer}
                            imageStyle={styles.profileImage}
                            source={Images.profile.bg_work}
                        >
                            <UIHeader RightIcon="more" />
                            <TouchableOpacity style={styles.btnCamera}>
                                <Image style={styles.camera} source={Images.profile.ic_camera} />
                            </TouchableOpacity>
                            {membershipLevel > 1 && (
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    colors={membershipOption.gradient}
                                    style={{
                                        width: '100%',
                                        height: membershipLevel === 2 ? 8 : 25,
                                        position: 'absolute',
                                        bottom: membershipLevel === 2 ? 0 : 5,
                                    }}
                                />
                            )}
                            {membershipLevel === 3 && (
                                <View
                                    style={{
                                        opacity: 1,
                                        position: 'absolute',
                                        height: 5,
                                        width: '100%',
                                        bottom: 0,
                                        backgroundColor: '#30e3eb',
                                    }}
                                ></View>
                            )}
                        </ImageBackground>
                        <View
                            style={[styles.contentContainer, { backgroundColor: _get(membershipOption, 'backgroundColor', Colors.white) }]}
                        >
                            <UIAvatar
                                membershipLevel={membershipLevel}
                                url={avatar?.includes('file') ? avatar : avatar ? BASE_IMAGE_URL + avatar : null}
                                onPressCamera={() => modalRef.open()}
                                onPressAvatar={onPressAvatar}
                                style={{ marginTop: verticalScale(-50) }}
                            />
                            <UIText
                                style={[styles.txtName, { color: membershipOption.color }]}
                                font="black"
                                numberOfLines={2}
                                ellipsizeMode="tail"
                                text={accountInfo.fullname || accountInfo.username}
                            />
                            <View style={styles.viewRate}>
                                <View style={styles.row}>
                                    <Image
                                        style={[styles.icStar, ratingsSumary >= 1 && { tintColor: Colors.yellow }]}
                                        source={Images.home.ic_star_vote}
                                    />
                                    <Image
                                        style={[styles.icStar, ratingsSumary >= 2 && { tintColor: Colors.yellow }]}
                                        source={Images.home.ic_star_vote}
                                    />
                                    <Image
                                        style={[styles.icStar, ratingsSumary >= 3 && { tintColor: Colors.yellow }]}
                                        source={Images.home.ic_star_vote}
                                    />
                                    <Image
                                        style={[styles.icStar, ratingsSumary >= 4 && { tintColor: Colors.yellow }]}
                                        source={Images.home.ic_star_vote}
                                    />
                                    <Image
                                        style={[styles.icStar, styles.icLastStar, ratingsSumary >= 5 && { tintColor: Colors.yellow }]}
                                        source={Images.home.ic_star_vote}
                                    />
                                </View>
                                <Text style={styles.textInfoPrice}>
                                    {t('profile.info_price')}
                                    <Text style={[styles.textDeHR, { color: membershipOption.color }]}>
                                        {' '}
                                        {CommonUtils.convertFromWei(price)} DeHR
                                    </Text>
                                </Text>
                                <TouchableOpacity onPress={setContactPrice}>
                                    <Image style={styles.icEditDeHR} source={Images.profile.ic_edit} />
                                </TouchableOpacity>
                            </View>
                            <UIText
                                numberOfLines={2}
                                ellipsizeMode="tail"
                                style={styles.txtHeadline}
                                text={`${headline ?? ''}`}
                            />
                            <View style={styles.viewLocation}>
                                <Image style={styles.icLocation} source={Images.profile.ic_position_2} />
                                <UIText
                                    numberOfLines={2}
                                    style={[styles.txtHeadline, { paddingHorizontal: 0 }]}
                                    text={experience[0]?.location}
                                />
                            </View>
                            <View style={styles.btnContainer}>
                                {/* =============== */}
                                {isUnlocking ? (
                                    <>
                                        <View style={[styles.viewBtn, { marginRight: scale(11) }]}>
                                            <UIButton
                                                text={t('profile.cancel_request')}
                                                type="BgGrey2"
                                                Icon={Images.profile.cancel_person}
                                                onPress={() => setIsUnlocking(false)}
                                            />
                                        </View>
                                        <View style={styles.viewBtn}>
                                            <UIButton text={t('profile.waiting')} type="BgGrey3" />
                                        </View>
                                    </>
                                ) : (
                                    <>
                                        <View style={[styles.viewBtn, { marginRight: scale(11) }]}>
                                            <UIButton
                                                onPress={onPressEdit}
                                                text={t('profile.edit_profile')}
                                                type="BgGrey2"
                                                style={{ height: verticalScale(44) }}
                                                textStyle={{ fontSize: moderateScale(16), color: Colors.blueDark }}
                                            />
                                        </View>
                                        <View style={styles.viewBtn}>
                                            <UIButton
                                                text={t('profile.my_network')}
                                                type="BgBlue"
                                                onPress={() => navigation.navigate(SCREEN.CONNECT_NETWORK)}
                                                //onPress={() => modalUnlockInfoRef.open()}
                                                style={{ height: verticalScale(44) }}
                                                textStyle={{ fontSize: moderateScale(16) }}
                                            />
                                        </View>
                                    </>
                                )}
                                {/* =============== */}
                                {/* <View style={[styles.viewBtn, { marginRight: scale(11) }]}>
                                    <UIButton
                                        text={t('profile.edit_profile')}
                                        type="BgBlue"
                                        style={styles.btnEdit}
                                        textStyle={styles.textEdit}
                                    />
                                </View>
                                <View style={styles.viewBtn}>
                                    <UIButton
                                        text={t('profile.profile_request')}
                                        type="BgBlue"
                                    />
                                </View> */}
                                {/* <View style={styles.btn}>
                                <UIButton
                                    onPress={onPressEdit}
                                    iconStyle={styles.icEdit}
                                    Icon={Images.profile.ic_edit}
                                    textStyle={{ color: Colors.blueDark }}
                                    style={{ height: verticalScale(85) }}
                                    type="BgGrey"
                                    text={t('profile.edit_profile')}
                                />
                            </View>
                            <View style={styles.btn}>
                                <TouchableOpacity onPress={setContactPrice} style={styles.btnContactPrice}>
                                    <Image style={[styles.icEdit, { tintColor: Colors.white }]} source={Images.profile.ic_edit} />
                                    <View>
                                        <UIText font="bold" style={{ color: Colors.white }} text={t('profile.contact_price')} />
                                        {newPrice != 0 && newPrice != undefined && (
                                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                                <UIText font="bold" style={styles.txtContactPrice} text={newPrice} />
                                                <Image style={styles.icLogo} source={Images.wallet.ic_logo} />
                                            </View>
                                        )}
                                        {contactPrice && newPrice == undefined && (
                                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                                <UIText
                                                    font="bold"
                                                    style={styles.txtContactPrice}
                                                    text={contactPrice ? CommonUtils.convertFromWei(contactPrice) : 0}
                                                />
                                                <Image style={styles.icLogo} source={Images.wallet.ic_logo} />
                                            </View>
                                        )}
                                    </View>
                                </TouchableOpacity>
                            </View> */}
                            </View>
                        </View>
                        <ScrollableTabView renderTabBar={(props) => <CustomTabbar {...props} />}>
                            <View tabLabel={t('profile.about')}>
                                <UIButtonEdit text={t('profile.summary')} onPress={() => onPressEdit(0)} />
                                <View style={styles.viewContent}>
                                    <UIText text={summary || ''} style={styles.textContent} />
                                </View>
                            </View>
                            <View tabLabel={t('profile.experience')}>
                                <UIButtonEdit text={t('profile.experience_endorsements')} onPress={() => onPressEdit(1)} />
                                <FlatList
                                    keyExtractor={(_, i) => i.toString()}
                                    data={experience}
                                    extraData={experience}
                                    renderItem={renderItemExperience}
                                />
                            </View>
                            <View tabLabel={t('profile.education')}>
                                <UIButtonEdit text={t('profile.education')} onPress={() => onPressEdit(2)} />
                                <FlatList
                                    keyExtractor={(_, i) => i.toString()}
                                    data={education}
                                    extraData={education}
                                    renderItem={renderItemEducation}
                                />
                            </View>
                            <View tabLabel={t('profile.skills')}>
                                <UIButtonEdit text={t('profile.skills_endorsements')} onPress={() => onPressEdit(3)} />
                                <FlatList
                                    data={skills}
                                    extraData={skills}
                                    keyExtractor={(_, i) => i.toString()}
                                    renderItem={renderItemSkill}
                                />
                            </View>
                            {LOCK_INFO.BUY === is_unlocked && <View tabLabel={t('profile.contact')}>
                                <UIButtonEdit text={t('profile.contact_info')} onPress={() => onPressEdit(4)} />
                                <FlatList
                                    data={[
                                        { name: 'Email', value: email },
                                    ]}
                                    keyExtractor={(_, i) => i.toString()}
                                    renderItem={({ item }) => (
                                        <UIItem
                                            name={item?.name}
                                            title={item?.value}
                                            stylesName={styles.textName}
                                            hideAction
                                            style={styles.item}
                                        />
                                    )}
                                />
                            </View>}
                        </ScrollableTabView>
                    </>
                </ScrollView>
            </View>
        );
    };

    return (
        <>
            {resume && about && summary ? renderProfile() : renderNoProfile()}
            <UIModal ref={(ref) => (modalRef = ref)}>
                <>
                    <View style={{ flex: 1 }} />
                    <TouchableOpacity activeOpacity={1} style={{ width: '88%' }}>
                        <View style={styles.viewModalTop}>
                            <TouchableOpacity onPress={onPressModalTakePhoto}>
                                <UIText text={t('profile.take_photo')} style={styles.textSelectImg} font="bold" />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={onPressModalChooseFrom}>
                                <UIText text={t('profile.choose_from')} style={[styles.textSelectImg, { marginBottom: 0 }]} font="bold" />
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: verticalScale(10) }} />
                        <TouchableOpacity style={styles.viewModalBot} onPress={onPressCancelModal}>
                            <UIText text={t('profile.cancel')} style={styles.textCancel} font="bold" />
                        </TouchableOpacity>
                    </TouchableOpacity>
                </>
            </UIModal>
            <UIModal ref={(ref) => (modalUnlockInfoRef = ref)}>
                <UnlockInfo
                    onCancel={() => modalUnlockInfoRef.close()}
                    onFinish={() => {
                        setIsUnlocking(true);
                        modalUnlockInfoRef.close();
                    }}
                />
            </UIModal>
        </>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});
export default connect(mapStateToProps, {
    accountUpdateAccountInfo,
})(IndexProfileContainer);

const styles = StyleSheet.create({
    viewContainer: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    container: {
        flex: 1,
        paddingHorizontal: verticalScale(30),
        backgroundColor: Colors.white,
        alignItems: 'center',
    },
    profileContainer: {
        alignItems: 'center',
        backgroundColor: Colors.white,
    },
    imgBg: {
        width: '100%',
        marginTop: verticalScale(30),
    },
    txtWelcome: {
        marginTop: verticalScale(30),
        fontSize: verticalScale(24),
        marginBottom: verticalScale(20),
    },
    txt: {
        textAlign: 'center',
    },
    btnCreate: {
        marginTop: verticalScale(47),
    },
    profileImage: {
        width: '100%',
        height: verticalScale(200),
    },
    profileImageContainer: {
        position: 'absolute',
        top: 0,
        width: '100%',
        height: verticalScale(200),
        alignItems: 'center',
        backgroundColor: Colors.white,
    },
    contentContainer: {
        marginTop: verticalScale(200),
        alignItems: 'center',
        width: '100%',
        backgroundColor: Colors.white,
    },
    txtName: {
        paddingHorizontal: verticalScale(16),
        textAlign: 'center',
        fontSize: verticalScale(24),
        color: Colors.blueDark,
        marginTop: verticalScale(15),
    },
    viewLocation: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        marginBottom: verticalScale(16),
    },
    icLocation: {
        width: scale(20),
        height: scale(20),
    },
    txtHeadline: {
        fontSize: verticalScale(14),
        lineHeight: verticalScale(18),
        marginTop: verticalScale(8),
        textAlign: 'center',
        paddingHorizontal: verticalScale(30),
    },
    btnContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingHorizontal: verticalScale(16),
        width: '100%',
        marginTop: verticalScale(15),
        marginBottom: verticalScale(20),
    },
    btn: {
        flex: 0.45,
    },
    icEdit: {
        width: verticalScale(22),
        height: verticalScale(22),
        marginEnd: verticalScale(5),
        tintColor: Colors.blueDark,
    },
    viewModalTop: {
        padding: verticalScale(12),
        backgroundColor: Colors.white,
        borderRadius: 10,
    },
    viewModalBot: {
        marginBottom: verticalScale(20),
        paddingVertical: verticalScale(12),
        backgroundColor: Colors.white,
        borderRadius: 10,
    },
    textSelectImg: {
        alignSelf: 'center',
        marginBottom: verticalScale(10),
        borderRadius: 10,
    },
    textCancel: {
        textTransform: 'capitalize',
        lineHeight: verticalScale(20),
        alignSelf: 'center',
        borderRadius: 10,
    },
    icStar: {
        width: verticalScale(16),
        height: verticalScale(16),
        tintColor: '#d3dae1',
        marginRight: verticalScale(2),
    },
    icLastStar: {
        marginRight: verticalScale(14),
    },
    viewRate: {
        marginTop: verticalScale(8),
        flexDirection: 'row',
    },
    row: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    icLogo: {
        width: verticalScale(40),
        height: verticalScale(14),
        marginLeft: verticalScale(5),
        tintColor: Colors.white,
    },
    txtContactPrice: {
        fontSize: moderateScale(16),
        color: Colors.white,
    },
    btnContactPrice: {
        width: '100%',
        height: verticalScale(85),
        paddingHorizontal: verticalScale(10),
        paddingVertical: verticalScale(10),
        backgroundColor: Colors.blueDark,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: verticalScale(4),
    },
    btnCamera: {
        width: scale(36),
        height: scale(36),
        borderRadius: scale(18),
        borderWidth: scale(1),
        borderColor: Colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.lightGray2,
        position: 'absolute',
        bottom: verticalScale(15),
        right: scale(20),
    },
    camera: {
        width: verticalScale(24),
        height: verticalScale(24),
        tintColor: Colors.darkGray,
    },
    textInfoPrice: {
        color: Colors.title,
        fontFamily: fontFamily.regular,
        fontSize: moderateScale(14),
        lineHeight: moderateScale(18),
        marginRight: scale(11),
    },
    textDeHR: {
        color: Colors.blueLight2,
        fontFamily: fontFamily.bold,
        fontSize: moderateScale(14),
        lineHeight: moderateScale(18),
    },
    icEditDeHR: {
        width: scale(18),
        height: scale(18),
    },
    viewBtn: {
        flex: 1,
    },
    btnEdit: {
        backgroundColor: Colors.border,
    },
    textEdit: {
        color: Colors.blueDark,
    },
    viewContent: {
        padding: scale(16),
        backgroundColor: Colors.white,
    },
    textContent: {
        fontSize: moderateScale(14),
        lineHeight: moderateScale(18),
    },
    textName: {
        color: Colors.title,
    },
    item: {
        marginBottom: verticalScale(2),
    },
    viewTabs: {
        flex: 1,
        paddingHorizontal: scale(16),
    },
});
