import SpecialistSkill from './SpecialistSkill';
import UpdateSpecialistSkill from './UpdateSpecialistSkill';
import OtherSkill from './OtherSkill';
import UpdateOtherSkill from './UpdateOtherSkill';
import Language from './Language';
import UpdateLanguage from './UpdateLanguage';

export { SpecialistSkill, UpdateSpecialistSkill, OtherSkill, UpdateOtherSkill, Language, UpdateLanguage };
