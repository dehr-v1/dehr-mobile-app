import React, { useState } from 'react';
import { View, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';
import { UIHeader, UITextInputForm } from '../../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { useNavigation, useRoute } from '@react-navigation/native';
import { verticalScale } from '../../../../utils/ScalingUtils';
import RNPickerSelect from 'react-native-picker-select';
import { UIButton, UIText } from '../../../../components/elements';
import { fontFamily } from '../../../../theme/Fonts';
import { useTheme } from '@/theme';
import { SCREEN, LANGUAGE_LEVEL } from '../../../../constant';
import { connect } from 'react-redux';
import { actionUpdateProfile } from '../../duck/actions';

const UpdateLanguage = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { params } = useRoute() || {};
    const { isAdd, item, index } = params || {};
    const { Images } = useTheme();

    const { profileReducer, actionUpdateProfile } = props || {};
    const { accountInfo } = profileReducer || {};
    const { resume } = accountInfo || {};
    const { skills } = resume || {};
    const { languages, others, specialists } = skills || {};

    const [skill, setSkill] = useState(item?.title || '');
    const [level, setLevel] = useState(item?.proficiency || '');

    const onAddNew = () => {
        const newData = [...languages, { title: skill, proficiency: level }];
        actionUpdateProfile({ data: buildBody(newData) }, () => {
            navigation.goBack();
        });
    };

    const buildBody = (newData) => {
        let skills = {
            languages: newData,
        };
        if (specialists.length != 0) {
            skills = {
                ...skills,
                specialists,
            };
        }
        if (others.length != 0) {
            skills = {
                ...skills,
                others,
            };
        }
        return { skills };
    };

    const onSave = () => {
        const newItem = {
            title: skill,
            proficiency: level,
        };
        const newData = languages.map((e, i) => {
            return i != index ? e : newItem;
        });
        actionUpdateProfile({ data: buildBody(newData) }, () => {
            navigation.goBack();
        });
    };

    const onClickBtnDelete = () => {
        const newData = languages.filter((e, i) => i != index);
        actionUpdateProfile({ data: buildBody(newData) }, () => {
            navigation.goBack();
        });
    };

    const onPressLocation = () => {
        navigation.navigate(SCREEN.SEARCH_SCREEN, {
            title: 'Language',
            data: [{ name: 'France' }, { name: 'English' }, { name: 'Vietnamese' }, { name: 'Chinese' }, { name: 'Japanese' }],
            placeholder: 'Language',
            onChoose: (item) => {
                setSkill(item.name);
            },
        });
    };

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={styles.container}>
                <UIHeader
                    LeftIcon="close"
                    onPress={() => navigation.goBack()}
                    title={t(isAdd ? 'profile.add_language' : 'profile.edit_language')}
                    onPressRight={onSave}
                    rightTitle={isAdd ? '' : t('profile.save')}
                />
                <View style={styles.contentContainer}>
                    <UIText style={styles.titlePicker} font="bold" text={t('profile.language')} />
                    <TouchableOpacity onPress={onPressLocation}>
                        <View style={styles.pickerContainer}>
                            <UIText text={skill} />
                            <View style={styles.icDownContainer}>
                                <Image style={styles.icDown} source={Images.profile.ic_down} />
                            </View>
                        </View>
                    </TouchableOpacity>
                    <UIText style={styles.titlePicker} font="bold" text={t('profile.poficiency')} />
                    <View style={styles.pickerContainer}>
                        <RNPickerSelect
                            textInputProps={styles.picker}
                            onValueChange={(value) => setLevel(value)}
                            style={{
                                inputAndroid: {
                                    color: Colors.blueDark,
                                    width: '100%',
                                    height: verticalScale(50),
                                    paddingTop: verticalScale(10),
                                    lineHeight: verticalScale(20),
                                },
                            }}
                            useNativeAndroidPickerStyle
                            value={level}
                            items={LANGUAGE_LEVEL}
                        />
                        <View style={styles.icDownContainer}>
                            <Image style={styles.icDown} source={Images.profile.ic_down} />
                        </View>
                    </View>
                </View>
                <View style={styles.btnContainer}>
                    <UIButton
                        type={isAdd ? '' : 'BgWhite'}
                        onPress={isAdd ? onAddNew : onClickBtnDelete}
                        text={t(isAdd ? 'profile.save' : 'profile.delete_language')}
                    />
                </View>
            </View>
        </ScrollView>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {
    actionUpdateProfile,
})(UpdateLanguage);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
    },
    pickerContainer: {
        height: verticalScale(50),
        width: '100%',
        backgroundColor: Colors.gray,
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(6),
        justifyContent: 'center',
        paddingHorizontal: verticalScale(15),
    },
    picker: {
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
    },
    icDown: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    icDownContainer: {
        position: 'absolute',
        right: verticalScale(15),
    },
    titlePicker: {
        marginTop: verticalScale(30),
        marginBottom: verticalScale(10),
    },
    btnContainer: {
        marginTop: 'auto',
        paddingTop: verticalScale(10),
        borderTopWidth: 1,
        borderTopColor: Colors.border,
        paddingHorizontal: verticalScale(30),
    },
});
