import React, { useState, useEffect } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { UIHeader } from '../../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { useNavigation } from '@react-navigation/native';
import { SCREEN, SKILL_LEVEL } from '../../../../constant';
import { UIItem } from '../../components';
import { connect } from 'react-redux';
import { actionUpdateProfile } from '../../duck/actions';

const SpecialistSkill = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();

    const { profileReducer, actionUpdateProfile } = props || {};
    const { accountInfo } = profileReducer || {};
    const { resume } = accountInfo || {};
    const { skills } = resume || {};
    const { specialists } = skills || {};

    const [data, setData] = useState(specialists);

    useEffect(() => {
        setData(specialists);
    }, [specialists]);

    const moveToAddNew = () => {
        navigation.navigate(SCREEN.UPDATE_SPECIALIST_SKILL, { isAdd: true });
    };

    const moveToEdit = (index) => {
        const item = data[index];
        navigation.navigate(SCREEN.UPDATE_SPECIALIST_SKILL, { isAdd: false, item, index });
    };

    const renderItem = ({ item, index }) => {
        return (
            <UIItem
                key={`skill${index}`}
                name={item.title}
                description={SKILL_LEVEL[item.proficiency - 1].label}
                onPress={() => moveToEdit(index)}
            />
        );
    };

    return (
        <View style={styles.container}>
            <UIHeader
                LeftIcon="back"
                RightIcon="plus"
                style={{ backgroundColor: Colors.white }}
                onPress={() => navigation.goBack()}
                onPressRight={moveToAddNew}
                title={t('profile.specialist_skills')}
            />
            <View style={styles.contentContainer}>
                <FlatList renderItem={renderItem} extraData={data} data={data} />
            </View>
        </View>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {
    actionUpdateProfile,
})(SpecialistSkill);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.gray,
    },
    contentContainer: {},
});
