import React, { useState, useEffect } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { UIHeader } from '../../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { useNavigation } from '@react-navigation/native';
import { SCREEN, SKILL_LEVEL } from '../../../../constant';
import { UIItem } from '../../components';
import { connect } from 'react-redux';

const OtherSkill = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();

    const { profileReducer } = props || {};
    const { accountInfo } = profileReducer || {};
    const { resume } = accountInfo || {};
    const { skills } = resume || {};
    const { others } = skills || {};

    const [data, setData] = useState(others);

    useEffect(() => {
        setData(others);
    }, [others]);

    const moveToAddNew = () => {
        navigation.navigate(SCREEN.UPDATE_OTHER_SKILL, { isAdd: true });
    };

    const moveToEdit = (index) => {
        const item = data[index];
        navigation.navigate(SCREEN.UPDATE_OTHER_SKILL, { isAdd: false, item, index });
    };

    const renderItem = ({ item, index }) => {
        return (
            <UIItem
                key={`otherSkill${index}`}
                name={item.title}
                description={SKILL_LEVEL[item.proficiency - 1].label}
                onPress={() => moveToEdit(index)}
            />
        );
    };

    return (
        <View style={styles.container}>
            <UIHeader
                LeftIcon="back"
                RightIcon="plus"
                style={{ backgroundColor: Colors.white }}
                onPress={() => navigation.goBack()}
                onPressRight={moveToAddNew}
                title={t('profile.other_skills')}
            />
            <View style={styles.contentContainer}>
                <FlatList renderItem={renderItem} extraData={data} data={data} />
            </View>
        </View>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {})(OtherSkill);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.gray,
    },
    contentContainer: {},
});
