import React, { useState } from 'react';
import { View, StyleSheet, Image, ScrollView } from 'react-native';
import { UIHeader, UITextInputForm } from '../../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { useNavigation, useRoute } from '@react-navigation/native';
import { verticalScale } from '../../../../utils/ScalingUtils';
import RNPickerSelect from 'react-native-picker-select';
import { UIButton, UIText } from '../../../../components/elements';
import { fontFamily } from '../../../../theme/Fonts';
import { useTheme } from '@/theme';
import { connect } from 'react-redux';
import { actionUpdateProfile, actionGetAccountInfo } from '../../duck/actions';
import { SKILL_LEVEL } from '../../../../constant';
import { SCREEN } from '../../../../constant';

const UpdateSpecialistSkill = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { params } = useRoute() || {};
    const { isAdd, item, index, onDeleteSkill } = params || {};
    const { Images } = useTheme();

    const { profileReducer, actionGetAccountInfo } = props || {};
    const { accountInfo } = profileReducer || {};
    const { resume } = accountInfo || {};
    const { skills } = resume || {};
    const { specialists, languages, others } = skills || {};

    const [type, setType] = useState(item?.type || '');
    const [skill, setSkill] = useState(item?.title || '');
    const [level, setLevel] = useState(item?.proficiency || '');

    const onAddNew = async () => {
        //const newData = [...specialists, { title: skill, proficiency: level }];
        // actionUpdateProfile({ data: buildBody(newData) }, () => {
        //     navigation.goBack();
        // });
        const newItem = {
            title: skill,
            proficiency: level,
        };
        await actionGetAccountInfo();
        navigation.navigate(SCREEN.CREATE_PROFILE, { addSkill: newItem, merge: true });
    };

    const buildBody = (newData) => {
        let skills = {
            specialists: newData,
        };
        if (languages.length != 0) {
            skills = {
                ...skills,
                languages,
            };
        }
        if (others.length != 0) {
            skills = {
                ...skills,
                others,
            };
        }
        return { skills };
    };

    const onClickBtnDelete = () => {
        onDeleteSkill(index);
        navigation.goBack();
    };

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={styles.container}>
                <UIHeader
                    LeftIcon="close"
                    onPress={() => navigation.goBack()}
                    title={t(isAdd ? 'profile.add_specialist_skill' : 'profile.edit_specialist_skill')}
                    onPressRight={onClickBtnDelete}
                    rightTitleStyle={{ color: Colors.red }}
                    rightTitle={isAdd ? '' : t('profile.delete_skill')}
                />
                <View style={styles.contentContainer}>
                    <UITextInputForm
                        placeholder={t('profile.type')}
                        title="Type"
                        value={type}
                        onChangeText={(text) => setType(text)}
                        required
                    />
                    <View style={{ height: verticalScale(30) }}></View>
                    <UITextInputForm
                        placeholder={t('profile.enter_skill')}
                        title="Skill"
                        value={skill}
                        onChangeText={(text) => setSkill(text)}
                        required
                    />
                    <UIText style={styles.titlePicker} font="bold" text="Proficiency" />
                    <View style={styles.pickerContainer}>
                        <RNPickerSelect
                            placeholder={{
                                label: t('profile.level_experience'),
                                value: null,
                                color: Colors.blueDark,
                            }}
                            textInputProps={styles.picker}
                            onValueChange={(value) => setLevel(value)}
                            style={{ ...pickerSelectStyles, }}
                            useNativeAndroidPickerStyle={false}
                            value={level}
                            items={SKILL_LEVEL}
                        />
                        <View style={styles.icDownContainer}>
                            <Image style={styles.icDown} source={Images.profile.ic_down} />
                        </View>
                    </View>
                </View>
                <View style={styles.btnContainer}>
                    <UIButton
                        type={''}
                        onPress={onAddNew}
                        text={t(isAdd ? 'profile.save' : 'profile.save')}
                    />
                </View>
            </View>
        </ScrollView>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {
    actionUpdateProfile,
    actionGetAccountInfo,
})(UpdateSpecialistSkill);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        marginTop: verticalScale(30),
    },
    pickerContainer: {
        height: verticalScale(50),
        width: '100%',
        backgroundColor: Colors.gray,
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(6),
        justifyContent: 'center',
        paddingHorizontal: verticalScale(15),
    },
    picker: {
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
    },
    icDown: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    icDownContainer: {
        position: 'absolute',
        right: verticalScale(15),
    },
    titlePicker: {
        marginTop: verticalScale(30),
        marginBottom: verticalScale(10),
    },
    btnContainer: {
        marginTop: 'auto',
        paddingTop: verticalScale(10),
        borderTopWidth: 1,
        borderTopColor: Colors.border,
        paddingHorizontal: verticalScale(30),
    },
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        color: Colors.blueDark,
        width: '100%',
        height: verticalScale(50),
        paddingTop: verticalScale(10),
        lineHeight: verticalScale(20),
        borderRadius: verticalScale(10),
    },
    inputAndroid: {
        color: Colors.blueDark,
        width: '100%',
        height: verticalScale(50),
        paddingTop: verticalScale(10),
        lineHeight: verticalScale(20),
        borderRadius: verticalScale(10),
    },
});
