import React, { useState } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { UIHeader, UITextInputForm } from '../../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { useNavigation, useRoute } from '@react-navigation/native';
import { verticalScale } from '../../../../utils/ScalingUtils';
import { UIButton } from '../../../../components/elements';
import { fontFamily } from '../../../../theme/Fonts';
import { connect } from 'react-redux';
import { actionUpdateProfile } from '../../duck/actions';

const UpdateSocialLink = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { params } = useRoute() || {};
    const { isAdd, item, index } = params || {};

    const { profileReducer, actionUpdateProfile } = props || {};
    const { accountInfo } = profileReducer || {};
    const { resume } = accountInfo || {};
    const { addtionalInfo } = resume || {};
    const { siteCompany, hobbies, socialLinks } = addtionalInfo || {};

    const [type, setType] = useState(item?.type || '');
    const [link, setLink] = useState(item?.value || '');

    const onAddNew = () => {
        const newData = [...socialLinks, { type: type, value: link }];
        const body = {
            addtionalInfo: {
                socialLinks: newData,
            },
        };
        actionUpdateProfile({ data: body }, () => {
            navigation.goBack();
        });
    };

    const onSave = () => {
        const newItem = {
            type: type,
            value: link,
        };

        const newData = socialLinks.map((e, i) => {
            return i != index ? e : newItem;
        });
        const body = {
            addtionalInfo: {
                socialLinks: newData,
            },
        };
        actionUpdateProfile({ data: body }, () => {
            navigation.goBack();
        });
    };

    const onClickBtnDelete = () => {
        const newData = socialLinks.filter((e, i) => i != index);
        const body = {
            addtionalInfo: {
                socialLinks: newData,
            },
        };
        actionUpdateProfile({ data: body }, () => {
            navigation.goBack();
        });
    };

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={styles.container}>
                <UIHeader
                    LeftIcon="close"
                    onPress={() => navigation.goBack()}
                    title={t(isAdd ? 'profile.add_social_link' : 'profile.edit_social_link')}
                    onPressRight={onSave}
                    rightTitle={isAdd ? '' : t('profile.save')}
                />
                <View style={styles.contentContainer}>
                    <UITextInputForm
                        placeholder={t('profile.type')}
                        title={t('profile.type')}
                        value={type}
                        onChangeText={(text) => setType(text)}
                        required
                    />
                    <UITextInputForm
                        placeholder={t('profile.link')}
                        title={t('profile.enter_link')}
                        value={link}
                        onChangeText={(text) => setLink(text)}
                    />
                </View>
                <View style={styles.btnContainer}>
                    <UIButton
                        type={isAdd ? '' : 'BgWhite'}
                        onPress={isAdd ? onAddNew : onClickBtnDelete}
                        text={t(isAdd ? 'profile.save' : 'profile.delete_social_link')}
                    />
                </View>
            </View>
        </ScrollView>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {
    actionUpdateProfile,
})(UpdateSocialLink);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        marginTop: verticalScale(30),
    },
    pickerContainer: {
        height: verticalScale(50),
        width: '100%',
        backgroundColor: Colors.gray,
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(6),
        justifyContent: 'center',
        padding: verticalScale(15),
    },
    picker: {
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
    },
    icDown: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    icDownContainer: {
        position: 'absolute',
        right: verticalScale(15),
    },
    titlePicker: {
        marginTop: verticalScale(30),
        marginBottom: verticalScale(10),
    },
    btnContainer: {
        marginTop: 'auto',
        paddingTop: verticalScale(10),
        borderTopWidth: 1,
        borderTopColor: Colors.border,
        paddingHorizontal: verticalScale(30),
    },
});
