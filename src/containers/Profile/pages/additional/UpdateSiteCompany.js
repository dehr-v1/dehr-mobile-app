import React, { useState } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { UIHeader, UITextInputForm } from '../../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { useNavigation, useRoute } from '@react-navigation/native';
import { verticalScale } from '../../../../utils/ScalingUtils';
import { UIButton } from '../../../../components/elements';
import { fontFamily } from '../../../../theme/Fonts';
import { connect } from 'react-redux';
import { actionUpdateProfile } from '../../duck/actions';

const UpdateSiteCompany = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { params } = useRoute() || {};
    const { isAdd, item, index } = params || {};

    const { profileReducer, actionUpdateProfile } = props || {};
    const { accountInfo } = profileReducer || {};
    const { resume } = accountInfo || {};
    const { addtionalInfo } = resume || {};
    const { hobbies, socialLinks } = addtionalInfo || {};

    const [type, setType] = useState(item?.title || '');
    const [link, setLink] = useState(item?.proficiency || '');

    const onAddNew = () => {
        const newData = [...siteCompany, { title: type, proficiency: link }];
        const body = {
            ...resume,
            addtionalInfo: [
                {
                    siteCompany: newData,
                    hobbies,
                    socialLinks,
                },
            ],
        };
        actionUpdateProfile({ data: body }, () => {
            navigation.goBack();
        });
    };

    const onSave = () => {
        const newItem = {
            title: type,
            proficiency: link,
        };
        const newData = siteCompany.map((e, i) => {
            return i != index ? e : newItem;
        });
        const body = {
            ...resume,
            addtionalInfo: [
                {
                    siteCompany: newData,
                    hobbies,
                    socialLinks,
                },
            ],
        };
        actionUpdateProfile({ data: body }, () => {
            navigation.goBack();
        });
    };

    const onClickBtnDelete = () => {
        const newData = siteCompany.filter((e, i) => i != index);
        const body = {
            ...resume,
            addtionalInfo: [
                {
                    siteCompany: newData,
                    hobbies,
                    socialLinks,
                },
            ],
        };
        actionUpdateProfile({ data: body }, () => {
            navigation.goBack();
        });
    };

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={styles.container}>
                <UIHeader
                    LeftIcon="close"
                    onPress={() => navigation.goBack()}
                    title={t(isAdd ? 'profile.add_site_company' : 'profile.edit_site_company')}
                    onPressRight={onSave}
                    rightTitle={isAdd ? '' : t('profile.save')}
                />
                <View style={styles.contentContainer}>
                    <UITextInputForm
                        placeholder={t('profile.type')}
                        title={t('profile.type_site_company')}
                        value={type}
                        onChangeText={(text) => setType(text)}
                        required
                    />
                    <UITextInputForm
                        placeholder={t('profile.link')}
                        title={t('profile.enter_link')}
                        value={link}
                        onChangeText={(text) => setLink(text)}
                        required
                    />
                </View>
                <View style={styles.btnContainer}>
                    <UIButton
                        type={isAdd ? '' : 'BgWhite'}
                        onPress={isAdd ? onAddNew : onClickBtnDelete}
                        text={t(isAdd ? 'profile.save' : 'profile.delete_site_company')}
                    />
                </View>
            </View>
        </ScrollView>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {
    actionUpdateProfile,
})(UpdateSiteCompany);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        marginTop: verticalScale(30),
    },
    pickerContainer: {
        height: verticalScale(50),
        width: '100%',
        backgroundColor: Colors.gray,
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(6),
        justifyContent: 'center',
        padding: verticalScale(15),
    },
    picker: {
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
    },
    btnContainer: {
        marginTop: 'auto',
        paddingTop: verticalScale(10),
        borderTopWidth: 1,
        borderTopColor: Colors.border,
        paddingHorizontal: verticalScale(30),
        marginBottom: verticalScale(30),
    },
});
