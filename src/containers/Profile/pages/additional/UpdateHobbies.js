import React, { useState } from 'react';
import { View, StyleSheet, Image, ScrollView } from 'react-native';
import { UIHeader, UITextInputForm } from '../../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { useNavigation, useRoute } from '@react-navigation/native';
import { verticalScale } from '../../../../utils/ScalingUtils';
import { UIButton, UIText } from '../../../../components/elements';
import { fontFamily } from '../../../../theme/Fonts';
import { connect } from 'react-redux';
import { actionUpdateProfile } from '../../duck/actions';

const UpdateHobbies = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();

    const { profileReducer, actionUpdateProfile } = props || {};
    const { accountInfo } = profileReducer || {};
    const { resume } = accountInfo || {};
    const { addtionalInfo } = resume || {};
    const { siteCompany, hobbies, socialLinks } = addtionalInfo || {};

    const [hobby, setHobby] = useState(hobbies || '');

    const onAddNew = () => {
        const newData = hobby;
        const body = {
            ...resume,
            addtionalInfo: [
                {
                    siteCompany,
                    hobbies: newData,
                    socialLinks,
                },
            ],
        };
        actionUpdateProfile({ data: body }, () => {
            navigation.goBack();
        });
    };

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={styles.container}>
                <UIHeader LeftIcon="close" onPress={() => navigation.goBack()} title={t('hobbies')} />
                <View style={styles.contentContainer}>
                    <UITextInputForm
                        placeholder={t('profile.enter_hobby')}
                        title={t('profile.hobby')}
                        value={hobby}
                        onChangeText={(text) => setHobby(text)}
                        required
                    />
                </View>
                <View style={styles.btnContainer}>
                    <UIButton onPress={onAddNew} text={t('profile.save')} />
                </View>
            </View>
        </ScrollView>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {
    actionUpdateProfile,
})(UpdateHobbies);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        marginTop: verticalScale(30),
    },
    pickerContainer: {
        height: verticalScale(50),
        width: '100%',
        backgroundColor: Colors.gray,
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(6),
        justifyContent: 'center',
        padding: verticalScale(15),
    },
    picker: {
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
    },
    icDown: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    icDownContainer: {
        position: 'absolute',
        right: verticalScale(15),
    },
    titlePicker: {
        marginTop: verticalScale(30),
        marginBottom: verticalScale(10),
    },
    btnContainer: {
        marginTop: 'auto',
        paddingTop: verticalScale(10),
        borderTopWidth: 1,
        borderTopColor: Colors.border,
        paddingHorizontal: verticalScale(30),
        marginBottom: verticalScale(30),
    },
});
