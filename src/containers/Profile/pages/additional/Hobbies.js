import React, { useState, useEffect } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { UIHeader } from '../../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { useNavigation } from '@react-navigation/native';
import { SCREEN } from '../../../../constant';
import { UIItem } from '../../components';
import { connect } from 'react-redux';

const Hobbies = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();

    const { profileReducer } = props || {};
    const { accountInfo } = profileReducer || {};
    const { resume } = accountInfo || {};
    const { addtionalInfo } = resume || {};
    const { hobbies } = addtionalInfo || {};

    const [data, setData] = useState(hobbies);

    useEffect(() => {
        setData(hobbies);
    }, [hobbies]);

    const moveToAddNew = () => {
        navigation.navigate(SCREEN.UPDATE_HOBBIES, { isAdd: true });
    };

    const moveToEdit = (index) => {
        const item = data[index];
        navigation.navigate(SCREEN.UPDATE_HOBBIES, { isAdd: false, item, index });
    };

    const renderItem = ({ item, index }) => {
        return <UIItem key={`otherSkill${index}`} name={item} onPress={() => moveToEdit(index)} />;
    };

    return (
        <View style={styles.container}>
            <UIHeader
                LeftIcon="back"
                RightIcon="plus"
                style={{ backgroundColor: Colors.white }}
                onPress={() => navigation.goBack()}
                onPressRight={moveToAddNew}
                title={t('profile.hobbies')}
            />
            <View style={styles.contentContainer}>
                <FlatList renderItem={renderItem} extraData={data} data={data} />
            </View>
        </View>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {})(Hobbies);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.gray,
    },
    contentContainer: {},
});
