import Hobbies from './Hobbies';
import UpdateHobbies from './UpdateHobbies';
import SiteCompany from './SiteCompany';
import UpdateSiteCompany from './UpdateSiteCompany';
import SocialLinks from './SocialLinks';
import UpdateSocialLink from './UpdateSocialLink';

export { Hobbies, UpdateHobbies, SiteCompany, UpdateSiteCompany, SocialLinks, UpdateSocialLink };
