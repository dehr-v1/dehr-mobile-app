import React, { useState, useEffect } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { UIHeader } from '../../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { useNavigation } from '@react-navigation/native';
import { SCREEN } from '../../../../constant';
import { UIItem } from '../../components';
import { connect } from 'react-redux';

const SocialLinks = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();

    const { profileReducer } = props || {};
    const { accountInfo } = profileReducer || {};
    const { resume } = accountInfo || {};
    const { addtionalInfo } = resume || {};
    const { socialLinks } = addtionalInfo || {};

    const [data, setData] = useState(socialLinks);

    useEffect(() => {
        setData(socialLinks);
    }, [socialLinks]);

    const moveToAddNew = () => {
        navigation.navigate(SCREEN.UPDATE_SOCIAL_LINKS, { isAdd: true });
    };

    const moveToEdit = (index) => {
        const item = data[index];
        navigation.navigate(SCREEN.UPDATE_SOCIAL_LINKS, { isAdd: false, item, index });
    };

    const renderItem = ({ item, index }) => {
        return <UIItem name={item.type} description={item.value} onPress={() => moveToEdit(index)} />;
    };

    return (
        <View style={styles.container}>
            <UIHeader
                LeftIcon="back"
                RightIcon="plus"
                style={{ backgroundColor: Colors.white }}
                onPress={() => navigation.goBack()}
                onPressRight={moveToAddNew}
                title={t('profile.social_links')}
            />
            <View style={styles.contentContainer}>
                <FlatList renderItem={renderItem} extraData={data} data={data} />
            </View>
        </View>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {})(SocialLinks);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.gray,
    },
    contentContainer: {},
});
