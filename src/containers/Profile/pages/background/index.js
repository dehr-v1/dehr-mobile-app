import Education from './Education';
import UpdateEducation from './UpdateEducation';
import Experience from './Experience';
import UpdateExperience from './UpdateExperience';

export { Education, UpdateEducation, Experience, UpdateExperience };
