import React, { useState, useEffect } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { UIHeader } from '../../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { useNavigation } from '@react-navigation/native';
import { SCREEN } from '../../../../constant';
import { UIItem } from '../../components';
import { connect } from 'react-redux';

const Experience = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();

    const { profileReducer } = props || {};
    const { accountInfo } = profileReducer || {};
    const { resume } = accountInfo || {};
    const { background } = resume || {};
    const { experience } = background || {};
    const [data, setData] = useState(experience);

    useEffect(() => {
        setData(experience);
    }, [experience]);

    const moveToAddNew = () => {
        navigation.navigate(SCREEN.UPDATE_EXPERIENCE, { isAdd: true });
    };

    const moveToEdit = (index) => {
        const item = data[index];
        navigation.navigate(SCREEN.UPDATE_EXPERIENCE, { isAdd: false, item, index });
    };

    const renderItem = ({ item, index }) => {
        return (
            <UIItem
                name={item.title}
                startDate={item.startDate}
                endDate={item.endDate}
                title={item.companyName}
                description={item.description}
                location={item.location}
                media={item.media}
                onPress={() => moveToEdit(index)}
            />
        );
    };

    return (
        <View style={styles.container}>
            <UIHeader
                LeftIcon="back"
                RightIcon="plus"
                style={{ backgroundColor: Colors.white }}
                onPress={() => navigation.goBack()}
                onPressRight={moveToAddNew}
                title={t('profile.experience')}
            />
            <View style={styles.contentContainer}>
                <FlatList renderItem={renderItem} extraData={data} data={data} />
            </View>
        </View>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {})(Experience);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.gray,
    },
    contentContainer: {},
});
