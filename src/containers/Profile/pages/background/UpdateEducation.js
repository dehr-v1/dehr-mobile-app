import React, { useState, useRef } from 'react';
import { View, StyleSheet, Image, TouchableOpacity, TextInput, ScrollView, FlatList, ImageBackground, TouchableWithoutFeedback } from 'react-native';
import { UIHeader, UIModal, UITextInputForm } from '../../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { useNavigation, useRoute } from '@react-navigation/native';
import { verticalScale, moderateScale } from '../../../../utils/ScalingUtils';
import RNPickerSelect from 'react-native-picker-select';
import { UIButton, UIText } from '../../../../components/elements';
import { fontFamily } from '../../../../theme/Fonts';
import { useTheme } from '@/theme';
import moment from 'moment';
import { connect } from 'react-redux';
import { actionUpdateProfile, actionUploadMedia, actionGetAccountInfo } from '../../duck/actions';
import ImagePicker from 'react-native-image-crop-picker';
import UploadPhotoModal from '../../../../components/common/UploadPhotoModal';
import { BASE_IMAGE_URL, MEDIA_TYPE } from '../../../../constant/value.constant';
import DatePicker from 'react-native-date-picker';
import { SCREEN } from '../../../../constant';
import Toast from '../../../../components/elements/Toast';

const UpdateEducation = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { params } = useRoute() || {};
    const { isAdd, item, index, onDeleteEducation } = params || {};
    const { Images } = useTheme();

    const { profileReducer, actionUpdateProfile, actionUploadMedia, actionGetAccountInfo } = props || {};
    const { accountInfo } = profileReducer || {};
    const { resume } = accountInfo || {};
    const { background } = resume || {};
    const { education, experience } = background || {};

    const [school, setSchool] = useState(item?.school || '');
    const [degree, setDegree] = useState(item?.degree || '');
    const [fieldStudy, setFieldStudy] = useState(item?.fieldOfStudy || '');
    const [startDate, setStartDate] = useState(item?.startDate || 0);
    const [endDate, setEndDate] = useState(item?.endDate || 0);
    const [description, setDescription] = useState(item?.description || '');
    const [count, setCount] = useState(item?.description?.length || 0);
    const [dStartDate, setDStartDate] = useState(startDate !== 0 ? moment(new Date(startDate)).format('MMM YYYY') : '');
    const [dEndDate, setDEndDate] = useState(endDate != 0 ? moment(new Date(startDate)).format('MMM YYYY') : '');

    const [showStartDate, setShowStartDate] = useState(false);
    const [showEndDate, setShowEndDate] = useState(false);
    const [listImage, setListImage] = useState(item?.media || []);
    const [linkImageValue, setLinkImageValue] = useState([]);

    let pickerRef = useRef();
    let modalAddImageLink = useRef();

    const onChangeDescription = (text) => {
        setDescription(text);
        setCount(text.length);
    };

    const onAddNew = () => {
        let listMediaDoc = [];
        let listMediaLink = [];
        let listMediaUpload = [];
        listImage.forEach((e) => {
            if (e.type == MEDIA_TYPE.DOCUMENT || e.type == MEDIA_TYPE.LINK) listMediaDoc.push(e);
            else listMediaUpload.push(e.value);
        });
        actionUploadMedia(listMediaUpload, async (data) => {
            let newItem = {
                school,
                degree,
                fieldOfStudy: fieldStudy,
                startDate: new Date(startDate).getTime(),
                endDate: new Date(endDate).getTime(),
                description,
            };
            if (data != null && data.length > 0 && data != undefined) {
                listMediaDoc = [...listMediaDoc, ...data.map((e) => ({ value: e, type: MEDIA_TYPE.DOCUMENT }))];
            }
            newItem = {
                ...newItem,
                media: listMediaDoc,
            };
            if (listMediaDoc.length > 0 || listMediaLink.length > 0) {
                await actionGetAccountInfo();
                navigation.navigate(SCREEN.CREATE_PROFILE, { addEducation: newItem, merge: true });
            } else {
                Toast(t('profile.upload_media_error'), 'error');
            }
        });
    };

    const onClickBtnDelete = () => {
        onDeleteEducation(index);
        navigation.goBack();
    };

    const onChangeDate = (selectedDate) => {
        setStartDate(selectedDate);
        setDStartDate(moment(selectedDate).format('MMM YYYY'));
    };

    const onChangeEndDate = (selectedDate) => {
        setEndDate(selectedDate);
        setDEndDate(moment(selectedDate).format('MMM YYYY'));
    };

    const onPressModalTakePhoto = () => {
        ImagePicker.openCamera({
            multiple: true,
        }).then((images) => {
            const imageNew = {
                value: images,
                type: MEDIA_TYPE.FILE,
            };
            pickerRef.close();
            setListImage([...listImage, imageNew]);
        });
    };

    const onPressModalChooseFrom = () => {
        ImagePicker.openPicker({
            multiple: true,
        }).then((images) => {
            pickerRef.close();
            let arrImage = images.map((e) => {
                return {
                    value: e,
                    type: MEDIA_TYPE.FILE,
                };
            });
            setListImage([...listImage, ...arrImage]);
        });
    };

    const onDeleteImage = (index) => {
        setListImage(listImage.filter((e, i) => i != index));
    };

    const renderImage = ({ item, index }) => {
        const uri =
            item.type == MEDIA_TYPE.LINK ? item.value : item.type == MEDIA_TYPE.DOCUMENT ? BASE_IMAGE_URL + item.value : item.value.path;
        return (
            <ImageBackground style={[styles.image, styles.imgContainer]} imageStyle={styles.image} source={{ uri: uri }}>
                <TouchableOpacity onPress={() => onDeleteImage(index)} style={styles.deleteContainer}>
                    <Image style={styles.icDelete} source={Images.home.ic_delete} />
                </TouchableOpacity>
            </ImageBackground>
        );
    };

    const openModalPickImage = () => {
        pickerRef.open();
    };

    const openModalImageLink = () => {
        modalAddImageLink.open();
    };

    const onChangeValue = async (text) => {
        await setLinkImageValue(text);
    };

    const handelAddLink = async () => {
        try {
            modalAddImageLink.close();
            setListImage([
                ...listImage,
                {
                    value: linkImageValue,
                    type: MEDIA_TYPE.LINK,
                },
            ]);
            setLinkImageValue('');
        } catch (error) { }
    };

    const cancelAddLink = () => {
        modalAddImageLink.close();
        setLinkImageValue('');
    };

    return (
        <View style={styles.container}>
            <UIHeader
                LeftIcon="close"
                onPress={() => navigation.goBack()}
                title={t(isAdd ? 'profile.add_education' : 'profile.edit_education')}
                onPressRight={onAddNew}
                rightTitle={isAdd ? '' : t('profile.save')}
            />
            <ScrollView contentContainerStyle={styles.scrollContentContainer} style={styles.contentContainer}>
                <UITextInputForm
                    placeholder={t('profile.school_name')}
                    title={t('profile.school')}
                    value={school}
                    onChangeText={(text) => setSchool(text)}
                    required
                />
                <UIText style={styles.titlePicker} font="bold" text={t('profile.degree')} />
                <View style={styles.pickerContainer}>
                    <RNPickerSelect
                        textInputProps={styles.picker}
                        onValueChange={(value) => setDegree(value)}
                        style={{
                            inputAndroid: {
                                color: Colors.blueDark,
                                width: '100%',
                                height: verticalScale(50),
                                paddingTop: verticalScale(10),
                                lineHeight: verticalScale(20),
                            },
                        }}
                        useNativeAndroidPickerStyle={false}
                        value={degree}
                        items={[
                            { label: "Bachelor's", value: "Bachelor's" },
                            { label: "Master's", value: "Master's" },
                            { label: 'Doctoral', value: 'Doctoral' },
                        ]}
                    />
                    <View style={styles.icDownContainer}>
                        <Image style={styles.icDown} source={Images.profile.ic_down} />
                    </View>
                </View>
                <UITextInputForm
                    placeholder={t('profile.language')}
                    title={t('profile.field_study')}
                    value={fieldStudy}
                    onChangeText={(text) => setFieldStudy(text)}
                    required
                />
                <View style={styles.dateContainer}>
                    <TouchableOpacity onPress={() => setShowStartDate(true)} style={styles.inputDateContainer}>
                        <UITextInputForm
                            placeholder="May 2021"
                            title={t('profile.start_date')}
                            value={dStartDate}
                            editable={false}
                            disable="none"
                            // onChangeText={text => setStartDate(text)}
                            required
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setShowEndDate(true)} style={styles.inputDateContainer}>
                        <UITextInputForm
                            placeholder="May 2021"
                            title={t('profile.end_date')}
                            value={dEndDate}
                            editable={false}
                            disable="none"
                            // onChangeText={text => setEndDate(text)}
                            required
                        />
                    </TouchableOpacity>
                </View>
                <UIText style={styles.titlePicker} font="bold" text={t('profile.description')} />
                <TextInput
                    maxLength={200}
                    placeholderTextColor={Colors.lightGray}
                    allowFontScaling={false}
                    style={styles.inputDescription}
                    multiline
                    value={description}
                    onChangeText={(text) => onChangeDescription(text)}
                    placeholder={t('profile.enter_description')}
                />
                <View style={styles.countContainer}>
                    <UIText style={styles.txtCount} text={`${count}/200`} />
                </View>
                <UIText style={styles.titlePicker} font="bold" text={t('profile.media')} />
                <View style={styles.dateContainer}>
                    <View style={styles.inputDateContainer}>
                        <UIButton onPress={openModalPickImage} style={[styles.btnMedia, styles.btnBlue]} text={t('profile.upload')} />
                    </View>
                    <View style={styles.inputDateContainer}>
                        <UIButton onPress={openModalImageLink} type="BgWhite" style={styles.btnMedia} text={t('profile.link')} />
                    </View>
                </View>
                <View style={styles.groupImage}>
                    <FlatList horizontal renderItem={renderImage} data={listImage} extraData={listImage} />
                </View>
            </ScrollView>
            <View style={styles.btnContainer}>
                <UIButton
                    type={isAdd ? '' : 'BgWhite'}
                    onPress={isAdd ? onAddNew : onClickBtnDelete}
                    text={t(isAdd ? 'profile.save' : 'profile.delete_education')}
                />
            </View>
            <DatePicker
                modal
                open={showStartDate}
                date={startDate ? new Date(startDate) : new Date()}
                mode="date"
                maximumDate={new Date()}
                onConfirm={(date) => {
                    setShowStartDate(false);
                    onChangeDate(date);
                }}
                onCancel={() => {
                    setShowStartDate(false);
                }}
            />
            <DatePicker
                modal
                open={showEndDate}
                date={endDate ? new Date(endDate) : new Date()}
                mode="date"
                maximumDate={new Date()}
                onConfirm={(date) => {
                    setShowEndDate(false);
                    onChangeEndDate(date);
                }}
                onCancel={() => {
                    setShowEndDate(false);
                }}
            />
            <UIModal ref={(ref) => (pickerRef = ref)}>
                <UploadPhotoModal
                    onPressCancelModal={() => pickerRef.close()}
                    onPressModalTakePhoto={onPressModalTakePhoto}
                    onPressModalChooseFrom={onPressModalChooseFrom}
                />
            </UIModal>
            <UIModal ref={(ref) => (modalAddImageLink = ref)}>
                <TouchableWithoutFeedback>
                    <View style={styles.modalEnterImageLink}>
                        <UIText font="bold" style={styles.txtTitleModal} text={t('profile.enter_image_link')} />
                        <TextInput
                            placeholderTextColor={Colors.lightGray}
                            allowFontScaling={false}
                            style={styles.inputDescription}
                            multiline
                            value={linkImageValue}
                            onChangeText={onChangeValue}
                            placeholder={t('profile.enter_your_image_link')}
                        />
                        <View style={styles.groupButton}>
                            <UIButton
                                onPress={cancelAddLink}
                                style={styles.btnModal}
                                type="BgWhite"
                                text={t('profile.cancel')}
                            />
                            <UIButton
                                disable={linkImageValue === ''}
                                onPress={handelAddLink}
                                style={styles.btnModal}
                                text={t('profile.add')}
                            />
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </UIModal>
        </View>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {
    actionUpdateProfile,
    actionUploadMedia,
    actionGetAccountInfo,
})(UpdateEducation);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    scrollContentContainer: {
        paddingBottom: verticalScale(20),
        flexGrow: 1,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        marginTop: verticalScale(30),
    },
    pickerContainer: {
        height: verticalScale(50),
        width: '100%',
        backgroundColor: Colors.gray,
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(6),
        justifyContent: 'center',
        padding: verticalScale(15),
        marginBottom: verticalScale(15),
    },
    picker: {
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
    },
    icDown: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    icDownContainer: {
        position: 'absolute',
        right: verticalScale(15),
    },
    titlePicker: {
        marginTop: verticalScale(5),
        marginBottom: verticalScale(10),
        fontSize: verticalScale(14),
        color: Colors.blueDark,
    },
    btnContainer: {
        marginTop: 'auto',
        paddingTop: verticalScale(10),
        borderTopWidth: 1,
        borderTopColor: Colors.border,
        paddingHorizontal: verticalScale(30),
    },
    icCheck: {
        width: verticalScale(16),
        height: verticalScale(16),
        tintColor: Colors.blueDark,
    },
    icCheckContainer: {
        width: verticalScale(24),
        height: verticalScale(24),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.gray,
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: verticalScale(6),
    },
    txtCurrentWork: {
        fontSize: verticalScale(16),
        color: Colors.blueDark,
        marginLeft: verticalScale(13),
    },
    currentWorkContainer: {
        flexDirection: 'row',
        marginTop: verticalScale(15),
    },
    dateContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: verticalScale(15),
    },
    inputDateContainer: {
        flex: 0.48,
    },
    inputDescription: {
        width: '100%',
        backgroundColor: Colors.gray,
        height: verticalScale(175),
        borderRadius: verticalScale(6),
        borderWidth: 1,
        borderColor: Colors.border,
        paddingHorizontal: verticalScale(15),
        paddingTop: verticalScale(15),
        paddingBottom: verticalScale(15),
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
        color: Colors.blueDark,
    },
    txtCount: {
        fontSize: verticalScale(14),
        fontFamily: fontFamily.regular,
        color: '#878eae',
        marginTop: verticalScale(10),
    },
    countContainer: {
        width: '100%',
        alignItems: 'flex-end',
    },
    btnMedia: {
        height: verticalScale(42),
    },
    btnBlue: {
        backgroundColor: '#4286cb',
    },
    image: {
        width: verticalScale(60),
        height: verticalScale(60),
    },
    imgContainer: {
        marginTop: verticalScale(20),
        marginRight: verticalScale(20),
    },
    icDelete: {
        width: verticalScale(20),
        height: verticalScale(20),
        borderRadius: verticalScale(10),
    },
    deleteContainer: {
        top: -verticalScale(10),
        right: -verticalScale(10),
        position: 'absolute',
    },
    modalEnterImageLink: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: Colors.white,
        borderTopLeftRadius: verticalScale(20),
        borderTopRightRadius: verticalScale(20),
        justifyContent: 'center',
        padding: verticalScale(30),
        width: '100%',
    },
    groupButton: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
        paddingTop: verticalScale(36),
    },
    btnModal: {
        width: '45%',
        height: verticalScale(44),
    },
    txtTitleModal: {
        fontSize: moderateScale(20),
        lineHeight: moderateScale(26),
        textAlign: 'center',
        marginBottom: verticalScale(15),
    },
    groupImage: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
    },
});
