import React, { useState, useRef } from 'react';
import { View, StyleSheet, Image, TouchableOpacity, TextInput, ScrollView, FlatList, ImageBackground } from 'react-native';
import { UIHeader, UIModal, UITextInputForm } from '../../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { useNavigation, useRoute } from '@react-navigation/native';
import { verticalScale } from '../../../../utils/ScalingUtils';
import RNPickerSelect from 'react-native-picker-select';
import { UIButton, UIText } from '../../../../components/elements';
import { fontFamily } from '../../../../theme/Fonts';
import { useTheme } from '@/theme';
import { SCREEN } from '../../../../constant';
import moment from 'moment';
import { connect } from 'react-redux';
import { actionUpdateProfile, actionUploadMedia } from '../../duck/actions';
import UploadPhotoModal from '../../../../components/common/UploadPhotoModal';
import ImagePicker from 'react-native-image-crop-picker';
import { BASE_IMAGE_URL } from '../../../../constant/value.constant';
import DatePicker from 'react-native-date-picker';

const UpdateCurrentPosition = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { params } = useRoute() || {};
    const { isAdd, item, index } = params || {};
    const { Images } = useTheme();

    const { actionUpdateProfile, profileReducer, actionUploadMedia } = props || {};
    const { accountInfo } = profileReducer || {};
    const { resume } = accountInfo || {};
    const { intro } = resume || {};
    const { current_position } = intro || [];

    const [title, setTitle] = useState(item?.title || '');
    const [employmentType, setemploymentType] = useState(item?.employmentType || '');
    const [companyName, setCompanyName] = useState(item?.company_name || '');
    const [location, setLocation] = useState(item?.location || '');
    const [currentWork, setCurrentWork] = useState(item?.is_current_work || false);
    const [startDate, setStartDate] = useState(item?.start_date || 0);
    const [endDate, setEndDate] = useState(item?.endDate || 0);
    const [description, setDescription] = useState(item?.description || '');
    const [count, setCount] = useState(item?.description?.length || 0);
    const [dStartDate, setDStartDate] = useState(startDate !== 0 ? moment(new Date(startDate)).format('MMM YYYY') : '');
    const [dEndDate, setDEndDate] = useState(currentWork ? 'Present' : endDate != 0 ? moment(new Date(startDate)).format('MMM YYYY') : '');

    const [showStartDate, setShowStartDate] = useState(false);
    const [showEndDate, setShowEndDate] = useState(false);
    const [listImage, setListImage] = useState(item?.media || []);

    let pickerRef = useRef();

    const onChangeDescription = (text) => {
        setDescription(text);
        setCount(text.length);
    };

    const onChangeCurrentWork = () => {
        if (!currentWork) {
            setDEndDate('Present');
            setEndDate(0);
        } else {
            setDEndDate('');
        }
        setCurrentWork(!currentWork);
    };

    const onPressLocation = () => {
        navigation.navigate(SCREEN.SEARCH_SCREEN, {
            title: 'Location',
            data: null,
            placeholder: 'Location',
            onChoose: (item) => {
                setLocation(item.name);
            },
        });
    };

    const onAddNew = () => {
        const dataMedia = [];
        const dataUpload = [];
        listImage.forEach((e, i) => {
            if (e.path) {
                dataUpload.push(e);
            } else {
                dataMedia.push(e);
            }
        });
        actionUploadMedia(dataUpload, (data) => {
            const newItem = {
                title,
                employmentType: employmentType,
                company_name: companyName,
                location,
                startDate: new Date(startDate).getTime(),
                endDate: currentWork ? 0 : new Date(endDate).getTime(),
                desc: description,
                is_current_work: currentWork,
                media: [...dataMedia, ...data],
            };
            const newData = [...current_position, newItem];
            const body = {
                ...resume,
                intro: {
                    ...intro,
                    current_position: newData,
                },
            };
            actionUpdateProfile({ data: body }, () => {
                navigation.goBack();
            });
        });
    };

    const onSave = () => {
        const dataMedia = [];
        const dataUpload = [];
        listImage.forEach((e, i) => {
            if (e.path) {
                dataUpload.push(e);
            } else {
                dataMedia.push(e);
            }
        });
        actionUploadMedia(dataUpload, (data) => {
            const newItem = {
                title,
                employmentType: employmentType,
                company_name: companyName,
                location,
                startDate: new Date(startDate).getTime(),
                endDate: currentWork ? 0 : new Date(endDate).getTime(),
                desc: description,
                is_current_work: currentWork,
                media: [...dataMedia, ...data],
            };
            const newData = current_position.map((e, i) => {
                return i != index ? e : newItem;
            });
            const body = {
                ...resume,
                intro: {
                    ...intro,
                    current_position: newData,
                },
            };
            actionUpdateProfile({ data: body }, () => {
                navigation.goBack();
            });
        });
    };

    const onClickBtnDelete = () => {
        const newData = current_position.filter((e, i) => i != index);
        const body = {
            ...resume,
            intro: {
                ...intro,
                current_position: newData,
            },
        };
        actionUpdateProfile({ data: body }, () => {
            navigation.goBack();
        });
    };

    const onChangeDate = (event, selectedDate) => {
        setShowStartDate(Platform.OS === 'ios');
        setStartDate(selectedDate);
        setDStartDate(moment(selectedDate).format('MMM YYYY'));
    };

    const onChangeEndDate = (event, selectedDate) => {
        setShowEndDate(Platform.OS === 'ios');
        setEndDate(selectedDate);
        setDEndDate(moment(selectedDate).format('MMM YYYY'));
    };

    const onPressUpload = () => {
        pickerRef.open();
    };

    const onPressModalTakePhoto = () => {
        ImagePicker.openCamera({
            multiple: true,
        }).then((images) => {
            const imageNew = [images];
            pickerRef.close();
            setListImage([...listImage, ...imageNew]);
        });
    };

    const onPressModalChooseFrom = () => {
        ImagePicker.openPicker({
            multiple: true,
        }).then((images) => {
            pickerRef.close();
            setListImage([...listImage, ...images]);
        });
    };

    const onDeleteImage = (index) => {
        setListImage(listImage.filter((e, i) => i != index));
    };

    const renderImage = ({ item, index }) => {
        return (
            <ImageBackground
                style={[styles.image, styles.imgContainer]}
                imageStyle={styles.image}
                source={{ uri: item?.path ? item.path : BASE_IMAGE_URL + item }}
            >
                <TouchableOpacity onPress={() => onDeleteImage(index)} style={styles.deleteContainer}>
                    <Image style={styles.icDelete} source={Images.home.ic_delete} />
                </TouchableOpacity>
            </ImageBackground>
        );
    };

    return (
        <View style={styles.container}>
            <UIHeader
                LeftIcon="close"
                onPress={() => navigation.goBack()}
                title={t(isAdd ? 'profile.add_current_position' : 'profile.edit_current_position')}
                onPressRight={onSave}
                rightTitle={isAdd ? '' : t('profile.save')}
            />
            <ScrollView contentContainerStyle={styles.scrollContentContainer} style={styles.contentContainer}>
                <UITextInputForm
                    placeholder={t('profile.enter_title')}
                    title={t('profile.title')}
                    value={title}
                    onChangeText={(text) => setTitle(text)}
                    required
                />
                <UIText style={styles.titlePicker} font="bold" text={t('profile.employment_type')} />
                <View style={styles.pickerContainer}>
                    <RNPickerSelect
                        textInputProps={styles.picker}
                        onValueChange={(value) => setemploymentType(value)}
                        style={{
                            inputAndroid: {
                                color: Colors.blueDark,
                                width: '100%',
                                height: verticalScale(50),
                                paddingTop: verticalScale(10),
                                lineHeight: verticalScale(20),
                            },
                        }}
                        useNativeAndroidPickerStyle
                        value={employmentType}
                        items={[
                            { label: 'Fulltime', value: 'Fulltime' },
                            { label: 'Partime', value: 'Partime' },
                            { label: 'Intern', value: 'Intern' },
                        ]}
                    />
                    <View style={styles.icDownContainer}>
                        <Image style={styles.icDown} source={Images.profile.ic_down} />
                    </View>
                </View>
                <UITextInputForm
                    placeholder={t('profile.enter_company_name')}
                    title={t('profile.company_name')}
                    value={companyName}
                    onChangeText={(text) => setCompanyName(text)}
                    required
                />
                <UIText style={styles.titlePicker} font="bold" text={t('profile.location')} />
                <TouchableOpacity onPress={onPressLocation}>
                    <View style={styles.pickerContainer}>
                        <UIText text={location} />
                        <View style={styles.icDownContainer}>
                            <Image style={styles.icDown} source={Images.profile.ic_down} />
                        </View>
                    </View>
                </TouchableOpacity>
                <View style={styles.currentWorkContainer}>
                    <TouchableOpacity style={styles.icCheckContainer} onPress={onChangeCurrentWork}>
                        {currentWork && <Image style={styles.icCheck} source={Images.profile.ic_check} />}
                    </TouchableOpacity>
                    <UIText style={styles.txtCurrentWork} text={t('profile.current_work')} />
                </View>
                <View style={styles.dateContainer}>
                    <TouchableOpacity onPress={() => setShowStartDate(true)} style={styles.inputDateContainer}>
                        <UITextInputForm
                            placeholder="May 2021"
                            title={t('profile.start_date')}
                            value={dStartDate}
                            editable={false}
                            disable="none"
                            // onChangeText={text => setStartDate(text)}
                            required
                        />
                    </TouchableOpacity>
                    <TouchableOpacity disabled={currentWork} onPress={() => setShowEndDate(true)} style={styles.inputDateContainer}>
                        <UITextInputForm
                            placeholder="May 2021"
                            title={t('profile.end_date')}
                            value={dEndDate}
                            editable={false}
                            disable="none"
                            // onChangeText={text => setEndDate(text)}
                            required
                        />
                    </TouchableOpacity>
                </View>
                <UIText style={styles.titlePicker} font="bold" text={t('profile.description')} />
                <TextInput
                    maxLength={200}
                    allowFontScaling={false}
                    placeholderTextColor={Colors.lightGray}
                    style={styles.inputDescription}
                    multiline
                    value={description}
                    onChangeText={(text) => onChangeDescription(text)}
                    placeholder={t('profile.enter_description')}
                />
                <View style={styles.countContainer}>
                    <UIText style={styles.txtCount} text={`${count}/200`} />
                </View>
                <UIText style={styles.titlePicker} font="bold" text={t('profile.media')} />
                <View style={styles.dateContainer}>
                    <View style={styles.inputDateContainer}>
                        <UIButton onPress={onPressUpload} style={[styles.btnMedia, styles.btnBlue]} text={t('profile.upload')} />
                    </View>
                    <View style={styles.inputDateContainer}>
                        <UIButton type="BgWhite" disable style={styles.btnMedia} text={t('profile.link')} />
                    </View>
                </View>
                <FlatList horizontal renderItem={renderImage} data={listImage} extraData={listImage} />
            </ScrollView>
            <View style={styles.btnContainer}>
                <UIButton
                    type={isAdd ? '' : 'BgWhite'}
                    onPress={isAdd ? onAddNew : onClickBtnDelete}
                    text={t(isAdd ? 'profile.save' : 'profile.delete_current_position')}
                />
            </View>
            <DatePicker
                modal
                open={showStartDate}
                date={startDate ? new Date(startDate) : new Date()}
                mode="date"
                maximumDate={new Date()}
                onDateChange={onChangeDate}
                onConfirm={(date) => {
                    setShowStartDate(false);
                    onChangeDate(date);
                }}
                onCancel={() => {
                    setShowStartDate(false);
                }}
            />
            <DatePicker
                modal
                open={showEndDate}
                date={currentWork ? new Date() : endDate ? new Date(endDate) : new Date()}
                mode="date"
                maximumDate={new Date()}
                onDateChange={onChangeDate}
                onConfirm={(date) => {
                    setShowEndDate(false);
                    onChangeEndDate(date);
                }}
                onCancel={() => {
                    setShowEndDate(false);
                }}
            />
            <UIModal ref={(ref) => (pickerRef = ref)}>
                <UploadPhotoModal
                    onPressCancelModal={() => pickerRef.close()}
                    onPressModalTakePhoto={onPressModalTakePhoto}
                    onPressModalChooseFrom={onPressModalChooseFrom}
                />
            </UIModal>
        </View>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {
    actionUpdateProfile,
    actionUploadMedia,
})(UpdateCurrentPosition);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    scrollContentContainer: {
        paddingBottom: verticalScale(20),
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        marginTop: verticalScale(30),
    },
    pickerContainer: {
        height: verticalScale(50),
        width: '100%',
        backgroundColor: Colors.gray,
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: verticalScale(6),
        justifyContent: 'center',
        paddingHorizontal: verticalScale(15),
        marginBottom: verticalScale(15),
    },
    picker: {
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
    },
    icDown: {
        width: verticalScale(20),
        height: verticalScale(20),
    },
    icDownContainer: {
        position: 'absolute',
        right: verticalScale(15),
    },
    titlePicker: {
        marginTop: verticalScale(5),
        marginBottom: verticalScale(10),
        fontSize: verticalScale(14),
        color: Colors.blueDark,
    },
    btnContainer: {
        marginTop: 'auto',
        paddingTop: verticalScale(10),
        borderTopWidth: 1,
        borderTopColor: Colors.border,
        paddingHorizontal: verticalScale(30),
    },
    icCheck: {
        width: verticalScale(16),
        height: verticalScale(16),
        tintColor: Colors.blueDark,
    },
    icCheckContainer: {
        width: verticalScale(24),
        height: verticalScale(24),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.gray,
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: verticalScale(6),
    },
    txtCurrentWork: {
        fontSize: verticalScale(16),
        color: Colors.blueDark,
        marginLeft: verticalScale(13),
    },
    currentWorkContainer: {
        flexDirection: 'row',
        marginTop: verticalScale(5),
    },
    dateContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: verticalScale(15),
    },
    inputDateContainer: {
        flex: 0.48,
    },
    inputDescription: {
        width: '100%',
        backgroundColor: Colors.gray,
        height: verticalScale(175),
        borderRadius: verticalScale(6),
        borderWidth: 1,
        borderColor: Colors.border,
        paddingHorizontal: verticalScale(15),
        paddingTop: verticalScale(15),
        paddingBottom: verticalScale(15),
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
        color: Colors.blueDark,
    },
    txtCount: {
        fontSize: verticalScale(14),
        fontFamily: fontFamily.regular,
        color: '#878eae',
        marginTop: verticalScale(10),
    },
    countContainer: {
        width: '100%',
        alignItems: 'flex-end',
    },
    btnMedia: {
        height: verticalScale(42),
    },
    btnBlue: {
        backgroundColor: '#4286cb',
    },
    image: {
        width: verticalScale(60),
        height: verticalScale(60),
    },
    imgContainer: {
        marginTop: verticalScale(20),
        marginRight: verticalScale(20),
    },
    icDelete: {
        width: verticalScale(20),
        height: verticalScale(20),
        borderRadius: verticalScale(10),
    },
    deleteContainer: {
        top: -verticalScale(10),
        right: -verticalScale(10),
        position: 'absolute',
    },
});
