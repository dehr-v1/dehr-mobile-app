import React, { useState, useEffect } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { UIHeader } from '../../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { useNavigation } from '@react-navigation/native';
import { SCREEN } from '../../../../constant';
import { UISwiper } from '../../components';
import { connect } from 'react-redux';
import { actionUpdateProfile } from '../../duck/actions';

const IndustrialKnowledge = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { profileReducer, actionUpdateProfile } = props || {};
    const { accountInfo } = profileReducer || {};
    const { resume } = accountInfo || {};
    const { intro } = resume || {};
    const { industrial_knowledge } = intro || {};
    const [data, setData] = useState(industrial_knowledge);

    useEffect(() => {
        setData(industrial_knowledge);
    }, [industrial_knowledge]);

    const onCreate = (item) => {
        const body = {
            ...resume,
            intro: {
                ...intro,
                industrial_knowledge: [...data, item.name],
            },
        };
        actionUpdateProfile({ data: body }, () => {});
    };

    const onUpdate = (item, index) => {
        const newData = data.map((e, i) => {
            if (i != index) {
                return e;
            }
            return item.name;
        });
        const body = {
            ...resume,
            intro: {
                ...intro,
                industrial_knowledge: newData,
            },
        };
        actionUpdateProfile({ data: body }, () => {});
    };

    const onDelete = (index) => {
        const newData = data.filter((e, i) => i != index);
        const body = {
            ...resume,
            intro: {
                ...intro,
                industrial_knowledge: newData,
            },
        };
        actionUpdateProfile({ data: body }, () => {});
    };

    const moveToAddNew = () => {
        navigation.navigate(SCREEN.SEARCH_SCREEN, {
            title: t('profile.edit_industrial_knowledge'),
            data: [
                { name: 'Developer Design' },
                { name: 'Officer Design' },
                { name: 'UI Design' },
                { name: 'UX Design' },
                { name: 'Game Design' },
                { name: 'Backend Developer' },
            ],
            placeholder: t('profile.industry_ex'),
            onChoose: (item) => {
                onCreate(item);
            },
        });
    };

    const moveToEdit = (index) => {
        navigation.navigate(SCREEN.SEARCH_SCREEN, {
            title: t('profile.edit_industrial_knowledge'),
            data: [{ name: 'Developer Design' }, { name: 'Officer Design' }, { name: 'UI Design' }, { name: 'UX Design' }],
            placeholder: t('profile.industry_ex'),
            onChoose: (item) => {
                onUpdate(item, index);
            },
        });
    };

    const renderItem = ({ item, index }) => {
        return <UISwiper onDelete={() => onDelete(index)} onEdit={() => moveToEdit(index)} name={item?.name || item} />;
    };

    return (
        <View style={styles.container}>
            <UIHeader
                LeftIcon="back"
                RightIcon="plus"
                style={{ backgroundColor: Colors.white }}
                onPress={() => navigation.goBack()}
                onPressRight={moveToAddNew}
                title={t('profile.industrial_knowledge')}
            />
            <View style={styles.contentContainer}>
                <FlatList renderItem={renderItem} extraData={data} data={data} />
            </View>
        </View>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {
    actionUpdateProfile,
})(IndustrialKnowledge);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.gray,
    },
    contentContainer: {},
});
