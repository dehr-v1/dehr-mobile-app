import React, { useState, useEffect } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { UIHeader } from '../../../../components/common';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../../theme/Variables';
import { useNavigation } from '@react-navigation/native';
import { SCREEN } from '../../../../constant';
import { UIItem } from '../../components';
import { connect } from 'react-redux';

const CurrentPosition = (props) => {
    const { t } = useTranslation();
    const navigation = useNavigation();

    const { profileReducer } = props || {};
    const { accountInfo } = profileReducer || {};
    const { resume } = accountInfo || {};
    const { intro } = resume || {};
    const { current_position } = intro || [];
    const [data, setData] = useState(current_position);

    useEffect(() => {
        setData(current_position);
    }, [current_position]);

    const moveToAddNew = () => {
        navigation.navigate(SCREEN.UPDATE_CURRENT_POSITION, { isAdd: true });
    };

    const moveToEdit = (index) => {
        const item = data[index];
        navigation.navigate(SCREEN.UPDATE_CURRENT_POSITION, { isAdd: false, item, index });
    };

    const renderItem = ({ item, index }) => {
        return (
            <UIItem
                name={item.title}
                startDate={item.startDate}
                endDate={item.endDate}
                title={item.company_name}
                description={item.description}
                location={item.location}
                media={item.media}
                onPress={() => moveToEdit(index)}
            />
        );
    };

    return (
        <View style={styles.container}>
            <UIHeader
                LeftIcon="back"
                RightIcon="plus"
                style={{ backgroundColor: Colors.white }}
                onPress={() => navigation.goBack()}
                onPressRight={moveToAddNew}
                title={t('profile.experience')}
            />
            <View style={styles.contentContainer}>
                <FlatList renderItem={renderItem} extraData={data} data={data} />
            </View>
        </View>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {})(CurrentPosition);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.gray,
    },
    contentContainer: {},
});
