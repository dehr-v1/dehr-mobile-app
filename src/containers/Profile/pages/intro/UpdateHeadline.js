import React, { useState } from 'react';
import { View, StyleSheet, TextInput, ScrollView } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import { Colors } from '../../../../theme/Variables';
import { UIButton, UIText } from '../../../../components/elements';
import { verticalScale } from '../../../../utils/ScalingUtils';
import { fontFamily } from '../../../../theme/Fonts';
import { UIHeader } from '../../../../components/common';
import { connect } from 'react-redux';
import { actionUpdateProfile } from '../../duck/actions';

const UpdateHeadline = (props) => {
    const { actionUpdateProfile, profileReducer } = props || {};
    const { accountInfo } = profileReducer || {};
    const { resume } = accountInfo || {};
    const { intro } = resume || {};
    const navigation = useNavigation();
    const { t } = useTranslation();
    const [count, setCount] = useState(0);
    const [headline, setHeadline] = useState((intro && intro.headline) || '');

    const onChangeHeadline = (text) => {
        setHeadline(text);
        setCount(text.length);
    };

    const onSave = () => {
        const data = {
            intro: {
                headline,
            },
        };
        actionUpdateProfile({ data }, () => {
            navigation.goBack();
        });
    };

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={styles.container}>
                <UIHeader onPress={() => navigation.goBack()} LeftIcon="close" title={t('profile.headline')} />
                <View style={styles.contentContainer}>
                    <TextInput
                        maxLength={200}
                        allowFontScaling={false}
                        placeholderTextColor={Colors.lightGray}
                        style={styles.inputHeadline}
                        multiline
                        value={headline}
                        onChangeText={(text) => onChangeHeadline(text)}
                        placeholder={t('profile.enter_headline')}
                    />
                    <UIText style={styles.txtCount} text={`${count}/200`} />
                </View>
                <View style={styles.btnContainer}>
                    <UIButton onPress={onSave} text={t('profile.save')} />
                </View>
            </View>
        </ScrollView>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {
    actionUpdateProfile,
})(UpdateHeadline);

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        flex: 1,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        alignItems: 'flex-end',
        flex: 1,
    },
    inputHeadline: {
        width: '100%',
        backgroundColor: Colors.gray,
        height: verticalScale(175),
        borderRadius: verticalScale(6),
        borderWidth: 1,
        borderColor: Colors.border,
        marginTop: verticalScale(25),
        paddingHorizontal: verticalScale(15),
        paddingTop: verticalScale(15),
        paddingBottom: verticalScale(15),
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
        color: Colors.blueDark,
    },
    txtCount: {
        fontSize: verticalScale(14),
        fontFamily: fontFamily.regular,
        color: '#878eae',
        marginTop: verticalScale(10),
    },
    btnContainer: {
        marginTop: 'auto',
        paddingTop: verticalScale(10),
        borderTopWidth: 1,
        borderTopColor: Colors.border,
        paddingHorizontal: verticalScale(30),
    },
});
