import React, { useState, useEffect, useRef } from 'react';
import { View, StyleSheet, Image, TextInput, TouchableOpacity, TouchableWithoutFeedback, Dimensions } from 'react-native';
import { Colors } from '../../../../theme/Variables';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@/theme';
import { UIHeader, UIModal } from '../../../../components/common';
import { useNavigation, useRoute } from '@react-navigation/native';
import { UIButton, UIText } from '../../../../components/elements';
import { connect } from 'react-redux';
import { moderateScale, verticalScale } from '../../../../utils/ScalingUtils';
import { actionUpdateProfile } from '../../duck/actions';
import { actionBalanceWallet } from '../../../Wallet/duck/actions';
import { actionSetOption } from '../../duck/actions';
import { getSeedPhase, getCurrentUser, setCurrentUser } from '../../../../services/persistence/store.config';
import * as KardiaClient from 'kardia-js-sdk';
import Web3 from 'web3';
import { EXCHANGE_DATA_CONTRACT_ADDRESS, RPC_ENDPOINT } from '../../../../constant/value.constant';
import ExchangeData from '../../../../abis/ExchangeData.json';
import RadioButton from '../../../../components/common/RadioButton';
import Toast from '../../../../components/elements/Toast';
import CustomAlert from '../../../../components/Alert';

const EXCHANGE_OPTIONS = {
    MIDDLEMAN: 'Middleman',
    DIRECT: 'Direct',
};
const web3 = new Web3();
const kardiaClient = new KardiaClient.default({ endpoint: RPC_ENDPOINT });
const transactionModule = kardiaClient.transaction;
const contractInstance = kardiaClient.contract;

const { width } = Dimensions.get('window');

const UpdateContactPrice = (props) => {
    const { t } = useTranslation();
    const { Images } = useTheme();
    const navigation = useNavigation();
    const { params } = useRoute() || {};
    const { profileReducer, actionUpdateProfile, actionBalanceWallet, actionSetOption } = props;
    const { accountInfo } = profileReducer || {};
    const { contact_price_obj, contact } = accountInfo || {};

    const { exchangeOption } = contact || {};
    // Phone Verify
    const [showPopupVerify, setShowPopupVerify] = useState(false);
    const [phoneVerify, setPhoneVerify] = useState('');
    const [userInfo, setUserInfo] = useState({});
    const { oldPrice } = params || 0;
    const [newPrice, setNewPirce] = useState();
    const [timeProcessing, setProcessing] = useState();
    const [notiOption, setNotiOption] = useState(0);
    const [loading, isLoading] = useState(false);

    let modalRef = useRef();
    let expireModalRef = useRef();

    useEffect(async () => {
        const user = JSON.parse(await getCurrentUser());
        setUserInfo(user);
    }, []);

    const msToTime = (duration) => {
        let seconds = duration / 1000;
        const hours = parseInt(seconds / 3600); // 3,600 seconds in 1 hour
        seconds = seconds % 3600; // seconds remaining after extracting hours
        const minutes = parseInt(seconds / 60); // 60 seconds in 1 minute
        seconds = parseInt(seconds % 60);
        return hours + 'h:' + minutes + 'm:' + seconds + 's';
    };

    const updateContactPrice = async (modalRef) => {
        if (
            new Date().getTime() - parseInt(contact_price_obj && contact_price_obj.length > 0 ? contact_price_obj[0].created_at : 0) <=
            259200000
        ) {
            // 72 hours
            const time =
                new Date().getTime() -
                (parseInt(contact_price_obj && contact_price_obj.length > 0 ? contact_price_obj[0].created_at : 0) || 0);
            setProcessing(msToTime(259200000 - time));
            expireModalRef.open();
        } else {
            if (newPrice && newPrice > 0) {
                isLoading(true);
                const price = parseInt(newPrice);
                const getWallet = await getSeedPhase();
                const wallet = JSON.parse(getWallet);
                const privateKey = wallet?.privateKey;
                const address = wallet?.address;
                if (address) {
                    if (privateKey) {
                        contractInstance.updateAbi(ExchangeData.abi);
                        contractInstance.updateByteCode(ExchangeData.bytecode);
                        const newPriceBN = web3.utils.toBN(web3.utils.toWei(newPrice, 'Ether'));
                        const invocation = contractInstance.invokeContract('updatePrice', [newPriceBN]);
                        const txHash = await invocation.send(privateKey, EXCHANGE_DATA_CONTRACT_ADDRESS);

                        /**
                         * waiting for transaction accept to block
                         * time may later 20(s)
                         * */
                        const interval = setInterval(async function () {
                            const receipt = await transactionModule.getTransactionReceipt(txHash);
                            if (receipt) {
                                clearInterval(interval);
                                /**
                                 * Transaction status. 1 if transaction is executed successfully, otherwise 0.
                                 * */
                                if (receipt.status === 1) {
                                    isLoading(false);
                                    Toast('SUCCESS!');
                                    closeModal(modalRef);
                                } else {
                                    /**
                                     * => Lỗi
                                     * check lỗi ở API sau
                                     * GET: https://backend-dev.kardiachain.io/api/v1/txs/{txHash}
                                     * */
                                    isLoading(false);
                                    Toast('ERROR!', 'error');
                                }
                            }
                        }, 1000);
                    }
                } else {
                    Toast(t('wallet.setup_wallet'), 'error');
                    isLoading(false);
                    closeModal(modalRef);
                }
            }
        }
    };

    const closeModal = (modalRef) => {
        if (modalRef != null) {
            modalRef.close();
        }
        navigation.navigate('Profile', { newPrice: newPrice });
    };

    const openModalConfirm = (modalRef) => {
        if (exchangeOption === null) {
            modalRef.open();
        } else {
            updateContactPrice();
        }
    };

    const inputData = async (modalRef) => {
        const user = JSON.parse(await getCurrentUser());
        const newUser = {
            ...user,
            phoneNumber: phoneVerify,
        };
        setUserInfo(newUser);
        setCurrentUser(JSON.stringify(newUser));
        setShowPopupVerify(false);
        onConfirmModal(newUser, modalRef);
    };

    const alertVerify = (modalRef) => (
        <CustomAlert
            isEdittext={true}
            displayTitle={t('login.verify_account')}
            displayMsg={t('login.confirm_email')}
            displayName={(userInfo && userInfo.username) ?? ''}
            visibility={showPopupVerify}
            dismissAlert={setShowPopupVerify}
            resultPhone={setPhoneVerify}
            displayOk={t('login.verify_now')}
            onPressOK={() => inputData(modalRef)}
        />
    );

    const onConfirmModal = (userInfo, modalRef) => {
        if (!userInfo.phoneNumber || !userInfo.email) {
            setShowPopupVerify(true);
        } else {
            const data = {
                exchangeOption: notiOption === 0 ? EXCHANGE_OPTIONS.DIRECT : EXCHANGE_OPTIONS.MIDDLEMAN,
                info: {
                    email: userInfo.email || '',
                    phoneNumber: userInfo.phoneNumber || '',
                },
            };
            actionSetOption(data, () => {
                modalRef.close();
            });
        }
    };

    return (
        <View style={styles.container}>
            {alertVerify(modalRef)}
            <UIHeader LeftIcon="back" onPress={() => navigation.goBack()} title={t('profile.contact_price')} />
            <View style={styles.contentContainer}>
                <View style={[styles.row, styles.oldPriceContainer]}>
                    <UIText style={styles.txtOldPrice} text={t('profile.old_price')} />
                    <View style={styles.row}>
                        <UIText font="bold" style={styles.txtPrice} text={oldPrice || 0} />
                        <Image style={styles.icLogo} source={Images.wallet.ic_logo} />
                    </View>
                </View>
                <View style={styles.priceContainer}>
                    <UIText style={styles.txtPriceMoney} text="$ 126" />
                </View>
                <View style={[styles.row, styles.oldPriceContainer, { borderWidth: 1, borderColor: Colors.border }]}>
                    <UIText style={styles.txtOldPrice} text={t('profile.new_price')} />
                    <View style={styles.row}>
                        <TextInput
                            onChangeText={(value) => setNewPirce(value)}
                            keyboardType="decimal-pad"
                            maxLength={7}
                            placeholder="0.00"
                            placeholderTextColor={Colors.placeholder}
                            style={styles.inputNewPrice}
                        />
                        <Image style={styles.icLogo} source={Images.wallet.ic_logo} />
                    </View>
                </View>
                <View style={[styles.row, { justifyContent: 'space-between' }]}>
                    <View style={styles.row}>
                        <UIText style={styles.txtInfo} text={`${t('profile.scav')}:`} />
                        <UIText style={styles.txtMoneySmall} text="121.23" />
                        <Image style={styles.icLogoSmal} source={Images.wallet.ic_logo} />
                    </View>
                    <UIText style={styles.txtPriceMoney} text="$ 178" />
                </View>
                <View style={styles.row}>
                    <UIText style={styles.txtInfo} text={`${t('profile.successful_rate')}: `} />
                    <UIText style={styles.txtRate} text="2-10%" />
                </View>
                <View style={styles.row}>
                    <UIText style={styles.txtInfo} text={`${t('profile.cont')}: `} />
                    <UIText style={styles.txtRate} text="14dp" />
                </View>
                <UIButton
                    isLoading={loading}
                    onPress={() => openModalConfirm(modalRef)}
                    style={{ marginTop: verticalScale(50) }}
                    text={t('wallet.confirm')}
                />
                <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginTop: verticalScale(32) }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <UIText font="black" style={styles.txtCancel} text={t('profile.cancel')} />
                    </TouchableOpacity>
                </View>
            </View>
            <UIModal ref={(ref) => (modalRef = ref)}>
                <>
                    <TouchableWithoutFeedback>
                        <View style={styles.modalContainer}>
                            <UIText font="bold" style={styles.txtConfirmInfo} text={t('profile.confirm_info')} />
                            <UIText style={styles.txtDesc} text={t('profile.confirm_info_desc')} />
                            <View style={[styles.row, { alignItems: 'flex-start', marginBottom: verticalScale(10) }]}>
                                <RadioButton isChoose={notiOption == 0} onPress={() => setNotiOption(0)} />
                                <UIText style={styles.txtNotiOption} text={t('profile.noti_option_1')} />
                            </View>
                            <View style={[styles.row, { alignItems: 'flex-start' }]}>
                                <RadioButton isChoose={notiOption == 1} onPress={() => setNotiOption(1)} />
                                <UIText style={styles.txtNotiOption} text={t('profile.noti_option_2')} />
                            </View>
                            <View style={[styles.row, { justifyContent: 'space-between', marginTop: verticalScale(20) }]}>
                                <UIButton
                                    onPress={() => closeModal(modalRef)}
                                    style={{ flex: 0.47 }}
                                    type="BgWhite"
                                    text={t('profile.cancel')}
                                />
                                <UIButton
                                    onPress={() => onConfirmModal(userInfo, modalRef)}
                                    style={{ flex: 0.47 }}
                                    text={t('wallet.confirm')}
                                />
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </>
            </UIModal>
            <UIModal ref={(ref) => (expireModalRef = ref)}>
                <TouchableWithoutFeedback>
                    <View style={styles.modalContainer}>
                        <UIText font="bold" style={styles.txtConfirmInfo} text={t('home.notification')} />
                        <UIText style={styles.txtDesc} text={t('wallet.expire_contact_price', { time: timeProcessing })} />
                        <UIButton
                            onPress={() => expireModalRef.close()}
                            style={{ marginTop: verticalScale(30) }}
                            text={t('wallet.confirm')}
                        />
                    </View>
                </TouchableWithoutFeedback>
            </UIModal>
        </View>
    );
};

const mapStateToProps = (state) => ({
    walletReducer: state.walletReducer,
    profileReducer: state.profileReducer,
});
export default connect(mapStateToProps, {
    actionUpdateProfile,
    actionBalanceWallet,
    actionSetOption,
})(UpdateContactPrice);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentContainer: {
        paddingHorizontal: verticalScale(30),
        flex: 1,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    icLogo: {
        width: verticalScale(45.8),
        height: verticalScale(15),
        tintColor: Colors.blueDark,
    },
    oldPriceContainer: {
        backgroundColor: Colors.gray,
        justifyContent: 'space-between',
        padding: verticalScale(20),
        borderRadius: verticalScale(6),
    },
    txtPrice: {
        fontSize: moderateScale(20),
        color: Colors.title,
        marginRight: verticalScale(5),
    },
    txtOldPrice: {
        fontSize: moderateScale(18),
        color: Colors.title,
        textTransform: 'uppercase',
    },
    priceContainer: {
        width: '100%',
        alignItems: 'flex-end',
        marginVertical: verticalScale(5),
    },
    txtPriceMoney: {
        fontSize: moderateScale(12),
        color: Colors.title,
    },
    inputNewPrice: {
        fontSize: moderateScale(20),
        color: Colors.green,
        marginRight: verticalScale(5),
        width: verticalScale(100),
        textAlign: 'right',
    },
    icLogoSmal: {
        width: verticalScale(29.8),
        height: verticalScale(9.8),
        tintColor: Colors.blueDark,
    },
    txtInfo: {
        fontSize: moderateScale(12),
        color: Colors.lightGray,
    },
    txtMoneySmall: {
        fontSize: moderateScale(12),
        color: Colors.green,
        marginHorizontal: verticalScale(5),
    },
    txtRate: {
        fontSize: moderateScale(12),
        color: '#d52d3a',
    },
    txtCancel: {
        fontSize: moderateScale(16),
        color: Colors.lightGray,
    },
    modalContainer: {
        backgroundColor: Colors.white,
        borderRadius: verticalScale(10),
        width: width - verticalScale(60),
        paddingHorizontal: verticalScale(20),
        paddingTop: verticalScale(25),
        paddingBottom: verticalScale(20),
    },
    txtConfirmInfo: {
        fontSize: moderateScale(20),
        color: Colors.blueDark,
        textAlign: 'center',
        width: '100%',
    },
    txtDesc: {
        lineHeight: moderateScale(22),
        marginTop: verticalScale(20),
        marginBottom: verticalScale(10),
    },
    txtNotiOption: {
        fontSize: moderateScale(15),
        lineHeight: moderateScale(22),
        color: Colors.blueDark,
        marginLeft: verticalScale(15),
    },
});
