import React, { useEffect, useRef, useState } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import { UIHeader, UIModal, UITextInputForm } from '../../../../components/common';
import { verticalScale } from '../../../../utils/ScalingUtils';
import { Colors } from '../../../../theme/Variables';
import { UIAvatar } from '../../components';
import { SCREEN } from '../../../../constant';
import { connect } from 'react-redux';
import { actionUpdateAvatar } from '../../duck/actions';
import { UIText } from '../../../../components/elements';
import ImagePicker from 'react-native-image-crop-picker';
import { BASE_IMAGE_URL } from '../../../../constant/value.constant';

const AccountInfo = (props) => {
    const { t } = useTranslation();
    const { Images } = useTheme();
    const navigation = useNavigation();
    const { profileReducer, actionUpdateAvatar } = props || {};
    const { accountInfo } = profileReducer || {};
    const { avatar: avatarUrl } = accountInfo || '';
    let modalRef = useRef();

    const [avatar, setAvatar] = useState(BASE_IMAGE_URL + ((avatarUrl && avatarUrl) || ''));

    useEffect(() => {
        setAvatar(BASE_IMAGE_URL + avatarUrl);
    }, [avatarUrl]);

    const onPressModalTakePhoto = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
            includeBase64: true,
        }).then((image) => {
            modalRef.close();
            const { path } = image;
            actionUpdateAvatar(path);
            setAvatar(path);
        });
    };

    const onPressModalChooseFrom = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,
            includeBase64: true,
        }).then((image) => {
            modalRef.close();
            const { path } = image;
            actionUpdateAvatar(path);
            setAvatar(path);
        });
    };

    const onPressCancelModal = () => {
        modalRef.close();
    };

    return (
        <View style={styles.container}>
            <UIHeader
                onPress={() => navigation.goBack()}
                LeftIcon="back"
                rightTitle={t('profile.edit')}
                onPressRight={() => navigation.navigate(SCREEN.UPDATE_ACCOUNT_INFO)}
                title={t('profile.account_info')}
            />
            <ScrollView style={styles.scrollContainer} contentContainerStyle={styles.contentContainer}>
                <UIAvatar url={avatar} />
                <View style={{ marginTop: verticalScale(15) }} />
                <UITextInputForm value={accountInfo.fullname} editable={false} title={t('profile.full_name')} />
                <UITextInputForm value={accountInfo.username} editable={false} title={t('profile.username')} />
                <UITextInputForm value={accountInfo.email} editable={false} title={t('profile.email')} />
                <UITextInputForm value={accountInfo.phoneNumber} editable={false} title={t('profile.phone_number')} />
                {/* <UITextInputForm editable={false} title={t('profile.password')} /> */}
            </ScrollView>
            <UIModal ref={(ref) => (modalRef = ref)}>
                <>
                    <View style={{ flex: 1 }} />
                    <TouchableOpacity activeOpacity={1} style={{ width: '88%' }}>
                        <View style={styles.viewModalTop}>
                            <TouchableOpacity onPress={onPressModalTakePhoto}>
                                <UIText text={t('profile.take_photo')} style={styles.textSelectImg} font="bold" />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={onPressModalChooseFrom}>
                                <UIText text={t('profile.choose_from')} style={[styles.textSelectImg, { marginBottom: 0 }]} font="bold" />
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: verticalScale(10) }} />
                        <TouchableOpacity style={styles.viewModalBot} onPress={onPressCancelModal}>
                            <UIText text={t('profile.cancel')} style={styles.textCancel} font="bold" />
                        </TouchableOpacity>
                    </TouchableOpacity>
                </>
            </UIModal>
        </View>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});
export default connect(mapStateToProps, {
    actionUpdateAvatar,
})(AccountInfo);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    scrollContainer: {},
    contentContainer: {
        alignItems: 'center',
        marginTop: verticalScale(25),
        marginHorizontal: verticalScale(16),
        paddingBottom: verticalScale(40),
        flexGrow: 1,
    },
    viewModalTop: {
        padding: verticalScale(12),
        backgroundColor: Colors.white,
        borderRadius: 10,
    },
    viewModalBot: {
        marginBottom: verticalScale(20),
        paddingVertical: verticalScale(12),
        backgroundColor: Colors.white,
        borderRadius: 10,
    },
    textSelectImg: {
        alignSelf: 'center',
        marginBottom: verticalScale(10),
        borderRadius: 10,
    },
    textCancel: {
        textTransform: 'capitalize',
        lineHeight: verticalScale(20),
        alignSelf: 'center',
        borderRadius: 10,
    },
});
