import React, { useState, useEffect, useRef } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import { UIHeader, UIModal, UITextInputForm } from '../../../../components/common';
import { verticalScale } from '../../../../utils/ScalingUtils';
import { Colors } from '../../../../theme/Variables';
import { UIAvatar } from '../../components';
import { UIButton, UIText } from '../../../../components/elements';
import { connect } from 'react-redux';
import { accountUpdateAccountInfo, actionUpdateAvatar } from '../../duck/actions';
import ImagePicker from 'react-native-image-crop-picker';
import { BASE_IMAGE_URL } from '../../../../constant/value.constant';

const fakeData = {
    fullname: 'Pham Tan Tai',
    username: 'Tai8292',
    email: 'tai828292@gmail.com',
    phoneNumber: '+84 636 677 781',
};

const UpdateAccountInfo = (props) => {
    const { t } = useTranslation();
    const { actionUpdateAvatar } = props;
    const { isLoading, accountInfo } = props.profileReducer || false;
    const { avatar: avatarUrl } = accountInfo || '';
    const { Images } = useTheme();
    const navigation = useNavigation();
    const [fullname, setFullname] = useState(accountInfo.fullname);
    const [username, setUsername] = useState(accountInfo.username);
    const [email, setEmail] = useState(accountInfo.email);
    const [phoneNumber, setPhoneNumber] = useState(accountInfo.phoneNumber);
    const [avatar, setAvatar] = useState(BASE_IMAGE_URL + ((avatarUrl && avatarUrl) || ''));
    const [image, setImage] = useState(null);
    let modalRef = useRef();

    useEffect(() => {
        setAvatar(BASE_IMAGE_URL + ((avatarUrl && avatarUrl) || ''));
    }, [avatarUrl]);

    const onPressSave = () => {
        const { accountUpdateAccountInfo } = props;
        const data = {
            phoneNumber,
            image: image,
            _id: accountInfo._id,
        };
        accountUpdateAccountInfo({ data }, () => {
            navigation.goBack();
        });
    };

    const onPressModalTakePhoto = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
            includeBase64: true,
        }).then((img) => {
            modalRef.close();
            setImage(img);
            const { path } = img;
            setAvatar(path);
        });
    };

    const onPressModalChooseFrom = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,
            includeBase64: true,
        }).then((img) => {
            modalRef.close();
            setImage(img);
            const { path } = img;
            setAvatar(path);
        });
    };

    const onPressCancelModal = () => {
        modalRef.close();
    };

    return (
        <View style={styles.container}>
            <UIHeader onPress={() => navigation.goBack()} LeftIcon="back" title={t('profile.account_info')} />
            <ScrollView style={styles.scrollContainer} contentContainerStyle={styles.contentContainer}>
                <UIAvatar onPressCamera={() => modalRef.open()} url={avatar} />
                <View style={{ marginTop: verticalScale(15) }} />
                <UITextInputForm
                    value={fakeData.fullname}
                    title={t('profile.full_name')}
                    value={fullname}
                    onChangeText={(text) => setFullname(text)}
                />
                <UITextInputForm value={fakeData.username} title={t('profile.username')} value={username} editable={false} />
                <UITextInputForm value={fakeData.email} title={t('profile.email')} value={email} editable={false} />
                <UITextInputForm
                    value={fakeData.phoneNumber}
                    title={t('profile.phone_number')}
                    value={phoneNumber}
                    onChangeText={(text) => setPhoneNumber(text)}
                />
                {/* <UITextInputForm editable={false} title={t('profile.password')} /> */}
                <UIButton isLoading={isLoading} onPress={onPressSave} style={styles.btnSave} text={t('profile.save')} />
            </ScrollView>
            <UIModal ref={(ref) => (modalRef = ref)}>
                <>
                    <View style={{ flex: 1 }} />
                    <TouchableOpacity activeOpacity={1} style={{ width: '88%' }}>
                        <View style={styles.viewModalTop}>
                            <TouchableOpacity onPress={onPressModalTakePhoto}>
                                <UIText text={t('profile.take_photo')} style={styles.textSelectImg} font="bold" />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={onPressModalChooseFrom}>
                                <UIText text={t('profile.choose_from')} style={[styles.textSelectImg, { marginBottom: 0 }]} font="bold" />
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: verticalScale(10) }} />
                        <TouchableOpacity style={styles.viewModalBot} onPress={onPressCancelModal}>
                            <UIText text={t('profile.cancel')} style={styles.textCancel} font="bold" />
                        </TouchableOpacity>
                    </TouchableOpacity>
                </>
            </UIModal>
        </View>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});
export default connect(mapStateToProps, {
    accountUpdateAccountInfo,
    actionUpdateAvatar,
})(UpdateAccountInfo);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    scrollContainer: {},
    contentContainer: {
        alignItems: 'center',
        marginTop: verticalScale(25),
        marginHorizontal: verticalScale(16),
        paddingBottom: verticalScale(40),
    },
    btnSave: {
        marginTop: verticalScale(20),
    },
    viewModalTop: {
        padding: verticalScale(12),
        backgroundColor: Colors.white,
        borderRadius: 10,
    },
    viewModalBot: {
        marginBottom: verticalScale(20),
        paddingVertical: verticalScale(12),
        backgroundColor: Colors.white,
        borderRadius: 10,
    },
    textSelectImg: {
        alignSelf: 'center',
        marginBottom: verticalScale(10),
        borderRadius: 10,
    },
    textCancel: {
        textTransform: 'capitalize',
        lineHeight: verticalScale(20),
        alignSelf: 'center',
        borderRadius: 10,
    },
});
