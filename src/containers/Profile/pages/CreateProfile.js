import React, { useState, useRef, useEffect } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, Text, FlatList } from 'react-native';
import { UIHeader, UIModal, CustomTabbar } from '../../../components/common';
import { useTheme } from '@/theme';
import { useTranslation } from 'react-i18next';
import { Colors } from '../../../theme/Variables';
import { UIAvatar, UIButtonAdd, UICategory, UIItem, UISubCategory } from '../components';
import { verticalScale, scale } from '../../../utils/ScalingUtils';
import { useNavigation, useRoute } from '@react-navigation/native';
import { SCREEN } from '../../../constant';
import { connect } from 'react-redux';
import { actionUpdateProfile, accountUpdateAccountInfo } from '../duck/actions';
import ImagePicker from 'react-native-image-crop-picker';
import { UIButton, UIText } from '../../../components/elements';
import { BASE_IMAGE_URL, LOCK_INFO, SKILL_LEVEL } from '../../../constant/value.constant';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import Toast from '../../../components/elements/Toast';

const CreateProfile = (props) => {
    const { profileReducer, actionUpdateProfile, accountUpdateAccountInfo } = props || {};
    const { accountInfo } = profileReducer || {};
    const { t } = useTranslation();
    const { params } = useRoute() || {};
    const { indexTab } = params || 0;
    const { Images } = useTheme();
    const navigation = useNavigation();
    const { avatar: avatarUrl, email, phoneNumber } = accountInfo || '';
    const [avatar, setAvatar] = useState(BASE_IMAGE_URL + ((avatarUrl && avatarUrl) || ''));
    const { resume } = accountInfo || {};
    const { intro, about, background, skills, addtionalInfo } = resume || {};
    const { headline } = intro || {};
    const { summary } = about || {};
    const { is_unlocked } = false;
    const { experience, education } = (background && background) || [];
    const [newExperience, setExperience] = useState(experience ?? []);
    const [newEducation, setEducation] = useState(education ?? []);
    const [newSkill, setNewSkill] = useState(skills ?? []);
    const { siteCompany, hobbies, socialLinks } = addtionalInfo || {};
    const [location, setLocation] = useState('');
    const [indexEdit, setIndexEdit] = useState(-1);
    let modalRef = useRef();

    useEffect(() => {
        setAvatar(BASE_IMAGE_URL + ((avatarUrl && avatarUrl) || ''));
    }, [avatarUrl]);

    useEffect(() => {
        if (params?.addExperience) {
            if (indexEdit >= 0) {
                const newData = newExperience.map((e, i) => {
                    return i != indexEdit ? e : params?.addExperience;
                });
                setExperience(newData);
            } else {
                newExperience.push(params?.addExperience);
                setExperience(newExperience);
            }
        } else {
            const newData = newExperience.filter((item) => item !== params?.addExperience);
            setExperience(newData);
        }
        setIndexEdit(-1);
    }, [params?.addExperience]);

    useEffect(() => {
        if (params?.addEducation) {
            if (indexEdit >= 0) {
                const newData = newEducation.map((e, i) => {
                    return i != indexEdit ? e : params?.addEducation;
                });
                setEducation(newData);
            } else {
                newEducation.push(params?.addEducation);
                setEducation(newEducation);
            }
        }
    }, [params?.addEducation]);

    useEffect(() => {
        if (params?.addSkill) {
            if (indexEdit >= 0) {
                const newData = newSkill.map((e, i) => {
                    return i != indexEdit ? e : params?.addSkill;
                });
                setNewSkill(newData);
            } else {
                newSkill.push(params?.addSkill);
                setNewSkill(newSkill);
                setNewSkill(newSkill);
                setNewSkill(newSkill);
                setNewSkill(newSkill);
            }
        }
    }, [params?.addSkill]);

    const convertArrayToString = (arr) => {
        let str = '';
        if (arr && arr.length > 0) {
            arr.forEach((e, i) => {
                str += i == arr.length - 1 ? e?.title || e?.school || e : `${e?.title || e?.school || e}\n`;
            });
        }
        return str;
    };

    const convertArrayNetWorkToString = (arr) => {
        let str = '';
        if (arr && arr.length > 0) {
            arr.forEach((e, i) => {
                str += i == arr.length - 1 ? e?.value || e : `${e?.value || e}\n`;
            });
        }
        return str;
    };

    const onPressAvatar = () => {
        navigation.navigate(SCREEN.ACCOUNT_INFO);
    };

    const onPressModalTakePhoto = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
            includeBase64: true,
        }).then((image) => {
            modalRef.close();
            const { path } = image;
            setAvatar(path);
            updateAvatar(image);
        });
    };

    const onPressModalChooseFrom = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,
            includeBase64: true,
        }).then((image) => {
            modalRef.close();
            const { path } = image;
            setAvatar(path);
            updateAvatar(image);
        });
    };

    const updateAvatar = (avatar) => {
        const data = {
            image: avatar,
            _id: accountInfo._id,
        };
        accountUpdateAccountInfo({ data }, () => {});
    };

    const onPressCancelModal = () => {
        modalRef.close();
    };

    const buildBodyBackground = () => {
        let background = background || {};
        if (newExperience.length > 0) {
            background = {
                ...background,
                experience: newExperience,
            };
        }
        if (newEducation.length > 0) {
            background = {
                ...background,
                education: newEducation,
            };
        }
        return { background };
    };

    const buildBodyAbout = () => {
        let about = about || {};
        if (summary != null) {
            about = {
                ...about,
                summary: summary,
            };
        }
        return { about };
    };

    const buildBodyIntro = () => {
        let intro = intro || {};
        if (headline != null) {
            intro = {
                ...intro,
                headline: headline,
            };
        }
        return { intro };
    };

    const buildBodySkill = () => {
        let skills = newSkill || [];
        return { skills };
    };

    const onSave = () => {
        try {
            if (!buildBodyAbout()?.about?.summary) {
                Toast(t('profile.miss_summary'), 'error');
            } else {
                let data = {};
                if (buildBodyBackground()) {
                    data = { ...data, ...buildBodyBackground() };
                }
                if (buildBodyAbout()) {
                    data = { ...data, ...buildBodyAbout() };
                }
                if (buildBodyIntro()) {
                    data = { ...data, ...buildBodyIntro() };
                }
                if (buildBodySkill()) {
                    data = { ...data, ...buildBodySkill() };
                }
                actionUpdateProfile({ data }, () => {
                    navigation.goBack();
                });
            }
        } catch (error) {}
    };

    //Item
    const renderItemExperience = ({ item, index }) => {
        return (
            <View>
                <UIItem
                    name={item.title}
                    media={item.media}
                    startDate={new Date(item.startDate)}
                    startDate={new Date(item.endDate)}
                    title={item.companyName}
                    description={item.description}
                    stylesName={styles.textName}
                    onPress={() => moveToEditExperience(false, item, index)}
                />
                <View style={styles.lineFull} />
            </View>
        );
    };

    const renderItemEducation = ({ item, index }) => {
        return (
            <View>
                <UIItem
                    name={item.school}
                    title={item.description}
                    media={item.media}
                    startDate={new Date(item.startDate)}
                    startDate={new Date(item.endDate)}
                    stylesName={styles.textName}
                    onPress={() => moveToEditEducation(false, item, index)}
                />
                <View style={styles.lineFull} />
            </View>
        );
    };

    const renderItemSkill = ({ item, index }) => {
        return (
            <View>
                <UIItem
                    name={item.title}
                    title={SKILL_LEVEL[item.proficiency - 1].label}
                    stylesName={styles.textName}
                    onPress={() => moveToEditSkill(false, item, index)}
                />
                <View style={styles.lineFull} />
            </View>
        );
    };

    //Edit
    const onPressLocation = () => {
        navigation.navigate(SCREEN.SEARCH_SCREEN, {
            title: 'Location',
            data: null,
            placeholder: 'Location',
            onChoose: (item) => {
                setLocation(item.name);
            },
        });
    };

    const moveToEditExperience = (isAdd, item, index) => {
        setIndexEdit(isAdd ? -1 : index);
        navigation.navigate(SCREEN.UPDATE_EXPERIENCE, {
            isAdd: isAdd,
            item,
            index,
            onDeleteExperience: (index) => onDeleteExperience(index),
        });
    };

    const moveToEditEducation = (isAdd, item, index) => {
        setIndexEdit(isAdd ? -1 : index);
        navigation.navigate(SCREEN.UPDATE_EDUCATION, { isAdd: isAdd, item, index, onDeleteEducation: (index) => onDeleteEducation(index) });
    };

    const moveToEditSkill = (isAdd, item, index) => {
        setIndexEdit(isAdd ? -1 : index);
        navigation.navigate(SCREEN.UPDATE_SPECIALIST_SKILL, { isAdd: isAdd, item, index, onDeleteSkill: (index) => onDeleteSkill(index) });
    };

    const onDeleteExperience = (index) => {
        setExperience(newExperience.filter((e, i) => i !== index));
    };

    const onDeleteEducation = async (index) => {
        setEducation(newEducation.filter((e, i) => i !== index));
    };

    const onDeleteSkill = async (index) => {
        setNewSkill(newSkill.filter((e, i) => i !== index));
    };

    return (
        <View style={styles.container}>
            <UIHeader onPress={() => navigation.goBack()} LeftIcon="back" title={t('profile.your_profile')} />
            <ScrollableTabView initialPage={indexTab} renderTabBar={(props) => <CustomTabbar {...props} />}>
                <View style={styles.viewTabs} tabLabel={t('profile.about')}>
                    <UISubCategory
                        onPress={() => navigation.navigate(SCREEN.UPDATE_HEADLINE)}
                        icon={Images.profile.ic_headline}
                        name={t('profile.headline')}
                        editable
                        description={headline || t('profile.enter_headline')}
                    />
                    <View style={styles.line} />
                    <UISubCategory
                        onPress={onPressLocation}
                        icon={Images.profile.ic_position}
                        name={t('profile.location')}
                        editable
                        description={location || t('profile.choose_location')}
                    />
                    <View style={styles.line} />
                    <UISubCategory
                        onPress={() => navigation.navigate(SCREEN.UPDATE_ABOUT)}
                        icon={Images.profile.ic_specialist_skill}
                        name={t('profile.summary')}
                        editable
                        description={summary || t('profile.summary_des')}
                    />
                    <View style={styles.line} />
                    <UISubCategory
                        onPress={() => navigation.navigate(SCREEN.SOCIAL_LINKS)}
                        icon={Images.profile.ic_other_skill}
                        name={t('profile.social_links')}
                        editable
                        description={
                            socialLinks && socialLinks.length > 0 ? convertArrayNetWorkToString(socialLinks) : t('profile.social_links_des')
                        }
                    />
                </View>
                <View style={styles.viewTabsNoPadding} tabLabel={t('profile.experience')}>
                    <FlatList
                        data={newExperience}
                        extraData={newExperience}
                        renderItem={renderItemExperience}
                        ListFooterComponent={
                            <UIButtonAdd
                                icon={Images.profile.ic_experience}
                                text={t('profile.add_new_experience')}
                                onPress={() => moveToEditExperience(true, null, 0)}
                            />
                        }
                    />
                </View>
                <View style={styles.viewTabsNoPadding} tabLabel={t('profile.education')}>
                    <FlatList
                        data={newEducation}
                        extraData={newEducation}
                        renderItem={renderItemEducation}
                        ListFooterComponent={
                            <UIButtonAdd
                                icon={Images.profile.ic_education}
                                text={t('profile.add_new_education')}
                                onPress={() => moveToEditEducation(true, null, 0)}
                            />
                        }
                    />
                </View>
                <View style={styles.viewTabsNoPadding} tabLabel={t('profile.skills')}>
                    <FlatList
                        data={newSkill}
                        extraData={newSkill}
                        renderItem={renderItemSkill}
                        ListFooterComponent={
                            <UIButtonAdd
                                icon={Images.profile.ic_other_skill}
                                text={t('profile.add_new_skills')}
                                onPress={() => moveToEditSkill(true, null, 0)}
                            />
                        }
                    />
                </View>
                {LOCK_INFO.BUY === is_unlocked && (
                    <View style={styles.viewTabsNoPadding} tabLabel={t('profile.contact')}>
                        <FlatList
                            data={[{ name: 'Email', value: email }]}
                            keyExtractor={(_, i) => i.toString()}
                            renderItem={({ item }) => (
                                <UIItem name={item?.name} title={item?.value} stylesName={styles.textName} hideAction style={styles.item} />
                            )}
                            ListFooterComponent={
                                <UIButtonAdd
                                    icon={Images.profile.ic_other_skill}
                                    text={t('profile.add_new_contact')}
                                    // onPress = {() => navigation.navigate(SCREEN.UPDATE_CONTACT_PRICE)}
                                />
                            }
                        />
                    </View>
                )}
            </ScrollableTabView>
            <View style={styles.viewAction}>
                <UIButton onPress={onSave} style={styles.btnDone} text={t('profile.done')} type="BgBlue" />
            </View>
            {/* <ScrollView style={styles.scrollContainer} contentContainerStyle={styles.contentContainer}>
                <UIAvatar url={avatar} onPressAvatar={onPressAvatar} onPressCamera={() => modalRef.open()} />
                <View style={{ marginTop: verticalScale(40) }} />
                <UICategory name={t('profile.about')}>
                    <UISubCategory
                        onPress={() => navigation.navigate(SCREEN.UPDATE_HEADLINE)}
                        icon={Images.profile.ic_headline}
                        name={t('profile.headline')}
                        editable
                        description={headline || t('profile.enter_headline')}
                    />
                    <View style={styles.line} />
                    <UISubCategory
                        onPress={() => navigation.navigate(SCREEN.UPDATE_ABOUT)}
                        icon={Images.profile.ic_headline}
                        name={t('profile.summary')}
                        editable
                        description={summary || t('profile.summary_des')}
                    />
                    <View style={styles.line} />
                    <UISubCategory
                        onPress={() => navigation.navigate(SCREEN.SOCIAL_LINKS)}
                        icon={Images.profile.ic_other_skill}
                        name={t('profile.social_links')}
                        editable
                        description={
                            socialLinks && socialLinks.length > 0 ? convertArrayNetWorkToString(socialLinks) : t('profile.social_links_des')
                        }
                    />
                </UICategory>
                <UICategory name={t('profile.background')}>
                    <UISubCategory
                        onPress={() => navigation.navigate(SCREEN.EXPERIENCE)}
                        icon={Images.profile.ic_experience}
                        name={t('profile.experience')}
                        editable
                        description={experience && experience.length > 0 ? convertArrayToString(experience) : t('profile.experience_des')}
                    />
                    <View style={styles.line} />
                    <UISubCategory
                        onPress={() => navigation.navigate(SCREEN.EDUCATION)}
                        icon={Images.profile.ic_education}
                        name={t('profile.education')}
                        editable
                        description={education && education.length > 0 ? convertArrayToString(education) : t('profile.education_des')}
                    />
                </UICategory>
                <UICategory name={t('profile.skills')}>
                    <UISubCategory
                        onPress={() => navigation.navigate(SCREEN.SPECIALIST_SKILL)}
                        icon={Images.profile.ic_specialist_skill}
                        name={t('profile.specialist_skills')}
                        editable
                        description={
                            specialists && specialists.length > 0 ? convertArrayToString(specialists) : t('profile.specialist_skills_des')
                        }
                    />
                    <View style={styles.line} />
                    <UISubCategory
                        onPress={() => navigation.navigate(SCREEN.LANGUAGE)}
                        icon={Images.profile.ic_language}
                        name={t('profile.languages')}
                        editable
                        description={languages && languages.length > 0 ? convertArrayToString(languages) : t('profile.languages_des')}
                    />
                    <View style={styles.line} />
                    <UISubCategory
                        onPress={() => navigation.navigate(SCREEN.OTHER_SKILL)}
                        icon={Images.profile.ic_other_skill}
                        name={t('profile.other_skills')}
                        editable
                        description={others && others.length > 0 ? convertArrayToString(others) : t('profile.other_skills_des')}
                    />
                </UICategory>
                <UICategory name={t('profile.contact_info')}>
                    <UISubCategory
                        icon={Images.profile.ic_email}
                        customContainer={{ width: verticalScale(38), height: verticalScale(38) }}
                        customStyle={{ width: verticalScale(20), height: verticalScale(20) }}
                        name={t('profile.email')}
                        description={email || ''}
                    />
                    <View style={styles.line} />
                    <UISubCategory
                        onPress={() => navigation.navigate(SCREEN.SETUP_KYC)}
                        customContainer={{ width: verticalScale(38), height: verticalScale(38) }}
                        customStyle={{ width: verticalScale(20), height: verticalScale(20) }}
                        icon={Images.profile.ic_phone}
                        editable={phoneNumber == ''}
                        name={t('profile.phone_number')}
                        description={phoneNumber || t('profile.input_phone')}
                    />
                </UICategory>
            </ScrollView> */}
            <UIModal ref={(ref) => (modalRef = ref)}>
                <>
                    <View style={{ flex: 1 }} />
                    <TouchableOpacity activeOpacity={1} style={{ width: '88%' }}>
                        <View style={styles.viewModalTop}>
                            <TouchableOpacity onPress={onPressModalTakePhoto}>
                                <UIText text={t('profile.take_photo')} style={styles.textSelectImg} font="bold" />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={onPressModalChooseFrom}>
                                <UIText text={t('profile.choose_from')} style={[styles.textSelectImg, { marginBottom: 0 }]} font="bold" />
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: verticalScale(10) }} />
                        <TouchableOpacity style={styles.viewModalBot} onPress={onPressCancelModal}>
                            <UIText text={t('profile.cancel')} style={styles.textCancel} font="bold" />
                        </TouchableOpacity>
                    </TouchableOpacity>
                </>
            </UIModal>
        </View>
    );
};

const mapStateToProps = (state) => ({
    profileReducer: state.profileReducer,
});

export default connect(mapStateToProps, {
    actionUpdateProfile,
    accountUpdateAccountInfo,
})(CreateProfile);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    scrollContainer: {},
    contentContainer: {
        alignItems: 'center',
        marginTop: verticalScale(20),
        marginHorizontal: verticalScale(16),
        paddingBottom: verticalScale(40),
    },
    line: {
        borderTopWidth: 1,
        borderTopColor: Colors.border,
        width: '89%',
        alignSelf: 'flex-end',
    },
    lineFull: {
        borderTopWidth: 1,
        borderTopColor: Colors.border,
        width: '100%',
    },
    viewModalTop: {
        padding: verticalScale(12),
        backgroundColor: Colors.white,
        borderRadius: 10,
    },
    viewModalBot: {
        marginBottom: verticalScale(20),
        paddingVertical: verticalScale(12),
        backgroundColor: Colors.white,
        borderRadius: 10,
    },
    textSelectImg: {
        alignSelf: 'center',
        marginBottom: verticalScale(10),
        borderRadius: 10,
    },
    textCancel: {
        textTransform: 'capitalize',
        lineHeight: verticalScale(20),
        alignSelf: 'center',
        borderRadius: 10,
    },
    viewTabs: {
        flex: 1,
        paddingHorizontal: scale(16),
    },
    viewTabsNoPadding: {
        flex: 1,
    },
    textName: {
        color: Colors.title,
    },
    viewAction: {
        paddingHorizontal: scale(30),
        marginBottom: verticalScale(20),
    },
    btnDone: {
        // marginHorizontal: scale(16),
    },
});
