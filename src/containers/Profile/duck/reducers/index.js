import { ProfileAction } from '../actions';

const profile = {
    accountInfo: {},
    isLoading: false,
};
const profileReducer = (state = profile, action) => {
    const { type, payload } = action;
    switch (type) {
        case ProfileAction.UPDATE_PROFILE.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ProfileAction.UPDATE_PROFILE.SUCCESS:
            return {
                ...state,
                isLoading: false,
                accountInfo: { ...state.accountInfo, resume: payload },
            };
        case ProfileAction.UPDATE_PROFILE.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ProfileAction.UPDATE_ACCOUNT_INFO.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ProfileAction.UPDATE_ACCOUNT_INFO.SUCCESS:
            return {
                ...state,
                isLoading: false,
                accountInfo: payload,
            };
        case ProfileAction.UPDATE_ACCOUNT_INFO.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ProfileAction.GET_ACCOUNT_INFO.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ProfileAction.GET_ACCOUNT_INFO.SUCCESS:
            return {
                ...state,
                isLoading: false,
                accountInfo: payload,
            };
        case ProfileAction.GET_ACCOUNT_INFO.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ProfileAction.GET_LOCATION.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ProfileAction.GET_LOCATION.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case ProfileAction.GET_LOCATION.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ProfileAction.UPDATE_AVATAR.PENDING:
            return {
                ...state,
                accountInfo: {
                    ...state.accountInfo,
                    avatar_url: payload,
                },
            };
        case ProfileAction.UPLOAD_MEDIA.PENDING:
            return {
                ...state,
                isLoading: false,
            };
        case ProfileAction.UPLOAD_MEDIA.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case ProfileAction.UPLOAD_MEDIA.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ProfileAction.SET_OPTION.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case ProfileAction.SET_OPTION.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case ProfileAction.SET_OPTION.ERROR:
            return {
                ...state,
                isLoading: false,
            };

        default:
            return state;
    }
};

export { profileReducer };
