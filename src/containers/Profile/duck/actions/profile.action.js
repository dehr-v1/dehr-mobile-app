import { ProfileAction } from './profile.create.action';

export const actionUpdateProfile = (payload, callback) => ({
    type: ProfileAction.UPDATE_PROFILE.PENDING,
    payload,
    callback,
});

export const accountUpdateAccountInfo = (payload, callback) => ({
    type: ProfileAction.UPDATE_ACCOUNT_INFO.PENDING,
    payload,
    callback,
});

export const actionGetAccountInfo = (callback) => ({
    type: ProfileAction.GET_ACCOUNT_INFO.PENDING,
    callback,
});

export const actionGetLocation = (callback) => ({
    type: ProfileAction.GET_LOCATION.PENDING,
    callback,
});

export const actionUpdateAvatar = (payload) => ({
    type: ProfileAction.UPDATE_AVATAR.PENDING,
    payload,
});

export const actionUploadMedia = (payload, callback) => ({
    type: ProfileAction.UPLOAD_MEDIA.PENDING,
    payload,
    callback,
});

export const actionSetOption = (payload, callback) => ({
    type: ProfileAction.SET_OPTION.PENDING,
    payload,
    callback
});
