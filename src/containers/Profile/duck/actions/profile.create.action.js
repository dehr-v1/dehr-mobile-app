import { createActionSet } from '../../../../utils';

export const ProfileAction = {
    UPDATE_PROFILE: createActionSet('UPDATE_PROFILE'),
    UPDATE_ACCOUNT_INFO: createActionSet('UPDATE_ACCOUNT_INFO'),
    GET_ACCOUNT_INFO: createActionSet('GET_ACCOUNT_INFO'),
    GET_LOCATION: createActionSet('GET_LOCATION'),
    UPDATE_AVATAR: createActionSet('UPDATE_AVATAR'),
    UPLOAD_MEDIA: createActionSet('UPLOAD_MEDIA'),
    SET_OPTION: createActionSet('SET_OPTION'),
};
