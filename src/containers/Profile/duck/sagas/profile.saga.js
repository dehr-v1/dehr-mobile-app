import { put, call, takeLatest, takeEvery } from 'redux-saga/effects';
import Toast from '../../../../components/elements/Toast';
import { getCurrentUser, setCurrentUser } from '../../../../services/persistence/store.config';
import { ProfileAction } from '../actions';
import { LoginAction } from '../../../Login/duck/actions';

import {
    getAccountInfo,
    getLocation,
    updateAccountInfo,
    updateResume,
    uploadImage,
    setOption,
} from '../../../../services/remote/profile.service';

function* onUpdateProfile(action) {
    const { payload, callback } = action;
    const { data } = payload || {};
    try {
        requestBody = {
            ...data,
        };
        const accountInfo = yield call(() => updateResume(requestBody));
        yield put({
            type: ProfileAction.UPDATE_PROFILE.SUCCESS,
            payload: accountInfo,
        });
        yield put({
            type: ProfileAction.GET_ACCOUNT_INFO.PENDING,
        });
        callback && callback(accountInfo);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: ProfileAction.UPDATE_PROFILE.ERROR,
            payload: error,
        });
    }
}

export function* watchonUpdateProfile() {
    yield takeLatest(ProfileAction.UPDATE_PROFILE.PENDING, onUpdateProfile);
}

function* onUpdateAccountInfo(action) {
    const { payload, callback } = action;
    const { data } = payload || {};
    try {
        // const formData = new FormData();
        // if (data.firstName) {
        //     formData.append('firstName', data.firstName);
        // }
        // if (data.lastName) {
        //     formData.append('lastName', data.lastName);
        // }
        // if (data.phoneNumber) {
        //     formData.append('phoneNumber', data.phoneNumber);
        // }
        // if (data.gender) {
        //     formData.append('gender', data.gender);
        // }
        // if (data.passportId) {
        //     formData.append('passportId', data.passportId);
        // }

        // if (data.passportFront) {
        //     const imageFront = {
        //         uri: data.passportFront.path,
        //         type: data.passportFront.mime,
        //         name: data.passportFront.mime.replace('/', '.'),
        //     };
        //     formData.append('passportFront', imageFront);
        // }
        // if (data.passportBack) {
        //     const imageBack = {
        //         uri: data.imageBack.path,
        //         type: data.imageBack.mime,
        //         name: data.imageBack.mime.replace('/', '.'),
        //     };
        //     formData.append('passportBack', imageBack);
        // }

        // if (data.passportSelfie) {
        //     const imageSelfie = {
        //         uri: data.passportSelfie.path,
        //         type: data.passportSelfie.mime,
        //         name: data.passportSelfie.mime.replace('/', '.'),
        //     };
        //     formData.append('passportSelfie', imageSelfie);
        // }

        if (data.image) {
            const image = {
                uri: data.image.path,
                type: data.image.mime,
                name: data.image.mime.replace('/', '.'),
            };
            formData.append('avatar', image);
        }
        const response = yield call(() => updateAccountInfo(formData));
        yield put({
            type: ProfileAction.UPDATE_ACCOUNT_INFO.SUCCESS,
            payload: response.data,
        });
        yield put({
            type: ProfileAction.GET_ACCOUNT_INFO.PENDING,
        });
        callback && callback();
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: ProfileAction.UPDATE_ACCOUNT_INFO.ERROR,
            payload: error,
        });
    }
}

export function* watchonUpdateAccountInfo() {
    yield takeLatest(ProfileAction.UPDATE_ACCOUNT_INFO.PENDING, onUpdateAccountInfo);
}

function* onGetAccountInfo(action) {
    try {
        const { callback } = action;
        const data = yield call(() => getAccountInfo());
        const user = yield call(() => getCurrentUser());
        const userOld = JSON.parse(user);
        const newUser = {
            ...data,
            email: userOld.email,
            phoneNumber: userOld.phoneNumber,
        };
        setCurrentUser(JSON.stringify(newUser));
        yield put({
            type: ProfileAction.GET_ACCOUNT_INFO.SUCCESS,
            payload: data,
        });
        yield put({
            type: LoginAction.AUTH_LOGIN_USER.SUCCESS,
            payload: data,
        });
        callback && callback(newUser);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: ProfileAction.GET_ACCOUNT_INFO.ERROR,
            payload: error,
        });
    }
}

export function* watchonUpdateGetAccountInfo() {
    yield takeLatest(ProfileAction.GET_ACCOUNT_INFO.PENDING, onGetAccountInfo);
}

function* onGetLocation(action) {
    try {
        const { callback } = action;
        const data = yield call(() => getLocation());
        yield put({
            type: ProfileAction.GET_LOCATION.SUCCESS,
        });
        callback && callback(data);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: ProfileAction.GET_LOCATION.ERROR,
            payload: error,
        });
    }
}

export function* watchonGetLocation() {
    yield takeLatest(ProfileAction.GET_LOCATION.PENDING, onGetLocation);
}

function* onUploadMedia(action) {
    try {
        const { payload, callback } = action;
        const arr = [];
        if (payload.length > 0) {
            const formData = new FormData();
            payload.forEach((e, i) => {
                const image = {
                    uri: e.path,
                    type: e.mime,
                    name: e.mime.replace('/', '.'),
                };
                formData.append(`files`, image);
            });
            const uploadResponse = yield call(() => uploadImage(formData));
            uploadResponse.files.forEach((e, i) => {
                arr.push(e.filename);
            });
            callback && callback(arr);
        } else {
            callback && callback(arr);
        }
    } catch (error) {
        yield put({
            type: ProfileAction.UPLOAD_MEDIA.ERROR,
            payload: error,
        });
    }
}

export function* watchonUploadMedia() {
    yield takeLatest(ProfileAction.UPLOAD_MEDIA.PENDING, onUploadMedia);
}

function* onSetOption(action) {
    const { payload, callback } = action;
    try {
        const response = yield call(() => setOption(payload));
        yield put({
            type: ProfileAction.SET_OPTION.SUCCESS,
        });
        yield put({
            type: ProfileAction.GET_ACCOUNT_INFO.PENDING,
        });
        callback && callback();
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: ProfileAction.SET_OPTION.ERROR,
            payload: error,
        });
        callback && callback();
    }
}

export function* watchonSetOption() {
    yield takeLatest(ProfileAction.SET_OPTION.PENDING, onSetOption);
}
