import { all } from 'redux-saga/effects';
import {
    watchonGetLocation,
    watchonUpdateAccountInfo,
    watchonUpdateGetAccountInfo,
    watchonUpdateProfile,
    watchonUploadMedia,
    watchonSetOption,
} from './profile.saga';

export function* profileSaga() {
    yield all([
        watchonUpdateProfile(),
        watchonUpdateAccountInfo(),
        watchonUpdateGetAccountInfo(),
        watchonGetLocation(),
        watchonUploadMedia(),
        watchonSetOption(),
    ]);
}
