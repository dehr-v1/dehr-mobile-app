import { all } from 'redux-saga/effects';
import {
    watchLoginUserByEmail,
    watchForgotUserByEmail,
    watchRegisterUserByEmail,
    watchChangePassUser,
    watchVerifyUser,
    watchReSendCode,
    watchonLogOut,
    watchonCheckWallet
} from './auth.saga';

export function* authSaga() {
    yield all([
        watchLoginUserByEmail(),
        watchRegisterUserByEmail(),
        watchForgotUserByEmail(),
        watchVerifyUser(),
        watchChangePassUser(),
        watchReSendCode(),
        watchonLogOut(),
        watchonCheckWallet()
    ]);
}
