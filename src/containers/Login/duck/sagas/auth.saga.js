import { put, call, takeLatest } from 'redux-saga/effects';
import Toast from '../../../../components/elements/Toast';
import { setOAuthToken, setCurrentUser, getCurrentUser } from '../../../../services/persistence/store.config';
import { ProfileAction } from '../../../Profile/duck/actions';
import { LoginAction } from '../actions';
import {
    changePassUser,
    forgotUser,
    loginUser,
    registerUser,
    verifyEmail,
    updateNotificationToken,
    logOut,
} from '../../../../services/remote/auth.service';
import { registerWalletBlockChain } from '../../../../services/remote/wallet.service';
import messaging from '@react-native-firebase/messaging';
import { NOTIFICATION_TOPIC } from '../../../../constant/index';

function subscribeToTopic() {
    messaging()
        .subscribeToTopic(NOTIFICATION_TOPIC.BREAKING_NEW)
        .then(() => console.log('Subscribed to topic!'));
}

function unsubscribeFromTopic() {
    messaging()
        .unsubscribeFromTopic(NOTIFICATION_TOPIC.BREAKING_NEW)
        .then(() => console.log('Unsubscribed from topic!'));
}

function* onLoginUserByEmail(action) {
    const { payload, callback } = action;
    const { data } = payload || {};
    try {
        const user = yield call(() => loginUser(data));
        const userOld = yield call(() => getCurrentUser());
        const newUser = {
            ...user,
            email: data.email,
            phoneNumber: (userOld && userOld.phoneNumber) || '',
        };
        setCurrentUser(JSON.stringify(newUser));
        setOAuthToken(user.token);
        yield put({
            type: LoginAction.AUTH_LOGIN_USER.SUCCESS,
            payload: user,
        });
        callback && callback(1);
        yield put({
            type: LoginAction.AUTH_UPDATE_NOTIFICATION_TOKEN.PENDING,
        });
        subscribeToTopic();
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: LoginAction.AUTH_LOGIN_USER.ERROR,
            payload: error,
        });
    }
}

function* onRegisterUserByEmail(action) {
    const { payload, callback } = action;
    const { data } = payload || {};
    try {
        const user = yield call(() => registerUser(data));
        setOAuthToken(user.token);
        setCurrentUser(JSON.stringify(user));
        yield put({
            type: LoginAction.AUTH_REGISTER_USER.SUCCESS,
            payload: user,
        });
        Toast('The activation link has sent to your email');
        callback && callback();
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: LoginAction.AUTH_REGISTER_USER.ERROR,
            payload: error,
        });
    }
}

function* onForgotUserByEmail(action) {
    const { payload, callback } = action;
    const { data } = payload || {};
    try {
        const response = yield call(() => forgotUser(data));
        yield put({
            type: LoginAction.AUTH_FORGOT_USER.SUCCESS,
        });
        Toast('The new password has sent to your email');
        callback && callback();
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: LoginAction.AUTH_FORGOT_USER.ERROR,
            payload: error,
        });
    }
}

function* onVerify(action) {
    const { payload, callback } = action;
    const { data } = payload || {};
    try {
        const user = yield call(() => verifyEmail(data));
        yield put({
            type: LoginAction.AUTH_VERIFY_USER.SUCCESS,
            payload: user,
        });
        callback && callback();
        yield put({
            type: ProfileAction.GET_ACCOUNT_INFO.PENDING,
        });
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: LoginAction.AUTH_VERIFY_USER.ERROR,
            payload: error,
        });
    }
}

function* onChangePassUser(action) {
    const { payload, callback } = action;
    const { data } = payload || {};
    try {
        const user = yield call(() => changePassUser(data));
        yield put({
            type: LoginAction.AUTH_CHANGE_PASS_USER.SUCCESS,
            payload: user,
        });
        callback && callback();
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: LoginAction.AUTH_CHANGE_PASS_USER.ERROR,
            payload: error,
        });
    }
}

function* onReSendCode(action) {
    const { payload, callback } = action;
    const { data } = payload || {};
    try {
        const user = yield call(() => forgotUser(data));
        yield put({
            type: LoginAction.AUTH_RESEND_CODE.SUCCESS,
            payload: user,
        });
        callback && callback();
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: LoginAction.AUTH_RESEND_CODE.ERROR,
            payload: error,
        });
    }
}

function* onLogOut(action) {
    const { payload, callback } = action;
    try {
        const response = yield call(() => logOut());
        yield put({
            type: LoginAction.AUTH_LOGOUT.SUCCESS,
            payload: true,
        });
        unsubscribeFromTopic();
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: LoginAction.AUTH_LOGOUT.ERROR,
            payload: error,
        });
    }
    callback && callback(true);
}

function* onCheckWallet(action) {
    const { payload, callback } = action;
    try {
        const checksum = yield call(() => registerWalletBlockChain(payload.data.email, payload.data.phoneNumber));
        yield put({
            type: LoginAction.AUTH_VERIFY_CHECKSUM.SUCCESS,
            payload: checksum,
        });
        callback && callback(checksum);
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: LoginAction.AUTH_VERIFY_CHECKSUM.ERROR,
            payload: error,
        });
    }
}

function* onUpdateNotificationToken(action) {
    const { payload, callback } = action;
    const { data } = payload || { data: { notiToken: yield messaging().getToken() } };
    try {
        const user = yield call(() => updateNotificationToken(data));
        yield put({
            type: LoginAction.AUTH_UPDATE_NOTIFICATION_TOKEN.SUCCESS,
            payload: user,
        });
        callback && callback();
    } catch (error) {
        Toast(`${error.message}`, 'error');
        yield put({
            type: LoginAction.AUTH_UPDATE_NOTIFICATION_TOKEN.ERROR,
            payload: error,
        });
    }
}

export function* watchonCheckWallet() {
    yield takeLatest(LoginAction.AUTH_VERIFY_CHECKSUM.PENDING, onCheckWallet);
}

export function* watchonLogOut() {
    yield takeLatest(LoginAction.AUTH_LOGOUT.PENDING, onLogOut);
}

export function* watchLoginUserByEmail() {
    yield takeLatest(LoginAction.AUTH_LOGIN_USER.PENDING, onLoginUserByEmail);
}

export function* watchRegisterUserByEmail() {
    yield takeLatest(LoginAction.AUTH_REGISTER_USER.PENDING, onRegisterUserByEmail);
}

export function* watchForgotUserByEmail() {
    yield takeLatest(LoginAction.AUTH_FORGOT_USER.PENDING, onForgotUserByEmail);
}

export function* watchVerifyUser() {
    yield takeLatest(LoginAction.AUTH_VERIFY_USER.PENDING, onVerify);
}

export function* watchChangePassUser() {
    yield takeLatest(LoginAction.AUTH_CHANGE_PASS_USER.PENDING, onChangePassUser);
}

export function* watchReSendCode() {
    yield takeLatest(LoginAction.AUTH_RESEND_CODE.PENDING, onReSendCode);
}

export function* watchUpdateNotificationToken() {
    yield takeLatest(LoginAction.AUTH_UPDATE_NOTIFICATION_TOKEN.PENDING, onUpdateNotificationToken);
}
