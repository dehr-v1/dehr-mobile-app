import { LoginAction } from './login.create.action';

export const actionLoginUser = (payload, callback) => ({
    type: LoginAction.AUTH_LOGIN_USER.PENDING,
    payload,
    callback,
});

export const actionRegisterUser = (payload, callback) => ({
    type: LoginAction.AUTH_REGISTER_USER.PENDING,
    payload,
    callback,
});

export const actionForgotUser = (payload, callback) => ({
    type: LoginAction.AUTH_FORGOT_USER.PENDING,
    payload,
    callback,
});

export const actionVerifyUser = (payload, callback) => ({
    type: LoginAction.AUTH_VERIFY_USER.PENDING,
    payload,
    callback,
});

export const actionReSendCode = (payload, callback) => ({
    type: LoginAction.AUTH_RESEND_CODE.PENDING,
    payload,
    callback,
});

export const actionChangePassUser = (payload, callback) => ({
    type: LoginAction.AUTH_CHANGE_PASS_USER.PENDING,
    payload,
    callback,
});

export const actionLogOut = (callback) => ({
    type: LoginAction.AUTH_LOGOUT.PENDING,
    callback,
});

export const setUser = (payload) => ({
    type: LoginAction.SET_USER,
    payload,
});

export const actionVerifyWallet = (payload, callback) => ({
    type: LoginAction.AUTH_VERIFY_CHECKSUM.PENDING,
    payload,
    callback,
});

export const actionUpdateNotificationToken = (payload, callback) => ({
    type: LoginAction.AUTH_UPDATE_NOTIFICATION_TOKEN.PENDING,
    payload,
    callback,
});
