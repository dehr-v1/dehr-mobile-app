import { createActionSet } from '../../../../utils';

export const LoginAction = {
    AUTH_LOGIN_USER: createActionSet('AUTH_LOGIN_USER'),
    AUTH_FORGOT_USER: createActionSet('AUTH_FORGOT_USER'),
    AUTH_REGISTER_USER: createActionSet('AUTH_REGISTER_USER'),
    AUTH_VERIFY_USER: createActionSet('AUTH_VERIFY_USER'),
    AUTH_VERIFY_CHECKSUM: createActionSet('AUTH_VERIFY_CHECKSUM'),
    AUTH_RESEND_CODE: createActionSet('AUTH_RESEND_CODE'),
    AUTH_CHANGE_PASS_USER: createActionSet('AUTH_CHANGE_PASS_USER'),
    AUTH_UPDATE_NOTIFICATION_TOKEN: createActionSet('AUTH_UPDATE_NOTIFICATION_TOKEN'),
    AUTH_LOGOUT: createActionSet('AUTH_LOGOUT'),
    SET_USER: 'SET_USER',
    KYC_SUCCESS: 'KYC_SUCCESS',
};
