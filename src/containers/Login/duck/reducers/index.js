import { LoginAction } from '../actions';
const auth = {
    user: {},
    isLoading: false,
};
const authReducer = (state = auth, action) => {
    const { type, payload } = action;
    switch (type) {
        case LoginAction.AUTH_LOGIN_USER.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case LoginAction.AUTH_LOGIN_USER.SUCCESS:
            return {
                ...state,
                isLoading: false,
                user: payload,
            };
        case LoginAction.AUTH_LOGIN_USER.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case LoginAction.AUTH_LOGOUT.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case LoginAction.AUTH_LOGOUT.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case LoginAction.AUTH_LOGOUT.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case LoginAction.SET_USER:
            return {
                ...state,
                user: payload,
                isLoading: false,
            };
        case LoginAction.KYC_SUCCESS:
            return {
                ...state,
                user: {
                    ...state.user,
                    kycStatus: 'verified',
                },
                isLoading: false,
            };
        default:
            return state;
    }
};

const registerReducer = (state = auth, action) => {
    const { type, payload } = action;
    switch (type) {
        case LoginAction.AUTH_REGISTER_USER.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case LoginAction.AUTH_REGISTER_USER.SUCCESS:
            return {
                ...state,
                isLoading: false,
                user: payload,
            };
        case LoginAction.AUTH_REGISTER_USER.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
};

const forgotReducer = (state = auth, action) => {
    const { type, payload } = action;
    switch (type) {
        case LoginAction.AUTH_FORGOT_USER.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case LoginAction.AUTH_FORGOT_USER.SUCCESS:
            return {
                ...state,
                isLoading: false,
                user: payload,
            };
        case LoginAction.AUTH_FORGOT_USER.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
};

const verifyReducer = (state = auth, action) => {
    const { type, payload } = action;
    switch (type) {
        case LoginAction.AUTH_VERIFY_USER.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case LoginAction.AUTH_VERIFY_USER.SUCCESS:
            return {
                ...state,
                isLoading: false,
                user: payload,
            };
        case LoginAction.AUTH_VERIFY_USER.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
};

const changePassReducer = (state = auth, action) => {
    const { type, payload } = action;
    switch (type) {
        case LoginAction.AUTH_CHANGE_PASS_USER.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case LoginAction.AUTH_CHANGE_PASS_USER.SUCCESS:
            return {
                ...state,
                isLoading: false,
                user: payload,
            };
        case LoginAction.AUTH_CHANGE_PASS_USER.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
};

const checkSumReducer = (state = auth, action) => {
    const { type, payload } = action;
    switch (type) {
        case LoginAction.AUTH_VERIFY_CHECKSUM.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case LoginAction.AUTH_VERIFY_CHECKSUM.SUCCESS:
            return {
                ...state,
                isLoading: false,
            };
        case LoginAction.AUTH_VERIFY_CHECKSUM.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
};

const notificationReducer = (state = auth, action) => {
    const { type, payload } = action;
    switch (type) {
        case LoginAction.AUTH_UPDATE_NOTIFICATION_TOKEN.PENDING:
            return {
                ...state,
                isLoading: true,
            };
        case LoginAction.AUTH_UPDATE_NOTIFICATION_TOKEN.SUCCESS:
            return {
                ...state,
                isLoading: false,
                user: payload,
            };
        case LoginAction.AUTH_UPDATE_NOTIFICATION_TOKEN.ERROR:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
};

export { authReducer, registerReducer, forgotReducer, verifyReducer, changePassReducer, checkSumReducer, notificationReducer };
