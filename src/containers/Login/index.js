import React, { useState } from 'react';
import { View, StyleSheet, TouchableOpacity, KeyboardAvoidingView, Alert } from 'react-native';
import { useTheme } from '@/theme';
import { fontFamily } from '../../theme/Fonts';
import { Colors } from '../../theme/Variables';
import { UIButton, UIText } from '../../components/elements';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { navigateAndSimpleReset } from '@/navigators/Root';
import { SCREEN } from '../../constant';
import { connect } from 'react-redux';
import { actionLoginUser, actionVerifyWallet } from './duck/actions';
import { actionGetAccountInfo } from '../Profile/duck/actions';
import InputEdit from '../../components/InputEdit';
import CustomAlert from '../../components/Alert';
import { moderateScale, verticalScale } from '../../utils/ScalingUtils';

const IndexLoginContainer = (props) => {
    const { Layout, Gutters, Colors } = useTheme();
    const navigation = useNavigation();
    const { t } = useTranslation();
    const { authReducer, actionLoginUser, actionGetAccountInfo } = props;
    const { isLoading } = authReducer || false;
    const [loginCount, setLoginCount] = useState(0);
    const [disabled, setDisabled] = useState(true);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isValidEmail, checkEmail] = useState(false);
    const [isValidPassword, checkPassword] = useState(false);
    const [showPopupNoti, setShowPopupNoti] = useState(false);

    const validatorInputEmail = (text, isValid) => {
        setEmail(text)
        checkEmail(isValid)
        setDisabled(!isValid || !isValidPassword);
    };

    const validatorInputPass = (text, isValid) => {
        setPassword(text);
        checkPassword(isValid)
        setDisabled(!isValid || !isValidEmail);
    };

    const onLogin = () => {
        const data = {
            email,
            password,
        };
        actionLoginUser({ data }, (loginCount) => {
            setLoginCount(loginCount)
            actionGetAccountInfo((user) => {
                navigationMain()
            });
        });
    };

    const navigationMain = () => {
        if (loginCount == 1) {
            navigation.navigate(SCREEN.MAIN, {
                screen: 'Main',
                params: {
                    screen: 'Profile',
                },
            });
        } else {
            navigateAndSimpleReset(SCREEN.MAIN);
        }
    }

    const alertNotification = () =>
        <CustomAlert
            isEdittext={false}
            displayTitle={t('profile.notification')}
            displayMsg={t('login.verify_checksum_error')}
            visibility={showPopupNoti}
            dismissAlert={setShowPopupNoti}
            displayOk={t('profile.i_got_it')}
            onPressOK={setShowPopupNoti}
        />

    return (
        <KeyboardAvoidingView style={[Layout.fill, Gutters.largeHPadding]}>
            {alertNotification()}
            <UIText font="bold" style={styles.textTitle} text={t('login.welcome')} />
            <View style={{ height: verticalScale(30) }} />
            <View style={styles.inputView}>
                <UIText font="bold" style={styles.textEdit} text={t('profile.email')} />
                <InputEdit
                    style={styles.inputText}
                    placeholder={t('profile.email')}
                    placeholderTextColor={Colors.placeholder}
                    pattern={[/^[\w-\.]+@([\w-]+\.)+[\w-]{1,4}$/]}
                    onChangeText={(text, isValids) => {
                        const isValid = isValids?.every((valid) => valid);
                        validatorInputEmail(text, isValid)
                    }}
                />
                {!isValidEmail && email.length > 0 ? <UIText style={styles.textError} text={t('login.email_wrong')} /> : <View style={{ height: verticalScale(26) }} />}
            </View>
            <View style={styles.inputView}>
                <UIText font="bold" style={styles.textEdit} text={t('profile.password')} />
                <InputEdit
                    secureTextEntry
                    style={styles.inputText}
                    placeholder={t('profile.password')}
                    placeholderTextColor={Colors.placeholder}
                    pattern={[/.{6,}/]}
                    onChangeText={(text, isValids) => {
                        const isValid = isValids?.every((valid) => valid);
                        validatorInputPass(text, isValid)
                    }}
                />
                {!isValidPassword && password.length > 0 ? <UIText style={styles.textError} text={t('login.password_length')} /> : <View style={{ height: verticalScale(26) }} />}
            </View>
            <View style={[Layout.fill, Layout.colVCenter]}>
                <TouchableOpacity onPress={() => navigation.navigate(SCREEN.FORGOT_PASS)}>
                    <UIText font="bold" style={styles.forgot} text={t('login.forgot_pass_button')} />
                </TouchableOpacity>
                <View style={styles.viewLogin}>
                    <UIButton onPress={onLogin} isLoading={isLoading} disable={disabled} style={[styles.loginBtn, disabled ? { backgroundColor: Colors.disable } : { backgroundColor: Colors.blueDark }]} text={t('login.login')} />
                    <View style={[Layout.rowCenter, styles.viewRegister]}>
                        <UIText style={styles.registerText} text={t('login.dont_account')} />
                        <TouchableOpacity onPress={() => navigation.navigate(SCREEN.REGISTER)}>
                            <UIText font="bold" style={styles.registerTextBold} text={t('login.register')} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </KeyboardAvoidingView>
    );
};

const mapStateToProps = (state) => ({
    authReducer: state.authReducer,
    profileReducer: state.profileReducer,
});
export default connect(mapStateToProps, {
    actionLoginUser,
    actionGetAccountInfo,
    actionVerifyWallet
})(IndexLoginContainer);

const styles = StyleSheet.create({
    textTitle: {
        fontSize: moderateScale(36),
        color: Colors.title,
        lineHeight: moderateScale(46),
        marginTop: verticalScale(60),
    },
    inputView: {
        width: '100%',
        height: verticalScale(80),
        marginBottom: verticalScale(30),
        justifyContent: 'center',
        flexDirection: 'column',
    },
    textEdit: {
        fontSize: moderateScale(14),
        color: Colors.title,
        marginBottom: verticalScale(5),
    },
    textError: {
        fontSize: moderateScale(9),
        color: 'red',
    },
    inputText: {
        backgroundColor: Colors.gray,
        width: '100%',
        borderRadius: 8,
        height: verticalScale(55),
        fontSize: moderateScale(16),
        paddingStart: verticalScale(20),
        paddingEnd: verticalScale(20),
        fontFamily: fontFamily.regular,
        color: Colors.blueDark,
    },
    forgot: {
        color: Colors.blueLight2,
        fontSize: moderateScale(14),
        marginTop: verticalScale(40),
    },
    viewLogin: {
        width: '100%',
        position: 'absolute',
        bottom: verticalScale(10),
        left: 0,
        right: 0
    },
    loginBtn: {
        width: '100%',
        backgroundColor: Colors.blueDark,
        borderRadius: 8,
        height: verticalScale(52),
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 1,
    },
    viewRegister: {
        marginTop: verticalScale(20),
        marginBottom: verticalScale(30),
    },
    registerText: {
        color: Colors.title,
        fontSize: moderateScale(16),
        marginEnd: verticalScale(5),
    },
    registerTextBold: {
        color: Colors.blueLight2,
        fontSize: moderateScale(14),
        lineHeight: moderateScale(20),
    },
});