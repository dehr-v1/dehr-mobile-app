import React, { useState } from 'react';
import { View, StyleSheet, KeyboardAvoidingView } from 'react-native';
import { useTheme } from '@/theme';
import { HeaderLeft } from '../../../../components/CustomHeader';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { SCREEN } from '../../../../constant';
import { UIButton, UIText } from '../../../../components/elements';
import { fontFamily } from '../../../../theme/Fonts';
import { Colors } from '../../../../theme/Variables';
import { connect } from 'react-redux';
import { actionForgotUser } from '../../duck/actions';
import InputEdit from '../../../../components/InputEdit';
import CustomAlert from '../../../../components/Alert';
import { moderateScale, verticalScale } from '../../../../utils/ScalingUtils';

const IndexForgotContainer = (props) => {
    const { Layout, Gutters } = useTheme();
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { forgotReducer, actionForgotUser } = props;
    const { isLoading } = forgotReducer || false;
    const [disabled, setDisabled] = useState(true);
    const [email, setEmail] = useState('');
    const [isValidEmail, checkEmail] = useState(false);
    const [showPopupNoti, setShowPopupNoti] = useState(false);
    const [forgot, setForgot] = useState({});

    const validatorInputEmail = (text, isValid) => {
        setEmail(text);
        checkEmail(isValid)
        setDisabled(!isValid)
    };

    const onForgot = () => {
        const data = {
            email,
        };
        actionForgotUser({ data }, () => {
            setForgot(data)
            setShowPopupNoti(true)
        });
    };

    const navigationLogin = () => {
        navigation.navigate(SCREEN.LOGIN, forgot);
    }

    const alertNotification = () =>
        <CustomAlert
            isEdittext={false}
            displayTitle={t('profile.notification')}
            displayMsg={t('login.register_active')}
            displayOk={t('profile.i_got_it')}
            visibility={showPopupNoti}
            dismissAlert={setShowPopupNoti}
            onPressOK={navigationLogin}
        />

    return (
        <KeyboardAvoidingView style={[Layout.fill, Gutters.largeHPadding]}>
            <HeaderLeft navigation={navigation} />
            <View style={styles.container}>
                {alertNotification()}
                <UIText font="bold" style={styles.textTitle} text={t('login.forgot_pass')} />
                <View style={styles.inputView}>
                    <UIText style={styles.textEdit} text={t('login.forgot_hint')} />
                    <View style={{ height: verticalScale(20) }} />
                    <UIText font="bold" style={styles.textEdit} text={t('profile.email')} />
                    <InputEdit
                        style={styles.inputText}
                        placeholder={t('profile.email')}
                        placeholderTextColor={Colors.placeholder}
                        pattern={[/^[\w-\.]+@([\w-]+\.)+[\w-]{1,4}$/]}
                        onChangeText={(text, isValids) => {
                            const isValid = isValids?.every((valid) => valid);
                            validatorInputEmail(text, isValid)
                        }}
                    />
                    {!isValidEmail && email.length > 0 ? <UIText style={styles.textError} text={t('login.email_wrong')} /> : <View style={{ height: verticalScale(26) }} />}
                </View>
                <View style={styles.viewLogin}>
                    <UIButton onPress={onForgot} isLoading={isLoading} disable={disabled} style={[styles.loginBtn, disabled ? { backgroundColor: Colors.disable } : { backgroundColor: Colors.blueDark }]} text={t('login.confirm')} />
                </View>
            </View>
        </KeyboardAvoidingView>
    );
};

const mapStateToProps = (state) => ({
    forgotReducer: state.forgotReducer,
});
export default connect(mapStateToProps, { actionForgotUser })(IndexForgotContainer);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: verticalScale(20),
    },
    textTitle: {
        fontSize: moderateScale(36),
        lineHeight: moderateScale(46),
        color: Colors.title,
    },
    inputView: {
        width: '100%',
        height: verticalScale(80),
        marginTop: verticalScale(80),
        marginBottom: verticalScale(30),
        justifyContent: 'center',
        flexDirection: 'column',
    },
    textEdit: {
        fontSize: moderateScale(14),
        color: Colors.title,
        marginBottom: verticalScale(5),
    },
    inputText: {
        backgroundColor: Colors.gray,
        width: '100%',
        borderRadius: 8,
        height: verticalScale(55),
        fontSize: moderateScale(16),
        paddingStart: verticalScale(20),
        paddingEnd: verticalScale(20),
        fontFamily: fontFamily.regular,
        justifyContent: 'center',
        color: Colors.blueDark,
    },
    forgot: {
        color: Colors.blueDark,
        fontSize: moderateScale(14),
        fontFamily: fontFamily.bold,
    },
    viewLogin: {
        width: '100%',
        position: 'absolute',
        bottom: verticalScale(100),
        left: 0,
        right: 0
    },
    loginBtn: {
        width: '100%',
        backgroundColor: Colors.blueDark,
        borderRadius: 8,
        height: verticalScale(50),
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 1,
    },
    loginText: {
        color: 'white',
        fontFamily: fontFamily.black,
    },
    textError: {
        fontSize: moderateScale(9),
        color: 'red',
    },
});
