import CreatePass from './CreatePass';
import ForgotPass from './ForgotPass';
import Register from './Register';

import VerifyEmail from './VerifyEmail';

export { CreatePass, ForgotPass, Register, VerifyEmail };
