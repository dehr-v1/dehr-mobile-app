import React from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet, KeyboardAvoidingView } from 'react-native';
import { useTheme } from '@/theme';
import { HeaderLeft } from '../../../../components/CustomHeader';
import { UIButton, UIText } from '../../../../components/elements';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { moderateScale, verticalScale } from '../../../../utils/ScalingUtils';
import { navigateAndSimpleReset } from '@/navigators/Root';
import { SCREEN } from '../../../../constant';
import { fontFamily } from '../../../../theme/Fonts';
import { Colors } from '../../../../theme/Variables';
import { actionChangePassUser } from '../../duck/actions';
import InputEdit from '../../../../components/InputEdit';

const IndexCreatePassContainer = () => {
    const { Layout, Gutters } = useTheme();
    const navigation = useNavigation();
    const { t } = useTranslation();
    const { changePassReducer, actionChangePassUser } = props;
    const { isLoading } = changePassReducer || false;
    const [disabled, setDisabled] = useState(true);
    const [newPass, setNewPass] = useState('');
    const [verifyPass, setVerifyPass] = useState('');
    const [isValidNewPassword, checkNewPassword] = useState(false);
    const [isValidVerfiyPassword, checkVerifyassword] = useState(false);

    const validatorInputNewPass = (text, isValid) => {
        setNewPass(text);
        checkNewPassword(isValid)
        setDisabled(!(isValidNewPassword && isValidVerfiyPassword && newPass == verifyPass))
    };

    const validatorInputVerifyPass = (text, isValid) => {
        setVerifyPass(text);
        checkVerifyassword(isValid)
        setDisabled(!(isValidNewPassword && isValidVerfiyPassword && newPass == verifyPass))
    };

    const onCreateNewPass = () => {
        const data = {
            password: newPass,
            code: verifyPass,
        };
        actionChangePassUser({ data }, () => {
            navigateAndSimpleReset(SCREEN.LOGIN);
        });
    };

    return (
        <KeyboardAvoidingView style={[Layout.fill, Gutters.largeHPadding]}>
            <HeaderLeft navigation={navigation} />
            <View style={styles.container}>
                <UIText font="bold" style={styles.textTitle} text={t('login.create_pass')} />
                <View style={styles.inputView}>
                    <UIText font="bold" style={styles.textEdit} text={t('login.new_pass')} />
                    <InputEdit
                        secureTextEntry
                        style={styles.inputText}
                        placeholder={t('login.new_pass')}
                        placeholderTextColor={Colors.placeholder}
                        pattern={[/.{6,}/]}
                        onChangeText={(text, isValids) => {
                            const isValid = isValids?.every((valid) => valid);
                            validatorInputNewPass(text, isValid)
                        }}
                    />
                    {!isValidPassword && password.length > 0 ? <UIText style={styles.textError} text={t('login.password_length')} /> : <View style={{ height: verticalScale(26) }} />}
                </View>
                <View style={styles.inputView}>
                    <UIText font="bold" style={styles.textEdit} text={t('login.verify_pass')} />
                    <InputEdit
                        secureTextEntry
                        style={styles.inputText}
                        placeholder={t('login.verify_pass')}
                        placeholderTextColor={Colors.placeholder}
                        pattern={[/.{6,}/]}
                        onChangeText={(text, isValids) => {
                            const isValid = isValids?.every((valid) => valid);
                            validatorInputVerifyPass(text, isValid)
                        }}
                    />
                    {!isValidVerfiyPassword && verifyPass.length > 0 ? <UIText style={styles.textError} text={t('login.password_length')} /> : <View style={{ height: verticalScale(26) }} />}
                </View>
                <View style={styles.viewLogin}>
                    <UIButton onPress={!disabled && onCreateNewPass} isLoading={isLoading} disable={disabled} style={[styles.loginBtn, disabled ? { backgroundColor: Colors.disable } : { backgroundColor: Colors.blueDark }]} text={t('login.create_now')} />
                </View>
            </View>
        </KeyboardAvoidingView>
    );
};

const mapStateToProps = (state) => ({
    changePassReducer: state.changePassReducer,
});
export default connect(mapStateToProps, { actionChangePassUser })(IndexCreatePassContainer);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: verticalScale(20),
    },
    textTitle: {
        width: '70%',
        fontSize: moderateScale(36),
        color: Colors.title,
    },
    inputView: {
        width: '100%',
        height: verticalScale(80),
        marginBottom: verticalScale(30),
        justifyContent: 'center',
        flexDirection: 'column',
    },
    textEdit: {
        fontSize: moderateScale(14),
        color: Colors.title,
        marginBottom: verticalScale(5),
    },
    inputText: {
        backgroundColor: Colors.gray,
        width: '100%',
        borderRadius: 8,
        height: verticalScale(55),
        fontSize: moderateScale(16),
        paddingStart: verticalScale(20),
        paddingEnd: verticalScale(20),
        fontFamily: fontFamily.regular,
        color: 'white',
    },
    loginBtn: {
        width: '100%',
        backgroundColor: Colors.blueDark,
        borderRadius: 8,
        height: verticalScale(50),
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: verticalScale(250),
        opacity: 1,
    },
    viewLogin: {
        width: '100%',
        position: 'absolute',
        bottom: verticalScale(100),
        left: 0,
        right: 0
    }
});
