import React, { useEffect, useState } from 'react';
import { View, StyleSheet, ActivityIndicator, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import { useTheme } from '@/theme';
import { HeaderLeft } from '../../../../components/CustomHeader';
import { UIText } from '../../../../components/elements';
import { moderateScale, verticalScale } from '../../../../utils/ScalingUtils';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import { SCREEN } from '../../../../constant';
import { Colors } from '../../../../theme/Variables';
import useCountdown from '../../../../theme/hooks/useCountdown';
import { connect } from 'react-redux';
import { actionRegisterUser, actionVerifyUser, actionReSendCode } from '../../duck/actions';

const IndexVerifyContainer = (props) => {
    const { Layout, Gutters } = useTheme();
    const navigation = useNavigation();
    const route = useRoute();
    const { t } = useTranslation();
    const { verifyReducer, actionVerifyUser, registerReducer, actionReSendCode } = props;
    const { isLoading } = verifyReducer || registerReducer || false;
    const [disabled, setDisabled] = useState(true);
    const { secondsLeft, restartCountdown } = useCountdown(30); // 180
    const [timerSecond, setTimerSecond] = useState(0);
    const [timerMinute, setTimerMinute] = useState(0);

    useEffect(() => {
        const divisor_for_minutes = secondsLeft % (60 * 60);
        const minutes = Math.floor(divisor_for_minutes / 60);
        const divisor_for_seconds = divisor_for_minutes % 60;
        const seconds = Math.ceil(divisor_for_seconds);
        setTimerMinute(minutes);
        setTimerSecond(seconds);
        if (minutes == 0 && seconds == 0) {
            setDisabled(false);
        }
    });

    const resetOTP = () => {
        restartCountdown(true);
        setDisabled(true);
        const data = route.params;
        actionReSendCode({ data }, () => {});
    };

    const onVerify = (code) => {
        setDisabled(true);
        const data = {
            code,
        };
        actionVerifyUser({ data }, () => {
            if (route.params.password == null) {
                navigation.navigate(SCREEN.CREATE_PASS);
            } else {
                navigation.navigate(SCREEN.LOGIN);
            }
        });
    };

    return (
        <KeyboardAvoidingView style={[Layout.fill, Gutters.largeHPadding]}>
            <HeaderLeft navigation={navigation} />
            <View style={styles.container}>
                <UIText font="bold" style={styles.textTitle} text={t('login.verify_email')} />
                <View style={{ height: verticalScale(30) }} />
                <UIText style={styles.text} text={t('login.four_code')} />
                <UIText style={styles.textEmail} text={route.params.email} />
                <View style={{ height: verticalScale(20) }} />
                <OTPInputView
                    style={styles.otpInput}
                    pinCount={4}
                    autoFocusOnLoad
                    codeInputFieldStyle={styles.underlineStyleBase}
                    codeInputHighlightStyle={styles.underlineStyleHighLighted}
                    onCodeFilled={(code) => onVerify(code)}
                />
                <View style={{ height: verticalScale(30) }} />
                <View style={[Layout.fill, Layout.row, Layout.justifyContentBetween]}>
                    <TouchableOpacity onPress={resetOTP}>
                        <UIText
                            font={disabled ? '' : 'bold'}
                            style={(styles.btnResendEnable, { color: disabled ? Colors.disable : Colors.blueDark })}
                            text={t('login.resend_code')}
                        />
                    </TouchableOpacity>
                    <UIText
                        style={styles.timer}
                        text={
                            (timerMinute < 10 ? '0' + timerMinute : timerMinute) +
                            ':' +
                            (timerSecond < 10 ? '0' + timerSecond : timerSecond)
                        }
                    />
                </View>
                {isLoading && <ActivityIndicator color={Colors.blueDark} size="large" style={[Gutters.smallMargin]} />}
            </View>
        </KeyboardAvoidingView>
    );
};

const mapStateToProps = (state) => ({
    registerReducer: state.registerReducer,
    verifyReducer: state.verifyReducer,
});
export default connect(mapStateToProps, {
    actionRegisterUser,
    actionVerifyUser,
    actionReSendCode,
})(IndexVerifyContainer);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: verticalScale(20),
    },
    textTitle: {
        fontSize: moderateScale(36),
        lineHeight: moderateScale(46),
        color: Colors.title,
    },
    text: {
        color: Colors.blueDark,
        fontSize: moderateScale(16),
    },
    textEmail: {
        color: Colors.blueLight2,
        fontSize: moderateScale(16),
    },
    btnResendEnable: {
        color: Colors.blueDark,
        fontSize: moderateScale(14),
    },
    timer: {
        color: '#3e4a7e',
        fontSize: moderateScale(16),
    },
    otpInput: {
        width: '100%',
        height: verticalScale(100),
    },
    borderStyleHighLighted: {
        borderColor: Colors.blueDark,
    },
    underlineStyleBase: {
        width: verticalScale(70),
        height: verticalScale(90),
        borderWidth: 1,
        fontSize: moderateScale(40),
        backgroundColor: Colors.gray,
        color: Colors.blueDark,
    },
    underlineStyleHighLighted: {
        borderColor: Colors.blueDark,
    },
});
