import React, { useState } from 'react';
import { View, StyleSheet, KeyboardAvoidingView, Text, TouchableOpacity, Alert } from 'react-native';
import { useTheme } from '@/theme';
import { UIButton, UIText } from '../../../../components/elements';
import { moderateScale, verticalScale } from '../../../../utils/ScalingUtils';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { SCREEN } from '../../../../constant';
import { fontFamily } from '../../../../theme/Fonts';
import { Colors } from '../../../../theme/Variables';
import { connect } from 'react-redux';
import { actionRegisterUser } from '../../duck/actions';
import InputEdit from '../../../../components/InputEdit';
import CustomAlert from '../../../../components/Alert';

const IndexRegisterContainer = (props) => {
    const { Layout, Gutters } = useTheme();
    const navigation = useNavigation();
    const { t } = useTranslation();
    const { registerReducer, actionRegisterUser } = props;
    const { isLoading } = registerReducer || false;
    const [disabled, setDisabled] = useState(true);
    const [username, setUserName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [register, setRegister] = useState({});
    const [isValidUsername, checkUsername] = useState(false);
    const [isValidEmail, checkEmail] = useState(false);
    const [isValidPassword, checkPassword] = useState(false);
    const [showPopupNoti, setShowPopupNoti] = useState(false);

    const validatorInputUsername = (text, isValid) => {
        setUserName(text);
        checkUsername(isValid)
        setDisabled(!isValid || !isValidPassword || !isValidEmail);
    };

    const validatorInputEmail = (text, isValid) => {
        setEmail(text);
        checkEmail(isValid)
        setDisabled(!isValid || !isValidPassword || !isValidUsername);
    };

    const validatorInputPassword = (text, isValid) => {
        setPassword(text);
        checkPassword(isValid)
        setDisabled(!isValid || !isValidEmail || !isValidUsername);
    };

    const onRegister = () => {
        const data = {
            username,
            email,
            password,
        };
        actionRegisterUser({ data }, () => {
            setRegister(data)
            setShowPopupNoti(true)
        });
    };

    const navigationLogin = () => {
        navigation.navigate(SCREEN.LOGIN, register);
    }

    const alertNotification = () =>
        <CustomAlert
            isEdittext={false}
            displayTitle={t('profile.notification')}
            displayMsg={t('login.register_active')}
            displayOk={t('profile.i_got_it')}
            visibility={showPopupNoti}
            dismissAlert={setShowPopupNoti}
            onPressOK={navigationLogin}
        />

    return (
        <KeyboardAvoidingView style={[Layout.fill, Gutters.largeHPadding]}>
            <View style={styles.container}>
                {alertNotification()}
                <UIText font="bold" style={styles.textTitle} text={t('login.create_acc')} />
                <View style={{ height: verticalScale(30) }} />
                <View style={styles.inputView}>
                    <UIText font="bold" style={styles.textEdit} text={t('profile.username')} />
                    <InputEdit
                        style={styles.inputText}
                        placeholder={t('profile.username')}
                        placeholderTextColor={Colors.placeholder}
                        pattern={[/.{3,}/]}
                        onChangeText={(text, isValids) => {
                            const isValid = isValids?.every((valid) => valid);
                            validatorInputUsername(text, isValid)
                        }}
                    />
                    {!isValidUsername && username.length > 0 ? <UIText style={styles.textError} text={t('login.user_lenght')} /> : <View style={{ height: verticalScale(26) }} />}
                </View>
                <View style={styles.inputView}>
                    <UIText font="bold" style={styles.textEdit} text={t('profile.email')} />
                    <InputEdit
                        style={styles.inputText}
                        placeholder={t('profile.email')}
                        placeholderTextColor={Colors.placeholder}
                        pattern={[/^[\w-\.]+@([\w-]+\.)+[\w-]{1,4}$/]}
                        onChangeText={(text, isValids) => {
                            const isValid = isValids?.every((valid) => valid);
                            validatorInputEmail(text, isValid)
                        }}
                    />
                    {!isValidEmail && email.length > 0 ? <UIText style={styles.textError} text={t('login.email_wrong')} /> : <View style={{ height: verticalScale(26) }} />}
                </View>
                <View style={styles.inputView}>
                    <UIText font="bold" style={styles.textEdit} text={t('profile.password')} />
                    <InputEdit
                        style={styles.inputText}
                        placeholder={t('profile.password')}
                        placeholderTextColor={Colors.placeholder}
                        pattern={[/.{6,}/]}
                        onChangeText={(text, isValids) => {
                            const isValid = isValids?.every((valid) => valid);
                            validatorInputPassword(text, isValid)
                        }}
                    />
                    {!isValidPassword && password.length > 0 ? <UIText style={styles.textError} text={t('login.password_length')} /> : <View style={{ height: verticalScale(26) }} />}
                </View>
                <View style={styles.viewLogin}>
                    <Text style={styles.textNormal}>
                        {t('login.term1')}
                        <Text style={styles.policy}>{t('login.term2')}</Text>
                        {t('login.term3')}
                    </Text>
                    <UIButton onPress={onRegister} isLoading={isLoading} disable={disabled} style={[styles.loginBtn, disabled ? { backgroundColor: Colors.disable } : { backgroundColor: Colors.blueDark }]} text={t('login.register')} />
                    <View style={[Layout.fill, Layout.rowCenter]}>
                        <UIText style={styles.registerText} text={t('login.have_account')} />
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <UIText font="bold" style={styles.registerTextBold} text={t('login.login')} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </KeyboardAvoidingView>
    );
};

const mapStateToProps = (state) => ({
    registerReducer: state.registerReducer,
});
export default connect(mapStateToProps, { actionRegisterUser })(IndexRegisterContainer);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: verticalScale(40),
    },
    textTitle: {
        fontSize: moderateScale(36),
        lineHeight: moderateScale(46),
        color: Colors.title,
    },
    inputView: {
        width: '100%',
        height: verticalScale(80),
        marginBottom: verticalScale(30),
        justifyContent: 'center',
        flexDirection: 'column',
    },
    textEdit: {
        fontSize: moderateScale(14),
        color: Colors.title,
        marginBottom: verticalScale(5),
    },
    inputText: {
        backgroundColor: Colors.gray,
        width: '100%',
        borderRadius: 8,
        height: verticalScale(55),
        fontSize: moderateScale(16),
        paddingStart: verticalScale(20),
        paddingEnd: verticalScale(20),
        fontFamily: fontFamily.regular,
        color: Colors.blueDark,
    },
    textNormal: {
        fontFamily: fontFamily.medium,
        color: Colors.title,
        fontSize: moderateScale(12),
        textAlign: 'center'
    },
    policy: {
        fontFamily: fontFamily.medium,
        color: Colors.blueDark,
        fontSize: moderateScale(12),
    },
    loginBtn: {
        width: '100%',
        backgroundColor: Colors.blueDark,
        borderRadius: 8,
        height: verticalScale(50),
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 1,
        marginTop: verticalScale(20),
        marginBottom: verticalScale(20)
    },
    viewTerm: {
        width: '60%'
    },
    viewLogin: {
        width: '100%',
        position: 'absolute',
        bottom: verticalScale(40),
        left: 0,
        right: 0
    },
    loginText: {
        color: 'white',
        fontFamily: fontFamily.black,
    },
    registerText: {
        color: Colors.title,
        fontSize: moderateScale(16),
        marginEnd: verticalScale(5),
    },
    registerTextBold: {
        color: Colors.blueDark,
        fontSize: moderateScale(16),
    },
    textError: {
        fontSize: moderateScale(9),
        color: 'red',
    },
});
