import React, { useEffect } from 'react';
import { View } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { SCREEN } from '../../constant';
import { getCurrentUser, getFirstTime, setFirstPopUp } from '../../services/persistence/store.config';
import { navigateAndSimpleReset } from '@/navigators/Root';
import { setUser } from '../Login/duck/actions';
import { connect } from 'react-redux';

const SplashPage = (props) => {
    const checkIsLogin = async () => {
        let screen = SCREEN.WELCOME;
        const isFirstTime = (await getFirstTime()) ?? 'true';
        if (isFirstTime != 'true') {
            screen = SCREEN.WELCOME;
        } else {
            const user = (await getCurrentUser()) || null;
            if (user != null) {
                props.setUser(JSON.parse(user));
                screen = SCREEN.MAIN;
            } else {
                screen = SCREEN.LOGIN;
            }
        }
        setTimeout(() => {
            SplashScreen.hide();
            navigateAndSimpleReset(screen);
        }, 30);
    };

    useEffect(() => {
        setFirstPopUp('true');
        checkIsLogin();
    }, []);

    return <View />;
};

const mapStateToProps = (state) => ({});
export default connect(mapStateToProps, {
    setUser,
})(SplashPage);
