export const CATEGORY = {
    ABOUT: 'ABOUT',
    EXPERIENCE: 'EXPERIENCE',
    EDUCATION: 'EDUCATION',
    SKILL: 'SKILL',
    NETWORK: 'NETWORK',
};

export const BASE_IMAGE_URL = 'https://..../static/';

export const SERVICE_URL = {
    DEHR_HOST: 'https://....',
    GOOGLE_MAP_URL: 'https://maps.googleapis.com/maps/api/',
};

export const MISSION_TYPE = {
    KYC: 'kyc',
    PROFILE: 'profile',
    WALLET: 'wallet',
    ENDORSE: 'endorse',
    FRIEND: 'friend',
    PRICE: 'price',
};

export const TRANSACTION_TYPE = {
    WITHDRAW: 'withdraw',
    DEPOSITED: 'deposited',
};

export const NOTIFICATION_TYPE = {
    NETWORK: 'NETWORK_REQUEST',
    PROFILE: 'PROFILE_REQUEST',
    WALLET: 'WALLET',
    KYC: 'KYC',
    MISSION: 'MISSION',
    REFERRAL: 'REFERRAL',
    DAILY_QUEST: 'DAILY_QUEST',
    JOB_APPLICATION: 'JOB_APPLICATION',
};

export const NOTIFICATION_TOPIC = {
    BREAKING_NEW: 'breaking_news',
};

export const TRANSACTION_FILTER = {
    ALL: 0,
    RECEIVE: 1,
    WITHDRAW: 2,
};

export const KYC_STATUS = {
    UNVERIFIED: 'unverified',
    VERIFYING: 'verifying',
    VERIFIED: 'verified',
};

export const NETWORK_STATUS = {
    REQUESTED: 'requested',
    ACCEPTED: 'accepted',
    REJECTED: 'rejected',
};

export const LOCK_INFO = {
    BUY: 1,
    NO_BUY: 0,
};

export const STATUS_INFO = {
    OPEN: 0,
    CANCEL: 1,
    ACCEPTED: 2,
    REJECTED: 2,
};

export const ROLE_INFO = {
    PRO: 'pro',
    MEMBER: 'member',
};

export const SKILL_LEVEL = [
    { label: 'Normal', value: 1 },
    { label: 'Good', value: 2 },
    { label: 'Very Good', value: 3 },
    { label: 'Expert', value: 4 },
    { label: 'Very Expert', value: 5 },
];
export const LANGUAGE_LEVEL = [
    { label: 'Elementary proficiency', value: 1 },
    { label: 'Limited working proficency', value: 2 },
    { label: 'Full professional proficency', value: 3 },
    { label: 'Professional working proficency', value: 4 },
    { label: 'Native or bilingual proficency', value: 5 },
];

export const RPC_ENDPOINT = 'https://dev-1.kardiachain.io';
export const DEHR_CONTRACT_ADDRESS = '0xXXXXXXXXXXXXXX';
export const EXCHANGE_DATA_CONTRACT_ADDRESS = '0xexxxxxxxxxxxXXXX';
export const DEHR_WEBSITE = 'dehr.network';

export const MEDIA_TYPE = {
    LINK: 'link',
    DOCUMENT: 'document',
    FILE: 'file',
};
