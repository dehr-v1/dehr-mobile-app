import { combineReducers } from 'redux';
import { authReducer } from '../../containers/Login/duck/reducers';
import { profileReducer } from '../../containers/Profile/duck/reducers';
import { walletReducer } from '../../containers/Wallet/duck/reducers';
import { missionReducer } from '../../containers/Mission/duck/reducers';
import { notificationReducer } from '../../containers/Notification/duck/reducers';
import { chatReducer, networkReducer } from '../../containers/Chat/duck/reducers';
import { homeReducer, searchReducer } from '../../containers/Home/duck/reducers';
import { jobsReducer, companiesReducer } from '../../containers/Jobs/duck/reducers';

export default combineReducers({
    authReducer,
    profileReducer,
    chatReducer,
    networkReducer,
    walletReducer,
    missionReducer,
    notificationReducer,
    homeReducer,
    searchReducer,
    jobsReducer,
    companiesReducer
});
