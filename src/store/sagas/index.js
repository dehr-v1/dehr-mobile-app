import { all, fork } from 'redux-saga/effects';
import { chatSaga, networkSaga } from '../../containers/Chat/duck/sagas';
import { homeSaga, searchSaga } from '../../containers/Home/duck/sagas';
import { companiesSaga, jobsSaga } from '../../containers/Jobs/duck/sagas';
import { authSaga } from '../../containers/Login/duck/sagas';
import { missionSaga } from '../../containers/Mission/duck/sagas';
import { profileSaga } from '../../containers/Profile/duck/sagas';
import { walletSaga } from '../../containers/Wallet/duck/sagas';
import { notificationSaga } from '../../containers/Notification/duck/sagas';
// import * as chatSaga from '../../containers/Chat/duck/sagas/chat.saga';
// import * as networkSaga from '../../containers/Chat/duck/sagas/network.saga';
// import * as homeSaga from '../../containers/Home/duck/sagas/home.saga';
// import * as searchSaga from '../../containers/Home/duck/sagas/search.saga';
// import * as companiesSaga from '../../containers/Jobs/duck/sagas';
// import * as jobsSaga from '../../containers/Jobs/duck/sagas/jobs.saga';
// import * as authSaga from '../../containers/Login/duck/sagas/auth.saga';
// import * as missionSaga from '../../containers/Mission/duck/sagas/mission.sagas';
// import * as profileSaga from '../../containers/Profile/duck/sagas/profile.saga';
// import * as walletSaga from '../../containers/Wallet/duck/sagas/wallet.saga';

export default function* rootSaga() {
    yield all(
        // [
        //     ...Object.values(chatSaga),
        //     ...Object.values(networkSaga),
        //     ...Object.values(homeSaga),
        //     ...Object.values(searchSaga),
        //     ...Object.values(authSaga),
        //     ...Object.values(missionSaga),
        //     ...Object.values(profileSaga),
        //     ...Object.values(walletSaga),
        //     ...Object.values(jobsSaga),
        //     ...Object.values(companiesSaga),
        // ].map(fork)
        [
            chatSaga(),
            networkSaga(),
            homeSaga(),
            searchSaga(),
            authSaga(),
            missionSaga(),
            notificationSaga(),
            profileSaga(),
            walletSaga(),
            jobsSaga(),
            companiesSaga(),
        ]
    );
}
