/**
 * This file contains the application's variables.
 *
 * Define color, sizes, etc. here instead of duplicating them throughout the components.
 * That allows to change them more easily later on.
 */

/**
 * Colors
 */
export const Colors = {
    // Example colors:
    transparent: 'rgba(0,0,0,0)',
    inputBackground: '#FFFFFF',
    white: '#ffffff',
    text: '#212529',
    primary: '#E14032',
    success: '#28a745',
    error: '#dc3545',
    black: '#000000',
    title: '#070e2f',
    input: '#363947',
    blueDark: '#0f1d5e',
    blueLight: '#00a3ff',
    blueLight2: '#1a31a0',
    blueLight3: '#00a2ff',
    blueLight4: '#30e4eb',
    blueLight5: '#edf7ff',
    gray: '#f6f8fa',
    message: '#666769',
    date: '#141414',
    border: '#e7e8ee',
    lightGray: '#929ca4',
    lightGray2: '#e2e2e2',
    lightGray3: '#e4e4e4',
    lightGray4: '#140f1d5e',
    lightGray4: '#9ba4ac',
    lightGray5: '#949ea6',
    lightYellow: '#ff9d19',
    hint: '#3e464d',
    disable: '#b7bbce',
    disable2: '#d3dae1',
    placeholder: '#b2beca',
    green: '#18d359',
    grayMessage: '#f0f1f5',
    blue: '#162b8a',
    grayInput: '#ecedf2',
    orange: '#f59624',
    yellow: '#ffc700',
    lightBlue: '#30a7eb',
    lightGreen: '#00af83',
    lightGreen2: '#28d359',
    grayHiring: '#f3f4f7',
    darkGray: '#9fa4be',
    darkGray2: '#394653',
    bgSearch: '#000000',
    red: '#d52d3a'
};

export const NavigationColors = {
    primary: Colors.primary,
};

/**
 * FontSize
 */
export const FontSize = {
    small: 16,
    regular: 20,
    large: 40,
};

/**
 * Metrics Sizes
 */
const tiny = 5; // 10
const small = tiny * 2; // 10
const regular = tiny * 3; // 15
const large = regular * 2; // 30
export const MetricsSizes = {
    tiny,
    small,
    regular,
    large,
};
