import React from 'react';
import { Provider } from 'react-redux';
import { ApplicationNavigator } from '@/navigators';
import './translations';
import { store } from './store';
import { RootSiblingParent } from 'react-native-root-siblings';
import 'react-native-gesture-handler';

const App = () => (
    <Provider store={store}>
        <RootSiblingParent>
            <ApplicationNavigator />
        </RootSiblingParent>
    </Provider>
);

export default App;
