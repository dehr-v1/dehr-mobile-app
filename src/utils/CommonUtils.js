import React from 'react';
import moment from 'moment';
import { Text } from 'react-native';
import Web3 from 'web3';
const web3 = new Web3();

export const convertFromWei = (amount) => {
    return web3.utils.fromWei(amount || '0', 'Ether');
};

export const formatShortTime = (input) => {
    const date = moment(input);
    const nowDate = moment();
    const minutes = nowDate.diff(date, 'minutes');
    const hours = nowDate.diff(date, 'hours');
    const days = nowDate.diff(date, 'days');
    if (minutes < 1) return date.startOf('seconds').fromNow();
    if (hours < 1) return date.startOf('minutes').fromNow();
    if (days < 1) return date.startOf('hours').fromNow();
    if (days < 3) return date.startOf('day').fromNow();
    return date.format('hh:mm - MMM DD, YYYY');
};

export const getHighlightedText = (text, highlight) => {
    if (!highlight) {
        return <Text>{text}</Text>;
    }
    return (
        <Text>
            {text.split(highlight).reduce((prev, current, i) => {
                if (!i) {
                    return [current];
                }
                return prev.concat(
                    <Text key={highlight + current} style={{ fontWeight: 'bold' }}>
                        {highlight}
                    </Text>,
                    current
                );
            }, [])}
        </Text>
    );
};

export const convertPrice = (amount) => {
    return Number(amount).toFixed(2);
};
