import React, { useState, useRef } from 'react';

import { Modal, View, StyleSheet } from 'react-native';
import { UIButton, UIText } from '../components/elements';
import { verticalScale, moderateScale, width } from '../utils/ScalingUtils';
import { Colors } from '../theme/Variables';
import { useTranslation } from 'react-i18next';
import PhoneInput from 'react-native-phone-number-input';
import { fontFamily } from '../theme/Fonts';

export default function CustomAlert({
  isEdittext,
  isRadioButton,
  displayTitle,
  displayMsg,
  displayName,
  visibility,
  dismissAlert,
  resultPhone,
  displayCancel,
  displayOk,
  onPressCancel,
  onPressOK,
}) {
  const { t } = useTranslation();
  const [phone, setPhone] = useState('');
  const phoneInput = useRef(null);

  const validatorInputPhone = (text, isValid) => {
    setPhone(text)
    resultPhone(text)
  };

  return (
    <View>
      <Modal
        visible={visibility}
        animationType={'fade'}
        transparent={true}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(52, 52, 52, 0.8)',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View
            style={styles.viewDialog}>
            <UIText font="bold" style={styles.txtTitle} text={displayTitle} />
            <UIText style={styles.txtContent} text={displayMsg} />
            {displayName ? <UIText font="bold" style={styles.txtContent} text={displayName} /> : <View />}
            {isEdittext && (
              // <View>
              //   <View style={styles.inputView}>
              //     <UIText font="bold" style={styles.textEdit} text={t('profile.email')} />
              //     <InputEdit
              //       style={styles.inputText}
              //       placeholder={t('profile.email')}
              //       placeholderTextColor={Colors.placeholder}
              //       pattern={[/^[\w-\.]+@([\w-]+\.)+[\w-]{1,4}$/]}
              //       onChangeText={(text, isValids) => {
              //         const isValid = isValids?.every((valid) => valid);
              //         validatorInputEmail(text, isValid)
              //       }}
              //     />
              //     {!isValidEmail && email.length > 0 ? <UIText style={styles.textError} text={t('login.email_wrong')} /> : <View style={{ height: verticalScale(26) }} />}
              //   </View>
              //   <View style={styles.btnContainer}>
              //     <UIButton
              //       disable={disabled}
              //       style={{ width: verticalScale(130) }}
              //       onPress={onPressOK}
              //       text={t('login.verify_now')}
              //     />
              //   </View>
              // </View>
              <View style={styles.inputView}>
                <UIText font="bold" style={styles.textEdit} text={t('profile.phone_number')} />
                <PhoneInput
                  ref={phoneInput}
                  defaultValue={phone}
                  defaultCode="VN"
                  layout="first"
                  withShadow
                  autoFocus
                  textContainerStyle={styles.textInput}
                  onChangeFormattedText={(text) => {
                    validatorInputPhone(text)
                  }}
                />
              </View>
            )}
            {isRadioButton && (
              <View>
                <UIRadio text={t('search.advance_relevant')} isCheck={true} onPressRadio={() => { }} />
                <UIRadio text={t('search.advance_relevant')} isCheck={false} onPressRadio={() => { }} />
              </View>
            )}
            <View style={styles.btnContainer}>
              {(displayCancel && onPressCancel) ? <UIButton
                type={isRadioButton ? '' : "BorderRed"}
                style={{ width: verticalScale(130) }}
                onPress={onPressCancel}
                text={displayCancel}
              /> : <View />}

              {(displayOk && onPressOK) ? <UIButton
                style={{ width: verticalScale(130) }}
                onPress={onPressOK}
                text={displayOk}
              /> : <View />}
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  viewDialog: {
    alignItems: 'center',
    backgroundColor: 'white',
    width: '90%',
    borderWidth: 1,
    borderColor: '#fff',
    borderRadius: 7,
    elevation: 10,
    padding: verticalScale(20),
  },
  btnContainer: {
    width: '100%',
    flexDirection: 'row',
    marginTop: verticalScale(20),
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtTitle: {
    fontSize: moderateScale(20),
    color: Colors.blueDark,
    marginBottom: verticalScale(20),
  },
  txtContent: {
    width: '95%',
    fontSize: moderateScale(16),
    color: Colors.blueDark
  },
  inputView: {
    width: width - verticalScale(100),
    height: verticalScale(150),
    marginBottom: verticalScale(10),
    flexDirection: 'column'
  },
  textEdit: {
    fontSize: moderateScale(14),
    color: Colors.title,
    marginBottom: verticalScale(5),
  },
  textError: {
    fontSize: moderateScale(9),
    color: 'red',
  },
  textInputPhone: {
    height: verticalScale(50),
    backgroundColor: Colors.gray,
    borderRadius: verticalScale(6),
    borderColor: Colors.border,
    borderWidth: 1,
    paddingTop: verticalScale(15),
    paddingRight: verticalScale(30),
    paddingBottom: Platform.OS == 'ios' ? verticalScale(15) : verticalScale(10),
    paddingLeft: verticalScale(15),
    color: Colors.blueDark,
    fontSize: verticalScale(16),
    fontFamily: fontFamily.regular,
    lineHeight: Platform.OS == 'ios' ? verticalScale(20) : verticalScale(25),
    flexDirection: 'row',
    alignItems: 'center',
  },
})