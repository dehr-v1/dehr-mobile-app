import React from 'react';
import { Image, ImageBackground, TouchableOpacity, StyleSheet } from 'react-native';
import { useTheme } from '@/theme';
import { verticalScale } from '../../utils/ScalingUtils';
import { BASE_IMAGE_URL } from '../../constant/value.constant';

const Avatar = (props) => {
    const { Images } = useTheme();
    const { url, defaultImage, onPressAvatar } = props;

    const isNumeric = (value) => {
        return /^-?\d+$/.test(value);
    };

    const placeholder = defaultImage != null ? defaultImage : Images.profile.ic_avatar;

    return (
        <TouchableOpacity activeOpacity={onPressAvatar ? 0.7 : 1} onPress={() => {
            onPressAvatar && onPressAvatar()
        }}>
            {isNumeric(url) && (
                <Image
                    style={[styles.container]}
                    imageStyle={[styles.container, styles.imageContainer]}
                    source={url || placeholder}
                />
            )}
            {!isNumeric(url) && (
                <ImageBackground
                    style={[styles.container]}
                    imageStyle={[styles.container, styles.imageContainer]}
                    source={url != '' && url != null && url != BASE_IMAGE_URL ? { uri: url } : placeholder}
                />
            )}
        </TouchableOpacity>
    );
};

export default Avatar;

const styles = StyleSheet.create({
    container: {
        width: verticalScale(40),
        height: verticalScale(40),
    },
    imageContainer: {
        borderRadius: verticalScale(40),
    },
    imageContainerBot: {
        marginTop: verticalScale(-30),
    },
});
