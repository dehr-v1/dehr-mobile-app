import React from 'react';
import { TouchableOpacity, View, StyleSheet } from 'react-native';
import { verticalScale } from '../../utils/ScalingUtils';
import { UIText } from '../../components/elements';
import { Colors } from '../../theme/Variables';

const UIRadio = (props) => {
    const { isCheck, text, onPressRadio } = props;
    return (
        <TouchableOpacity onPress={onPressRadio} style={styles.checkboxContainer}>
            <View style={[styles.checkbox, isCheck && styles.checkboxSelect]}>
                {isCheck && <View style={styles.select} />}
            </View>
            <UIText text={text} />
        </TouchableOpacity>
    );
};

export default UIRadio;

const styles = StyleSheet.create({
    checkbox: {
        width: verticalScale(20),
        height: verticalScale(20),
        borderRadius: verticalScale(10),
        borderWidth: verticalScale(3),
        borderColor: Colors.border,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: verticalScale(15),
    },
    checkboxContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: verticalScale(50),
        marginTop: verticalScale(5),
        marginBottom: verticalScale(15),
    },
    checkboxSelect: {
        borderColor: Colors.blue,
        padding: verticalScale(6),
    },
    select: {
        width: verticalScale(10),
        height: verticalScale(10),
        borderRadius: verticalScale(5),
        backgroundColor: Colors.blue,
    },
});
