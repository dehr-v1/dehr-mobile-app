import React from 'react';
import { Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { Colors } from '../../theme/Variables';
import { moderateScale } from '../../utils/ScalingUtils';
import { fontFamily } from '../../theme/Fonts';

const styles = StyleSheet.create({
    text: {
        color: Colors.title,
        fontFamily: fontFamily.regular,
        fontSize: moderateScale(16),
        lineHeight: moderateScale(26),
    },
});

const getFontFamily = (font) => {
    switch (font) {
        case 'medium':
            return fontFamily.medium;
        case 'bold':
            return fontFamily.bold;
        case 'light':
            return fontFamily.light;
        case 'black':
            return fontFamily.black;
        default:
            return fontFamily.regular;
    }
};
const AppText = (props) => {
    const { style, font, ...others } = props;

    return (
        <Text
            onPress={props.onPress}
            allowFontScaling={false}
            style={[styles.text, style, { fontFamily: getFontFamily(font) }]}
            {...others}
        >
            {props.text || props.children}
        </Text>
    );
};

AppText.defaultProps = {
    style: {},
    text: '',
    font: '',
};

AppText.propTypes = {
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    text: PropTypes.string,
    font: PropTypes.string,
};

export default AppText;
