import React, { useRef } from 'react';
import { View, TextInput, StyleSheet, Image, TouchableOpacity, Platform } from 'react-native';
import { fontFamily } from '../../theme/Fonts';
import { Colors } from '../../theme/Variables';
import { verticalScale } from '../../utils/ScalingUtils';
import { useTheme } from '@/theme';
import { UIText } from '.';

const TextInputField = (props) => {
    const { onChangeText, searchStyle, inputStyle, underline, value, isError, error, editable = true, ...other } = props;
    const { Images } = useTheme();
    const textInputRef = useRef(null);
    const textUnderlineRef = useRef(null);

    const onPressClear = () => {
        onChangeText && onChangeText('');
        if (textInputRef.current) {
            textInputRef.current.clear();
        }
        if (textUnderlineRef.current) {
            textUnderlineRef.current.clear();
        }
    };

    return (
        <View style={(styles.container, searchStyle)}>
            {underline ? (
                <TextInput
                    ref={textUnderlineRef}
                    style={[styles.input, inputStyle]}
                    value={value}
                    editable={editable}
                    placeholderTextColor={Colors.lightGray}
                    onChangeText={onChangeText}
                    underlineColorAndroid="transparent"
                    {...other}
                />
            ) : (
                <TextInput
                    ref={textInputRef}
                    style={[styles.input, inputStyle]}
                    value={value}
                    editable={editable}
                    placeholderTextColor={Colors.lightGray}
                    onChangeText={onChangeText}
                    {...other}
                />
            )}
            {value != '' && editable && (
                <TouchableOpacity onPress={onPressClear} style={styles.icClearContainer}>
                    <Image style={styles.icClear} source={Images.header.ic_clear} />
                </TouchableOpacity>
            )}
            {isError && <UIText style={styles.txtError} text="Error" />}
        </View>
    );
};

export default TextInputField;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        paddingVertical: verticalScale(10),
        justifyContent: 'center',
    },
    input: {
        height: verticalScale(50),
        backgroundColor: Colors.gray,
        borderRadius: verticalScale(6),
        borderColor: Colors.border,
        borderWidth: 1,
        paddingTop: verticalScale(15),
        paddingRight: verticalScale(15),
        paddingBottom: Platform.OS == 'ios' ? verticalScale(15) : verticalScale(10),
        paddingLeft: verticalScale(15),
        color: Colors.blueDark,
        fontSize: verticalScale(16),
        fontFamily: fontFamily.regular,
        lineHeight: Platform.OS == 'ios' ? verticalScale(20) : verticalScale(25),
    },
    icClearContainer: {
        width: verticalScale(16),
        height: verticalScale(16),
        position: 'absolute',
        top: verticalScale(15),
        right: verticalScale(10),
    },
    icClear: {
        width: verticalScale(16),
        height: verticalScale(16),
    },
    txtError: {
        color: Colors.error,
        fontSize: verticalScale(14),
        marginTop: verticalScale(5),
    },
});
