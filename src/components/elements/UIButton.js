import React, { Component } from 'react';
import { TouchableOpacity, Text, View, ActivityIndicator, StyleSheet, Image } from 'react-native';
import PropTypes from 'prop-types';
import { color } from '../../utils';
import { Colors } from '../../theme/Variables';
import { moderateScale, scale, verticalScale } from '../../utils/ScalingUtils';
import { fontFamily } from '../../theme/Fonts';

const renderButtonContent = (props) => {
    const { type, onPress, textStyle, text, isLoading, Icon, IconRight, style, disable, iconStyle } = props;
    switch (type) {
        case 'BgWhite':
            return (
                <TouchableOpacity
                    style={[styles.styleButton, { backgroundColor: Colors.white, borderColor: Colors.blueDark, borderWidth: 1 }, style]}
                    onPress={onPress}
                    disabled={disable || isLoading}
                >
                    {Icon && <Image source={Icon} style={[styles.icon, iconStyle]} />}
                    {isLoading ? (
                        <ActivityIndicator size="small" color={Colors.blueDark} />
                    ) : (
                        <Text
                            allowFontScaling={false}
                            style={[styles.styleText, { color: disable ? Colors.disable : Colors.blueDark }, textStyle]}
                        >
                            {text}
                        </Text>
                    )}
                    {IconRight && <Image source={IconRight} style={[styles.icon, iconStyle]} />}
                </TouchableOpacity>
            );
        case 'BgGrey':
            return (
                <TouchableOpacity
                    style={[styles.styleButton, { backgroundColor: Colors.disable }, style]}
                    onPress={onPress}
                    disabled={disable || isLoading}
                >
                    {Icon && <Image source={Icon} style={[styles.icon, iconStyle]} />}
                    {isLoading ? (
                        <ActivityIndicator size="small" color={Colors.white} />
                    ) : (
                        <Text allowFontScaling={false} style={[styles.styleText, { color: Colors.white }, textStyle]}>
                            {text}
                        </Text>
                    )}
                    {IconRight && <Image source={IconRight} style={[styles.icon, iconStyle]} />}
                </TouchableOpacity>
            );
        case 'BgBlue':
            return (
                <TouchableOpacity
                    style={[styles.styleButton, { backgroundColor: disable ? Colors.disable : Colors.lightBlue }, style]}
                    onPress={onPress}
                    disabled={disable || isLoading}
                >
                    {Icon && <Image source={Icon} style={[styles.icon, iconStyle]} />}
                    {isLoading ? (
                        <ActivityIndicator size="small" color={Colors.white} />
                    ) : (
                        <Text allowFontScaling={false} style={[styles.styleText, { color: Colors.white }, textStyle]}>
                            {text}
                        </Text>
                    )}
                    {IconRight && <Image source={IconRight} style={[styles.icon, iconStyle]} />}
                </TouchableOpacity>
            );
        case 'BgGreen':
            return (
                <TouchableOpacity
                    style={[styles.styleButton, { backgroundColor: disable ? Colors.disable : Colors.lightGreen }, style]}
                    onPress={onPress}
                    disabled={disable || isLoading}
                >
                    {Icon && <Image source={Icon} style={[styles.icon, iconStyle]} />}
                    {isLoading ? (
                        <ActivityIndicator size="small" color={Colors.white} />
                    ) : (
                        <Text allowFontScaling={false} style={[styles.styleText, { color: Colors.white }, textStyle]}>
                            {text}
                        </Text>
                    )}
                    {IconRight && <Image source={IconRight} style={[styles.icon, iconStyle]} />}
                </TouchableOpacity>
            );
        case 'BgOrange':
            return (
                <TouchableOpacity
                    style={[styles.styleButton, { backgroundColor: disable ? Colors.disable : Colors.orange }, style]}
                    onPress={onPress}
                    disabled={disable || isLoading}
                >
                    {Icon && <Image source={Icon} style={[styles.icon, iconStyle]} />}
                    {isLoading ? (
                        <ActivityIndicator size="small" color={Colors.white} />
                    ) : (
                        <Text allowFontScaling={false} style={[styles.styleText, { color: Colors.white }, textStyle]}>
                            {text}
                        </Text>
                    )}
                    {IconRight && <Image source={IconRight} style={[styles.icon, iconStyle]} />}
                </TouchableOpacity>
            );
        case 'BorderRed':
            return (
                <TouchableOpacity
                    style={[styles.styleButton, { backgroundColor: Colors.white, borderColor: Colors.error, borderWidth: 1 }, style]}
                    onPress={onPress}
                    disabled={disable || isLoading}
                >
                    {Icon && <Image source={Icon} style={[styles.icon, iconStyle]} />}
                    {isLoading ? (
                        <ActivityIndicator size="small" color={Colors.error} />
                    ) : (
                        <Text allowFontScaling={false} style={[styles.styleText, { color: Colors.error }, textStyle]}>
                            {text}
                        </Text>
                    )}
                    {IconRight && <Image source={IconRight} style={[styles.icon, iconStyle]} />}
                </TouchableOpacity>
            );
        case 'BgGreyText':
            return (
                <TouchableOpacity
                    style={[styles.styleButton, { backgroundColor: Colors.white, borderColor: '#707070', borderWidth: 1 }, style]}
                    onPress={onPress}
                    disabled={disable || isLoading}
                >
                    {Icon && <Image source={Icon} style={[styles.icon, iconStyle]} />}
                    {isLoading ? (
                        <ActivityIndicator size="small" color={Colors.error} />
                    ) : (
                        <Text allowFontScaling={false} style={[styles.styleText, { color: '#707070' }, textStyle]}>
                            {text}
                        </Text>
                    )}
                    {IconRight && <Image source={IconRight} style={[styles.icon, iconStyle]} />}
                </TouchableOpacity>
            );
        case 'BgGrey2':
            return (
                <TouchableOpacity
                    style={[styles.styleButton, { backgroundColor: Colors.lightGray3 }, style]}
                    onPress={onPress}
                    disabled={disable || isLoading}
                >
                    {Icon && <Image source={Icon} style={[styles.icon, iconStyle]} />}
                    {isLoading ? (
                        <ActivityIndicator size="small" color={Colors.error} />
                    ) : (
                        <Text allowFontScaling={false} style={[styles.styleText, { color: Colors.darkGray2 }, textStyle]}>
                            {text}
                        </Text>
                    )}
                    {IconRight && <Image source={IconRight} style={[styles.icon, iconStyle]} />}
                </TouchableOpacity>
            );
        case 'BgGrey3':
            return (
                <TouchableOpacity
                    style={[styles.styleButton, { backgroundColor: Colors.lightGray3 }, style]}
                    onPress={onPress}
                    disabled={disable || isLoading}
                    activeOpacity={1}
                >
                    {Icon && <Image source={Icon} style={[styles.icon, iconStyle]} />}
                    {isLoading ? (
                        <ActivityIndicator size="small" color={Colors.error} />
                    ) : (
                        <Text allowFontScaling={false} style={[styles.styleText, { color: '#9ba4ac' }, textStyle]}>
                            {text}
                        </Text>
                    )}
                    {IconRight && <Image source={IconRight} style={[styles.icon, iconStyle]} />}
                </TouchableOpacity>
            );
        default:
            return (
                <TouchableOpacity
                    style={[styles.styleButton, styles.shadow, style, { backgroundColor: disable ? Colors.disable : Colors.blueDark }]}
                    onPress={onPress}
                    disabled={disable || isLoading}
                >
                    {Icon && <Image source={Icon} style={[styles.icon, iconStyle]} />}
                    {isLoading ? (
                        <ActivityIndicator size="small" color={Colors.white} />
                    ) : (
                        <Text allowFontScaling={false} style={[styles.styleText, textStyle]}>
                            {text}
                        </Text>
                    )}
                    {IconRight && <Image source={IconRight} style={[styles.icon, iconStyle]} />}
                </TouchableOpacity>
            );
    }
};
const AppButton = (props) => {
    const { onPress, style, disable, isLoading } = props;
    return <>{renderButtonContent(props)}</>;
};

AppButton.defaultProps = {
    type: '',
    onPress: () => { },
    textStyle: {},
    text: '',
    colors: ['#000', '#000'],
    style: {},
    disable: false,
    isLoading: false,
    backgroundColor: '#FFF',
};

AppButton.propTypes = {
    // type: PropTypes.oneOf(['', 'BgWhite', 'BgGrey']).isRequired,
    onPress: PropTypes.func.isRequired,
    textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    text: PropTypes.string,
    colors: PropTypes.array,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    disable: PropTypes.bool,
    isLoading: PropTypes.bool,
    backgroundColor: PropTypes.string,
    Icon: PropTypes.any,
};

export default AppButton;

const styles = StyleSheet.create({
    styleButton: {
        backgroundColor: Colors.blueDark,
        width: '100%',
        height: verticalScale(50),
        borderRadius: scale(4),
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    shadow: {
        shadowColor: Colors.white,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
    },
    styleText: {
        color: Colors.white,
        fontSize: moderateScale(18),
        lineHeight: moderateScale(24),
        fontFamily: fontFamily.bold,
    },
    disableButton: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(255,255,255, 0.7)',
    },
    icon: {
        marginRight: scale(6),
    },
});
