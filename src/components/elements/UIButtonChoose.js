import React from 'react';
import { TouchableOpacity, StyleSheet, Image } from 'react-native';
import { UIText } from './';
import { Colors } from '../../theme/Variables';
import { moderateScale, verticalScale } from '../../utils/ScalingUtils';

const UIButtonChoose = (props) => {
    const { text, isChoose, icon, onPress, containerStyle } = props;

    return (
        <TouchableOpacity onPress={onPress} style={[styles.container, isChoose && { backgroundColor: Colors.blueDark }, containerStyle]}>
            {icon && <Image source={icon} style={[styles.ic, isChoose && { tintColor: Colors.white }]} />}
            <UIText font="bold" style={[styles.txt, isChoose && { color: Colors.white }]} text={text} />
        </TouchableOpacity>
    );
};

export default UIButtonChoose;

const styles = StyleSheet.create({
    container: {
        height: verticalScale(36),
        paddingHorizontal: verticalScale(15),
        borderRadius: verticalScale(6),
        backgroundColor: 'rgb(236, 237, 242)',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: verticalScale(4),
        flexDirection: 'row',
    },
    txt: {
        fontSize: moderateScale(14),
        color: Colors.blueDark,
    },
    ic: {
        width: verticalScale(24),
        height: verticalScale(24),
        tintColor: Colors.blueDark,
        marginRight: verticalScale(5),
    },
});
