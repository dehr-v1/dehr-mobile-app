import UIText from './UIText';
import UIButton from './UIButton';
import UIAvatar from './UIAvatar';
import UITextInput from './UITextInput';
import UIRadio from './UIRadio';
import UIButtonChoose from './UIButtonChoose';

export { UIText, UIAvatar, UIButton, UITextInput, UIRadio, UIButtonChoose };
