import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { UIModal } from '.';
import { Colors } from '../../theme/Variables';
import { verticalScale } from '../../utils/ScalingUtils';
import { UIText } from '../elements';
import { useTranslation } from 'react-i18next';

const UploadPhotoModal = (props) => {
    const { pRef, onPressModalTakePhoto, onPressModalChooseFrom, onPressCancelModal } = props;
    const { t } = useTranslation();

    return (
        <>
            <View style={{ flex: 1 }} />
            <TouchableOpacity activeOpacity={1} style={{ width: '88%' }}>
                <View style={styles.viewModalTop}>
                    <TouchableOpacity onPress={onPressModalTakePhoto}>
                        <UIText text={t('profile.take_photo')} style={styles.textSelectImg} font="bold" />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={onPressModalChooseFrom}>
                        <UIText text={t('profile.choose_from')} style={[styles.textSelectImg, { marginBottom: 0 }]} font="bold" />
                    </TouchableOpacity>
                </View>
                <View style={{ height: verticalScale(10) }} />
                <TouchableOpacity style={styles.viewModalBot} onPress={onPressCancelModal}>
                    <UIText text={t('profile.cancel')} style={styles.textCancel} font="bold" />
                </TouchableOpacity>
            </TouchableOpacity>
        </>
    );
};

export default UploadPhotoModal;

const styles = StyleSheet.create({
    viewModalTop: {
        padding: verticalScale(12),
        backgroundColor: Colors.white,
        borderRadius: 10,
    },
    viewModalBot: {
        marginBottom: verticalScale(20),
        paddingVertical: verticalScale(12),
        backgroundColor: Colors.white,
        borderRadius: 10,
    },
    textSelectImg: {
        alignSelf: 'center',
        marginBottom: verticalScale(10),
        borderRadius: 10,
    },
    textCancel: {
        textTransform: 'capitalize',
        lineHeight: verticalScale(20),
        alignSelf: 'center',
        borderRadius: 10,
    },
});
