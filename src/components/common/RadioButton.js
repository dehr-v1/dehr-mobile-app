import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { Colors } from '../../theme/Variables';
import { verticalScale } from '../../utils/ScalingUtils';

const RadioButton = (props) => {
    const { isChoose = false, onPress } = props;

    return (
        <TouchableOpacity onPress={onPress} style={[styles.container, isChoose && styles.choose]}>
            {isChoose && <View style={styles.dot} />}
        </TouchableOpacity>
    );
};

export default RadioButton;

const styles = StyleSheet.create({
    container: {
        width: verticalScale(20),
        height: verticalScale(20),
        borderWidth: verticalScale(2),
        borderColor: Colors.lightGray,
        backgroundColor: Colors.gray,
        borderRadius: verticalScale(10),
        justifyContent: 'center',
        alignItems: 'center',
    },
    choose: {
        borderColor: Colors.blueDark,
        backgroundColor: Colors.white,
    },
    dot: {
        width: verticalScale(10),
        height: verticalScale(10),
        borderRadius: verticalScale(5),
        backgroundColor: Colors.blueDark,
    },
});
