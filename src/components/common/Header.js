import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Platform } from 'react-native';
import PropTypes from 'prop-types';
import { verticalScale } from '../../utils/ScalingUtils';
import { UIText, UIAvatar } from '../elements';
import { useTheme } from '@/theme';
import { Colors } from '../../theme/Variables';
import { fontFamily } from '../../theme/Fonts';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingHorizontal: verticalScale(16),
        paddingBottom: verticalScale(20),
        paddingTop: verticalScale(20),
        height: Platform.OS === 'ios' ? verticalScale(65) : verticalScale(70),
        alignItems: 'center',
    },
    titleStyle: {
        fontSize: verticalScale(20),
        lineHeight: verticalScale(24),
        fontFamily: fontFamily.bold,
        color: Colors.title,
        textAlign: 'center',
        textTransform: 'capitalize',
    },
    leftContainer: {
        width: verticalScale(36),
        height: verticalScale(36),
        justifyContent: 'center',
        alignItems: 'flex-start',
        flex: 0.14
    },
    leftButton: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    centerContainer: {
        flex: 0.72,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
const getIcon = (name, Images) => {
    switch (name) {
        case 'close':
            return <Image source={Images.header.ic_close} />;
        case 'plus':
            return <Image source={Images.header.ic_plus} />;
        case 'more':
            return <Image source={Images.header.ic_more} />;
        case 'chatbot':
            return <Image source={Images.header.ic_chat_bot} />;
        case 'edit':
            return <Image source={Images.header.ic_edit} />;
        case 'back_white':
            return <Image style={{ tintColor: Colors.white }} source={Images.header.ic_back} />;
        case 'back_gray':
            return <Image style={{ tintColor: Colors.darkGray }} source={Images.header.ic_back} />;
        case 'back_invert':
            return <Image style={{ tintColor: Colors.title }} source={Images.header.ic_back} />;
        default:
            return <Image source={Images.header.ic_back} />;
    }
};
const Header = (props) => {
    const { Images } = useTheme();
    const {
        style,
        title,
        LeftIcon = '',
        RightIcon,
        avatar,
        onPress,
        onPressRight,
        rightTitle,
        leftTitle = '',
        leftTitleStyle,
        rightTitleStyle,
        titleStyle,
        centerComponent,
        centerStyle,
        leftComponent,
        rightComponent,
    } = props;
    return (
        <View style={[style, styles.container]}>
            <View style={styles.leftContainer}>
                {LeftIcon != '' || leftTitle != '' || leftComponent ? (
                    <TouchableOpacity style={styles.leftButton} onPress={onPress}>
                        {leftTitle ? (
                            <UIText style={leftTitleStyle} text={leftTitle} />
                        ) : leftComponent ? (
                            leftComponent()
                        ) : (
                            getIcon(LeftIcon, Images)
                        )}
                    </TouchableOpacity>
                ) : null}
            </View>
            <View style={[styles.centerContainer, centerStyle]}>
                {title != '' ? (
                    <UIText numberOfLines={1} ellipsizeMode="tail" text={title} font="bold" style={(styles.titleStyle, titleStyle)} />
                ) : centerComponent ? (
                    centerComponent()
                ) : null}
            </View>
            <View style={[styles.leftContainer]}>
                {(RightIcon != '' || rightTitle != '' || rightComponent) && avatar === '' ? (
                    <TouchableOpacity style={styles.leftButton} onPress={onPressRight}>
                        {rightTitle != '' ? (
                            <UIText font='bold' style={rightTitleStyle} text={rightTitle} />
                        ) : rightComponent ? (
                            rightComponent()
                        ) : (
                            getIcon(RightIcon, Images)
                        )}
                    </TouchableOpacity>
                ) : null}
                {avatar != '' ? <UIAvatar url={avatar || Images.header.ic_chat_bot} /> : null}
            </View>
        </View>
    );
};

Header.defaultProps = {
    style: {},
    titleStyle: {},
    title: '',
    RightIcon: '',
    LeftIcon: '',
    avatar: '',
    onPressRight: () => { },
    rightTitle: '',
};

Header.propTypes = {
    onPress: PropTypes.func,
    LeftIcon: PropTypes.string,
    RightIcon: PropTypes.string,
    avatar: PropTypes.string,
    title: PropTypes.string,
    rightComponent: PropTypes.any,
    rightTitle: PropTypes.string,
    onPressRight: PropTypes.func,
    centerComponent: PropTypes.any,
    leftComponent: PropTypes.any,
};

export default Header;
