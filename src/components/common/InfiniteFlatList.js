import { useFocusEffect } from '@react-navigation/core';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { StyleSheet } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';

const InfiniteFlatList = (props) => {
    const { key, onFetchData, renderItem } = props;
    const { t } = useTranslation();
    const [page, setPage] = useState(1);
    const [totalPages, setTotalPages] = useState([]);
    const [list, setList] = useState([]);
    let isLoaded = false;

    useEffect(() => {
        if (!isLoaded) refresh();
        isLoaded = true;
    }, []);

    let fetchData = () => {
        if (page < totalPages) {
            const newPage = page + 1;
            onFetchData(newPage, (pageList) => {
                const { page, totalPages, items } = pageList;
                setPage(page);
                setTotalPages(totalPages);
                setList([...list, ...items]);
            });
        }
    };

    let refresh = () => {
        onFetchData(1, (pageList) => {
            const { page, totalPages, items } = pageList;
            setPage(page);
            setTotalPages(totalPages);
            setList([...list, ...items]);
        });
    };

    return (
        <FlatList
            key={key}
            data={list}
            extraData={list}
            renderItem={renderItem}
            onRefresh={refresh}
            onEndReached={fetchData}
            onEndReachedThreshold={0.5}
            keyExtractor={(item) => item._id || item.id}
        />
    );
};

export default InfiniteFlatList;

const styles = StyleSheet.create({});
