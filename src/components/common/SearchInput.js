import React from 'react';
import { View, StyleSheet, Image, TextInput, Text } from 'react-native';
import { useTheme } from '@/theme';
import { moderateScale, verticalScale } from '../../utils/ScalingUtils';
import { Colors } from '../../theme/Variables';
import { useTranslation } from 'react-i18next';

const SearchInput = (props) => {
    const { Images } = useTheme();
    const { t } = useTranslation();

    const { containerStyle, editable = true, onSearch, value } = props;

    return (
        <View style={[styles.container, containerStyle]}>
            <Image style={styles.icSearch} source={Images.home.ic_search} />
            {editable === true ? (
                <TextInput
                    value={value}
                    onChangeText={onSearch}
                    editable={editable}
                    style={styles.input}
                    placeholderTextColor={Colors.lightGray}
                    placeholder={t('home.search')}
                />
            ) : (
                <Text style={[styles.input, { color: Colors.lightGray }]}>{t('home.search')}</Text>
            )}
        </View>
    );
};

export default SearchInput;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: verticalScale(44),
        borderRadius: verticalScale(6),
        backgroundColor: Colors.white,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: verticalScale(22),
    },
    icSearch: {
        width: verticalScale(28),
        height: verticalScale(28),
    },
    input: {
        width: '100%',
        fontSize: moderateScale(16),
        color: Colors.blueDark,
        // marginRight: verticalScale(6),
        // marginTop: verticalScale(6),
        // height: verticalScale(44),
    },
});
