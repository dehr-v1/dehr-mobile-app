import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { UIText } from '../elements';
import { Colors } from '../../theme/Variables';
import { moderateScale, scale, verticalScale } from '../../utils';

const CustomTabbar = (props) => {
  const {
    goToPage,
    tabs,
    activeTab,
  } = props

  const renderTab = (name, page, isTabActive, onPressHandler) => {
    const backgroundColor = isTabActive ? Colors.blueDark : Colors.transparent
    const color = isTabActive ? Colors.white : Colors.title
    return (
      <TouchableOpacity
        style={[styles.btnTab, { backgroundColor }]}
        key={name}
        accessible={true}
        accessibilityLabel={name}
        accessibilityTraits='button'
        onPress={() => onPressHandler(page)}
      >
        <UIText
          style={[styles.textTab, { color }]}
          text={name}
          font="bold"
        />
      </TouchableOpacity>
    )
  }

  return (
    <View style={styles.container}>
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        bounces={false}
        contentContainerStyle={styles.contentContainer}
      >
        <View style={styles.viewEmpty} />
        {tabs.map((name, page) => {
          const isTabActive = activeTab === page;
          return renderTab(name, page, isTabActive, goToPage);
        })}
        <View style={styles.viewEmpty} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: verticalScale(46),
    backgroundColor: Colors.grayHiring,
    justifyContent: 'center',
  },
  contentContainer: {
    alignItems: 'center',
  },
  viewEmpty: {
    width: scale(16),
  },
  btnTab: {
    height: verticalScale(32),
    justifyContent: 'center',
    paddingHorizontal: scale(20),
    borderRadius: scale(18),
  },
  textTab: {
    fontSize: moderateScale(14),
  }
});

export default CustomTabbar;
