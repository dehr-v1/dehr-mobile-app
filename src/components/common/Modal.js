import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Modal, StyleSheet, TouchableWithoutFeedback, TouchableOpacity, Text } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(33,33,33,0.7)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    backdrop: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        zIndex: 0,
    },
});

const BottomSheetOrientation = ['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right'];

class BottomSheet extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
        };
    }

    setModalVisible = (visible, callBack) => {
        this.setState(
            {
                modalVisible: visible,
            },
            () => callBack && callBack()
        );
    };

    open = () => {
        this.setModalVisible(true);
    };

    close = () => {
        this.setModalVisible(false);
    };
    render() {
        const { animationType, children, customStyles, disabled, onPress, closeModal } = this.props;
        const { modalVisible } = this.state;
        return (
            <Modal
                transparent
                animationType={animationType}
                visible={modalVisible}
                supportedOrientations={BottomSheetOrientation}
                onRequestClose={() => {
                    this.setModalVisible(false);
                }}
            >
                <View style={[styles.container, customStyles]}>
                    <TouchableOpacity style={styles.backdrop} onPress={this.close} />
                    <TouchableWithoutFeedback disabled={disabled} onPress={onPress || this.close}>
                        {children}
                    </TouchableWithoutFeedback>
                </View>
            </Modal>
        );
    }
}

BottomSheet.defaultProps = {
    animationType: 'none',
    customStyles: {},
    children: <View />,
    disabled: false,
    onPress: null,
};

BottomSheet.propTypes = {
    animationType: PropTypes.oneOf(['none', 'slide', 'fade']),
    customStyles: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    children: PropTypes.node,
    disabled: PropTypes.bool,
    onPress: PropTypes.func,
};

export default BottomSheet;
