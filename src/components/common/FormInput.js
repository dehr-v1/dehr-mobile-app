import React from 'react';
import { View, StyleSheet } from 'react-native';
import { fontFamily } from '../../theme/Fonts';
import { Colors } from '../../theme/Variables';
import { verticalScale } from '../../utils/ScalingUtils';
import { UIText, UITextInput } from '../elements';

const FormInput = (props) => {
    const { title, titleStyle, required, value, onChangeText, containerStyle, disable, ...other } = props;
    return (
        <View style={[styles.container, containerStyle]} pointerEvents={disable}>
            <View style={styles.titleContainer}>
                <UIText font="bold" style={[styles.txtTitle, titleStyle]} text={title} />
                {required && <UIText style={styles.txtRequire} text=" *" />}
            </View>
            <UITextInput value={value} onChangeText={onChangeText} {...other} />
        </View>
    );
};

export default FormInput;

const styles = StyleSheet.create({
    container: {
        width: '100%',
    },
    titleContainer: {
        flexDirection: 'row',
    },
    txtTitle: {
        fontFamily: fontFamily.bold,
        fontSize: verticalScale(14),
        color: Colors.blueDark,
    },
    txtRequire: {
        color: Colors.error,
    },
});
