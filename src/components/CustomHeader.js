/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { Text, StyleSheet, View, Image, TouchableOpacity } from 'react-native';
import { moderateScale, verticalScale } from '../utils/ScalingUtils';
import { Colors } from '../theme/Variables';
import { useTheme } from '@/theme';

function Title({ title }) {
    return (
        <View style={styles.Title}>
            <Text style={styles.Text}>{title}</Text>
        </View>
    );
}

function HeaderRight({ navigation }) {
    const { Images } = useTheme();
    return (
        <TouchableOpacity
            style={{
                // backgroundColor: 'red',
                justifyContent: 'center',
                alignItems: 'flex-end',
                height: '100%',
                width: '100%',
                marginRight: verticalScale(40),
            }}
            onPress={() => navigation.popToTop()}
        >
            <Image style={styles.IconExit} source={Images.header.ic_back} />
        </TouchableOpacity>
    );
}
function HeaderLeft({ navigation }) {
    const { Images } = useTheme();
    return (
        <TouchableOpacity style={styles.IconBack} onPress={() => navigation.goBack()}>
            <Image style={styles.IconExit} source={Images.header.ic_back} />
        </TouchableOpacity>
    );
}

function Header({ title, navigation }) {
    const { Images } = useTheme();
    return (
        <View style={styles.Header}>
            <TouchableOpacity style={styles.Left} onPress={() => navigation.goBack()}>
                <Image style={styles.IconExit} source={Images.header.ic_back} />
            </TouchableOpacity>
            <View style={styles.TitleH}>
                <Text style={styles.Text}>{title}</Text>
            </View>
            <TouchableOpacity style={styles.Right} onPress={() => navigation.navigate('HomeScreen')}>
                <Image style={styles.IconExitH} source={Images.header.ic_back} />
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    Header: {
        flexDirection: 'row',
        height: verticalScale(45),
        borderBottomWidth: 0.5,
        marginBottom: verticalScale(10),
        // backgroundColor: 'white',
    },
    Left: {
        flex: 1,
        // borderWidth: 1,
        justifyContent: 'center',
        // borderColor: 'red',
    },
    Right: {
        flex: 1,
        // borderWidth: 1,
        justifyContent: 'center',
        // borderColor: 'red',
    },
    Title: {
        flex: 1,
        // borderWidth: 1,
        // borderColor: 'red',
        alignItems: 'center',
        justifyContent: 'center',
    },
    TitleH: {
        flex: 3,
        // borderWidth: 1,
        // borderColor: 'red',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Text: {
        // paddingLeft: 10,
        fontWeight: '700',
        fontSize: moderateScale(18),
        color: '#2D389C',
    },
    IconBack: {
        marginTop: verticalScale(20),
    },
    IconExit: {
        marginRight: verticalScale(10),
        height: verticalScale(45),
        width: verticalScale(40),
    },
    IconExitH: {
        marginLeft: verticalScale(50),
        height: verticalScale(45),
        width: verticalScale(40),
    },
});

export { Title };
export { HeaderRight };
export { HeaderLeft };
export { Header };
