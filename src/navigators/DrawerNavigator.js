import React, { useEffect } from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { DrawerContentScrollView, createDrawerNavigator, DrawerItem } from '@react-navigation/drawer';
import { SCREEN } from '../constant';
import { BestOfYouPage } from '../containers';
import MainNavigator from './Main';
import { moderateScale, verticalScale } from '../utils/ScalingUtils';
import { Colors } from '../theme/Variables';
import { useTheme } from '@/theme';
import { UIText } from '../components/elements';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';
import { fontFamily } from '../theme/Fonts';
import { navigateAndSimpleReset } from '@/navigators/Root';
import { connect } from 'react-redux';
import { BASE_IMAGE_URL } from '../constant/value.constant';
import { actionLogOut } from '../containers/Login/duck/actions';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import messaging from '@react-native-firebase/messaging';
import { removeDataLogOut } from '../services/persistence/store.config';

const Drawer = createDrawerNavigator();

const DrawerNavigator = (props) => {
    const { authReducer, actionLogOut } = props || {};
    const { user } = authReducer || {};

    return (
        <Drawer.Navigator
            drawerStyle={{ width: verticalScale(310) }}
            initialRouteName="Main"
            drawerContent={(props) => <DrawerNavigatorContent accountInfo={user} {...props} actionLogOut={actionLogOut} />}
        >
            <Drawer.Screen name="Main" component={MainNavigator} />
            <Drawer.Screen name={SCREEN.BEST_FOR_YOU} component={BestOfYouPage} />
        </Drawer.Navigator>
    );
};

const DrawerNavigatorContent = (props) => {
    const { Images } = useTheme();
    const { t } = useTranslation();
    const navigation = useNavigation();
    const { accountInfo, actionLogOut } = props;
    const { role, username, avatar_url } = accountInfo || {};
    const insets = useSafeAreaInsets();

    const logOut = async () => {
        actionLogOut(async () => {
            try {
                await removeDataLogOut()
                // const keys = await AsyncStorage.getAllKeys();
                // await AsyncStorage.multiRemove(keys);
            } catch (error) {
                console.error('Error clearing app data.');
            }
            navigateAndSimpleReset(SCREEN.LOGIN);
        });
    };

    return (
        <DrawerContentScrollView contentContainerStyle={{ paddingTop: insets.top }} {...props}>
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image style={styles.icAvatar} source={avatar_url ? { uri: BASE_IMAGE_URL + avatar_url } : Images.profile.ic_avatar} />
                    <View style={styles.nameContainer}>
                        <UIText numberOfLines={2} ellipsizeMode="tail" font="bold" style={styles.txtName} text={username} />
                        {/* <UIText style={styles.txtViewProfile} text={t('home.view_profile')} /> */}
                    </View>
                </View>
                {role == 'member' ? (
                    <TouchableOpacity
                        onPress={() => navigation.navigate(SCREEN.ACTIVE_PRO_ACCOUNT)}
                        style={[styles.row, { backgroundColor: '#fef4e8' }]}
                    >
                        <Image style={styles.icPro} source={Images.home.ic_pro} />
                        <UIText font="medium" style={styles.txtBecomePro} text={t('home.become_pro')} />
                    </TouchableOpacity>
                ) : (
                    <View />
                )}
                {/* // <TouchableOpacity style={[styles.row, { backgroundColor: "#f0faff" }]} >
                    //     <Image style={styles.icPro} source={Images.home.ic_user} />
                    //     <UIText font="medium" style={styles.txtBecomePro} text={t('home.switch_employer')} />
                    // </TouchableOpacity>} */}
                {role == 'member' ? (
                    <>
                        <DrawerItem
                            style={styles.itemContainer}
                            labelStyle={styles.txtItem}
                            label={t('home.best_for_you')}
                            onPress={() => navigation.navigate(SCREEN.BEST_FOR_YOU)}
                        />
                        <DrawerItem
                            style={styles.itemContainer}
                            labelStyle={styles.txtItem}
                            label={t('home.setting')}
                            onPress={() => navigation.navigate(SCREEN.BEST_FOR_YOU)}
                        />
                    </>
                ) : (
                    <>
                        <DrawerItem
                            style={styles.itemContainer}
                            labelStyle={styles.txtItem}
                            label={t('home.job_posted')}
                            onPress={() => navigation.navigate(SCREEN.JOB_POSTED)}
                        />
                        <DrawerItem
                            style={styles.itemContainer}
                            labelStyle={styles.txtItem}
                            label={t('home.matched_profiles')}
                            onPress={() => navigation.navigate(SCREEN.PEOPLE_MATCHED)}
                        />
                        <DrawerItem
                            style={styles.itemContainer}
                            labelStyle={styles.txtItem}
                            label={t('home.hiring_process')}
                            onPress={() => navigation.navigate(SCREEN.HIRING_PROCESS)}
                        />
                    </>
                )}
                <DrawerItem
                    style={styles.itemContainer}
                    labelStyle={styles.txtItem}
                    label={t('home.support')}
                    onPress={() => navigation.navigate(SCREEN.BEST_FOR_YOU)}
                />
                <DrawerItem style={styles.itemContainer} labelStyle={styles.txtItem} label={t('home.logout')} onPress={() => logOut()} />
                <UIText style={styles.txtVersion} text={`${t('home.version')} 1.0.0`} />
            </View>
        </DrawerContentScrollView>
    );
};

const mapStateToProps = (state) => ({
    authReducer: state.authReducer,
});
export default connect(mapStateToProps, {
    actionLogOut,
})(DrawerNavigator);

const styles = StyleSheet.create({
    container: {},
    header: {
        width: '100%',
        height: verticalScale(140),
        backgroundColor: Colors.blueDark,
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: verticalScale(15),
    },
    icAvatar: {
        width: verticalScale(40),
        height: verticalScale(40),
        borderRadius: verticalScale(20),
    },
    txtName: {
        marginEnd: verticalScale(14),
        fontSize: moderateScale(24),
        color: Colors.white,
    },
    nameContainer: {
        marginLeft: verticalScale(12),
    },
    txtViewProfile: {
        fontSize: moderateScale(12),
        color: Colors.white,
    },
    icPro: {
        width: verticalScale(36),
        height: verticalScale(36),
    },
    txtBecomePro: {
        fontSize: moderateScale(12),
        color: Colors.title,
        marginLeft: verticalScale(5),
        width: verticalScale(240),
        lineHeight: verticalScale(20),
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        paddingHorizontal: verticalScale(15),
        height: verticalScale(60),
    },
    txtItem: {
        fontSize: verticalScale(14),
        fontFamily: fontFamily.medium,
        color: Colors.title,
    },
    itemContainer: {
        paddingHorizontal: verticalScale(10),
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: Colors.border,
        marginVertical: 0,
    },
    txtVersion: {
        marginTop: verticalScale(15),
        marginLeft: verticalScale(25),
        fontSize: moderateScale(12),
        color: Colors.title,
    },
});
