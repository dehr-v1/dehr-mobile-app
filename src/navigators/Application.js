import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {
    IndexWelcomeContainer,
    IndexLoginContainer,
    IndexForgotContainer,
    IndexRegisterContainer,
    IndexCreatePassContainer,
    IndexVerifyContainer,
    IndexMessageContainer,
} from '@/containers';
import { NavigationContainer } from '@react-navigation/native';
import { navigationRef, navigate } from '@/navigators/Root';
import { SafeAreaView, Platform, StatusBar } from 'react-native';
import { useTheme } from '@/theme';
import { SCREEN } from '../constant';
import {
    AccountInfo,
    ActiveProAccount,
    BestOfYouPage,
    ClassPage,
    ConfirmSecure,
    CreatePassword,
    CreateProfile,
    CurrentPosition,
    DoneKyc,
    Education,
    EnterPhoneNumber,
    EssDetailPage,
    Experience,
    FillInfoKyc,
    HiringProcessPage,
    Hobbies,
    ImportSeedPhrase,
    IndexNewMessageContainer,
    IndexConnectNetWorkContainer,
    IndustrialKnowledge,
    JobPostedPage,
    Language,
    OrganisationPage,
    OtherSkill,
    PeopleMatchedPage,
    PeopleSponserPage,
    SearchJobPage,
    SearchAdvancePage,
    SecureWallet,
    SetupKYC,
    SiteCompany,
    SocialLinks,
    SpecialistSkill,
    Summary,
    UISearchScreen,
    UpdateAbout,
    UpdateAccountInfo,
    UpdateCurrentPosition,
    UpdateEducation,
    UpdateExperience,
    UpdateHeadline,
    UpdateHobbies,
    UpdateIndustrialKnowledge,
    AllTransactionPage,
    IndexNotificationContainer,
    UpdateLanguage,
    UpdateOtherSkill,
    UpdateSiteCompany,
    UpdateSocialLink,
    UpdateSpecialistSkill,
    UploadFileKyc,
    VerifyPhoneKyc,
    UpdateContactPrice,
    SplashPage,
    PeopleDetailPage,
    ProfileViewPage,
    ListPopUpPage,
    AllJobPage,
} from '../containers';
import { connect } from 'react-redux';
import MainNavigator from './Main';
import DrawerNavigator from './DrawerNavigator';
import messaging from '@react-native-firebase/messaging';
import { actionUpdateNotificationToken } from '../containers/Login/duck/actions';
import { actionAddNewNotification } from '../containers/Notification/duck/actions';
import Toast from '../components/elements/Toast';
import FirebaseMessagingHelper from '../services/firebase.service';
import PostJobPage from '../containers/Jobs/pages/PostJobPage';

const Stack = createStackNavigator();

// let MainNavigator

// @refresh reset
const ApplicationNavigator = (props) => {
    const { notificationReducer, actionUpdateNotificationToken, actionAddNewNotification } = props || {};
    const { Layout, darkMode, NavigationTheme } = useTheme();
    const { colors } = NavigationTheme;

    // const applicationIsLoading = useSelector(state => state.startup.loading)

    // useEffect(() => {
    //   if (MainNavigator == null && !applicationIsLoading) {
    //     MainNavigator = require('@/navigators/Main').default
    //     setIsApplicationLoaded(true)
    //   }
    // }, [applicationIsLoading])

    // on destroy needed to be able to reset when app close in background (Android)
    // useEffect(
    //   () => () => {
    //     setIsApplicationLoaded(false)
    //     MainNavigator = null
    //   },
    //   [],
    // )

    useEffect(() => {
        FirebaseMessagingHelper.requestUserPermission();
        FirebaseMessagingHelper.handleMessage((remoteMessage) => {
            Toast(`${remoteMessage?.notification?.title}\n ${remoteMessage?.notification?.body}`);
            actionAddNewNotification();
        });
        FirebaseMessagingHelper.handleTokenRefresh(async (token) => {
            const data = { notiToken: token };
            actionUpdateNotificationToken({ data }, async () => {});
        });
    }, []);

    return (
        <SafeAreaView style={[Layout.fill, { backgroundColor: colors.card }]}>
            <NavigationContainer theme={NavigationTheme} ref={navigationRef}>
                {Platform.OS === 'ios' ? (
                    <StatusBar barStyle="dark-content" />
                ) : (
                    <StatusBar barStyle={!darkMode ? 'light-content' : 'dark-content'} />
                )}
                <Stack.Navigator initialRouteName={SCREEN.SPLASH_SCREEN} headerMode="none">
                    <Stack.Screen name={SCREEN.SPLASH_SCREEN} component={SplashPage} />
                    <Stack.Screen name={SCREEN.WELCOME} component={IndexWelcomeContainer} />
                    <Stack.Screen
                        name={SCREEN.MAIN}
                        component={DrawerNavigator}
                        options={{
                            animationEnabled: false,
                        }}
                    />
                    {/* LOGIN */}
                    <Stack.Screen name={SCREEN.LOGIN} component={IndexLoginContainer} />
                    <Stack.Screen name={SCREEN.FORGOT_PASS} component={IndexForgotContainer} />
                    <Stack.Screen name={SCREEN.REGISTER} component={IndexRegisterContainer} />
                    <Stack.Screen name={SCREEN.VERIFY_EMAIL} component={IndexVerifyContainer} />
                    <Stack.Screen name={SCREEN.CREATE_PASS} component={IndexCreatePassContainer} />
                    {/* PROFILE */}
                    <Stack.Screen name={SCREEN.CREATE_PROFILE} component={CreateProfile} />
                    <Stack.Screen name={SCREEN.UPDATE_HEADLINE} component={UpdateHeadline} />
                    <Stack.Screen name={SCREEN.UPDATE_ABOUT} component={UpdateAbout} />
                    <Stack.Screen name={SCREEN.SPECIALIST_SKILL} component={SpecialistSkill} />
                    <Stack.Screen name={SCREEN.UPDATE_SPECIALIST_SKILL} component={UpdateSpecialistSkill} />
                    <Stack.Screen name={SCREEN.OTHER_SKILL} component={OtherSkill} />
                    <Stack.Screen name={SCREEN.UPDATE_OTHER_SKILL} component={UpdateOtherSkill} />
                    <Stack.Screen name={SCREEN.LANGUAGE} component={Language} />
                    <Stack.Screen name={SCREEN.UPDATE_LANGUAGE} component={UpdateLanguage} />
                    <Stack.Screen name={SCREEN.SEARCH_SCREEN} component={UISearchScreen} />
                    <Stack.Screen name={SCREEN.HOBBIES} component={Hobbies} />
                    <Stack.Screen name={SCREEN.UPDATE_HOBBIES} component={UpdateHobbies} />
                    <Stack.Screen name={SCREEN.SITE_COMPANY} component={SiteCompany} />
                    <Stack.Screen name={SCREEN.UPDATE_SITE_COMPANY} component={UpdateSiteCompany} />
                    <Stack.Screen name={SCREEN.SOCIAL_LINKS} component={SocialLinks} />
                    <Stack.Screen name={SCREEN.UPDATE_SOCIAL_LINKS} component={UpdateSocialLink} />
                    <Stack.Screen name={SCREEN.EDUCATION} component={Education} />
                    <Stack.Screen name={SCREEN.UPDATE_EDUCATION} component={UpdateEducation} />
                    <Stack.Screen name={SCREEN.EXPERIENCE} component={Experience} />
                    <Stack.Screen name={SCREEN.UPDATE_EXPERIENCE} component={UpdateExperience} />
                    <Stack.Screen name={SCREEN.CURRENT_POSITION} component={CurrentPosition} />
                    <Stack.Screen name={SCREEN.UPDATE_CURRENT_POSITION} component={UpdateCurrentPosition} />
                    <Stack.Screen name={SCREEN.INDUSTRIAL_KNOWLEDGE} component={IndustrialKnowledge} />
                    <Stack.Screen name={SCREEN.ACCOUNT_INFO} component={AccountInfo} />
                    <Stack.Screen name={SCREEN.UPDATE_ACCOUNT_INFO} component={UpdateAccountInfo} />
                    <Stack.Screen name={SCREEN.UPDATE_CONTACT_PRICE} component={UpdateContactPrice} />
                    {/* CHAT */}
                    <Stack.Screen name={SCREEN.MESSAGE} component={IndexMessageContainer} />
                    <Stack.Screen name={SCREEN.NEW_MESSAGE} component={IndexNewMessageContainer} />
                    <Stack.Screen name={SCREEN.CONNECT_NETWORK} component={IndexConnectNetWorkContainer} />
                    {/* WALLET */}
                    <Stack.Screen name={SCREEN.CREATE_PASSWORD} component={CreatePassword} />
                    <Stack.Screen name={SCREEN.SECURE_WALLET} component={SecureWallet} />
                    <Stack.Screen name={SCREEN.CONFIRM_SECURE} component={ConfirmSecure} />
                    <Stack.Screen name={SCREEN.IMPORT_SEED_PHRASE} component={ImportSeedPhrase} />
                    <Stack.Screen name={SCREEN.SETUP_KYC} component={SetupKYC} />
                    <Stack.Screen name={SCREEN.ENTER_PHONE_KYC} component={EnterPhoneNumber} />
                    <Stack.Screen name={SCREEN.VERIFY_PHONE_KYC} component={VerifyPhoneKyc} />
                    <Stack.Screen name={SCREEN.FILL_INFO_KYC} component={FillInfoKyc} />
                    <Stack.Screen name={SCREEN.UPLOAD_FILE_KYC} component={UploadFileKyc} />
                    <Stack.Screen name={SCREEN.DONE_KYC} component={DoneKyc} />
                    <Stack.Screen name={SCREEN.ALL_TRANSACTION} component={AllTransactionPage} />
                    {/* JOB */}
                    <Stack.Screen name={SCREEN.SEARCH_JOB} component={SearchJobPage} />
                    <Stack.Screen name={SCREEN.ALL_JOB_PAGE} component={AllJobPage} />
                    <Stack.Screen name={SCREEN.SEARCH_ADVANCE} component={SearchAdvancePage} />
                    <Stack.Screen name={SCREEN.JOB_POSTED} component={JobPostedPage} />
                    <Stack.Screen name={SCREEN.PEOPLE_MATCHED} component={PeopleMatchedPage} />
                    <Stack.Screen name={SCREEN.HIRING_PROCESS} component={HiringProcessPage} />
                    <Stack.Screen name={SCREEN.BEST_FOR_YOU} component={BestOfYouPage} />
                    <Stack.Screen name={SCREEN.ESS_DETAIL_PAGE} component={EssDetailPage} />
                    <Stack.Screen name={SCREEN.CLASS_PAGE} component={ClassPage} />
                    <Stack.Screen name={SCREEN.ORGANISATION_PAGE} component={OrganisationPage} />
                    <Stack.Screen name={SCREEN.PEOPLE_SPONSOR_PAGE} component={PeopleSponserPage} />
                    <Stack.Screen name={SCREEN.ACTIVE_PRO_ACCOUNT} component={ActiveProAccount} />
                    <Stack.Screen name={SCREEN.SUMMARY} component={Summary} />
                    <Stack.Screen name={SCREEN.PEOPLE_DETAIL_PAGE} component={PeopleDetailPage} />
                    <Stack.Screen name={SCREEN.PROFILE_VIEW_PAGE} component={ProfileViewPage} />
                    <Stack.Screen name={SCREEN.LIST_POPUP_PAGE} component={ListPopUpPage} />
                    <Stack.Screen name={SCREEN.POST_JOB_PAGE} component={PostJobPage} />
                    {/* NOTIFICATION */}
                    <Stack.Screen name={SCREEN.NOTIFICATION} component={IndexNotificationContainer} />
                </Stack.Navigator>
            </NavigationContainer>
        </SafeAreaView>
    );
};

const mapStateToProps = (state) => ({
    notificationReducer: state.notificationReducer,
});
export default connect(mapStateToProps, {
    actionUpdateNotificationToken,
    actionAddNewNotification,
})(ApplicationNavigator);
