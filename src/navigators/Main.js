import { IndexHomeContainer, IndexJobsContainer, IndexMissionContainer, IndexProfileContainer, IndexWalletContainer } from '@/containers';
import { useTheme } from '@/theme';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useNavigation } from '@react-navigation/native';
import React, { useEffect } from 'react';
import { Image } from 'react-native';
import { connect } from 'react-redux';
import { actionRemovePendingNotification } from '../containers/Notification/duck/actions';
import FirebaseMessagingHelper from '../services/firebase.service';
import { fontFamily } from '../theme/Fonts';
import { Colors } from '../theme/Variables';
import { verticalScale } from '../utils/ScalingUtils';

const Tab = createBottomTabNavigator();

// @refresh reset
const MainNavigator = (props) => {
    const navigation = useNavigation();
    const { notificationReducer, actionRemovePendingNotification } = props || {};
    const { pendingNotification, isLoading } = notificationReducer || {};

    useEffect(() => {
        FirebaseMessagingHelper.handleNotificationFromQuit(navigation);
        FirebaseMessagingHelper.handleNotificationFromBackground(navigation);
    }, []);

    if (pendingNotification != null) {
        FirebaseMessagingHelper.handleNotification(navigation, pendingNotification);
        actionRemovePendingNotification();
    }

    const { Images } = useTheme();
    return (
        <Tab.Navigator
            initialRouteName="Home"
            screenOptions={({ route }) => ({
                unmountOnBlur: true,
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    if (route.name === 'Home') {
                        iconName = Images.bottom.home_home;
                    } else if (route.name === 'Message') {
                        iconName = Images.bottom.home_chat;
                    } else if (route.name === 'Jobs') {
                        iconName = Images.bottom.home_job;
                    } else if (route.name === 'Wallet') {
                        iconName = Images.bottom.home_wallet;
                    } else if (route.name === 'Mission') {
                        iconName = Images.bottom.home_mission;
                    } else if (route.name === 'Profile') {
                        iconName = Images.bottom.home_profile;
                    }
                    return <Image style={{ width: verticalScale(35), height: verticalScale(35), tintColor: color }} source={iconName} />;
                },
            })}
            tabBarOptions={{
                activeTintColor: Colors.blueDark,
                inactiveTintColor: '#9fa4be',
                labelStyle: { fontSize: 12, fontFamily: fontFamily.medium },
                style: { height: 60 },
                tabStyle: { paddingTop: 5 },
            }}
        >
            <Tab.Screen name="Home" component={IndexHomeContainer} />
            {/* <Tab.Screen name="Message" component={IndexChatContainer} /> */}
            <Tab.Screen name="Jobs" component={IndexJobsContainer} />
            <Tab.Screen name="Wallet" component={IndexWalletContainer} />
            <Tab.Screen name="Mission" component={IndexMissionContainer} />
            <Tab.Screen name="Profile" component={IndexProfileContainer} />
        </Tab.Navigator>
    );
};

const mapStateToProps = (state) => ({
    notificationReducer: state.notificationReducer,
});

export default connect(mapStateToProps, {
    actionRemovePendingNotification,
})(MainNavigator);
